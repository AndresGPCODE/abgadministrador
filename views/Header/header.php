<?php
    SESSION_START();

    $NOMBRE = $_SESSION['Nombre'];

    $IMAGEN = $_SESSION['Imagen'];

    $EMAIL  = $_SESSION['Email'];

    $ID     = $_SESSION['nIDUsuario'];

    $ROL    = $_SESSION['Rol'];
    if($NOMBRE == null || $NOMBRE == ''){

          echo "<div class='container' align = 'center' style = 'margin-top: 10%;'>

                     <div class='row'>

                          <div class='col-md-2'></div>

                          <div class='col-md-8'>

                          <img src='assets/img/iconoAdvertencia.png' width='200px' height='200px'><h1>Oops!</h1>

                          </div>

                     </div><br>

                     <div class='row' >

                          <div class='col-md-2'></div>

                          <div class='col-md-8'>

                          <h3>No tienes autorización para ingresar</h3> <br> <h3>Inicia Sesión</h3> 

                          </div>

                     </div>

                     <div class='row'>
                          
                          <button class='button primary' ng-click='back()'> REGRESAR </button>
                     </div>

                </div>";
          die();
    }

?>
<div ng-init="contadores('<?php echo $ID ?>')" ng-if="'Usuario' == '<?php echo $ROL ?>'"></div>
<div ng-init="contadoresTodos()" ng-if="'Administrador' == '<?php echo $ROL ?>'"></div>

<nav class="sb-topnav navbar navbar-expand navbar-light bg-light">

            
            


                <div class="col-md-4">
                     
                     <a class="navbar-brand" ui-sref="header" align="center">ABG PASSPORT</a>

                    <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" data-toggle="collapse" data-target="#layoutSidenav_nav">

                            <i class="fas fa-bars"></i>

                    </button>

                </div>

                <div class="col-md-4"></div>

                <div class="col-md-2">   </div>

                <div class="col-md-1" style="text-align: right;">

                    <a class="nav-link dropdown-toggle" id="mensajeDrop"  data-toggle="dropdown" data-target="#mensaje" aria-hospoup="true"
                    aria-expanded="false" ng-click="correo( '<?php echo  $EMAIL ?>')" ng-if="'Usuario' == '<?php echo $ROL ?>'">
                        
                        <img  src="assets/img/correo.png" height="30px" width="30px"> 
                        <span class="badge navbar-badge" style="background-color: #044BFD; color: white; float: center;float: top;">{{contador}}</span>


                    </a>  

                    <a class="nav-link dropdown-toggle" id="mensajeDrop"  data-toggle="dropdown" data-target="#mensaje" aria-hospoup="true"
                    aria-expanded="false" ng-click="correoTodos()" ng-if="'Administrador' == '<?php echo $ROL ?>'">
                        
                        <img  src="assets/img/correo.png" height="30px" width="30px"> 
                        <span class="badge navbar-badge" style="background-color: #044BFD; color: white; float: center;float: top;">{{contador}}</span>


                    </a>                                  

                    <div class="dropdown-menu  dropdown-menu-right" aria-labelledby="mensajeDrop" id="mensaje">   

                        <div class="">  
                            
                            <!--MENSAJE-->
                            
                            
                            <a class="link border-top" ng-repeat="mensaje in mensajes" ng-click="cambiar(mensaje.Nombre,mensaje.nIDCliente)">

                                <hr style="color:  1px solid gray">
                                
                                <div class="d-flex no-block align-items-center p-10">
                                    
                                    <img src="{{mensaje.Foto}}" class="rounded-circle ml-2" height="50px" width="50px">                                                                                                                                                    
                                    <div class="m-l-10">
                                        
                                         <h5 class="m-b-0 ml-2">{{mensaje.Nombre}}</h5>
                                         <span class="mail-desc ml-2">{{mensaje.Texto}}</span>

                                    </div>
                                </div>

                            </a>

                            <hr style="color:  1px solid gray">
                            <a class="mr-5"align="right" style="text-decoration: underline;float: right;" ng-click="todos()">           Ver todos</a>
                            

                        </div>

                    </div>

                </div>

               <div class="col-md-1">

                <ul class="navbar-nav"> 
        
                    <li class="nav-item dropdown">  

                        <a class="nav-link dropdown-toggle" id="userDropdown" role="button" data-toggle="dropdown" data-target="#settings" aria-hospopup="true"
                        aria-expanded="false">
                            
                            <img width="50px" height="50px" class="rounded-circle" src=<?php echo '"' . $IMAGEN . '"' ?> />

                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown" id="settings"> 
                            
                          <?php echo '<span class="nav-text ml-5">' . $NOMBRE . '</span>'; ?>
                             <a class="dropdown-item" ng-click="logout()"><i class="fas fa-power-off mr-2"></i>SALIR</a>

                        </div>

                    </li>

                </ul>

                </div>

               
</nav>


<div id="layoutSidenav">

            <div id="layoutSidenav_nav" class="collapse show">

                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">

                    <div class="sb-sidenav-menu">

                        <div class="nav">

                            <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseLayouts0" aria-expanded="false" aria-controls="collapseLayouts">

                                <img width="60px" height="60px" class="rounded-circle" src=<?php echo '"' . $IMAGEN . '"' ?> />
        

        
                             <?php 

                               echo '<span class="nav-text">    ' . $NOMBRE . ' </span>';
                               
                               

                             ?>

                             <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>


                            </a>

                            <div class="collapse" id="collapseLayouts0" aria-labelledby="headingOne" data-parent="#sidenavAccordion">

                                <nav class="sb-sidenav-menu-nested nav">

                                    <a class="nav-link" ng-click="logout()">
                                        <div class="sb-nav-link-icon">
                                           
                                           <i class="fa fa-power-off fa-2x"></i> 

                                        </div>
                                        SALIR

                                    </a>

                                </nav>

                            </div>
                            
                            <div class="sb-sidenav-menu-heading">MENÚ</div>

                            <a class="nav-link" ui-sref="header.company">

                                <div class="sb-nav-link-icon">

                                    <i class="fa fa-building-o"></i>

                                </div>

                                EMPRESA

                            </a>
                            
                            <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">

                                <div class="sb-nav-link-icon">

                                    <i class="fa fa-user"></i>

                                </div>

                                USUARIOS
                                
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>

                            </a>

                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">

                                <nav class="sb-sidenav-menu-nested nav">

                                    <a class="nav-link" ui-sref="header.client">CLIENTES</a>

                                    <a class="nav-link" ui-sref="header.chofer">CHOFER</a>

                                    <a class="nav-link" ui-sref="header.user">ADMINISTRATIVOS</a>

                                </nav>

                            </div>

                            <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseLayouts2" aria-expanded="false" aria-controls="collapseLayouts">

                                <div class="sb-nav-link-icon">

                                    <i class="fa fa-users"></i>

                                </div>

                                PERFILES
                                
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>

                            </a>

                            <div class="collapse" id="collapseLayouts2" aria-labelledby="headingOne" data-parent="#sidenavAccordion">

                                <nav class="sb-sidenav-menu-nested nav">

                                    <a class="nav-link" ui-sref="header.role">ROLES</a>

                                    <!--<a class="nav-link" ui-sref="header.marketstall">PUESTOS</a>

                                    <a class="nav-link" ui-sref="header.departament">DEPARTAMENTOS</a>-->

                                </nav>

                            </div>

                            <a class="nav-link" ui-sref="header.Inventory">

                                <div class="sb-nav-link-icon">

                                    <i class="fa fa-car"></i>

                                </div>

                                AUTOMÓVILES

                            </a>

                            <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseLayouts3" aria-expanded="false" aria-controls="collapseLayouts">

                                <div class="sb-nav-link-icon">

                                    <i class="fa fa-signal"></i>

                                </div>

                                DISPOSITIVOS
                                
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>

                            </a>

                            <div class="collapse" id="collapseLayouts3" aria-labelledby="headingOne" data-parent="#sidenavAccordion">

                                <nav class="sb-sidenav-menu-nested nav">

                                    <a class="nav-link" ui-sref="header.dispositivo">REGISTRO DE DISPOSITIVOS</a>

                                    <a class="nav-link" ui-sref="header.asignacion">ASIGNAR DISPOSITIVO</a>

                                </nav>

                            </div>

                            <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseLayouts4" aria-expanded="false" aria-controls="collapseLayouts">

                                <div class="sb-nav-link-icon">

                                    <i class="fa fa-folder-open"></i>

                                </div>

                                DOCUMENTOS
                                
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>

                            </a>

                            <div class="collapse" id="collapseLayouts4" aria-labelledby="headingOne" data-parent="#sidenavAccordion">

                                <nav class="sb-sidenav-menu-nested nav">

                                    <a class="nav-link" ui-sref="header.documentosCliente">CLIENTES</a>

                                    <a class="nav-link" ui-sref="header.documentosAutomoviles">AUTOMÓVILES</a>

                                    <a class="nav-link" ui-sref="header.typeDocument">TIPO DOCUMENTOS</a>


                                </nav>

                            </div>

                            <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseLayouts5" aria-expanded="false" aria-controls="collapseLayouts">

                                <div class="sb-nav-link-icon">

                                    <i class="fa fa-code-fork"></i>

                                </div>

                                SOLICITUDES
                                
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>

                            </a>

                            <div class="collapse" id="collapseLayouts5" aria-labelledby="headingOne" data-parent="#sidenavAccordion">

                                <nav class="sb-sidenav-menu-nested nav">

                                    <a class="nav-link" ui-sref="header.solicitados">SOLICITADOS</a>

                                    <a class="nav-link" ui-sref="header.pendiente">POR ENTREGAR</a>

                                    <a class="nav-link" ui-sref="header.ruta">ESTADO</a>
                                    

                                </nav>

                            </div>

                            <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseLayouts6" aria-expanded="false" aria-controls="collapseLayouts">

                                <div class="sb-nav-link-icon">

                                    <i class="fa fa-bell"></i>

                                </div>

                                NOTIFICACIONES
                                
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>

                            </a>

                            <div class="collapse" id="collapseLayouts6" aria-labelledby="headingOne" data-parent="#sidenavAccordion">

                                <nav class="sb-sidenav-menu-nested nav">

                                    <a class="nav-link" ui-sref="header.NotificationCliente">CLIENTES</a>

                                    <a class="nav-link" ui-sref="header.NotificationChofer">CHOFER</a>

                                    <a class="nav-link" ui-sref="header.TipoNotificaciones">TIPO DE NOTIFICACIÓN</a>
                                    

                                </nav>

                            </div>

                            <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseLayouts7" aria-expanded="false" aria-controls="collapseLayouts">

                                <div class="sb-nav-link-icon">

                                    <i class="fa fa-list-alt"></i>

                                </div>

                                DETALLE ENTREGA
                                
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>

                            </a>

                            <div class="collapse" id="collapseLayouts7" aria-labelledby="headingOne" data-parent="#sidenavAccordion">

                                <nav class="sb-sidenav-menu-nested nav">

                                    <a class="nav-link" ui-sref="header.AccountState">DETALLE ENTREGA</a>

                                    <a class="nav-link" ui-sref="header.EstadoC">ESTADO DE CUENTA</a>
                                    

                                </nav>

                            </div>

                            <a class="nav-link" ui-sref="header.Maps">

                                <div class="sb-nav-link-icon">

                                    <i class="fa fa-map-marker"></i>

                                </div>

                                DASHBOARD

                            </a>

                            <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseLayouts8" aria-expanded="false" aria-controls="collapseLayouts">

                                <div class="sb-nav-link-icon">

                                    <i class="fa fa-road"></i>

                                </div>

                                GRÁFICAS
                                
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>

                            </a>

                            <div class="collapse" id="collapseLayouts8" aria-labelledby="headingOne" data-parent="#sidenavAccordion">

                                <nav class="sb-sidenav-menu-nested nav">

                                    <a class="nav-link" ui-sref="header.Mantenimiento">GRÁFICAS</a>
                                    

                                </nav>

                            </div>

                        </div>

                    </div>

                    <div class="sb-sidenav-footer">

                        <div class="small"></div>

                        

                    </div>

                </nav>

            </div>
        
            <div id="layoutSidenav_content">

                <div ui-view ></div>

            </div>

