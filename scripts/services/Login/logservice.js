'use strict';

angular.module('abgpassportApp')
  
       .service('logServices',['$http','configService', function($http,configService){

        return {

              session : function(log){

                      return $http.post(configService.apiUrl + 'Usuario.php','accion=buscar&log=' + JSON.stringify(log),

                        { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                      );
                      
              }
        }

       }]);