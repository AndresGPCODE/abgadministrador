<?php

	error_reporting(E_ALL ^ E_NOTICE);
    ini_set('display_errors','1');

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Request-Methods');

      include_once '../Configuraciones/Conexion.php';
      include_once '../Dispositivos/clComando.php';
      include_once '../Utilerias/clHerramientas_v2011.php';

      
      $method = $_SERVER['REQUEST_METHOD'];

      $UtileriasDatos   = new clHerramientasv2011();
      $fechaLocal       = $UtileriasDatos->getFechaYHoraActual_General();
      $fechaLocal       = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
      
      $comando = new clComando();

      
      $comando->conexion($SERVER,$USER,$PASSWORD,$BD);

      switch($method){

      			  

      	    case 'POST' : 

                     $request = json_decode(file_get_contents('php://input'),true); 

                     if($comando->buscarComando($request['nIDDispositivoAuto'],$request['Accion'])){

                          echo json_encode($comando->buscarComando($request['nIDDispositivoAuto'],$request['Accion']),JSON_UNESCAPED_UNICODE);

                     }else{

                          echo json_encode('null');

                     }
                      
                  
      }

?>