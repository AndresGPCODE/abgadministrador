<?php

/*
    ** Clase Entradas.
    ** Esta clase es la que controlara los procesos para la entidad entradas.
*/

/*
    ** Herramienta externa
*/
    include_once "../Utilerias/clHerramientas_v2011.php";

/*
    ** tbl_entradas
*/
class clUsuarios
{

    /*
        ** Atributos de conexion a la base de datos
    */
    private $servidor;
    private $usuario;
    private $password;
    private $basededatos;
    private $contarDatos   = 0;

    /*
        ** Constantes generales
    */
    private $ascendente     = "Ascendente";
    private $descendente    = "Descendente";

    /*
        ** Constantes para la base de datos
    */
    private $nombreTabla    = "tbl_usuarios";
    private $nombreVista    = "";
    private $id             = "nIDUsuario";
    private $campoOrdenar   = "nIDUsuario";
    private $ordenamiento   = "Descendente";
    private $limiteInferior = -1;
    private $limiteSuperior = -1;
    public  $last_id = 0;
    private $dt;
    private $dtGrabar;

    /*
        ** Constantes de estados
    */
    private $existeError    = FALSE;
    private $existenDatos   = FALSE;
    private $datosConexion  = FALSE;

    /*
        ** Mensajes
    */
    private $error = "";

    /**
     *************** Constructor ***************
     */
    function __construct() 
    {
        $this->Inicializacion();
    }
    
    /**
     *************** Función de conexión ***************
     * La función realiza la conexión a la base de datos 
     * mediante las credenciales previamente establecidas. 
     * 
     * @param [string] $servidor
     * @param [string] $usuario
     * @param [string] $password
     * @param [string] $basededatos
     * @return void
     */
    public function conexion($servidor, $usuario, $password, $basededatos)
    {
        
        $this->servidor     = $servidor;
        $this->usuario      = $usuario;
        $this->password     = $password;
        $this->basededatos  = $basededatos;

        $this->datosConexion    = TRUE; 


    }

    /**
     ************** Función setInfomacion **************
     * La función realiza la asignación de los datos almacenados
     * en la base de datos a las respectivas variables, los datos
     * que se encuentran como parametro son obligatorios para cada
     * tabla. 
     * 
     * @param [datetime] $FechaModificacion
     * @param [datetime] $FechaCreacion
     * @param [string] $Observaciones
     * @param [int] $bEstado
     * @param [boolean] $crear
     * @param [boolean] $modificar
     * @param [boolean] $eliminar
     * @return void
     */
    public function setInformacion(
        $nIDUsuario, $Usuario, $Password, $Clave, $Nombre, $Direccion,$RFC, $Telefono,
        $Celular, $Email, $Imagen, $Activo,$Fecha, $nIDRol, $nIDPuesto, $nIDDepartamento,
        $Sueldo, $Maquina, $Firma,$Estatus, $nIDExtension, $nIDEmpresa, $TipoDeRol,$FechaModificacion, $FechaCreacion,
        $Observaciones, $bEstado, $crear, $modificar, $eliminar
    ) 
    {
        $utileria = new clHerramientasv2011();
        
        
        $this->dtGrabar[$this->contarDatos]["nIDUsuario"]          = $nIDUsuario;
        $this->dtGrabar[$this->contarDatos]["Usuario"]             = $Usuario;
        $this->dtGrabar[$this->contarDatos]["Password"]            = $Password;
        $this->dtGrabar[$this->contarDatos]["Clave"]               = $Clave;
        $this->dtGrabar[$this->contarDatos]["Nombre"]              = $Nombre;
        $this->dtGrabar[$this->contarDatos]["Direccion"]           = $Direccion;
        $this->dtGrabar[$this->contarDatos]["RFC"]                 = $RFC;
        $this->dtGrabar[$this->contarDatos]["Telefono"]            = $Telefono;
        $this->dtGrabar[$this->contarDatos]["Celular"]             = $Celular;
        $this->dtGrabar[$this->contarDatos]["Email"]               = $Email;
        $this->dtGrabar[$this->contarDatos]["Imagen"]              = $Imagen;
        $this->dtGrabar[$this->contarDatos]["Activo"]              = $Activo;
        $this->dtGrabar[$this->contarDatos]["Fecha"]               = $Fecha;
        $this->dtGrabar[$this->contarDatos]["nIDRol"]              = $nIDRol;
        $this->dtGrabar[$this->contarDatos]["nIDPuesto"]           = $nIDPuesto;
        $this->dtGrabar[$this->contarDatos]["nIDDepartamento"]     = $nIDDepartamento;
        $this->dtGrabar[$this->contarDatos]["Sueldo"]              = $Sueldo;
        $this->dtGrabar[$this->contarDatos]["Maquina"]             = $Maquina;
        $this->dtGrabar[$this->contarDatos]["Firma"]               = $Firma;
        $this->dtGrabar[$this->contarDatos]["Estatus"]             = $Estatus;
        $this->dtGrabar[$this->contarDatos]["nIDExtension"]        = $nIDExtension;
        $this->dtGrabar[$this->contarDatos]["nIDEmpresa"]          = $nIDEmpresa;
        $this->dtGrabar[$this->contarDatos]["TipoDeRol"]           = $TipoDeRol;
     

        /*
         ** Estos datos son obligatorios para todas las tablas
        */
        $fecha = "";

        if( strlen($FechaModificacion) > 0 )
        {
            $fecha = $utileria->ConvertirFechaYHora_General($FechaModificacion);
            $this->dtGrabar[$this->contarDatos]["FechaModificacion"] = $fecha;
        } else {
            $this->dtGrabar[$this->contarDatos]["FechaModificacion"] = "01/01/1980";
        }

        $fecha = "";

        if(strlen($FechaCreacion)>0){
            $fecha=$utileria->ConvertirFechaYHora_General($FechaCreacion);
            $this->dtGrabar[$this->contarDatos]["FechaCreacion"] = $fecha;
        } else {
            $this->dtGrabar[$this->contarDatos]["FechaCreacion"] = "01/01/1980";
        }

        $this->dtGrabar[$this->contarDatos]["Observaciones"] = $Observaciones;

        if($bEstado >= 0 && $bEstado <= 5){
            $this->dtGrabar[$this->contarDatos]["bEstado"]  = $bEstado;
            $this->dtGrabar[$this->contarDatos]["sbEstado"] = $this->vEstado($bEstado,"bEstado");
        } else {
            $this->dtGrabar[$this->contarDatos]["bEstado"]  = 3;
            $this->dtGrabar[$this->contarDatos]["sbEstado"] = "No definido";
        }

        if ($crear==TRUE){
            $this->dtGrabar[$this->contarDatos]["Crear"]    = TRUE;
            $this->dtGrabar[$this->contarDatos]["Cambiar"]  = FALSE;
            $this->dtGrabar[$this->contarDatos]["Eliminar"] = FALSE;
        } else {
            if ($modificar==TRUE){
                $this->dtGrabar[$this->contarDatos]["Crear"]    = FALSE;
                $this->dtGrabar[$this->contarDatos]["Cambiar"]  = TRUE;
                $this->dtGrabar[$this->contarDatos]["Eliminar"] = FALSE;
            } else {
                if ($eliminar==TRUE){
                    $this->dtGrabar[$this->contarDatos]["Crear"]    = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Cambiar"]  = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Eliminar"] = TRUE;
                } else {
                    $this->dtGrabar[$this->contarDatos]["Crear"]    = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Cambiar"]  = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Eliminar"] = FALSE;
                }
            }
        }


        $this->contarDatos=$this->contarDatos+1;
        $this->existenDatos=TRUE;

        unset($utileria);

        return TRUE;

    }

    /**
     ************** Función setInformacionObj **************
     * Esta función permite darle valor al objeto para despues
     * retornar datos.
     * 
     * @param [object] $obj
     * @return void
     */
    public function setInformacionObj($obj)
    {
        $registros  = 0;
        $registros  = $obj->registrosCargados();

        if($registros > 0){
            
            $i=0;

            for($i=0; $i < $registros - 1; $i = $i + 1){

                $dato = $obj->getInformacionObj();

                $this->setInformacion(
                    $dato[$i]["nIDUsuario"],
                    $dato[$i]["Usuario"],
                    $dato[$i]["Password"],
                    $dato[$i]["Clave"],
                    $dato[$i]["Nombre"],
                    $dato[$i]["Direccion"],
                    $dato[$i]["RFC"],
                    $dato[$i]["Telefono"],
                    $dato[$i]["Celular"],
                    $dato[$i]["Email"],
                    $dato[$i]["Imagen"],
                    $dato[$i]["Activo"],
                    $dato[$i]["Fecha"],
                    $dato[$i]["nIDRol"],
                    $dato[$i]["nIDPuesto"],
                    $dato[$i]["nIDDepartamento"],
                    $dato[$i]["Sueldo"],
                    $dato[$i]["Maquina"],
                    $dato[$i]["Firma"],
                    $dato[$i]["Estatus"],
                    $dato[$i]["nIDExtension"],
                    $dato[$i]["nIDEmpresa"],
                    $dato[$i]["TipoDeRol"],
                    $dato[$i]["FechaModificacion"],
                    $dato[$i]["FechaCreacion"],
                    $dato[$i]["Observaciones"],
                    $dato[$i]["bEstado"],
                    $dato[$i]["Crear"],
                    $dato[$i]["Cambiar"],
                    $dato[$i]["Eliminar"]
                );
            }

        }
        return TRUE;
    }

    /**
     ************** Función getInformacionObj **************
     * Esta función graba los datos traídos de la base de datos
     * para despues agregarlos a una variable u objeto.
     * 
     * @return void
     */
    public function getInformacionObj()
    {
        return $this->dtGrabar;
    }

    /**
     ************** Función getInformacion **************
     * Esta función permite devolver el valor de las propiedades
     * que se encuentan en las variables.
     * 
     * @param [int] $numero
     * @return void
     */
    public function getInformacion($numero)
    {
        if($this->existenDatos == TRUE){
            if($numero >= 0){
                return $this->dtGrabar[$numero];
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    /**
     ************** Función getIndice **************
     *
     * @param [int] $nID
     * @return void
     */
    public function getIndice($nID)
    {
        if($this->contarDatos>0){

            $indice = -1;

            for($i=0; $i < $this->contarDatos - 1; $i = $i + 1){
                if($this->dtgrabar[$i][$this->id] == $nID){
                    $indice = $i;
                    break;
                }
            }
            return $indice;
        } else {
            return -1;
        }
    }

    /**
     ************** Función OrdenarTabla **************
     * Ordena la tabla con respecto al campo que se le
     * pasa por parametro.
     * 
     * @param [string] $campos
     * @return void
     */
    public function OrdenarTabla($campos)
    {
        $this->campoOrdenar = $campos;
    }

    /**
     ************** Función formaOrdenamiento **************
     * Ordena la tabla de forma ascendente o descendente según
     * el parametro.
     * 
     * @param [type] $forma
     * @return void
     */
    public function formaOrdenamiento($forma)
    {
        $this->ordenamiento = $forma;
    }

    /**
     ************** Función dtBase **************
     * Graba los registros de la base de datos.
     * 
     * @return void
     */
    public function dtBase()
    {
        return $this->dtGrabar;
    }

    /**
     ************** Función errorObjeto **************
     * La funcion retorna un error en caso de que este
     * exista.
     * 
     * @return void
     */
    public function errorObjeto()
    {
        if($this->existeError==TRUE){
            return $this->error;
        } else {
            return "No existe ningún mensaje de error.";
        }
    }

    /**
     ************** Función registrosCargados **************
     * Función para retornar el número de registros cargados.
     * @return void
     */
    public function registrosCargados()
    {
        return $this->contarDatos;
    }

    /**
     ************** Función formaDeOrdenamiento **************
     * Función para retornar la forma de ordenar la tabla.
     * 
     * @return void
     */
    public function formaDeOrdenamiento()
    {
        return $this->ordenamiento;
    }

    /**
     ************** Función errorDeObjeto **************
     * Función para retornar el error de un objeto en caso
     * de que este exista.
     * 
     * @return void
     */
    public function errorDeObjeto()
    {
        return $this->existeError;
    }

    /**
     ************** Función datosCargados **************
     *
     * @return void
     */
    public function datosCargados()
    {
        return $this->existenDatos;
    }

    /**
     ************** Función setlimiteInferior **************
     *
     * @param [int] $limite
     * @return void
     */
    public function setlimiteInferior($limite)
    {
        $this->limiteInferior = $limite;
    }

    /**
     ************** Función setlimiteSuperior **************
     *
     * @param [int] $limite
     * @return void
     */
    public function setlimiteSuperior($limite)
    {
        $this->limiteSuperior = $limite;
    }
    
    /**
     *************** Función Inicializacion ***************
     * Esta función permite inicializar atributos y contenido.
     * 
     * @return void
     */
    public function Inicializacion()
    {
        $this->InicializaAtributos();
        $this->InicializacionContenido();
    }

    /**
     *************** Función InicializaAtributos ***************
     * Inicializa los atributos.
     * 
     * @return void
     */
    private function InicializaAtributos()
    {
        $this->existeError      = FALSE;
        $this->existenDatos     = FALSE;
        $this->datosConexion    = FALSE;

        $this->error        = "No hay error";
        $this->ordenamiento = $this->ascendente;
        $this->contarDatos  = 0;
    }

    /**
     *************** Función InicializacionContenido ***************
     * Inicializa el contenido.
     * @return void
     */
    public function InicializacionContenido()
    {
        $this->contarDatos  = 0;
        $this->existeError  = FALSE;
    }

    /**
     *************** Función exiteEmail**************
     *
     * @param [int] $Email
     * @return void
     */

    public function existeEmail($email){
        //verifica si exite algún error con la conexión
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene las credenciales para conectarse a la base
                                    de datos[Cargar]";
            return FALSE;
        }

        //Abre la conexion 
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError    = TRUE;
            $this->error          = "Erroe no se puede conectar a la base de datos";
            return FALSE;
        }
        
        //CONSTRUYE LA CONSULTA
        $txtConsulta = "";
        $txtConsulta = "SELECT * ";
        $txtConsulta = $txtConsulta . "FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . "Email = '" . $email . "'";

        //EJECUTA LA CONSULTA

 
       
        mysqli_query($mysqli,"SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);
    
        if($res){
            if($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                return $registro;
            }else{
                return FALSE;
            }
        }else{
                return FALSE;
        }

    }

    /**
     *************** Función exiteEmail**************
     *
     * @param [int] $Email
     * @return void
     */

    public function existeUsuario($email){
        //verifica si exite algún error con la conexión
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene las credenciales para conectarse a la base
                                    de datos[Cargar]";
            return FALSE;
        }

        //Abre la conexion 
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError    = TRUE;
            $this->error          = "Erroe no se puede conectar a la base de datos";
            return FALSE;
        }
        
        //CONSTRUYE LA CONSULTA
        $txtConsulta = "";
        $txtConsulta = "SELECT * ";
        $txtConsulta = $txtConsulta . "FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . " Email = '" . $email . "'";
 
        //EJECUTA LA CONSULTA

 
       
        mysqli_query($mysqli,"SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);
    
        if($res){
            if($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                return $registro;
            }else{
                return FALSE;
            }
        }else{
                return FALSE;
        }

    }


    public function buscarEmail($email){
        //verifica si exite algún error con la conexión
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene las credenciales para conectarse a la base
                                    de datos[Cargar]";
            return FALSE;
        }

        //Abre la conexion 
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError    = TRUE;
            $this->error          = "Erroe no se puede conectar a la base de datos";
            return FALSE;
        }
        
        //CONSTRUYE LA CONSULTA
        $txtConsulta = "";
        $txtConsulta = "SELECT * ";
        $txtConsulta = $txtConsulta . "FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . "tbl_usuarios.Email = '" . $email . "'";
 
        //EJECUTA LA CONSULTA

 
       
        mysqli_query($mysqli,"SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);
    
        $regs = [];
        

        foreach ($res as $registro) {
            
            $regs[] = $registro;
        }return $regs;

        

    }

    /**
     *************** Función existeID ***************
     *
     * @param [int] $nID
     * @return void
     */
    public function existeID($nID)
    {

        // Verifica si existe algún error con la conexión
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        ="Error No tiene los datos para conectarse a la base de 
                                 datos [Cargar]";
            return FALSE;
        }

        // Abre la conexion
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT * ";
        $txtConsulta = $txtConsulta . "FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . $this->id ."=" . $nID ;

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Cargar la informacion
        if($res){
            if($registro=mysqli_fetch_array($res, MYSQLI_ASSOC)){
                return TRUE;
            } else {

                return FALSE;
            }

        } else {
            return FALSE;
        }
    }

    /**
     *************** Función vEstado ***************
     * Verifica el estado del registro.
     * 0 = Eliminado; 1 = Activo; 2 = Cancelado; 
     * 3 = No definido;
     * 
     * @param [string] $estado
     * @param [string] $nombreCampo
     * @return void
     */
    private function vEstado($estado, $nombreCampo)
    {
        $cadena = "";
        switch($estado){
            case 0: // Eliminado
                $cadena = "Eliminado";
                break;
            case 1: // Activo
                $cadena = "Activo";
                break;
            case 2: // Cancelado
                $cadena = "Cancelado";
                break;
            case 3: // No definifo
                $cadena = "No definido";
                break;
        }
        return $cadena;
    }

    /**
     *************** Funcion especialesHTML ***************
     *
     * @param [string] $str
     * @return void
     */
    public function especialesHTML($str)
    {
        $str = mb_convert_encoding($str,  'UTF-8');
        return $str;
    }

    /**
     ***************  Función consultar *************** 
     * Realiza una consulta simple.
     * 
     * @return void
     */

    public function datos(){

        //Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){

            $this->existeError          = TRUE;

            $this->error                = "Error no se puede conectar a la base de datos";

            return FALSE;
        }

        //GENERA LA CONSULTA
        $txtConsulta = "";

        $txtConsulta = "SELECT * ";

        $txtConsulta = $txtConsulta . " FROM ";

        $txtConsulta = $txtConsulta . $this->nombreTabla;

        $txtConsulta = $txtConsulta . " WHERE tbl_usuarios.nIDPuesto = 3";


        //EJECUTA LA CONSULTA
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);


        $regs = [];

        foreach($res as $registro){

            $regs[] = $registro;
        }return $regs;
    }



    public function datosT(){

        //A B R E   L A    C O N E X I Ó N
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);


        if(mysqli_connect_errno()){

            $this->existeError      = TRUE;

            $this->error            = "Error no se puede conectar a la base de datos";

            return FALSE;

        }

        //G E N E R A    L A   C O N S U L T A
        $txtConsulta = "";

        $txtConsulta = "SELECT * FROM ";

        $txtConsulta = $txtConsulta . $this->nombreTabla;

        $txtConsulta = $txtConsulta . " WHERE tbl_usuarios.nIDPuesto = 1 " ;

        //E J E C U T A   L A   C O N S U L T A 
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);


        $regs = [];

        foreach ($res as $registros) {
            
            $regs[] = $registros;

        }return $regs;
    }
    
    /**
     ***************  Función consultar *************** 
     * Realiza una consulta simple.
     * 
     * @return void
     */
    private function consultar()
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Genera la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT *";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

        if($this->formaDeOrdenamiento()==$this->ascendente){
            $txtConsulta = $txtConsulta . " ASC";
        } else {
            $txtConsulta = $txtConsulta . " DESC";
        }

        if($this->limiteInferior>=0){
            $txtConsulta = $txtConsulta . " LIMIT " . $this->limiteInferior . "," . $this->limiteSuperior;
        }



        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli, $txtConsulta);

        // Carga la informacion
        $contador = 0;
        $this->contarDatos = 0;

        if($res){

            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $nIDUsuario          = 0;
                $Usuario             = "";
                $Password            = "";
                $Clave               = "";
                $Nombre              = "";
                $Direccion           = "";
                $RFC                 = "";
                $Telefono            = "";
                $Celular             = "";
                $Email               = "";
                $Imagen              = "";
                $Activo              = "";
                $Fecha               = "";
                $nIDRol              = 0;
                $nIDPuesto           = 0;
                $nIDDepartamento     = 0;
                $Sueldo              = 0;
                $Maquina             = "";
                $Firma               = "";
                $Estatus             = "";
                $nIDExtension        = 0;
                $nIDEmpresa          = 0;

                // Campos base
                $FechaModificacion  = "";
                $FechaCreacion      = "";
                $Observaciones      = "";
                $bEstado            = 3;

                // Carga la información del registro leído
                if($registro['nIDUsuario'] != NULL){
                    $nIDUsuario = $registro['nIDUsuario'];
                }
                if($registro['Usuario'] != NULL){
                    $Usuario = $registro['Usuario'];
                }
                if($registro['Password'] != NULL){
                    $Password = $registro['Password'];
                }
                if($registro['Clave'] != NULL){
                    $Clave = $registro['Clave'];
                }
                if($registro['Nombre'] != NULL){
                    $Nombre = $registro['Nombre'];
                }
                if($registro['Direccion'] != NULL){
                    $Direccion = $registro['Direccion'];
                }
                if($registro['RFC'] != NULL){
                    $RFC = $registro['RFC'];
                }
                if($registro['Telefono'] != NULL){
                    $Telefono = $registro['Telefono'];
                }
                if($registro['Celular'] != NULL){
                    $Celular = $registro['Celular'];
                }
                if($registro['Email'] != NULL){
                    $Email = $registro['Email'];
                }
                if($registro['Imagen'] != NULL){
                    $Imagen = $registro['Imagen'];
                }
                if($registro['Activo'] != NULL){
                    $Activo = $registro['Activo'];
                }
                if($registro['Fecha'] != NULL){
                    $Fecha = $registro['Fecha'];
                }
                if($registro['nIDRol'] != NULL){
                    $nIDRol = $registro['nIDRol'];
                }
                if($registro['nIDPuesto'] != NULL){
                    $nIDPuesto = $registro['nIDPuesto'];
                }
                if($registro['nIDDepartamento'] != NULL){
                        $nIDDepartamento= $registro['nIDDepartamento'];
                }
                if($registro['Sueldo'] != NULL){
                    $Sueldo = $registro['Sueldo'];
                }
                if($registro['Maquina'] != NULL){
                    $Maquina = $registro['Maquina'];
                }
                if($registro['Firma'] != NULL){
                    $Firma = $registro['Firma'];
                }
                if($registro['Estatus'] != NULL){
                    $Estatus = $registro['Estatus'];
                }
                if($registro['nIDExtension'] != NULL){
                    $nIDExtension = $registro['nIDExtension'];
                }
                if($registro['nIDEmpresa'] != NULL){
                    $nIDEmpresa = $registro['nIDEmpresa'];
                }
                // Campos base
                if($registro['FechaModificacion']!= NULL){
                    $FechaModificacion = $registro['FechaModificacion'];
                }

                if($registro['FechaCreacion']!= NULL){
                    $FechaCreacion = $registro['FechaCreacion'];
                }

                if($registro['Observaciones']!= NULL){
                    $Observaciones = $registro['Observaciones'];
                }

                if($registro['bEstado']!= NULL){
                    $bEstado = $registro['bEstado'];
                }

                // Carga la información en el objeto
                $this->setInformacion(
                        $nIDUsuario,
                        $Usuario,
                        $Password,
                        $Clave,
                        $Nombre,
                        $Direccion,
                        $RFC,
                        $Telefono,
                        $Celular,
                        $Email,
                        $Imagen,
                        $Activo,
                        $Fecha,
                        $nIDRol,
                        $nIDPuesto,
                        $nIDDepartamento,
                        $Sueldo,
                        $Maquina,
                        $Firma,
                        $Estatus,
                        $nIDExtension,
                        $nIDEmpresa,
                        $FechaModificacion,
                        $FechaCreacion,
                        $Observaciones,
                        $bEstado,
                        FALSE,
                        TRUE,
                        FALSE
                );

            }
            
        } else {
            $this->existenDatos = FALSE;
        }
        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     ***************  Función consultarCondicion *************** 
     * Realiza una consulta con condición.
     * 
     * @param [string] $condicion
     * @return void
     */
    private function consultarCondicion($condicion)
    {

        try{
            // Abre la conexión            
            $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

            if(mysqli_connect_errno()){
                $this->existeError  = TRUE;
                $this->error        = "Error No se puede conectar a la base de datos";
                return FALSE;
            }

            $txtConsulta = "";
            $txtConsulta = "SELECT tbl_usuarios.nIDUsuario,tbl_usuarios.Usuario,tbl_usuarios.Clave,tbl_usuarios.Nombre,";
            $txtConsulta = $txtConsulta . "tbl_usuarios.Direccion,tbl_usuarios.RFC,tbl_usuarios.Telefono,tbl_usuarios.Celular,";
            $txtConsulta = $txtConsulta . "tbl_usuarios.Email,tbl_usuarios.Imagen,tbl_usuarios.Activo,tbl_usuarios.Fecha,";
            $txtConsulta = $txtConsulta . "tbl_usuarios.nIDRol,tbl_usuarios.Estatus,tbl_usuarios.nIDEmpresa,";
            $txtConsulta = $txtConsulta . "tbl_usuarios.FechaModificacion,tbl_usuarios.FechaCreacion,tbl_usuarios.Observaciones,";
            $txtConsulta = $txtConsulta . "tbl_usuarios.bEstado,tbl_roles.TipoDeRol";
            $txtConsulta = $txtConsulta . " FROM ";
            $txtConsulta = $txtConsulta . $this->nombreTabla;
            $txtConsulta = $txtConsulta . " INNER JOIN tbl_roles ON tbl_roles.nIDRol = tbl_usuarios.nIDRol "; 
            $txtConsulta = $txtConsulta . " WHERE ";
            $txtConsulta = $txtConsulta . $condicion;
            $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

            if($this->limiteInferior >= 0){
                $txtConsulta = $txtConsulta . " LIMIT " . $this->limiteInferior . "," . $this->limiteSuperior;
            }

            mysqli_query($mysqli, "SET NAMES UTF8");
            $res=mysqli_query($mysqli,$txtConsulta);

            // Cargar la informacion
            $contador           = 0;
            $this->contarDatos  = 0;

            if($res){

                while($registro=mysqli_fetch_array($res, MYSQLI_ASSOC)){
                    $nIDUsuario          = 0;
                    $Usuario             = "";
                    $Password            = "";
                    $Clave               = "";
                    $Nombre              = "";
                    $Direccion           = "";
                    $RFC                 = "";
                    $Telefono            = "";
                    $Celular             = "";
                    $Email               = "";
                    $Imagen              = "";
                    $Activo              = "";
                    $Fecha               = "";
                    $nIDRol              = 0;
                    $nIDPuesto           = 0;
                    $nIDDepartamento     = 0;
                    $Sueldo              = 0;
                    $Maquina             = "";
                    $Firma               = "";
                    $Estatus             = "";
                    $nIDExtension        = 0;
                    $nIDEmpresa          = 0;
                    $TipoDeRol           = "";

                    // Campos base
                    $FechaModificacion  = "";
                    $FechaCreacion      = "";
                    $Observaciones      = "";
                    $bEstado            = 3;

                    // Carga la info del registro leido
                    if($registro['nIDUsuario'] != NULL){
                        $nIDUsuario = $registro['nIDUsuario'];
                    }
                    if($registro['Usuario'] != NULL){
                        $Usuario = $registro['Usuario'];
                    }
                    if($registro['Password'] != NULL){
                        $Password = $registro['Password'];
                    }
                    if($registro['Clave'] != NULL){
                        $Clave = $registro['Clave'];
                    }
                    if($registro['Nombre'] != NULL){
                        $Nombre = $registro['Nombre'];
                    }
                    if($registro['Direccion'] != NULL){
                        $Direccion = $registro['Direccion'];
                    }
                    if($registro['RFC'] != NULL){
                        $RFC = $registro['RFC'];
                    }
                    if($registro['Telefono'] != NULL){
                        $Telefono = $registro['Telefono'];
                    }
                    if($registro['Celular'] != NULL){
                        $Celular = $registro['Celular'];
                    }
                    if($registro['Email'] != NULL){
                        $Email = $registro['Email'];
                    }
                    if($registro['Imagen'] != NULL){
                        $Imagen = $registro['Imagen'];
                    }
                    if($registro['Activo'] != NULL){
                        $Activo = $registro['Activo'];
                    }
                    if($registro['Fecha'] != NULL){
                        $Fecha = $registro['Fecha'];
                    }
                    if($registro['nIDRol'] != NULL){
                        $nIDRol = $registro['nIDRol'];
                    }
                    if($registro['nIDPuesto'] != NULL){
                        $nIDPuesto = $registro['nIDPuesto'];
                    }
                    if($registro['nIDDepartamento'] != NULL){
                        $nIDDepartamento = $registro['nIDDepartamento'];
                    }
                    if($registro['Sueldo'] != NULL){
                        $Sueldo = $registro['Sueldo'];
                    }
                    if($registro['Maquina'] != NULL){
                        $Maquina = $registro['Maquina'];
                    }
                    if($registro['Firma'] != NULL){
                        $Firma = $registro['Firma'];
                    }
                    if($registro['Estatus'] != NULL){
                        $Estatus= $registro['Estatus'];
                    }
                    if($registro['nIDExtension'] != NULL){
                        $nIDExtension = $registro['nIDExtension'];
                    }
                    if($registro['nIDEmpresa'] != NULL){
                        $nIDEmpresa = $registro['nIDEmpresa'];
                    }
                    if($registro['TipoDeRol'] != NULL){
                        $TipoDeRol = $registro['TipoDeRol'];
                    }

                    // Campos base
                    if($registro['FechaModificacion']!=NULL){
                        $FechaModificacion=$registro['FechaModificacion'];
                    }

                    if($registro['FechaCreacion']!=NULL){
                        $FechaCreacion=$registro['FechaCreacion'];
                    }

                    if($registro['Observaciones']!=NULL){
                        $Observaciones=$registro['Observaciones'];
                    }

                    if($registro['bEstado']!=NULL){
                        $bEstado=$registro['bEstado'];
                    }

                    // Carga la informacion en el objeto
                    $this->setInformacion(
                            $nIDUsuario,
                            $Usuario,
                            $Password,
                            $Clave,
                            $Nombre,
                            $Direccion,
                            $RFC,
                            $Telefono,
                            $Celular,
                            $Email,
                            $Imagen,
                            $Activo,
                            $Fecha,
                            $nIDRol,
                            $nIDPuesto,
                            $nIDDepartamento,
                            $Sueldo,
                            $Maquina,
                            $Firma,
                            $Estatus,
                            $nIDExtension,
                            $nIDEmpresa,
                            $TipoDeRol,
                            $FechaModificacion,
                            $FechaCreacion,
                            $Observaciones,
                            $bEstado,
                            FALSE,
                            TRUE,
                            FALSE
                    );
                }
            } else {
                $this->existenDatos = FALSE;
            }
            mysqli_close($mysqli);
            return TRUE;
        } catch (Exception $e) {
            echo 'Se capturo la siguiente excepción: ',  $e->getMessage(), "\n";
        }
    }

    /**
     *************** Función registrosTabla ***************
     *
     * @return void
     */
    private function registrosTabla()
    {

        // Abre la conexión
        $mysqli=mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT *";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

        if($this->formaDeOrdenamiento()==$this->Ascendente){
            $txtConsulta=$txtConsulta . " ASC";
        } else {
            $txtConsulta=$txtConsulta . " DESC";
        }

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Carga la informacion
        $contador=0;

        // Cuenta los registros
        if($res){
            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $contador = $contador + 1;
            }
        }

        mysqli_close($mysqli);
        return $contador;
    }


    public function buscarId($correo){

        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){

            $this->existeError = TRUE;

            $this->error       = "Error no se puede conectar a la base de datos";

            return 0;
        }

        $txtConsulta = "";

        $txtConsulta = " SELECT nIDUsuario ";

        $txtConsulta = $txtConsulta . " FROM ";

        $txtConsulta = $txtConsulta . $this->nombreTabla;

        $txtConsulta = $txtConsulta . " WHERE ";

        $txtConsulta = $txtConsulta . " Email = " . $correo;

        mysqli_query($mysqli,"SET NAMES UTF8");

        $res = mysqli_query($mysqli,$txtConsulta);

        $regs = array();

        while($r = mysqli_fetch_assoc($res)){

                    $regs[] = $r;

        }return $regs;

        mysqli_close($mysqli);
    }

    /**
     *************** Función contarRegistros ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function contarRegistros($condicion)
    {
        
        $contador = 0;

        // Abrea la conexion
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error No se puede conectar a la base de datos";
            return 0;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT count(nIDUsuario) as Total";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . $condicion;

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Cargar la informacion


        //$this->contarDatos=0;
        if($res){
            if($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                if($registro['Total']!=NULL){
                    $contador=$registro['Total'];
                }
            }
        }

        mysqli_close($mysqli);
        return $contador;
    }

    /**
     *************** Función contarRegistrosCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */
    private function contarRegistrosCondicion($condicion)
    {
        
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT *";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . $condicion;
        $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

        if($this->formaDeOrdenamiento()==$this->Ascendente){
            $txtConsulta=$txtConsulta . " ASC";
        } else {
            $txtConsulta=$txtConsulta . " DESC";
        }

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Carga la informacion
        $contador = 0;

        if($res){
            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $contador = $contador + 1;
            }
        }

        mysqli_close($mysqli);
        return $contador;
    }

     public function consultaGeneralEmail($tabla,$condicion)
    {
        
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT ";
        $txtConsulta = $txtConsulta . " Email ";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $tabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . $condicion;
        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        $regs = array();

        while($r = mysqli_fetch_assoc($res)){

              $regs[] = $r;

        }return $regs;

        mysqli_close($mysqli);
        
    }

    /**
     *************** Función ConsultarEstado ***************
     *
     * @param [int] $nID
     * @return void
     */
    private function ConsultarEstado($nID)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        $bestado = -1;

        // Generar consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT *";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . $this->id . "=" . $nID;
        $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

        if($this->formaDeOrdenamiento() == $this->Ascendente){
            $txtConsulta=$txtConsulta . " ASC";
        } else {
            $txtConsulta=$txtConsulta . " DESC";
        }

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Cargar la información
        if($res){
            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                if($registro['bEstado']!= NULL){
                    $bestado=$registro['bEstado'];
                    break;
                }
            }
            mysqli_close($mysqli);
            return $bestado;
        } else {
            mysqli_close($mysqli);
            return -1;
        }
    }

    /**
     *************** Función consultarID ***************
     *
     * @param [int] $usuarioo
     * @return void
     */
    private function consultarID($usuario)
    {

        // Abrea la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        $txtConsulta = "";
        $txtConsulta = "SELECT *";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . "nIDUsuario='" . $usuarioo . "'";

        //' Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);
        $nid = -1;

        // Cargar la informacion
        if($res){
            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $nid = -1;

                if($registro['nIDUsuario']!= NULL){
                    $nid = $registro['nIDUsuario'];
                    break;
                }
            }
            mysqli_close($mysqli);
            return $nid;
        } else {
            mysqli_close($mysqli);
            return -1;
        }
    }
    
    /**
     *************** Función borrar ***************
     *
     * @return void
     */
    private function borrar()
    {
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "DELETE FROM " . $this->nombreTabla;
        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función borrarID ***************
     *
     * @param [int] $nID
     * @return void
     */
    private function borrarID($nID)
    {
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "DELETE FROM " . $this->nombreTabla . " WHERE " . $this->id . "=" . $nID;
        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función borrarCondicion ***************
     *
     * @param [tstring] $condicion
     * @return void
     */
    private function borrarCondicion($condicion)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "DELETE FROM " . $this->nombreTabla . " WHERE " . $condicion;
        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función borrarTemporal ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @return void
     */
    private function borrarTemporal($nID, $observaciones)
    {
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " bEstado=0" . ",";

        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $Observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res=mysqli_query($mysqli,$txtInsercion);
        if($res==FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarEstado ***************
     *
     * @param [int] $nID
     * @param [int] $bEstado
     * @param [string] $Observaciones
     * @return void
     */
    private function actualizarEstado($nID, $bEstado, $Observaciones)
    {
        
        // Abre la conexión
        $mysqli=mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " bEstado=" . $bEstado . ",";

        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $Observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res=mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }


    /**
     *************** Función actualizarPassword ***************
     *
     * @param [int] $nID
     * @param [string] $password
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarPassword($nID, $password, $observaciones)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " Password='" . $this->especialesHTML($password) . "',";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res=mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }
        mysqli_close($mysqli);
        return TRUE;
    }
    
    /**
     *************** Función ActualizarNombre ***************
     * 
     * @param [int] $nID
     * @param [string] $nombre
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarNombre($nID, $nombre, $observaciones)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " Nombre=" . $this->especialesHTML($nombre) . ",";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarVIP ***************
     *
     * @param [int] $nIDUsuario
     * @param [string] $Usuario
     * @param [string] $Password
     * @param [string] $Clave
     * @param [string] $Nombre
     * @param [string] $Direccion
     * @param [string] $Telefono
     * @param [string] $Celular
     * @param [string] $Email
     * @param [string] $Imagen
     * @param [string] $Activo
     * @param [int] $nIDRol
     * @param [int] $nIDPuesto
     * @param [int] $nIDDepartamento
     * @param [string] $Sueldo
     * @param [string] $Maquina
     * @param [string] $Firma
     * @param [int] $nIDExtension
     * @param [int] $nIDEmpresa
     * @param [type] $observaciones
     * @return void
     */
    private function actualizarVIP($nIDUsuario,$Usuario,$Password,$Clave,$Nombre,$Direccion,$RFC,
    $Telefono,$Celular,$Email,$Imagen,$Activo,$Fecha,$nIDRol,$nIDPuesto,$nIDDepartamento,
    $Sueldo,$Maquina,$Firma,$Estatus,$nIDExtension,$nIDEmpresa, $observaciones)
    {

        // Abre la conexón
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion = "";
        $fechalocal = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " Usuario='" . $this->especialesHTML($Usuario) . "',";
        $txtInsercion = $txtInsercion . " Password='" . $this->especialesHTML($Password) . "',";
        $txtInsercion = $txtInsercion . " Clave='" . $this->especialesHTML($Clave) . "',";
        $txtInsercion = $txtInsercion . " Nombre='" . $this->especialesHTML($Nombre) . "',";
        $txtInsercion = $txtInsercion . " Direccion='" . $this->especialesHTML($Direccion) . "',";
        $txtInsercion = $txtInsercion . " RFC='" . $this->especialesHTML($RFC) . "',";
        $txtInsercion = $txtInsercion . " Telefono='" . $this->especialesHTML($Telefono) . "',";
        $txtInsercion = $txtInsercion . " Celular='" . $this->especialesHTML($Celular) . "',";
        $txtInsercion = $txtInsercion . " Email='" . $this->especialesHTML($Email) . "',";
        $txtInsercion = $txtInsercion . " Imagen='" . $this->especialesHTML($Imagen) . "',";
        $txtInsercion = $txtInsercion . " Activo='" . $this->especialesHTML($Activo) . "',";
        $txtInsercion = $txtInsercion . " Fecha='" . $this->especialesHTML($Fecha) . "',";
        $txtInsercion = $txtInsercion . " nIDRol='" . $this->especialesHTML($nIDRol) . "',";
        $txtInsercion = $txtInsercion . " nIDPuesto='" . $this->especialesHTMLÇ($nIDPuesto) . "',";
        $txtInsercion = $txtInsercion . " nIDDepartamento='" . $this->especialesHTML($nIDDepartamento) . "',";
        $txtInsercion = $txtInsercion . " Sueldo='" . $this->especialesHTML($Sueldo) . "',";
        $txtInsercion = $txtInsercion . " Maquina='" . $this->especialesHTML($Maquina) . "',";
        $txtInsercion = $txtInsercion . " Firma='" . $this->especialesHTML($Firma) . "',";
        $txtInsercion = $txtInsercion . " Estatus='" . $this->especialesHTML($Estatus) . "',";
        $txtInsercion = $txtInsercion . " nIDExtension='" . $this->especialesHTML($nIDExtension) . "',";
        $txtInsercion = $txtInsercion . " nIDEmpresa='" . $this->especialesHTML($nIDEmpresa) . "',";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nIDUsuario;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarNivel ***************
     *
     * @param [int] $nID
     * @param [int] $nIDNivel
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarNivel($nID, $nIDNivel, $observaciones)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion = "";
        $fechalocal = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " nIDNivel=" . $nIDNivel . ",";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarCodigoUnico ***************
     *
     * @param [int] $nID
     * @param [int] $codigoUnico
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarCodigoUnico($nID, $codigoUnico, $observaciones)
    {
        
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion = "";
        $fechalocal = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " CodigoUnico='" . $codigoUnico . "',";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    public function change($id,$pwd){
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError = TRUE;
            $this->error       = "Error no se puede conectar a la base de datos";
            return -1;
        }

        $txtInsercion = "";
        $fechalocal   = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion   = "UPDATE " . $this->nombreTabla;
        $txtInsercion   = $txtInsercion . " SET ";
        $txtInsercion   = $txtInsercion . " Password = '" . $pwd . "',";
         $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion   = $txtInsercion . " FechaModificacion =' " . $fechaLocal . "',";
        $txtInsercion   = $txtInsercion . "Observaciones =' Se ha cambiado la contraseña - " . $fechaLocal . "'";
        $txtInsercion   = $txtInsercion . " WHERE nIDUsuario = " . $id; 

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;

    }
    /**
     *************** Función actualizarTerminos ***************
     *
     * @param [int] $nID
     * @param [string] $aceptoTerminos
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarTerminos($nID, $aceptoTerminos, $observaciones)
    {
        
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion = "";
        $fechalocal = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " AceptoTerminos='" . $aceptoTerminos . "',";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }


    /**
     *************** Función grabar ***************
     *
     * @param [array] $tabla
     * @return void
     */
    private function grabar($tabla)
    {

        // Abre la conexion
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        for ($i = 0;$i<$this->contarDatos; $i++){
            if($tabla[$i]["Crear"]==TRUE){
                $txtInsercion = "INSERT INTO " . $this->nombreTabla;
                $txtInsercion = $txtInsercion . "(";

                $txtInsercion = $txtInsercion . "nIDUsuario,";
                $txtInsercion = $txtInsercion . "Usuario,";
                $txtInsercion = $txtInsercion . "Password,";
                $txtInsercion = $txtInsercion . "Clave,";
                $txtInsercion = $txtInsercion . "Nombre,";
                $txtInsercion = $txtInsercion . "Direccion,";
                $txtInsercion = $txtInsercion . "RFC,";
                $txtInsercion = $txtInsercion . "Telefono,";
                $txtInsercion = $txtInsercion . "Celular,";
                $txtInsercion = $txtInsercion . "Email,";
                $txtInsercion = $txtInsercion . "Imagen,";
                $txtInsercion = $txtInsercion . "Activo,";
                $txtInsercion = $txtInsercion . "Fecha,";
                $txtInsercion = $txtInsercion . "nIDRol,";
                $txtInsercion = $txtInsercion . "nIDPuesto,";
                $txtInsercion = $txtInsercion . "nIDDepartamento,";
                $txtInsercion = $txtInsercion . "Sueldo,";
                $txtInsercion = $txtInsercion . "Maquina,";
                $txtInsercion = $txtInsercion . "Firma,";
                $txtInsercion = $txtInsercion . "Estatus,";
                $txtInsercion = $txtInsercion . "nIDExtension,";
                $txtInsercion = $txtInsercion . "nIDEmpresa,";
                $txtInsercion = $txtInsercion . "FechaModificacion,";
                $txtInsercion = $txtInsercion . "FechaCreacion,";
                $txtInsercion = $txtInsercion . "Observaciones,";
                $txtInsercion = $txtInsercion . "bEstado";


                $txtInsercion = $txtInsercion . ")";
                $txtInsercion = $txtInsercion . " VALUES";
                $txtInsercion = $txtInsercion . "(";

                $txtInsercion = $txtInsercion . $tabla[$i]["nIDUsuario"] . ",";
                $txtInsercion = $txtInsercion . "'" .$tabla[$i]["Usuario"] . "',";
                $txtInsercion = $txtInsercion . "'" .$tabla[$i]["Password"] . "',";
                $txtInsercion = $txtInsercion . "'" .$tabla[$i]["Clave"] . "',";
                $txtInsercion = $txtInsercion . "'" .$tabla[$i]["Nombre"] . "',";
                $txtInsercion = $txtInsercion . "'" .$tabla[$i]["Direccion"] . "',";
                $txtInsercion = $txtInsercion . "'" .$tabla[$i]["RFC"] . "',";
                $txtInsercion = $txtInsercion . "'" .$tabla[$i]["Telefono"] . "',";
                $txtInsercion = $txtInsercion . "'" .$tabla[$i]["Celular"] . "',";
                $txtInsercion = $txtInsercion . "'" .$tabla[$i]["Email"] . "',";
                $txtInsercion = $txtInsercion . "'" .$tabla[$i]["Imagen"] . "',";
                $txtInsercion = $txtInsercion . "'" .$tabla[$i]["Activo"] . "',";
                $txtInsercion = $txtInsercion . "'" .$tabla[$i]["Fecha"] . "',";
                $txtInsercion = $txtInsercion .      $tabla[$i]["nIDRol"] . ",";
                $txtInsercion = $txtInsercion .      $tabla[$i]["nIDPuesto"] . ",";
                $txtInsercion = $txtInsercion .      $tabla[$i]["nIDDepartamento"] . ",";
                $txtInsercion = $txtInsercion . "'" .$tabla[$i]["Sueldo"] . "',";
                $txtInsercion = $txtInsercion . "'" .$tabla[$i]["Maquina"] . "',";
                $txtInsercion = $txtInsercion . "'" .$tabla[$i]["Firma"] . "',";
                $txtInsercion = $txtInsercion . "'" .$tabla[$i]["Estatus"] . "',";
                $txtInsercion = $txtInsercion . "'" .$tabla[$i]["nIDExtension"] . "',";
                $txtInsercion = $txtInsercion . $tabla[$i]["nIDEmpresa"] . ",";

                // Campos bases
                $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
                $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
                $txtInsercion = $txtInsercion . "'" . $fechaLocal . "',";
                $txtInsercion = $txtInsercion . "'" . $fechaLocal . "',";

                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Observaciones"] . "',";

                $txtInsercion = $txtInsercion . $tabla[$i]["bEstado"];

                $txtInsercion = $txtInsercion . ")";

            } else {

                if($tabla[$i]["Cambiar"]==TRUE){

                    $txtInsercion = "UPDATE " .  $this->nombreTabla;
                    $txtInsercion = $txtInsercion . " SET ";

                    
                    $txtInsercion = $txtInsercion . " Nombre = '" . $tabla[$i]["Nombre"] . "',";
                    $txtInsercion = $txtInsercion . " RFC = '" . $tabla[$i]["RFC"] . "',";
                    $txtInsercion = $txtInsercion . " Usuario = '" . $tabla[$i]["Usuario"] . "',";
                   // $txtInsercion = $txtInsercion . " Password = '" . $tabla[$i]["Password"] . "',";
                    $txtInsercion = $txtInsercion . " Telefono = " . $tabla[$i]["Telefono"] . ",";
                    $txtInsercion = $txtInsercion . " Celular = " . $tabla[$i]["Celular"] . ",";        
                    $txtInsercion = $txtInsercion . " Email = '" . $tabla[$i]["Email"] . "',";
                    $txtInsercion = $txtInsercion . " Imagen = '" . $tabla[$i]["Imagen"] . "',";
                    $txtInsercion = $txtInsercion . " Direccion = '" . $tabla[$i]["Direccion"] . "',";
                    $txtInsercion = $txtInsercion . " nIDEmpresa =" . $tabla[$i]["nIDEmpresa"] . ",";
                    $txtInsercion = $txtInsercion . " nIDRol =" . $tabla[$i]["nIDRol"] . ",";

                    // Basicas
                    $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
                    $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
                    $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

                    $txtInsercion = $txtInsercion . " Observaciones='". $tabla[$i]["Observaciones"] . "'";
                    $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$tabla[$i]["nIDUsuario"];

                    

                } else {

                    if($tabla[$i]["Eliminar"]==TRUE){
                        $txtInsercion = "DELETE FROM " . $this->nombreTabla . " WHERE " . $this->id . "=" .$tabla[$i]["nIDUsuario"];

                    }else{
                        $txtInsercion = "SELECT * FROM " . $this->nombreTabla . " WHERE Email= '"  .$tabla[$i]["Email"]. "'";
                    }

                }

            }
          
            //echo $txtInsercion;
            mysqli_query($mysqli, "SET NAMES UTF8");
            $res=mysqli_query($mysqli,$txtInsercion);

            $this->last_id = mysqli_insert_id($mysqli);
            if($res==FALSE){
                mysqli_close($mysqli);
                return FALSE;
            }

        }
        mysqli_close($mysqli);
        return TRUE;
    }
    
    /**
     *************** Función limpiarTabla ***************
     *
     * @return void
     */
    public function limpiarTabla()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->borrar() == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función eliminarPorID ***************
     *
     * @param [int] $nID
     * @return void
     */
    public function eliminarPorID($nID)
    {

        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->borrarID($nID) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función eliminarConCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function eliminarConCondicion($condicion)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if(strlen($condicion)<=0){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene condicion";
            return FALSE;
        }

        if ($this->borrarCondicion($condicion) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función ocultar ***************
     *
     * @param [int] $nID
     * @param [string] $Observaciones
     * @return void
     */
    public function ocultar($nID, $Observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->borrarTemporal($nID,$Observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarEstado ***************
     *
     * @param [int] $nID
     * @param [string] $Observaciones
     * @param [int] $bEstado
     * @return void
     */
    public function cambiarEstado($nID, $Observaciones, $bEstado)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarEstado($nID,$Observaciones,$bEstado) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarPassword ***************
     *
     * @param [int] $nID
     * @param [string] $Observaciones
     * @param [string] $Password
     * @return void
     */
    public function cambiarPassword($nID, $Observaciones, $Password)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarPassword($nID,$Password,$Observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }


    /**
     *************** Función cambiarNombre ***************
     *
     * @param [int] $nID
     * @param [string] $nombre
     * @param [string] $observaciones
     * @return void
     */
    public function cambiarNombre($nID, $nombre, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarNombre($nID,$nombre,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarVIP ***************
     *
     * @param [int] $nIDUsuario
     * @param [string] $Usuario
     * @param [string] $Password
     * @param [string] $Clave
     * @param [string] $Nombre
     * @param [string] $Direccion
     * @param [string] $Telefono
     * @param [string] $Celular
     * @param [string] $Email
     * @param [string] $Imagen
     * @param [string] $Activo
     * @param [int] $nIDRol
     * @param [int] $nIDPuesto
     * @param [int] $nIDDepartamento
     * @param [string] $Sueldo
     * @param [string] $Maquina
     * @param [string] $Firma
     * @param [int] $nIDExtension
     * @param [int] $nIDEmpresa
     * @param [string] $observaciones
     * @return void
     */
    public function cambiarVIP($nIDUsuario,$Usuario,$Password,$Clave,$Nombre,$Direccion,$RFC,
    $Telefono,$Celular,$Email,$Imagen,$Activo,$Fecha,$nIDRol,$nIDPuesto,$nIDDepartamento,
    $Sueldo,$Maquina,$Firma,$Estatus,$nIDExtension,$nIDEmpresa, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarVIP($nIDUsuario,$Usuario,$Password,$Clave,$Nombre,$Direccion,$RFC,
        $Telefono,$Celular,$Email,$Imagen,$Activo,$Fecha,$nIDRol,$nIDPuesto,$nIDDepartamento,
        $Sueldo,$Maquina,$Firma,$Estatus,$nIDExtension,$nIDEmpresa, $observaciones)==TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarNivel ***************
     *
     * @param [type] $nID
     * @param [type] $nIDNivel
     * @param [type] $observaciones
     * @return void
     */
    public function cambiarNivel($nID, $nIDNivel, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarNivel($nID,$nIDNivel, $observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarCodigoUnico ***************
     *
     * @param [int] $nID
     * @param [int] $codigoUnico
     * @param [string] $observaciones
     * @return void
     */
    public function cambiarCodigoUnico($nID, $codigoUnico, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarCodigoUnico($nID,$codigoUnico, $observaciones)==TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }
      
    
  
    /**
     *************** Función leer ***************
     *
     * @return void
     */
    public function leer()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if($this->consultar() == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

   

    /**
     *************** Función leerCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function leerCondicion($condicion)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if(strlen($condicion) <= 0){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene Condicion";
            return FALSE;
        }

        if($this->consultarCondicion($condicion) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función totalRegistros ***************
     *
     * @return void
     */
    public function totalRegistros()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError = TRUE;
            $this->error       = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->registrosTabla();
    }

    /**
     *************** Función numeroTotalRegistrosCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function numeroTotalRegistrosCondicion($condicion)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->contarRegistrosCondicion($condicion);
    }

    /**
     *************** Función getEstado ***************
     *
     * @param [int] $nID
     * @return void
     */
    public function getEstado($nID)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->ConsultarEstado($nID);
    }

    /**
     *************** Función getID ***************
     *
     * @param [int] $usuario
     * @return void
     */
    public function getID($usuario)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->consultarID($usuario);
    }



    /**
     *************** Función ejecutar ***************
     *
     * @return void
     */
    public function ejecutar()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }


        if($this->contarDatos<=0){
            return FALSE;
        }

        if($this->grabar($this->dtGrabar) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
?>