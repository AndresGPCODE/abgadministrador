<?php
    error_reporting(E_ALL ^ E_NOTICE);
    ini_set('display_errors','1');

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Request-Methods');

      include_once '../Configuraciones/Conexion.php';
      include_once '../Cliente/clEstadoCuenta.php';
      include_once '../Utilerias/clHerramientas_v2011.php';

      
      $method = $_SERVER['REQUEST_METHOD'];

      $UtileriasDatos   = new clHerramientasv2011();
      $fechaLocal       = $UtileriasDatos->getFechaYHoraActual_General();
      $fechaLocal       = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
      
      $edoCuenta = new clEstadoCuenta();

      
      $edoCuenta->conexion($SERVER,$USER,$PASSWORD,$BD);
      
      switch($method){

  


        case 'POST' :

              $action = $_POST['accion'];

              if($action == 'buscarDetalles'){

                $id = $_POST['id'];


              if($edoCuenta->consultarCondicion(" * "," WHERE tbl_estado_cuenta.bEstado = 1 and tbl_estado_cuenta.nIDSolicitud = " . $id)){

                    echo json_encode($edoCuenta->consultarCondicion(" * "," WHERE tbl_estado_cuenta.bEstado = 1 and tbl_estado_cuenta.nIDSolicitud = " . $id));
              }else{

                   echo 'null';
              }
              

            }else if($action == 'buscarEstado'){

                  $id = $_POST['id'];

                  $valores = " tbl_estado_cuenta.nIDEstadoCuenta,tbl_estado_cuenta.Detalle,tbl_estado_cuenta.Cantidad, ";
                  $valores = $valores . " tbl_estado_cuenta.Costo,tbl_estado_cuenta.nIDCliente,tbl_estado_cuenta.nIDSolicitud, ";
                  $valores = $valores . " DATE_FORMAT(tbl_estado_cuenta.FechaCreacion, '%d/%m/%Y  %H:%i:%s') AS FechaCreacion,tbl_automoviles.nIDAutomovil,tbl_automoviles.Modelo, ";
                  $valores = $valores . " tbl_automoviles.Marca,tbl_automoviles.Placas ";

                  $condicion = " INNER JOIN tbl_solicitud ON tbl_solicitud.nIDSolicitud = tbl_estado_cuenta.nIDSolicitud ";
                  $condicion = $condicion . " INNER JOIN tbl_automoviles ON tbl_automoviles.nIDAutomovil = tbl_solicitud.nIDAutomovil ";
                  $condicion = $condicion . " where tbl_estado_cuenta.bEstado = 1 and tbl_estado_cuenta.Estatus = 'NO PAGADO' and tbl_estado_cuenta.nIDCliente = " . $id;

                  if($edoCuenta->consultarCondicion($valores, $condicion)){

                    echo json_encode($edoCuenta->consultarCondicion($valores, $condicion));
              }else{

                   echo 'null';
              }



            }else if($action == 'agregar'){

                        $object = $_POST['objeto'];

                        $data   = json_decode($object);

                        $edoCuenta->setInformacion(

                                $data->nIDEstadoCuenta,
                                $data->Detalle,
                                $data->Cantidad,
                                $data->Costo,
                                $data->Descripcion,
                                $data->nIDCliente,
                                $data->nIDSolicitud,
                                'NO PAGADO',
                                '',
                                '',
                                'Registro de un nuevo detalle  - ' . $fechaLocal,
                                1,
                                TRUE,
                                FALSE,
                                FALSE

                        );

                        if($edoCuenta->ejecutar('tbl_estado_cuenta')){

                                echo "INSERTED";

                        }else{

                                echo "NOT INSERTED";
                        }
              
              }else if($action == 'modificar'){

                               $id = $_POST['id'];

                        $edoCuenta->setInformacion(

                                '',
                                '',
                                '',
                                '',
                                '',
                                $id,
                                '',
                                'PAGADO',
                                '',
                                '',
                                'Actualización de pago  - ' . $fechaLocal,
                                1,
                                FALSE,
                                TRUE,
                                FALSE

                        );

                        if($edoCuenta->ejecutar('tbl_estado_cuenta')){

                                echo "UPDATED";

                        }else{

                                echo "NOT UPDATED";
                        }

              }else if($action == 'eliminar'){

                               $id = $_POST['id'];

                      $observacion = 'Agencia eliminada temporalmente - ' . $fechaLocal;

                      if($agency->ocultar($id,$observacion)){

                            echo "DELETED";
                      }else{

                            echo "NOT DELETED";
                      }
              }
            
      }


?>