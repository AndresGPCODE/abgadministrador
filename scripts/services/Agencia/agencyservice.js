'use strict';

angular.module('abgpassportApp')
  .service('agencyServices', ['$http','configService',function($http,configService){
    return {

      get: function(){

          return $http.get(configService.apiUrl + 'Empresa.php?accion=consulta',

            { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }

        );

      },

      add : function(agencia){

          return $http.post(configService.apiUrl + 'Empresa.php','accion=agregar&agencia=' + JSON.stringify(agencia),

            { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

          );

      },

      update : function(agencia){

             return $http.post(configService.apiUrl + 'Empresa.php','accion=modificar&agencia=' + JSON.stringify(agencia),

              { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

            );

      },

      destroy : function(id){

              return $http.post(configService.apiUrl + 'Empresa.php','accion=eliminar&id=' + JSON.stringify(id),

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

              )

      }

      
    };

  }]);