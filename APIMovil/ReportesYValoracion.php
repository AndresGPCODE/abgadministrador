<?php
/*
------REPORTES DE INCIDENTES DE LOS CLIENTES (SINIESTRO O AVERIAS) VALORACION DE ENTREGA DEL VEHICULO POR PARTE DEL CLIENTE
*/
header('Content-Type: application/json');
date_default_timezone_set('America/Mexico_City');
$method = $_SERVER["REQUEST_METHOD"];
//$method = 'POST';
include 'conexion.php';
$image = '';
// base64ToImage($image, "pruebas.jpeg");
switch ($method) {
    case 'POST':
        $request = json_decode(file_get_contents('php://input'), true);
        //$request['idCliente'] = 3;
        //$request['option'] = 3; // 1 para obtener datos de proxima entrega, 2 para guardar evidencias de la entrega

        if (is_null($request['idCliente']) || is_null($request['option'])) {
            $request['resp'] = 'Los Datos enviados están Incompletos||' . $request . " || " . file_get_contents('php://input');
            $request['status'] = false;
            return json_encode($request);
        } else {
            switch ($request['option']) {
                case 1:
                    $resp = agregarReporteSiniestro($request);
                    if ($resp['status']) {
                        echo json_encode($resp);
                        return json_encode($resp);
                    } else {
                        $resp['resp'] = 'Error no se pudo preocesar la solicitud';
                        echo json_encode($resp);
                        return json_encode($resp);
                    }
                    break;
                case 2:
                    $resp = agregarReporteIncidencias($request);
                    if ($resp['status']) {
                        echo json_encode($resp);
                        return json_encode($resp);
                    } else {
                        $resp['resp'] = 'Error no se pudo preocesar la solicitud';
                        echo json_encode($resp);
                        return json_encode($resp);
                    }
                    break;
                case 3:
                    $resp = valorarEntrega($request);
                    if ($resp['status']) {
                        echo json_encode($resp);
                        return json_encode($resp);
                    } else {
                        $resp['resp'] = 'Error no se pudo preocesar la solicitud';
                        echo json_encode($resp);
                        return json_encode($resp);
                    }
                    break;
                default:
                    $resp['resp'] = 'Error no se pudo preocesar la solicitud';
                    $resp['status'] = false;
                    echo json_encode($resp);
                    return json_encode($resp);
                    break;
            } // fin del switch de option
        }
        break;
    default:
        $request['resp'] = 'No se puede procesar la informacion';
        $request['status'] = false;
        return json_encode($request);
}

function agregarReporteSiniestro($datos){
    // Insertar proximo cambio del cliente
    $pdo = Conexion();
    $fechaActual = date('Y-m-d H:i:s');
    $insert = "INSERT INTO tbl_reportes (tipoReporte,nIDCliente,nIDContacto,latitud,longitud,altitud,fechaCreacion,fechaModificacion,Observaciones,bEstado) 
    VALUES ('SINIESTRO', ". $datos['idCliente'] . ", " . otbtenerContactoId($datos['idCliente']) . ", " . $datos['Latitud'] . ", " . $datos['Longitud'] . ", 0.0, '$fechaActual', '$fechaActual', 'Reporte Nuevo', 1)";
    $ejecutar = $pdo->prepare($insert);
    // var_dump($insert);
    $ejecutar->execute();
    $result = $pdo->lastInsertId();
    $arr = [];
    if ($result > 0) {
        $row['lastID'] = $result;
        $row['status'] = true;
        $arr = $row;
        return $arr;
    } else {
        $row['status'] = false;
        $arr = $row;
        return $arr;
    }
}

function agregarReporteIncidencias($datos){
    // Insertar proximo cambio del cliente
    $pdo = Conexion();
    $fechaActual = date('Y-m-d H:i:s');
    $insert = "INSERT INTO tbl_reportes (tipoReporte,nIDCliente,nIDContacto,latitud,longitud,altitud,fechaCreacion,fechaModificacion,Observaciones,bEstado) 
    VALUES ('EMERGENCIA', " . $datos['idCliente'] . ", " . otbtenerContactoId($datos['idCliente']) . ", " . $datos['Latitud'] . ",  " . $datos['Longitud'] . ", 0.0, '$fechaActual', '$fechaActual', 'Reporte Nuevo', 1)";
    $ejecutar = $pdo->prepare($insert);
    // var_dump($insert);
    $ejecutar->execute();
    $result = $pdo->lastInsertId();
    $arr = [];
    if ($result > 0) {
        $row['lastID'] = $result;
        $row['status'] = true;
        $arr = $row;
        return $arr;
    } else {
        $row['status'] = false;
        $arr = $row;
        return $arr;
    }
}

function otbtenerContactoId($id){
    $pdo = Conexion();
    $slectSolicitud = "SELECT nIDContactoEmergencia FROM tbl_cat_contacto_emergencia WHERE nIDCliente = " . $id;
    $ejecutarSelect = $pdo->prepare($slectSolicitud);
    $ejecutarSelect->execute();
    $result = $ejecutarSelect->fetchAll(PDO::FETCH_ASSOC);
    if (count($result) > 0) {
        return $result[0]['nIDContactoEmergencia'];
    } else {
        return 0;
    }
}

function valorarEntrega($datos){
    // Insertar proximo cambio del cliente
    $pdo = Conexion();
    $fechaActual = date('Y-m-d H:i:s');
    $nIDUsuaro = otbtenerUsuarioId($datos['idCliente']);
    $insert = "INSERT INTO tbl_encuesta (Limpieza, Puntualidad, Amabilidad, Comentario, EstadoDelVehiculo, TanqueLleno, nIDCliente, nIDUsuario, FechaModificacion, FechaCreacion, Observaciones, bEstado) VALUES (". $datos['Limpieza']. "," . $datos['Puntualidad'] . "," . $datos['Amabilidad'] . ",'" . $datos['Comentario'] . "'," . $datos['EstadoDelVehiculo'] . "," . $datos['TanqueLleno'] . "," . $datos['idCliente'] . ",$nIDUsuaro,'$fechaActual','$fechaActual','Valoracion de entraga de vehiculo',1)";
    $ejecutar = $pdo->prepare($insert);
    // var_dump($insert);
    $ejecutar->execute();
    $result = $pdo->lastInsertId();
    $arr = [];
    if ($result > 0) {
        $row['lastID'] = $result;
        $row['status'] = true;
        $arr = $row;
        return $arr;
    } else {
        $row['status'] = false;
        $arr = $row;
        return $arr;
    }
}

function otbtenerUsuarioId($id){
    $pdo = Conexion();
    $slectSolicitud = "SELECT nIDUsuario FROM tbl_clientes WHERE nIDCliente = $id Limit 1";
    $ejecutarSelect = $pdo->prepare($slectSolicitud);
    $ejecutarSelect->execute();
    $result = $ejecutarSelect->fetchAll(PDO::FETCH_ASSOC);
    if (count($result) > 0) {
        return $result[0]['nIDUsuario'];
    } else {
        return 0;
    }
}
