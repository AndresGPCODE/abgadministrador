'use strict';

angular.module('abgpassportApp')

       .controller('UbicacionMapaCtrl', ['$scope','asignacionServices','historialService', 

        function($scope,asignacionServices,historialService){

            mapboxgl.accessToken = 'pk.eyJ1IjoiYnJpYW5hZnJpYXMiLCJhIjoiY2s4dDl3dXNoMHk2bzNobnNxcjl1YmF0NyJ9.lgYrXbnGrogqmH65_RPobQ';

            var map; //MAPA

            var markerr;// MARCADOR DE BUSQUEDA INDIVIDUAL

            var markerTodos;// TODOS LOS MARCADORES 

            var arreglo = [];// ARREGLO DE MARCADORES

            $scope.get = function(){

                  $scope.getDibuja();

                  asignacionServices.get().then(function(resp){console.log(resp.data);

                                     if(resp.data == 'null'){

                                            console.log('Not Found Data');

                                     }else{

                                            $scope.tipos = resp.data;

                                     }

                  },function(err){

                      console.log('error' + JSON.stringify(err));

                  });

            };


          
            $scope.getDibuja = function(){

                  

                  map =  new mapboxgl.Map({
                      container: 'mapa-mapbox', // container id
                      style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
                      center: [-103.3004668,20.6481645], // starting position [lng, lat]
                      zoom: 9 // starting zoom
                  });

                  var nav = new mapboxgl.NavigationControl();

                  map.addControl(nav, "top-left");

                 $scope.buscarTodos();

                  
            };
                
          

 
            $scope.busqueda = function(id){ 

            	console.log(arreglo);

            	if(markerr != undefined){
            		markerr.remove(); 

            	}

            	if(arreglo.length != 0){ 
            		for(var i = 0; i < arreglo.length; i++){

            				arreglo[i].remove();

            		}

            	} 

                  historialService.get(id).then(function(resp){

                                  if(resp.data == 'null'){

                                           console.log('error');

                                  }else{

                                     var geojson = {
                                          type: 'FeatureCollection',
                                          features: [{
                                            type: 'Feature',
                                            geometry: {
                                              type: 'Point',
                                              coordinates: [resp.data[0]['Longitud'], resp.data[0]['Latitud']]
                                            },
                                            properties: {
                                              title: resp.data[0]['Identificador'],
                                            }
                                          }]
                                        };
                                          
                                        // add markers to map
                                        geojson.features.forEach(function(marker) {

                                        // create a HTML element for each feature
                                        var el = document.createElement('div');
                                        el.className = 'marker'; 



                                        // make a marker for each feature and add to the map
                                       markerr =  new mapboxgl.Marker(el)

                                        .setLngLat(marker.geometry.coordinates)
                                        .setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
                                        .setHTML('<h3>' + marker.properties.title + '</h3>'))

                                        .addTo(map);
                                      
                                        });

                                        

                                  }

                  },function(err){

                    console.log('error' + JSON.stringify(err));

                  });

            };


            $scope.buscarTodos = function(){ 

            		if(markerr != undefined){

            			markerr.remove();
            		}

            		if(arreglo.length != 0){ 

            		for(var i = 0; i < arreglo.length; i++){

            				arreglo[i].remove();

            		}

            	} 

            	

                  historialService.getTodos().then(function(resp){

                                  if(resp.data == 'null'){


                                  }else{

                                      for(var i = 0; i < resp.data.length; i++){

                                        var geojson = {
                                          type: 'FeatureCollection',
                                          features: [{
                                            type: 'Feature',
                                            geometry: {
                                              type: 'Point',
                                              coordinates: [resp.data[i]['Longitud'], resp.data[i]['Latitud']]
                                            },
                                            properties: {
                                              title: resp.data[i]['Identificador'],
                                            }
                                          }]
                                        };

                                        // add markers to map
                                        geojson.features.forEach(function(marker) {

                                        // create a HTML element for each feature
                                       var el = document.createElement('div');
                                        el.className = 'marker';

                                        // make a marker for each feature and add to the map
                                       markerTodos = new mapboxgl.Marker(el)
                                        .setLngLat(marker.geometry.coordinates)
                                        .setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
                                        .setHTML('<h3>' + marker.properties.title + '</h3>'))
                                        .addTo(map);
                                        arreglo.push(markerTodos);
                                        });

                                      }
                                  }
                  })
            }

            

       }]);