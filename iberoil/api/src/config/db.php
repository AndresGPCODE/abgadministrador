<?php
    class db{
        private $dbHost     = 'localhost';
        private $dbUser     = 'briana';
        private $dbPassword = 'S0ft3rN1um';
        private $dbName     = 'bd_Iberoil';

        //Connection

        public function connection(){
            try {
                $mysqlConnect = "mysql:host=$this->dbHost;dbname=$this->dbName;charset=UTF8";
                $dbConnection = new PDO($mysqlConnect, $this->dbUser, $this->dbPassword);
                $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $dbConnection;
            } catch (Exception $e) {
                die('Error:' . $e->getMessage());
            }
            
        }
    }