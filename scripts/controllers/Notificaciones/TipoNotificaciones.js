'use strict';

angular.module('abgpassportApp')

	   .controller('TypeNotificationCtrl',['$scope','typeNotificationServices', 

	   	function($scope,typeNotificationServices){

	   	$scope.Tipo = {

	   		  nIDTipoNotificacion : '0',
	   		  TipoNotificacion    : '',
	   		  Descripcion 		  : ''

	   	};

	   	$scope.clean = function(){

	   		  $scope.Tipo = {

	   		  		nIDTipoNotificacion : '0',
	   		  		TipoNotificacion 	: '',
	   		  		Descripcion 		: ''

	   		  };

	   		  $("#add_modal").modal('hide');

	   	};

	   	$scope.TipoEdit = {

	   		  nIDTipoNotificacion : '0',
	   		  TipoNotificacion    : '',
	   		  Descripcion 		  : ''

	   	};

	   	$scope.cleanEdit = function(){

	   		  $scope.TipoEdit = {

	   		  		nIDTipoNotificacion : '0',
	   		  		TipoNotificacion 	: '',
	   		  		Descripcion 		: ''

	   		  };

	   		  $("#update_modal").modal('hide');

	   	};

	   	$scope.get = function(){

	   		  typeNotificationServices.get().then(function(resp){

	   		  						  if(resp.data == 'null'){

	   		  						  		 console.log('Not Found data');
	   		  						  		 $scope.Titulo = "No tienes tipos de notificaciones registrados";

	   		  						  }else{

	   		  						  		$scope.tipos = resp.data;

	   		  						  }

	   		  },function(err){

	   		  		console.log('error' + JSON.stringify(err));

	   		  });


	   	};

	   	$scope.add = function(objeto){

	   		if(objeto.TipoNotificacion.length == 0){

	   				 swal.fire({

	   		  			 title 						: 'Oops!',

	   		  			 text 						: 'Tipo de notificación se encuentra vacio completalo',

	   		  			 imageUrl 					: 'assets/img/advertencia.png',

	   		  			 showCloseButton			: true,

	   		  			 confirmButtonColor 		: '#044BFD'

	   		  		 });
	   		}else if(objeto.Descripcion.length == 0){

	   				 swal.fire({

	   		  			 title 						: 'Oops!',

	   		  			 text 						: 'Descripción se encuentra vacio completalo',

	   		  			 imageUrl 					: 'assets/img/advertencia.png',

	   		  			 showCloseButton			: true,

	   		  			 confirmButtonColor 		: '#044BFD'

	   		  		 });
	   		}else{

	   		  typeNotificationServices.add(objeto).then(function(resp){

	   		  						  if(resp.data == 'INSERTED'){

	   		  						  		 swal.fire({

	   		  						  		 	 title 						: 'Registro exitoso!',

	   		  						  		 	 imageUrl 					: 'assets/img/completo.png',

	   		  						  		 	 showCloseButton			: true,

	   		  						  		 	 confirmButtonColor 		: '#044BFD'

	   		  						  		 });

	   		  						  		 $scope.clean();
	   		  						  		 $scope.get();

	   		  						  }else{

	   		  						  	swal.fire({

	   		  						  		 title 					: 'Oops!',

	   		  						  		 text 					: 'No se completo el registro, Inténtalo de nuevo',

	   		  						  		 imageUrl 				: 'assets/img/error.png',

	   		  						  		 showCloseButton 		: true,

	   		  						  		 confirmButtonColor 	: '#044BFD'

	   		  						  	});

	   		  						  }
	   		  },function(err){

	   		  				console.log('error' + JSON.stringify(err));

	   		  });

	   		}

	   	};

	   	$scope.show = function(objeto){

	   		  $scope.TipoEdit.nIDTipoNotificacion = objeto.nIDTipoNotificacion;
	   		  $scope.TipoEdit.TipoNotificacion    = objeto.TipoNotificacion;
	   		  $scope.TipoEdit.Descripcion 	      = objeto.Descripcion;

	   	};

	   	$scope.update = function(objeto){

	   		  typeNotificationServices.update(objeto).then(function(resp){

	   		  						  if(resp.data == 'UPDATED'){

	   		  						  		 swal.fire({

	   		  						  		 	 title 						: 'Modificación exitosa!',

	   		  						  		 	 imageUrl 					: 'assets/img/completo.png',

	   		  						  		 	 showCloseButton			: true,

	   		  						  		 	 confirmButtonColor 		: '#044BFD'

	   		  						  		 });

	   		  						  		 $scope.cleanEdit();
	   		  						  		 $scope.get();

	   		  						  }else{

	   		  						  	swal.fire({

	   		  						  		 title 					: 'Oops!',

	   		  						  		 text 					: 'No se completo el registro, Inténtalo de nuevo',

	   		  						  		 imageUrl 				: 'assets/img/error.png',

	   		  						  		 showCloseButton 		: true,

	   		  						  		 confirmButtonColor 	: '#044BFD'

	   		  						  	});

	   		  						  }
	   		  },function(err){

	   		  		console.log('error' + JSON.stringify(err));

	   		  });

	   	};

	   	$scope.destroy = function(id){

              swal.fire({

                  title               : 'ADVERTENCIA',

                  text                : '¿Estás seguro de eliminar este registro?',

                  imageUrl            : 'assets/img/advertencia.png',

                  showCancelButton    : true,

                  showCloseButton     : true,                  

                  cancelButtonColor   : 'rgba(4,75,253,0.5)',                  

                  confirmButtonColor  : '#044BFD',

                  cancelButtonText    : 'Cancelar',

                  confirmButtonText   : 'Si',

              }).then((result) => {

                       if(result.value){

                                typeNotificationServices.destroy(id).then(function(resp){

                                              if(resp.data == "DELETED"){

                                                      swal.fire({

                                                          title              : 'Registro eliminado con exito!',

                                                          imageUrl           : 'assets/img/completo.png',

                                                          showCloseButton    : true,

                                                          confirmButtonColor : '#044BFD'

                                                      });

                                                      $scope.get();

                                              }else{

                                                      swal.fire({

                                                          title             : 'Oops!',

                                                          text              : 'No se pudo eliminar el registro inténtalo de nuevo',

                                                          imageUrl          : 'assets/img/error.png',

                                                          showCloseButton   : true,

                                                          confirmButtonColor: '#044BFD'

                                                      });

                                              }

                                },function(err){

                                      console.log('error' + JSON.stringify(err));

                                });

                       }else{

                                swal.fire({

                                    title               : 'Se cancelo la eliminicación!',

                                    imageUrl            : 'assets/img/error.png',

                                    showCloseButton     : true,

                                    confirmButtonColor  : '#044BFD'

                                });
                 
                       }
             
              });
        
        };



	   }]);