<?php
  error_reporting(E_ALL ^ E_NOTICE);  

  ini_set('display_errors', '1');

  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Request-Methods');

  include_once '../Configuraciones/Conexion.php';
  include_once '../Automovil/clAutomovil.php';
  include_once '../Utilerias/clHerramientas_v2011.php';

  $method         = $_SERVER['REQUEST_METHOD'];

  $UtileriasDatos = new clHerramientasv2011();
  $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
  $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

  $auto         = new clAutomovil();

  $auto->conexion($SERVER,$USER,$PASSWORD,$BD);

  switch($method){

        case 'GET' : 

              $action = $_GET['accion'];

              if($action == 'consultar'){
                  
                  $auto->leerCondicion('tbl_automoviles.bEstado = 1');

                  $registro = $auto->dtBase();

                  echo json_encode($registro);
              }else if($action == 'listado'){

      $auto->leerCondicion('tbl_automoviles.bEstado = 1 and Placas NOT IN( select Identificador from tbl_autos_dispositivos where bEstado = 1)');

                  $registro = $auto->dtBase();

                  echo json_encode($registro);
              }else if($action == 'disponibles'){

                  if($auto->consultarHistorial(' Estatus = "Disponible" and bEstado=1 ',' count(nIDAutomovil) AS Contador ', '')){

                      echo json_encode($auto->consultarHistorial(' Estatus = "Disponible" and bEstado=1 ',' count(nIDAutomovil) AS Contador ', ''));

                  }else{

                      echo "0";
                  }
              }else if($action == 'mantenimiento'){

                  if($auto->consultarHistorial(' Estatus = "Mantenimiento" and bEstado=1 ',' count(nIDAutomovil) AS Contador ', '')){

                      echo json_encode($auto->consultarHistorial(' Estatus = "Mantenimiento" and bEstado=1 ',' count(nIDAutomovil) AS Contador ', ''));

                  }else{

                      echo "0";
                  }
              }else if($action == 'ocupado'){

                  if($auto->consultarHistorial(' Estatus = "Ocupado" and bEstado=1 ',' count(nIDAutomovil) AS Contador ', '')){

                      echo json_encode($auto->consultarHistorial(' Estatus = "Ocupado" and bEstado=1 ',' count(nIDAutomovil) AS Contador ', ''));

                  }else{

                      echo "0";
                  }
              }

        case 'POST' : 

             $action = $_POST['accion'];

             if($action == 'buscar'){

                        $id = $_POST['id'];

                        $condicion =  "tbl_automoviles.nIDAutomovil = ". $id;

                        $valores = "tbl_automoviles.nIDAutomovil,tbl_clientes.nIDCliente,tbl_clientes.Nombre,";
                        $valores = $valores . "tbl_solicitud.Calle,tbl_solicitud.NoExterior,tbl_solicitud.FechaHora,tbl_solicitud.Estatus,";
                        $valores = $valores . "tbl_solicitud.KilometrajeInicio,tbl_solicitud.KilometrajeFin,tbl_solicitud.FechaFin ";

                        $inner = " INNER JOIN tbl_solicitud ON tbl_solicitud.nIDAutomovil = tbl_automoviles.nIDAutomovil";
                        $inner = $inner . " INNER JOIN tbl_clientes ON tbl_clientes.nIDCliente = tbl_solicitud.nIDCliente";

                        if($auto->consultarHistorial($condicion,$valores,$inner)){

                                  echo json_encode($auto->consultarHistorial($condicion,$valores,$inner));

                        }else{

                                  echo 'null';
                        }


             }else if($action == 'buscarGps'){

                        $placa = $_POST['placa'];

                        $condicion =  "tbl_automoviles.Placas = ". $placa;

                        $valores = " tbl_automoviles.Placas,tbl_autos_dispositivos.nIDDispositivo,tbl_dispositivos.Nombre,tbl_autos_dispositivos.Imei, ";
                        $valores = $valores . " tbl_autos_dispositivos.Celular,tbl_autos_dispositivos.Numero_Confianza, ";
                        $valores = $valores . " tbl_autos_dispositivos.FechaCreacion,tbl_autos_dispositivos.bEstado  ";

                        $inner = " INNER JOIN tbl_autos_dispositivos ON tbl_autos_dispositivos.Identificador = tbl_automoviles.Placas ";
                        $inner = $inner . " INNER JOIN tbl_dispositivos ON tbl_dispositivos.nIDDispositivo = tbl_autos_dispositivos.nIDDispositivo ";

                        if($auto->consultarHistorial($condicion,$valores,$inner)){

                                  echo json_encode($auto->consultarHistorial($condicion,$valores,$inner));

                        }else{

                                  echo 'null';
                        }


             }else if($action == 'agregar'){

                  $object = $_POST['Automovil'];

                  $data   = json_decode($object);

                  $Imagen = str_replace(' ', '+', $data->Imagen);

                  $auto->setInformacion(

                        $data->nIDAutomovil,
                        $data->Placas,
                        $data->Marca,
                        $data->Submarca,
                        $data->Version,
                        $data->Year,
                        $data->Modelo,
                        $data->Color1,
                        $data->Color2,
                        $data->NoSerie,
                        $data->NoMotor,
                        $data->NumeroPuertas,
                        $data->Estatus,
                        $data->MaxDiasRenta,
                        $Imagen,
                        '',
                        '',
                        'Nuevo automóvil - ' . $fechaLocal,
                        '1',
                        TRUE,
                        FALSE,
                        FALSE

                  );

                  if($auto->ejecutar('tbl_automoviles')){

                          echo "INSERTED";

                  }else{

                          echo "NOT INSERTED";

                  }

             }else if($action == 'modificar'){

                      $object = $_POST['Automovil'];

                      $data   = json_decode($object);

                      $Imagen = str_replace(' ', '+', $data->Imagen);

                      $auto->setInformacion(
                             $data->nIDAutomovil,
                             $data->Placas,
                             $data->Marca,
                             $data->Submarca,
                             $data->Version,
                             $data->Year,
                             $data->Modelo,
                             $data->Color1,
                             $data->Color2,
                             $data->NoSerie,
                             $data->NoMotor,
                             $data->NumeroPuertas,
                             $data->Estatus,
                             $data->MaxDiasRenta,
                             $Imagen,
                             '',
                             '',
                             'Modificación al automóvil - ' . $fechaLocal,
                             '1',
                             FALSE,
                             TRUE,
                             FALSE
                      );

                      if($auto->ejecutar('tbl_automoviles')){

                                echo "UPDATED";

                      }else{

                                echo "NOT UPDATED";

                      }

             }else if($action == 'eliminar'){

                      $id = $_POST['id'];

                      $observacion = 'Automóvil eliminado temporalmente - ' . $fechaLocal;

                      if($auto->ocultar($id,$observacion)){

                            echo "DELETED";
                      }else{

                            echo "NOT DELETED";
                      }
             }
  }

  ?>