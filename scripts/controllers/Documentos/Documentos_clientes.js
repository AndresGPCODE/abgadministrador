'use strict';


angular.module('abgpassportApp')

       .controller('DocumentosClientesCtrl',['$scope','documentclientServices','clientServices','documentServices','typedocumentServices',

       function($scope,documentclientServices,clientServices,documentServices,typedocumentServices){

        var archivo = '';

        var count = 0;

        $scope.documento = {

              nIDDocumento     : '0',
              Nombre           : '',
              nIDTipoDocumento : '0',
              Documento        : '',
              nIDCliente       : '0'

        };

        $scope.documentoEdit = {

              nIDDocumento     : '0',
              Nombre           : '',
              nIDTipoDocumento : '0',
              Documento        : '',
              nIDCliente       : '0'

        };


        $scope.clean = function(){

              $scope.documento = {

                    nIDDocumento     : '0',
                    Nombre           : '',
                    nIDTipoDocumento : '0',
                    Documento        : ''

              };

              $('#subir_modal').modal('hide');
        };

        $scope.cleanEdit = function(){

              $scope.documentoEdit = {

                    nIDDocumento     : '0',
                    Nombre           : '',
                    nIDTipoDocumento : '0',
                    Documento        : ''

              };

              $('#update_modal').modal('hide');
        }

       $scope.get = function(){

              //$scope.getTipos();     

              $scope.clientes = ''         ;

              clientServices.get().then(function(resp){

                          if(resp.data == 'null'){

                                 console.log('Not found data');

                          }else{

                                $scope.clientes = resp.data;

                          }

              },function(err){

                    console.log('error' + JSON.stringify(err));

              });
       };

       $scope.getTipos = function(id){

             $scope.tipos = '';

             typedocumentServices.getTipo(id).then(function(resp){

                                 if(resp.data == 'null'){

                                      console.log('Not found data');

                                 }else{

                                      $scope.tipos = resp.data;

                                 }

             },function(err){

                  console.log('error' + JSON.stringify(err));


             });

       };


       $scope.obtener = function(id){

            $scope.documento.nIDCliente = id;
            $scope.getTipos(id);

       };

       function ValidarExt() {

              var archivoInput = document.getElementById('Documento');

              var archivoRuta  = archivoInput.value;

              var expExtensiones = /(.PNG)|(.JPG)|(.JPEG)|(.png)|(.jpg)|(.PDF)|(.pdf) $/i;

              document.getElementById('VistaImagendocumento').style.display = 'none';

              if(!expExtensiones.test(archivoRuta)){

                    swal.fire({

                      title           : 'Oops!',

                      text            : 'Solo puedes subir imagenes con extension .png, .jpg, .jpeg',

                      imageUrl        : 'assets/img/advertencia.png',

                      showCloseButton : true,

                      confirmButtonColor: '#044BFD'

                    });

                    archivoRuta.value = '';

                    return false;

              }else{ 

                    if(archivoInput.files && archivoInput.files[0]){

                        var visor = new FileReader();

                        visor.addEventListener("load" , function(e){                                                       

                            $scope.documento.Documento = e.target.result;

                            document.getElementById("doc").src = $scope.documento.Documento;

                            document.getElementById('VistaImagendocumento').style.display = 'block';



                        });

                        visor.readAsDataURL(archivoInput.files[0]);
                    }

              }

        }(function(){

              document.getElementById('Documento').addEventListener("change", ValidarExt);
        })();

        function ValidarExtEdit() {

              var archivoInput = document.getElementById('documentoEdit');

              var archivoRuta  = archivoInput.value;

              var expExtensiones = /(.PNG)|(.JPG)|(.JPEG)|(.png)|(.PDF)|(.pdf) $/i;

              if(!expExtensiones.test(archivoRuta)){

                    swal.fire({

                      title           : 'Oops!',

                      text            : 'Solo puedes subir imagenes con extension .png, .jpg, .jpeg',

                      imageUrl        : 'assets/img/advertencia.png',

                      showCloseButton : true,

                      confirmButtonColor: '#044BFD'

                    });

                    archivoRuta.value = '';

                    return false;

              }else{ 

                    if(archivoInput.files && archivoInput.files[0]){

                        var visor = new FileReader();

                        visor.addEventListener("load" , function(e){ 
 
                            $scope.documentoEdit.Documento = e.target.result;

                        });

                        visor.readAsDataURL(archivoInput.files[0]);
                    }

              }

        }(function(){

              document.getElementById('documentoEdit').addEventListener("change", ValidarExtEdit);
        })();


      $scope.add = function(objeto){

            if(objeto.Nombre.length == 0){

                     swal.fire({

                         title                : 'Oops!',

                         text                 : 'Nombre del documento vacio completalo',

                         imageUrl             : 'assets/img/advertencia.png',

                         showCloseButton      : true,

                         colorButtonConfirm   : '#044BFD'

                     });

            }else if(objeto.Documento.length == 0){

                           swal.fire({

                               title                : 'Oops!',

                               text                 : 'Documento vacio completalo',

                               imageUrl             : 'assets/img/advertencia.png',

                               showCloseButton      : true,

                               colorButtonConfirm   : '#044BFD'

                          });

            }else if(objeto.nIDTipoDocumento == '0'){

                           swal.fire({

                               title                : 'Oops!',

                               text                 : 'Tipo de documento vacio completalo',

                               imageUrl             : 'assets/img/advertencia.png',

                               showCloseButton      : true,

                               colorButtonConfirm   : '#044BFD'

                     });

            }else{

              documentServices.add(objeto).then(function(resp){

                            if(resp.data == 'NOT INSERTED'){

                                   swal.fire({

                                       title          : 'Oops!',

                                       text           : 'No se completo el registro, Inténtalo de nuevo',

                                       imageUrl       : 'assets/img/error.png',

                                       showCloseButton: true,

                                       colorButtonConfirm   : '#044BFD'

                                   });

                            }else{

                                $scope.documento.nIDDocumento = resp.data;

                                $scope.addClienteDocumento();

                                $scope.clean();
                            }

            },function(err){

                console.log('error' + JSON.stringify(err));

            });

            }

            

      };

      $scope.addClienteDocumento = function(){

            documentclientServices.add($scope.documento).then(function(resp){

                                   if(resp.data == "INSERTED"){

                                          swal.fire({

                                              title              : 'Registro exitoso!',

                                              imageUrl           : 'assets/img/completo.png',

                                              showCloseButton    : true,

                                              colorButtonConfirm : '#044BFD'

                                          });

                                          $scope.clean();

                                          $scope.get();

                                   }else{


                                   }

            }, function(err){

                console.log('error' + JSON.stringify(err));

            });

      };

      $scope.getArchivos = function(id){

             $scope.documentosClientes = '';
             $scope.Titulo = ''

             document.getElementById('VistaImagen').style.display = 'none';

             documentclientServices.get(id).then(function(resp){ console.log(resp.data);
                
                                  if(resp.data == 'null'){

                                        console.log('Not found data');

                                        $scope.Titulo = 'No tienes documentos guardados';
                                  }else{

                                        $scope.documentosClientes = resp.data;

                                  }

             },function(err){

                    console.log('error' + JSON.stringify(err));

             });

       };



      $scope.Ver = function(Documento,id){

                  count = count + 1; console.log(count);

                  if(count == 1){

                        document.getElementById('doc').src                   = Documento;
                        document.getElementById('VistaImagen').style.display = 'block';
                  }else{
                              document.getElementById('VistaImagen').style.display = 'none';
                              document.getElementById('doc').src                   = '';  
                              count = 0;
                              $scope.getArchivos(id);
                       
                  }
                              
                                            
       };


       $scope.show = function(objeto){

              $scope.documentoEdit.nIDDocumento      = objeto.nIDDocumento;
              $scope.documentoEdit.Nombre            = objeto.Nombre;
              $scope.documentoEdit.nIDTipoDocumento  = objeto.nIDTipoDocumento;
              $scope.documentoEdit.Documento         = objeto.Documento;


       };


       $scope.update = function(objeto){ console.log(objeto);

             documentServices.update(objeto).then(function(resp){

                             if(resp.data == 'UPDATED'){

                                    swal.fire({

                                        title                    : 'Modificación exitosa!',

                                        imageUrl                 : 'assets/img/completo.png',

                                        showCloseButton          : true,

                                        confirmButtonColor       : '#044BFD'

                                    });

                                    $scope.cleanEdit();


                             }else{


                                  swal.fire({

                                      title                     : 'Oops!',

                                      text                      : 'No se logro el registro, Inténtalo de nuevo',

                                      imageUrl                  : 'assets/img/error.png',

                                      showCloseButton           : true,

                                      confirmButtonColor        : '#044BFD'

                                  });

                             }

             }, function(err){

                        console.log('error' + JSON.stringify(err));

             });

       };


       $scope.destroy = function(id,idC){

             swal.fire({

                  title                     : 'ADVERTENCIA',

                  text                      : '¿Estás seguro que deseas eliminar el registrio?',

                  imageUrl                  : 'assets/img/advertencia.png',

                  showCancelButton          : true,

                  showCloseButton           : true,

                  cancelButtonColor         : 'rgba(4,75,253,0.5)',

                  confirmButtonColor        : '#044BDF',

                  cancelButtonText          : 'Cancelar',

                  confirmButtonText         : 'Si',

             }).then((result) =>{

                    if(result.value){

                              documentServices.destroy(id).then(function(resp){

                                              if(resp.data == "DELETED"){

                                                $scope.destroyCliente(idC);

                                                      

                                              }else{

                                                      swal.fire({

                                                          title             : 'Oops!',

                                                          text              : 'No se pudo eliminar el registro inténtalo de nuevo',

                                                          imageUrl          : 'assets/img/error.png',

                                                          showCloseButton   : true,

                                                          confirmButtonColor: '#044BFD'

                                                      });

                                              }

                              },function(err){

                                  console.log('error' + JSON.stringify(err));

                              });

                    }else{

                          swal.fire({

                              title                   : 'Se cancelo la eliminación!',

                              imageUrl                : 'assets/img/error.png',

                              showCloseButton         : true,

                              confirmButtonColor      : '#044BFD'

                          });

                    }


             });

       };


       $scope.destroyCliente = function(id){

              documentclientServices.destroy(id).then(function(resp){

                                    if(resp.data == "DELETED"){
                                           swal.fire({

                                                title                 : 'Registro eliminado con exito!',

                                                imageUrl              : 'assets/img/completo.png',

                                                showCloseButton       : true,

                                                confirmButtonColor    : '#044BFD'

                                            });

                                            $scope.get();

                                    }else{

                                            swal.fire({

                                                title             : 'Oops!',

                                                text              : 'No se pudo eliminar el registro inténtalo de nuevo',

                                                imageUrl          : 'assets/img/error.png',

                                                showCloseButton   : true,

                                                confirmButtonColor: '#044BFD'

                                            });

                                    }
              },function(err){

                    console.log('error' + JSON.stringify(err));

              });

       };





       }]);