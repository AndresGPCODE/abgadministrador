'use strict';

angular.module('abgpassportApp')
       
       .service('contactServices',['$http','configService', function($http,configService){

            return {

                    get : function(id){

                        return $http.post(configService.apiUrl + 'Contacto.php','accion=contactos&id=' + JSON.stringify(id),

                          { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                        );

                    },

                    add : function(contacto){

                        return $http.post(configService.apiUrl + 'Contacto.php','accion=agregar&contacto=' + JSON.stringify(contacto),

                              { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                        );
                        
                    },

                    update : function(contacto){

                        return $http.post(configService.apiUrl + 'Contacto.php','accion=modificar&contacto=' + JSON.stringify(contacto),

                          { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                        );

                    },

                    destroy : function(id){

                            return $http.post(configService.apiUrl + 'Contacto.php','accion=eliminar&id=' + JSON.stringify(id),

                              { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                            );
                            
                    }

            };

       }]);