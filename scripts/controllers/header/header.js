'use strict';

angular.module('abgpassportApp')

       .controller('HeaderCtrl', ['$scope','sesionServices','$window','chatServices', 

        function($scope,sesionServices,$window,chatServices){

          var identificador = 0;

          var identificadorTodos = 0;

          setInterval(

            function(){

              if(identificador != 0){

                  $(".badge").on('load',$scope.contadores(identificador));

              }

              if(identificadorTodos != 0){

                  $(".badge").on('load',$scope.contadoresTodos());

              }

            },1000 * 60 * 1

            )
   
                  
                  $scope.logout = function(){

                        sesionServices.logout(sessionStorage.getItem('GUID')).then(function(resp){

                                      if(resp.data == 'UPDATED'){

                                            sessionStorage.clear();

                                            $window.location.href = './';
                                      }else{

                                        console.log('No puedes cerrar sesion');
                                      }

                        },function(err){

                              console.log('error' + JSON.stringify(err));

                        });
                  };  


                 $scope.back = function(){

                        $window.location.href = './';

                 };

                 $scope.correo = function(email){ 

                        sesionServices.get(email).then(function(resp){

                              if(resp.data[0]['nIDUsuario'] != 0){

                                     chatServices.chatsUser(resp.data[0]['nIDUsuario']).then(function(resp){

                                            if(resp.data.length != 0){

                                                  $scope.mensajes = resp.data;

                                                  $scope.Leer(resp.data[0]['nIDUsuario']);
                                                  //$scope.contador = resp.data.length; console.log(resp.data.length);

                                            }else{


                                            }

                                     },function(err){

                                          console.log('error' + JSON.stringify(err));

                                     });

                              }else{


                              }

                        },function(err){

                              console.log('error' + JSON.stringify(err));

                        });

                 };

                 $scope.correoTodos = function(){ 

                    chatServices.getChats().then(function(resp){ console.log(resp.data);
                          if(resp.data.length != 0){

                                                  $scope.mensajes = resp.data;

                                                  $scope.LeerTodos();

                                            }else{


                          }
                    },function(err){

                                          console.log('error' + JSON.stringify(err));

                                     });

                 };

                 $scope.contadores = function(id){

                        identificador = id;

                        chatServices.contadores(id).then(function(resp){

                                    if(resp.data[0]['Contador'] != 0){

                                          $scope.contador = resp.data[0]['Contador'];

                                    }else{

                                            $scope.contador = '';
                                    }
                        },function(err){

                              console.log('error' + JSON.stringify(err));

                        });
                 };

                 $scope.contadoresTodos = function(){

                        identificadorTodos = 1;

                        chatServices.contadoresTodos().then(function(resp){ console.log(resp.data);

                                    if(resp.data[0]['Contador'] != 0){

                                          $scope.contador = resp.data[0]['Contador'];

                                    }else{

                                            $scope.contador = '';
                                    }
                        },function(err){

                              console.log('error' + JSON.stringify(err));

                        });
                 };


                 $scope.Leer = function(id){

                       chatServices.Leer(id).then(function(resp){

                              $scope.contadoresTodos(id);

                       },function(err){

                            cosole.log('error' + JSON.stringify(err));

                       });
                 };

                 $scope.LeerTodos = function(){

                       chatServices.LeerTodos().then(function(resp){

                              $scope.contadoresTodos();

                       },function(err){

                            cosole.log('error' + JSON.stringify(err));

                       });
                 };


                 $scope.cambiar = function(user,client){ 

                  
                  console.log(user,client); 
                  sessionStorage.setItem('Nombre',user);
                  sessionStorage.setItem('Cliente',client);
                  sessionStorage.setItem('Inicio',"ENTRA");

                      $window.location.href = '/#!/home/Mensajes';

                 };

                 $scope.todos = function(){
                  $window.location.href = '/#!/home/Mensajes';
                 }

       }]);