<?php

    //Clase Cliente
    //Esta clase es la que controlara los datos de la empresa.

    //Herramienta externa
    include_once "../Utilerias/clHerramientas_v2011.php";

    //tbl_cliete


/**
 * 
 */
class clCliente
{
  
    //Atributos de conexion a la base de datos
    private $servidor;
    private $usuario;
    private $password;
    private $basededatos;
    private $contarDatos   = 0;

    //Constantes generales
    private $ascendente     = "Ascendente";
    private $descendente    = "Descendente";

    //Constantes para la base de datos
    private $nombreTabla    = "tbl_clientes";
    private $nombreVista    = "";
    private $id             = "nIDCliente";
    private $campoOrdenar   = "nIDCliente";
    private $ordenamiento   = "Descendente";
    private $limiteInferior = -1;
    private $limiteSuperior = -1;
    public  $last_id = 0;
    private $dt;
    private $dtGrabar;

    //Constantes de estado
    private $existeError    = FALSE;
    private $existenDatos   = FALSE;
    private $datosConexion  = FALSE;

    //Mensajes
    private $error          = "";

    //Constructor
  function __construct()
  {
    $this->Inicializacion();
  }

  /**
     *************** Función de conexión ***************
     * La función realiza la conexión a la base de datos 
     * mediante las credenciales previamente establecidas. 
     * 
     * @param [string] $servidor
     * @param [string] $usuario
     * @param [string] $password
     * @param [string] $basededatos
     * @return void
     */
    public function conexion($servidor, $usuario, $password, $basededatos)
    {
        $this->servidor     = $servidor;
        $this->usuario      = $usuario;
        $this->password     = $password;
        $this->basededatos  = $basededatos;

        $this->datosConexion    = TRUE;
    }

    /**
     ************** Función setInfomacion **************
     * La función realiza la asignación de los datos almacenados
     * en la base de datos a las respectivas variables, los datos
     * que se encuentran como parametro son obligatorios para cada
     * tabla. 
     * 
     * @param [datetime] $FechaModificacion
     * @param [datetime] $FechaCreacion
     * @param [string] $Observaciones
     * @param [int] $bEstado
     * @param [boolean] $crear
     * @param [boolean] $modificar
     * @param [boolean] $eliminar
     * @return void
     */
    public function setInformacion($nIDCliente,$Nombre,$Usuario,$Password,$RFC,$Email,$Celular,$Foto,$Calle,
        $NoExterior,$NoInterior,$Colonia,$Cp,$Pais,$NSS,$NoLicencia,$FechaVigencia_Licencia,$NoTarjeta,$FechaVigencia_Tarjeta,
        $NombreTitular,$nIDUsuario,$Token,$TipoDispositivo,$FechaModificacion,$FechaCreacion,$Observaciones,$bEstado, $crear, $modificar, $eliminar) 
    {
        $utileria = new clHerramientasv2011();
        
        $this->dtGrabar[$this->contarDatos]["nIDCliente"]                = $nIDCliente;
        $this->dtGrabar[$this->contarDatos]["Nombre"]                    = $Nombre;
        $this->dtGrabar[$this->contarDatos]["Usuario"]                   = $Usuario;
        $this->dtGrabar[$this->contarDatos]["Password"]                  = $Password;
        $this->dtGrabar[$this->contarDatos]["RFC"]                       = $RFC;
        $this->dtGrabar[$this->contarDatos]["Email"]                     = $Email;
        $this->dtGrabar[$this->contarDatos]["Celular"]                   = $Celular;
        $this->dtGrabar[$this->contarDatos]["Foto"]                      = $Foto;
        $this->dtGrabar[$this->contarDatos]["Calle"]                     = $Calle;
        $this->dtGrabar[$this->contarDatos]["NoExterior"]                = $NoExterior;
        $this->dtGrabar[$this->contarDatos]["NoInterior"]                = $NoInterior;
        $this->dtGrabar[$this->contarDatos]["Colonia"]                   = $Colonia;
        $this->dtGrabar[$this->contarDatos]["Cp"]                        = $Cp;
        $this->dtGrabar[$this->contarDatos]["Pais"]                      = $Pais;
        $this->dtGrabar[$this->contarDatos]["NSS"]                       = $NSS;
        $this->dtGrabar[$this->contarDatos]["NoLicencia"]                = $NoLicencia;
        $this->dtGrabar[$this->contarDatos]["FechaVigencia_Licencia"]    = $FechaVigencia_Licencia;
        $this->dtGrabar[$this->contarDatos]["NoTarjeta"]                 = $NoTarjeta;
        $this->dtGrabar[$this->contarDatos]["FechaVigencia_Tarjeta"]    = $FechaVigencia_Tarjeta;
        $this->dtGrabar[$this->contarDatos]["NombreTitular"]             = $NombreTitular;
        $this->dtGrabar[$this->contarDatos]["nIDUsuario"]                = $nIDUsuario;
        $this->dtGrabar[$this->contarDatos]["Token"]                     = $Token;
        $this->dtGrabar[$this->contarDatos]["TipoDispositivo"]           = $TipoDispositivo;

         /*
         ** Estos datos son obligatorios para todas las tablas
        */
        $fecha = "";

        if( strlen($FechaModificacion) > 0 )
        {
            $fecha = $utileria->ConvertirFechaYHora_General($FechaModificacion);
            $this->dtGrabar[$this->contarDatos]["FechaModificacion"] = $FechaModificacion;
        } else {
            $this->dtGrabar[$this->contarDatos]["FechaModificacion"] = "01/01/1980";
        }

        $fecha = "";

        if(strlen($FechaCreacion)>0){
            $fecha=$utileria->ConvertirFechaYHora_General($FechaCreacion);
            $this->dtGrabar[$this->contarDatos]["FechaCreacion"] = $fecha;
        } else {
            $this->dtGrabar[$this->contarDatos]["FechaCreacion"] = "01/01/1980";
        }

        $this->dtGrabar[$this->contarDatos]["Observaciones"] = $Observaciones;

        if($bEstado >= 0 && $bEstado <= 5){
            $this->dtGrabar[$this->contarDatos]["bEstado"]  = $bEstado;
            $this->dtGrabar[$this->contarDatos]["sbEstado"] = $this->vEstado($bEstado,"bEstado");
        } else {
            $this->dtGrabar[$this->contarDatos]["bEstado"]  = 1;
            $this->dtGrabar[$this->contarDatos]["sbEstado"] = "No definido";
        }

        if ($crear==TRUE){
            $this->dtGrabar[$this->contarDatos]["Crear"]    = TRUE;
            $this->dtGrabar[$this->contarDatos]["Cambiar"]  = FALSE;
            $this->dtGrabar[$this->contarDatos]["Eliminar"] = FALSE;
        } else {
            if ($modificar==TRUE){
                $this->dtGrabar[$this->contarDatos]["Crear"]    = FALSE;
                $this->dtGrabar[$this->contarDatos]["Cambiar"]  = TRUE;
                $this->dtGrabar[$this->contarDatos]["Eliminar"] = FALSE;
            } else {
                if ($eliminar==TRUE){
                    $this->dtGrabar[$this->contarDatos]["Crear"]    = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Cambiar"]  = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Eliminar"] = TRUE;
                } else {
                    $this->dtGrabar[$this->contarDatos]["Crear"]    = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Cambiar"]  = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Eliminar"] = FALSE;
                }
            }
        }


        $this->contarDatos=$this->contarDatos+1;
        $this->existenDatos=TRUE;

        unset($utileria);

        return TRUE;

    }

    /**
     ************** Función setInformacionObj **************
     * Esta función permite darle valor al objeto para despues
     * retornar datos.
     * 
     * @param [object] $obj
     * @return void
     */
    public function setInformacionObj($obj)
    {
        $registros  = 0;
        $registros  = $obj->registrosCargados();

        if($registros > 0){
            
            $i=0;

            for($i=0; $i < $registros - 1; $i = $i + 1){

                $dato = $obj->getInformacionObj();

                $this->setInformacion(
                    $dato[$i]["nIDCliente"],
                    $dato[$i]["Nombre"],
                    $dato[$i]["Usuario"],
                    $dato[$i]["Password"],
                    $dato[$i]["RFC"],                    
                    $dato[$i]["Email"],                    
                    $dato[$i]["Celular"],  
                    $dato[$i]["Foto"],                                      
                    $dato[$i]["Calle"],
                    $dato[$i]["NoExterior"],
                    $dato[$i]["NoInterior"],
                    $dato[$i]["Colonia"],
                    $dato[$i]["Cp"],
                    $dato[$i]["Pais"],
                    $dato[$i]["NSS"],
                    $dato[$i]["NoLicencia"],
                    $dato[$i]["FechaVigencia_Licencia"],
                    $dato[$i]["NoTarjeta"],
                    $dato[$i]["FechaVigencia_Tarjeta"],
                    $dato[$i]["NombreTitular"],
                    $dato[$i]["nIDUsuario"],
                    $dato[$i]["Token"],
                    $dato[$i]["TipoDispositivo"],
                    $dato[$i]["FechaModificacion"],
                    $dato[$i]["FechaCreacion"],
                    $dato[$i]["Observaciones"],
                    $dato[$i]["bEstado"],
                    $dato[$i]["Crear"],
                    $dato[$i]["Cambiar"],
                    $dato[$i]["Eliminar"]
                );
            }

        }
        return TRUE;
    }

    /**
     ************** Función getInformacionObj **************
     * Esta función graba los datos traídos de la base de datos
     * para despues agregarlos a una variable u objeto.
     * 
     * @return void
     */
    public function getInformacionObj()
    {
        return $this->dtGrabar;
    }

    /**
     ************** Función getInformacion **************
     * Esta función permite devolver el valor de las propiedades
     * que se encuentan en las variables.
     * 
     * @param [int] $numero
     * @return void
     */
    public function getInformacion($numero)
    {
        if($this->existenDatos == TRUE){
            if($numero >= 0){
                return $this->dtGrabar[$numero];
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    /**
     ************** Función getIndice **************
     *
     * @param [int] $nID
     * @return void
     */
    public function getIndice($nID)
    {
        if($this->contarDatos>0){

            $indice = -1;

            for($i=0; $i < $this->contarDatos - 1; $i = $i + 1){
                if($this->dtgrabar[$i][$this->id] == $nID){
                    $indice = $i;
                    break;
                }
            }
            return $indice;
        } else {
            return -1;
        }
    }

    /**
     ************** Función OrdenarTabla **************
     * Ordena la tabla con respecto al campo que se le
     * pasa por parametro.
     * 
     * @param [string] $campos
     * @return void
     */
    public function OrdenarTabla($campos)
    {
        $this->campoOrdenar = $campos;
    }

    /**
     ************** Función formaOrdenamiento **************
     * Ordena la tabla de forma ascendente o descendente según
     * el parametro.
     * 
     * @param [type] $forma
     * @return void
     */
    public function formaOrdenamiento($forma)
    {
        $this->ordenamiento = $forma;
    }

    /**
     ************** Función dtBase **************
     * Graba los registros de la base de datos.
     * 
     * @return void
     */
    public function dtBase()
    {
        return $this->dtGrabar;
    }

    /**
     ************** Función errorObjeto **************
     * La funcion retorna un error en caso de que este
     * exista.
     * 
     * @return void
     */
    public function errorObjeto()
    {
        if($this->existeError==TRUE){
            return $this->error;
        } else {
            return "No existe ningún mensaje de error.";
        }
    }

    /**
     ************** Función registrosCargados **************
     * Función para retornar el número de registros cargados.
     * @return void
     */
    public function registrosCargados()
    {
        return $this->contarDatos;
    }

    /**
     ************** Función formaDeOrdenamiento **************
     * Función para retornar la forma de ordenar la tabla.
     * 
     * @return void
     */
    public function formaDeOrdenamiento()
    {
        return $this->ordenamiento;
    }

    /**
     ************** Función errorDeObjeto **************
     * Función para retornar el error de un objeto en caso
     * de que este exista.
     * 
     * @return void
     */
    public function errorDeObjeto()
    {
        return $this->existeError;
    }

    /**
     ************** Función datosCargados **************
     *
     * @return void
     */
    public function datosCargados()
    {
        return $this->existenDatos;
    }

    /**
     ************** Función setlimiteInferior **************
     *
     * @param [int] $limite
     * @return void
     */
    public function setlimiteInferior($limite)
    {
        $this->limiteInferior = $limite;
    }

    /**
     ************** Función setlimiteSuperior **************
     *
     * @param [int] $limite
     * @return void
     */
    public function setlimiteSuperior($limite)
    {
        $this->limiteSuperior = $limite;
    }

    /**
     *************** Función Inicializacion ***************
     * Esta función permite inicializar atributos y contenido.
     * 
     * @return void
     */
    public function Inicializacion()
    {
        $this->InicializaAtributos();
        $this->InicializacionContenido();
    }

    /**
     *************** Función InicializaAtributos ***************
     * Inicializa los atributos.
     * 
     * @return void
     */
    private function InicializaAtributos()
    {
        $this->existeError      = FALSE;
        $this->existenDatos     = FALSE;
        $this->datosConexion    = FALSE;

        $this->error        = "No hay error";
        $this->ordenamiento = $this->ascendente;
        $this->contarDatos  = 0;
    }

    /**
     *************** Función InicializacionContenido ***************
     * Inicializa el contenido.
     * @return void
     */
    public function InicializacionContenido()
    {
        $this->contarDatos  = 0;
        $this->existeError  = FALSE;
    }

    /**
     *************** Función existeID ***************
     *
     * @param [int] $nID
     * @return void
     */
    public function existeID($nID)
    {

        // Verifica si existe algún error con la conexión
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        ="Error No tiene los datos para conectarse a la base de 
                                 datos [Cargar]";
            return FALSE;
        }

        // Abre la conexion
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT * ";
        $txtConsulta = $txtConsulta . "FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . $this->id ."=" . $nID ;

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Cargar la informacion
        if($res){
            if($registro=mysqli_fetch_array($res, MYSQLI_ASSOC)){
                return TRUE;
            } else {

                return FALSE;
            }

        } else {
            return FALSE;
        }
    }

   

    /**
     *************** Función vEstado ***************
     * Verifica el estado del registro.
     * 0 = Eliminado; 1 = Activo; 2 = Cancelado; 
     * 3 = No definido;
     * 
     * @param [string] $estado
     * @param [string] $nombreCampo
     * @return void
     */
    private function vEstado($estado, $nombreCampo)
    {
        $cadena = "";
        switch($estado){
            case 0: // Eliminado
                $cadena = "Eliminado";
                break;
            case 1: // Activo
                $cadena = "Activo";
                break;
            case 2: // Cancelado
                $cadena = "Cancelado";
                break;
            case 3: // No definido
                $cadena = "No definido";
                break;
        }
        return $cadena;
    }

    /**
     *************** Funcion especialesHTML ***************
     *
     * @param [string] $str
     * @return void
     */
    public function especialesHTML($str)
    {
        $str = mb_convert_encoding($str,  'UTF-8');
        return $str;
    }

    /**
     ***************  Función consultar *************** 
     * Realiza una consulta simple.
     * 
     * @return void
     */
    private function consultar()
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Genera la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT nIDCliente,Nombre,Usuario,Password,RFC,Email,Celular,Foto,Calle,NoExterior,NoInterior,";
        $txtConsulta = $txtConsulta . "Colonia,Cp,Pais,NSS,NoLicencia,DATE_FORMAT(FechaVigencia_Licencia, '%d-%m-%Y') AS FechaVigencia_Licencia,";
        $txtConsulta = $txtConsulta . "NoTarjeta,DATE_FORMAT(FechaVigencia_Tarjeta, '%d-%m-%Y') AS FechaVigencia_Tarjeta, NombreTitular,";
        $txtConsulta = $txtConsulta . "nIDUsuario,Token,TipoDispositivo,FechaModificacion,FechaCreacion,Observaciones,bEstado";

        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;
            //echo $txtConsulta;
        if($this->formaDeOrdenamiento()==$this->ascendente){
            $txtConsulta = $txtConsulta . " ASC";
        } else {
            $txtConsulta = $txtConsulta . " DESC";
        }

        if($this->limiteInferior>=0){
            $txtConsulta = $txtConsulta . " LIMIT " . $this->limiteInferior . "," . $this->limiteSuperior;
        }

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli, $txtConsulta);

        // Carga la informacion
        $contador = 0;
        $this->contarDatos = 0;

        if($res){

            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $nIDCliente                   = 0;
                $Nombre                       = "";
                $Usuario                      = "";
                $Password                     = "";
                $RFC                          = "";
                $Email                        = "";
                $Celular                      = "";
                $Foto                         = "";
                $Calle                        = "";
                $NoExterior                   = "";
                $NoInterior                   = "";
                $Colonia                      = "";
                $Cp                           = "";
                $Pais                         = "";
                $NSS                          = "";
                $NoLicencia                   = "";
                $FechaVigencia_Licencia       = "";
                $NoTarjeta                    = 0;
                $FechaVigencia_Tarjeta        = "";
                $NombreTitular                = "";
                $nIDUsuario                   = 0;
                $Token                        = 0;
                $TipoDispositivo              = "";
                
                // Campos base
                $FechaModificacion  = "";
                $FechaCreacion      = "";
                $Observaciones      = "";
                $bEstado            = 3;

                // Carga la información del registro leído
                if($registro['nIDCliente'] != NULL){
                    $nIDCliente = $registro['nIDCliente'];
                }
                if($registro['Nombre'] != NULL){
                    $Nombre = $registro['Nombre'];
                }
                if($registro['Usuario'] != NULL){
                    $Usuario = $registro['Usuario'];
                }
                if($registro['Password'] != NULL){
                    $Password = $registro['Password'];
                }
                if($registro['RFC'] != NULL){
                    $RFC = $registro['RFC'];
                }
                if($registro['Email'] != NULL){
                    $Email = $registro['Email'];
                }                
                if($registro['Celular'] != NULL){
                    $Celular = $registro['Celular'];
                }
                if($registro['Foto'] != NULL){
                    $Foto = $registro['Foto'];
                }
                if($registro['Calle'] != NULL){
                    $Calle = $registro['Calle'];
                }
                if($registro['NoExterior'] != NULL){
                    $NoExterior = $registro['NoExterior'];
                }
                if($registro['NoInterior'] != NULL){
                    $NoInterior = $registro['NoInterior'];
                }
                if($registro['Colonia'] != NULL){
                    $Colonia = $registro['Colonia'];
                }
                if($registro['Cp'] != NULL){
                    $Cp = $registro['Cp'];
                }
                if($registro['Pais'] != NULL){
                    $Pais = $registro['Pais'];
                }
                if($registro['NSS'] != NULL){
                    $NSS = $registro['NSS'];
                }
                if($registro['NoLicencia'] != NULL){
                    $NoLicencia = $registro['NoLicencia'];
                }
                if($registro['FechaVigencia_Licencia'] != NULL){
                    $FechaVigencia_Licencia = $registro['FechaVigencia_Licencia'];
                }
                if($registro['NoTarjeta'] != NULL){
                    $NoTarjeta = $registro['NoTarjeta'];
                }
                if($registro['FechaVigencia_Tarjeta'] != NULL){
                    $FechaVigencia_Tarjeta = $registro['FechaVigencia_Tarjeta'];
                }
                if($registro['NombreTitular'] != NULL){
                    $NombreTitular = $registro['NombreTitular'];
                }
                if($registro['nIDUsuario'] != NULL){
                    $nIDUsuario = $registro['nIDUsuario'];
                }
                if($registro['Token'] != NULL){
                    $Token = $registro['Token'];
                }
                if($registro['TipoDispositivo'] != NULL){
                    $TipoDispositivo = $registro['TipoDispositivo'];
                }


                // Campos base
                if($registro['FechaModificacion']!= NULL){
                    $FechaModificacion = $registro['FechaModificacion'];
                }

                if($registro['FechaCreacion']!= NULL){
                    $FechaCreacion = $registro['FechaCreacion'];
                }

                if($registro['Observaciones']!= NULL){
                    $Observaciones = $registro['Observaciones'];
                }

                if($registro['bEstado']!= NULL){
                    $bEstado = $registro['bEstado'];
                }

                // Carga la información en el objeto
                $this->setInformacion(
                        $nIDCliente,
                        $Nombre,
                        $Usuario,
                        $Password,
                        $RFC,                        
                        $Email,
                        $Celular,
                        $Foto,
                        $Calle,
                        $NoExterior,
                        $NoInterior,
                        $Colonia,
                        $Cp,
                        $Pais,
                        $NSS,
                        $NoLicencia,
                        $FechaVigencia_Licencia,
                        $NoTarjeta,
                        $FechaVigencia_Tarjeta,
                        $NombreTitular,
                        $nIDUsuario,
                        $Token,
                        $TipoDispositivo,
                        $FechaModificacion,
                        $FechaCreacion,
                        $Observaciones,
                        $bEstado,
                        FALSE,
                        TRUE,
                        FALSE
                );
            }

        } else {
            $this->existenDatos = FALSE;
        }
        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     ***************  Función consultarCondicion *************** 
     * Realiza una consulta con condición.
     * 
     * @param [string] $condicion
     * @return void
     */
    public function consultarCondicion($condicion)
    {

      
            // Abre la conexión
            $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

            if(mysqli_connect_errno()){
                $this->existeError  = TRUE;
                $this->error        = "Error No se puede conectar a la base de datos";
                return FALSE;
            }

            $txtConsulta = "";
            $txtConsulta = "SELECT *";
            $txtConsulta = $txtConsulta . " FROM ";
            $txtConsulta = $txtConsulta . $this->nombreTabla;
            $txtConsulta = $txtConsulta . " WHERE ";
            $txtConsulta = $txtConsulta . $condicion;
            $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

            if($this->limiteInferior >= 0){
                $txtConsulta = $txtConsulta . " LIMIT " . $this->limiteInferior . "," . $this->limiteSuperior;
            }

            // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli, $txtConsulta);

        // Carga la informacion
        $contador = 0;
        $this->contarDatos = 0;

        if($res){

            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $nIDCliente                   = 0;
                $Nombre                       = "";
                $Usuario                      = "";
                $Password                     = "";
                $RFC                          = "";
                $Email                        = "";
                $Celular                      = "";
                $Foto                         = "";
                $Calle                        = "";
                $NoExterior                   = "";
                $NoInterior                   = "";
                $Colonia                      = "";
                $Cp                           = "";
                $Pais                         = "";
                $NSS                          = "";
                $NoLicencia                   = "";
                $FechaVigencia_Licencia       = "";
                $NoTarjeta                    = 0;
                $FechaVigencia_Tarjeta        = "";
                $NombreTitular                = "";
                $nIDUsuario                   = 0;
                $Token                        = 0;
                $TipoDispositivo              = "";
                
                // Campos base
                $FechaModificacion  = "";
                $FechaCreacion      = "";
                $Observaciones      = "";
                $bEstado            = 3;

                // Carga la información del registro leído
                if($registro['nIDCliente'] != NULL){
                    $nIDCliente = $registro['nIDCliente'];
                }
                if($registro['Nombre'] != NULL){
                    $Nombre = $registro['Nombre'];
                }
                if($registro['Usuario'] != NULL){
                    $Usuario = $registro['Usuario'];
                }
                if($registro['Password'] != NULL){
                    $Password = $registro['Password'];
                }
                if($registro['RFC'] != NULL){
                    $RFC = $registro['RFC'];
                }
                if($registro['Email'] != NULL){
                    $Email = $registro['Email'];
                }                
                if($registro['Celular'] != NULL){
                    $Celular = $registro['Celular'];
                }
                if($registro['Foto'] != NULL){
                    $Foto = $registro['Foto'];
                }
                if($registro['Calle'] != NULL){
                    $Calle = $registro['Calle'];
                }
                if($registro['NoExterior'] != NULL){
                    $NoExterior = $registro['NoExterior'];
                }
                if($registro['NoInterior'] != NULL){
                    $NoInterior = $registro['NoInterior'];
                }
                if($registro['Colonia'] != NULL){
                    $Colonia = $registro['Colonia'];
                }
                if($registro['Cp'] != NULL){
                    $Cp = $registro['Cp'];
                }
                if($registro['Pais'] != NULL){
                    $Pais = $registro['Pais'];
                }
                if($registro['NSS'] != NULL){
                    $NSS = $registro['NSS'];
                }
                if($registro['NoLicencia'] != NULL){
                    $NoLicencia = $registro['NoLicencia'];
                }
                if($registro['FechaVigencia_Licencia'] != NULL){
                    $FechaVigencia_Licencia = $registro['FechaVigencia_Licencia'];
                }
                if($registro['NoTarjeta'] != NULL){
                    $NoTarjeta = $registro['NoTarjeta'];
                }
                if($registro['FechaVigencia_Tarjeta'] != NULL){
                    $FechaVigencia_Tarjeta = $registro['FechaVigencia_Tarjeta'];
                }
                if($registro['NombreTitular'] != NULL){
                    $NombreTitular = $registro['NombreTitular'];
                }
                if($registro['nIDUsuario'] != NULL){
                    $nIDUsuario = $registro['nIDUsuario'];
                }
                if($registro['Token'] != NULL){
                    $Token = $registro['Token'];
                }
                if($registro['TipoDispositivo'] != NULL){
                    $TipoDispositivo = $registro['TipoDispositivo'];
                }


                // Campos base
                if($registro['FechaModificacion']!= NULL){
                    $FechaModificacion = $registro['FechaModificacion'];
                }

                if($registro['FechaCreacion']!= NULL){
                    $FechaCreacion = $registro['FechaCreacion'];
                }

                if($registro['Observaciones']!= NULL){
                    $Observaciones = $registro['Observaciones'];
                }

                if($registro['bEstado']!= NULL){
                    $bEstado = $registro['bEstado'];
                }

                // Carga la información en el objeto
                $this->setInformacion(
                        $nIDCliente,
                        $Nombre,
                        $Usuario,
                        $Password,
                        $RFC,                        
                        $Email,
                        $Celular,
                        $Foto,
                        $Calle,
                        $NoExterior,
                        $NoInterior,
                        $Colonia,
                        $Cp,
                        $Pais,
                        $NSS,
                        $NoLicencia,
                        $FechaVigencia_Licencia,
                        $NoTarjeta,
                        $FechaVigencia_Tarjeta,
                        $NombreTitular,
                        $nIDUsuario,
                        $Token,
                        $TipoDispositivo,
                        $FechaModificacion,
                        $FechaCreacion,
                        $Observaciones,
                        $bEstado,
                        FALSE,
                        TRUE,
                        FALSE
                );
            }

        } else {
            $this->existenDatos = FALSE;
        }
        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función registrosTabla ***************
     *
     * @return void
     */
    private function registrosTabla()
    {

        // Abre la conexión
        $mysqli=mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT *";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

        if($this->formaDeOrdenamiento()==$this->ascendente){
            $txtConsulta=$txtConsulta . " ASC";
        } else {
            $txtConsulta=$txtConsulta . " DESC";
        }

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Carga la informacion
        $contador=0;

        // Cuenta los registros
        if($res){
            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $contador = $contador + 1;
            }
        }

        mysqli_close($mysqli);
        return $contador;
    }

    public function consultaGeneralEmail($tabla,$condicion)
    {
        
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT ";
        $txtConsulta = $txtConsulta . " Email ";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $tabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . $condicion;
        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        $regs = array();

        while($r = mysqli_fetch_assoc($res)){

              $regs[] = $r;

        }return $regs;

        mysqli_close($mysqli);
        
    }

    /**
     *************** Función contarRegistros ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function contarRegistros($condicion)
    {
        
        $contador = 0;

        // Abrea la conexion
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error No se puede conectar a la base de datos";
            return 0;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT count(nIDCliente) as Total";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . $condicion;

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Cargar la informacion


        //$this->contarDatos=0;
        if($res){
            if($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                if($registro['Total']!=NULL){
                    $contador=$registro['Total'];
                }
            }
        }

        mysqli_close($mysqli);
        return $contador;
    }

    /**
     *************** Función consultarID ***************
     *
     * @param [int] $nIDEmpresa
     * @return void
     */
    private function consultarID($nIDCliente)
    {

        // Abrea la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return 3;
        }

        $txtConsulta = "";
        $txtConsulta = "SELECT *";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . "nIDCliente='" . $nIDCliente . "'";

        //' Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);
        $nid = 3;

        // Cargar la informacion
        if($res){
            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $nid = 3;

                if($registro['nIDCliente']!= NULL){
                    $nid = $registro['nIDCliente'];
                    break;
                }
            }
            mysqli_close($mysqli);
            return $nid;
        } else {
            mysqli_close($mysqli);
            return 3;
        }
    }

    /**
     *************** Función borrar ***************
     *
     * @return void
     */
    private function borrar()
    {
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return 3;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "DELETE FROM " . $this->nombreTabla;
        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función borrarID ***************
     *
     * @param [int] $nID
     * @return void
     */
    private function borrarID($nID)
    {
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return 3;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "DELETE FROM " . $this->nombreTabla . " WHERE " . $this->id . "=" . $nID;
        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función borrarCondicion ***************
     *
     * @param [tstring] $condicion
     * @return void
     */
    private function borrarCondicion($condicion)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "DELETE FROM " . $this->nombreTabla . " WHERE " . $condicion;
        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función borrarTemporal ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @return void
     */
    private function borrarTemporal($nID, $observaciones)
    {
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return 3;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " bEstado= 0" . ",";

        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res=mysqli_query($mysqli,$txtInsercion);
        if($res==FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarEstado ***************
     *
     * @param [int] $nID
     * @param [int] $bEstado
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarEstado($nID, $bEstado, $observaciones)
    {
        
        // Abre la conexión
        $mysqli=mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " bEstado=" . $bEstado . ",";

        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res=mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarPassword ***************
     *
     * @param [int] $nID
     * @param [string] $password
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarPassword($nID, $password, $observaciones)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " Password='" . $this->especialesHTML($password) . "',";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res=mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }
        mysqli_close($mysqli);
        return TRUE;
    }

    
    /**
     *************** Función ActualizarNombre ***************
     * 
     * @param [int] $nID
     * @param [string] $nombre
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarNombre($nID, $nombre, $observaciones)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " Nombre=" . $this->especialesHTML($nombre) . ",";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarVIP ***************
     *
     * @param [int] $nIDEmpresa
     * @param [string] $RazonSocial
     * @param [string] $Calle
     * @param [string] $NoExterior
     * @param [string] $NoInterior
     * @param [string] $Colonia
     * @param [string] $Ciudad
     * @param [string] $Estado
     * @param [string] $Pais
     * @param [string] $Cp
     * @param [string] $RFC
     * @param [int] $NumeroDeTarjeta
     * @param [string] $Tipo
     * @param [string] $FechaVencimiento
     * @param [string] $Telefono
     * @param [string] $Email
     * @param [type] $observaciones
     * @return void
     */
    private function actualizarVIP($nIDCliente,$Nombre,$Usuario,$Password,$RFC,$Email,$Celular,$Foto,$Calle,
        $NoExterior,$NoInterior,$Colonia,$Cp,$Pais,$NSS,$NoLicencia,$FechaViegencia_Licencia,$NoTarjeta,$FechaViegencia_Tarjeta,
        $NombreTitular,$nIDUsuario,$Token,$TipoDispositivo,$observaciones)
    {

        // Abre la conexón
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion   = "UPDATE " .  $this->nombreTabla;
        $txtInsercion   = $txtInsercion . " SET ";
        $txtInsercion   = $txtInsercion . " Nombre ='" . $this->especialesHTML($Nombre) . "',";
        $txtInsercion   = $txtInsercion . " Usuario ='" . $this->especialesHTML($Usuario) . "',";
        $txtInsercion   = $txtInsercion . " Password ='" . $this->especialesHTML($Password) . "',";
        $txtInsercion   = $txtInsercion . " RFC ='" . $this->especialesHTML($RFC) . "',";
        $txtInsercion   = $txtInsercion . " Email ='" . $this->especialesHTML($Email) . "',";
        $txtInsercion   = $txtInsercion . " Celular ='" . $this->especialesHTML($Celular) . "',";
        $txtInsercion   = $txtInsercion . " Foto ='" . $this->especialesHTML($Foto) . "',";
        $txtInsercion   = $txtInsercion . " Calle ='" . $this->especialesHTML($Calle) . "',";
        $txtInsercion   = $txtInsercion . " NoExterior ='" . $this->especialesHTML($NoExterior) . "',";
        $txtInsercion   = $txtInsercion . " NoInterior ='" . $this->especialesHTML($NoInterior) . "',";
        $txtInsercion   = $txtInsercion . " Colonia ='" . $this->especialesHTML($Colonia) . "',";
        $txtInsercion   = $txtInsercion . " Cp ='" . $this->especialesHTML($Cp) . "',";
        $txtInsercion   = $txtInsercion . " Pais ='" . $this->especialesHTML($Pais) . "',";
        $txtInsercion   = $txtInsercion . " NSS ='" . $this->especialesHTML($NSS) . "',";
        $txtInsercion   = $txtInsercion . " NoLicencia ='" . $this->especialesHTML($NoLicencia) . "',";
        $txtInsercion   = $txtInsercion . " FechaViegencia_Licencia ='" . $this->especialesHTML($FechaViegencia_Licencia) . "',";
        $txtInsercion   = $txtInsercion . " NoTarjeta ='" . $this->especialesHTML($NoTarjeta) . "',";
        $txtInsercion   = $txtInsercion . " FechaViegencia_Tarjeta ='" . $this->especialesHTML($FechaViegencia_Tarjeta) . "',";
        $txtInsercion   = $txtInsercion . " NombreTitular ='" . $this->especialesHTML($NombreTitular) . "',";
        $txtInsercion   = $txtInsercion . " nIDUsuario ='" . $this->especialesHTML($nIDUsuario) . "',";
        $txtInsercion   = $txtInsercion . " Token ='" . $this->especialesHTML($Token) . "',";
        $txtInsercion   = $txtInsercion . " TipoDispositivo ='" . $this->especialesHTML($TipoDispositivo) . "',";

        

        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nIDCliente;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarNivel ***************
     *
     * @param [int] $nID
     * @param [int] $nIDNivel
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarNivel($nID, $nIDNivel, $observaciones)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion   = "UPDATE " .  $this->nombreTabla;
        $txtInsercion   = $txtInsercion . " SET ";
        $txtInsercion   = $txtInsercion . " nIDNivel=" . $nIDNivel . ",";


        $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion   = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion   = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion   = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarCodigoUnico ***************
     *
     * @param [int] $nID
     * @param [int] $codigoUnico
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarCodigoUnico($nID, $codigoUnico, $observaciones)
    {
        
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion   = "UPDATE " .  $this->nombreTabla;
        $txtInsercion   = $txtInsercion . " SET ";
        $txtInsercion   = $txtInsercion . " CodigoUnico='" . $codigoUnico . "',";


        $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion   = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion   = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion   = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarTerminos ***************
     *
     * @param [int] $nID
     * @param [string] $aceptoTerminos
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarTerminos($nID, $aceptoTerminos, $observaciones)
    {
        
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion   = "UPDATE " .  $this->nombreTabla;
        $txtInsercion   = $txtInsercion . " SET ";
        $txtInsercion   = $txtInsercion . " AceptoTerminos='" . $aceptoTerminos . "',";


        $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion   = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion   = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion   = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función grabar ***************
     *
     * @param [array] $tabla
     * @return void
     */
    private function grabar($tabla)
    {

        // Abre la conexion
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        for ($i = 0;$i<$this->contarDatos; $i++){
            if($tabla[$i]["Crear"]==TRUE){
                $txtInsercion = "INSERT INTO " . $this->nombreTabla;
                $txtInsercion = $txtInsercion . "(";

                $txtInsercion = $txtInsercion . "nIDCliente,";
                $txtInsercion = $txtInsercion . "Nombre,";
                $txtInsercion = $txtInsercion . "Usuario,";
                $txtInsercion = $txtInsercion . "Password,";
                $txtInsercion = $txtInsercion . "RFC,";
                $txtInsercion = $txtInsercion . "Email,";
                $txtInsercion = $txtInsercion . "Celular,";
                $txtInsercion = $txtInsercion . "Foto,";
                $txtInsercion = $txtInsercion . "Calle,";
                $txtInsercion = $txtInsercion . "NoExterior,";
                $txtInsercion = $txtInsercion . "NoInterior,";
                $txtInsercion = $txtInsercion . "Colonia,";
                $txtInsercion = $txtInsercion . "Cp,";
                $txtInsercion = $txtInsercion . "Pais,";
                $txtInsercion = $txtInsercion . "NSS,";
                $txtInsercion = $txtInsercion . "NoLicencia,";
                $txtInsercion = $txtInsercion . "FechaVigencia_Licencia,";
                $txtInsercion = $txtInsercion . "NoTarjeta,";
                $txtInsercion = $txtInsercion . "FechaVigencia_Tarjeta,";
                $txtInsercion = $txtInsercion . "NombreTitular,";
                $txtInsercion = $txtInsercion . "nIDUsuario,";
                $txtInsercion = $txtInsercion . "Token,";
                $txtInsercion = $txtInsercion . "TipoDispositivo,";
                $txtInsercion = $txtInsercion . "FechaModificacion,";
                $txtInsercion = $txtInsercion . "FechaCreacion,";
                $txtInsercion = $txtInsercion . "Observaciones,";
                $txtInsercion = $txtInsercion . "bEstado";


                $txtInsercion = $txtInsercion . ")";
                $txtInsercion = $txtInsercion . " VALUES";
                $txtInsercion = $txtInsercion . "(";

                $txtInsercion = $txtInsercion . $tabla[$i]["nIDCliente"] . ",";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Nombre"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Usuario"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Password"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["RFC"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Email"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Celular"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Foto"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Calle"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["NoExterior"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["NoInterior"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Colonia"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Cp"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Pais"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["NSS"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["NoLicencia"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["FechaVigencia_Licencia"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["NoTarjeta"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["FechaVigencia_Tarjeta"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["NombreTitular"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["nIDUsuario"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Token"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["TipoDispositivo"] . "',";


                // Campos bases
                $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
                $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
                $txtInsercion = $txtInsercion . "'" . $fechaLocal . "',";
                $txtInsercion = $txtInsercion . "'" . $fechaLocal . "',";

                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Observaciones"] . "',";

                $txtInsercion = $txtInsercion . $tabla[$i]["bEstado"];

                $txtInsercion = $txtInsercion . ")";

            } else {

                if($tabla[$i]["Cambiar"]==TRUE){

                $txtInsercion = "UPDATE " .  $this->nombreTabla;
                $txtInsercion = $txtInsercion . " SET ";

                   
              $txtInsercion = $txtInsercion . "Nombre ='" . $tabla[$i]["Nombre"] . "',";
              $txtInsercion = $txtInsercion . "Usuario='"       . $tabla[$i]["Usuario"] . "',";
              $txtInsercion = $txtInsercion . "Password='"  . $tabla[$i]["Password"] . "',";
              $txtInsercion = $txtInsercion . "RFC='"  . $tabla[$i]["RFC"] . "',";
              $txtInsercion = $txtInsercion . "Email='"  . $tabla[$i]["Email"] . "',";
              $txtInsercion = $txtInsercion . "Celular='"  . $tabla[$i]["Celular"] . "',";
              $txtInsercion = $txtInsercion . "Foto='"  . $tabla[$i]["Foto"] . "',";
              $txtInsercion = $txtInsercion . "Calle='"  . $tabla[$i]["Calle"] . "',";
              $txtInsercion = $txtInsercion . "NoExterior='"  . $tabla[$i]["NoExterior"] . "',";
              $txtInsercion = $txtInsercion . "NoInterior='"  . $tabla[$i]["NoInterior"] . "',";
              $txtInsercion = $txtInsercion . "Colonia='"  . $tabla[$i]["Colonia"] . "',";
              $txtInsercion = $txtInsercion . "Cp='"  . $tabla[$i]["Cp"] . "',";
              $txtInsercion = $txtInsercion . "Pais='"  . $tabla[$i]["Pais"] . "',";
              $txtInsercion = $txtInsercion . "NSS='"  . $tabla[$i]["NSS"] . "',";
              $txtInsercion = $txtInsercion . "NoLicencia='"  . $tabla[$i]["NoLicencia"] . "',";
              $txtInsercion = $txtInsercion . "FechaVigencia_Licencia='"  . $tabla[$i]["FechaVigencia_Licencia"] . "',";
              $txtInsercion = $txtInsercion . "NoTarjeta='"  . $tabla[$i]["NoTarjeta"] . "',";
              $txtInsercion = $txtInsercion . "FechaVigencia_Tarjeta='"  . $tabla[$i]["FechaVigencia_Tarjeta"] . "',";
              $txtInsercion = $txtInsercion . "NombreTitular='"  . $tabla[$i]["NombreTitular"] . "',";
              $txtInsercion = $txtInsercion . "nIDUsuario='"  . $tabla[$i]["nIDUsuario"] . "',";
              $txtInsercion = $txtInsercion . "Token='"  . $tabla[$i]["Token"] . "',";
              $txtInsercion = $txtInsercion . "TipoDispositivo='"  . $tabla[$i]["TipoDispositivo"] . "',";


                    // Basicas
                    $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
                    $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
                    $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

                    $txtInsercion = $txtInsercion . " Observaciones='". $tabla[$i]["Observaciones"] . "'";
                    $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$tabla[$i]["nIDCliente"];

                    

                } else {

                    if($tabla[$i]["Eliminar"]==TRUE){
                        $txtInsercion = "DELETE FROM " . $this->nombreTabla . " WHERE " . $this->id . "=" .$tabla[$i]["nIDCliente"];

                    }
                }

            }

           //echo $txtInsercion;

            mysqli_query($mysqli, "SET NAMES UTF8");
            $res=mysqli_query($mysqli,$txtInsercion);

           
            $this->last_id = mysqli_insert_id($mysqli);
          
            if($res==FALSE){
                mysqli_close($mysqli);
                return FALSE;
            }

        }
        mysqli_close($mysqli);
        return TRUE;
    }

    public function mostrarID(){

           // Abre la conexion
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        return $this->last_id;

        mysqli_close($mysqli);


    }

    /**
     *************** Función limpiarTabla ***************
     *
     * @return void
     */
    public function limpiarTabla()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->borrar() == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función eliminarPorID ***************
     *
     * @param [int] $nID
     * @return void
     */
    public function eliminarPorID($nID)
    {

        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->borrarID($nID) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función eliminarConCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function eliminarConCondicion($condicion)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if(strlen($condicion)<=0){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene condicion";
            return FALSE;
        }

        if ($this->borrarCondicion($condicion) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función ocultar ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @return void
     */
    public function ocultar($nID, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->borrarTemporal($nID,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarEstado ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @param [int] $bEstado
     * @return void
     */
    public function cambiarEstado($nID, $observaciones, $bEstado)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarEstado($nID,$bEstado,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }


     /**
     *************** Función cambiarPassword ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @param [string] $password
     * @return void
     */
    public function cambiarPassword($nID, $observaciones, $password)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarPassword($nID,$password,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarNombre ***************
     *
     * @param [int] $nID
     * @param [string] $nombre
     * @param [string] $observaciones
     * @return void
     */
    public function cambiarNombre($nID, $nombre, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarNombre($nID,$nombre,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarVIP ***************
     *
     * @param [int] $nIDEmpresa
     * @param [string] $RazonSocial
     * @param [string] $Calle
     * @param [string] $NoExterior
     * @param [string] $NoInterior
     * @param [string] $Colonia
     * @param [string] $Ciudad
     * @param [string] $Estado
     * @param [string] $Pais
     * @param [string] $Cp
     * @param [string] $RFC
     * @param [string] $NumeroTarjeta
     * @param [string] $Tipo
     * @param [string] $FechaVencimiento
     * @param [string] $Telefono
     * @param [string] $Email
     * @param [string] $observaciones
     * @return void
     */
    public function cambiarVIP($nIDCliente,$Nombre,$Usuario,$Password,$RFC,$Email,$Celular,$Foto,$Calle,
        $NoExterior,$NoInterior,$Colonia,$Cp,$Pais,$NSS,$NoLicencia,$FechaViegencia_Licencia,$NoTarjeta,$FechaViegencia_Tarjeta,
        $NombreTitular,$nIDUsuario,$Token,$TipoDispositivo,$observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarVIP($nIDCliente,$Nombre,$Usuario,$Password,$RFC,$Email,$Celular,$Foto,$Calle,
        $NoExterior,$NoInterior,$Colonia,$Cp,$Pais,$NSS,$NoLicencia,$FechaViegencia_Licencia,$NoTarjeta,$FechaViegencia_Tarjeta,
        $NombreTitular,$nIDUsuario,$Token,$TipoDispositivo,$observaciones)==TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarNivel ***************
     *
     * @param [type] $nID
     * @param [type] $nIDNivel
     * @param [type] $observaciones
     * @return void
     */
    public function cambiarNivel($nID, $nIDNivel, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarNivel($nID,$nIDNivel, $observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarCodigoUnico ***************
     *
     * @param [int] $nID
     * @param [int] $codigoUnico
     * @param [string] $observaciones
     * @return void
     */
    public function cambiarCodigoUnico($nID, $codigoUnico, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarCodigoUnico($nID,$codigoUnico, $observaciones)==TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función leer ***************
     *
     * @return void
     */
    public function leer()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if($this->consultar() == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función leerCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function leerCondicion($condicion)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if(strlen($condicion) <= 0){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene Condicion";
            return FALSE;
        }

        if($this->consultarCondicion($condicion) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función totalRegistros ***************
     *
     * @return void
     */
    public function totalRegistros()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError = TRUE;
            $this->error       = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->registrosTabla();
    }

    /**
     *************** Función numeroTotalRegistrosCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function numeroTotalRegistrosCondicion($condicion)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->contarRegistrosCondicion($condicion);
    }

    /**
     *************** Función getEstado ***************
     *
     * @param [int] $nID
     * @return void
     */
    public function getEstado($nID)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->ConsultarEstado($nID);
    }

    /**
     *************** Función getID ***************
     *
     * @param [int] $usuario
     * @return void
     */
    public function getID($usuario)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->consultarID($usuario);
    }

    /**
     *************** Función ejecutar ***************
     *
     * @return void
     */
    public function ejecutar()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }


        if($this->contarDatos<=0){
            return FALSE;
        }

        if($this->grabar($this->dtGrabar) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
?>