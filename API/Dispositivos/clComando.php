<?php

    //Clase Automovil
    //Esta clase es la que controlara los datos de la empresa.

    //Herramienta externa
    include_once "../Utilerias/clHerramientas_v2011.php";

    //tbl_automoviles


/**
 * 
 */
class clComando
{
  
    //Atributos de conexion a la base de datos
    private $servidor;
    private $usuario;
    private $password;
    private $basededatos;
    private $contarDatos   = 0;

    //Constantes generales
    private $ascendente     = "Ascendente";
    private $descendente    = "Descendente";

    //Constantes para la base de datos
    private $nombreTabla    = "tbl_dispositivos_comandos";
    private $nombreVista    = "";
    private $id             = "nIDDispositivoComando";
    private $campoOrdenar   = "nIDDispositivoComando";
    private $ordenamiento   = "Descendente";
    private $limiteInferior = -1;
    private $limiteSuperior = -1;
    public  $last_id = 0;
    private $dt;
    private $dtGrabar;

    //Constantes de estado
    private $existeError    = FALSE;
    private $existenDatos   = FALSE;
    private $datosConexion  = FALSE;

    //Mensajes
    private $error          = "";

    //Constructor
  function __construct()
  {
    $this->Inicializacion();
  }

  /**
     *************** Función de conexión ***************
     * La función realiza la conexión a la base de datos 
     * mediante las credenciales previamente establecidas. 
     * 
     * @param [string] $servidor
     * @param [string] $usuario
     * @param [string] $password
     * @param [string] $basededatos
     * @return void
     */
    public function conexion($servidor, $usuario, $password, $basededatos)
    {
        $this->servidor     = $servidor;
        $this->usuario      = $usuario;
        $this->password     = $password;
        $this->basededatos  = $basededatos;

        $this->datosConexion    = TRUE;
    }

    /**
     ************** Función setInfomacion **************
     * La función realiza la asignación de los datos almacenados
     * en la base de datos a las respectivas variables, los datos
     * que se encuentran como parametro son obligatorios para cada
     * tabla. 
     * 
     * @param [datetime] $FechaModificacion
     * @param [datetime] $FechaCreacion
     * @param [string] $Observaciones
     * @param [int] $bEstado
     * @param [boolean] $crear
     * @param [boolean] $modificar
     * @param [boolean] $eliminar
     * @return void
     */
    public function setInformacion($nIDDispositivoComando,$nIDDispositivo,$Accion,$Comando,$Separador,$Parametro1,$Parametro2,$Parametro3,
    $Parametro4,$Parametro5,$FechaModificacion,$FechaCreacion,$Observaciones,$bEstado, $crear, $modificar, $eliminar) 
    {
        $utileria = new clHerramientasv2011();
        
        $this->dtGrabar[$this->contarDatos]["nIDDispositivoComando"]   = $nIDDispositivoComando;
        $this->dtGrabar[$this->contarDatos]["nIDDispositivo"]          = $nIDDispositivo;
        $this->dtGrabar[$this->contarDatos]["Accion"]                  = $Accion;
        $this->dtGrabar[$this->contarDatos]["Comando"]                 = $Comando;
        $this->dtGrabar[$this->contarDatos]["Separador"]               = $Separador;
        $this->dtGrabar[$this->contarDatos]["Parametro1"]              = $Parametro1;
        $this->dtGrabar[$this->contarDatos]["Parametro2"]              = $Parametro2;
        $this->dtGrabar[$this->contarDatos]["Parametro3"]              = $Parametro3;
        $this->dtGrabar[$this->contarDatos]["Parametro4"]              = $Parametro4;
        $this->dtGrabar[$this->contarDatos]["Parametro5"]              = $Parametro5;



         /*
         ** Estos datos son obligatorios para todas las tablas
        */
        $fecha = "";

        if( strlen($FechaModificacion) > 0 )
        {
            $fecha = $utileria->ConvertirFechaYHora_General($FechaModificacion);
            $this->dtGrabar[$this->contarDatos]["FechaModificacion"] = $FechaModificacion;
        } else {
            $this->dtGrabar[$this->contarDatos]["FechaModificacion"] = "01/01/1980";
        }

        $fecha = "";

        if(strlen($FechaCreacion)>0){
            $fecha=$utileria->ConvertirFechaYHora_General($FechaCreacion);
            $this->dtGrabar[$this->contarDatos]["FechaCreacion"] = $fecha;
        } else {
            $this->dtGrabar[$this->contarDatos]["FechaCreacion"] = "01/01/1980";
        }

        $this->dtGrabar[$this->contarDatos]["Observaciones"] = $Observaciones;

        if($bEstado >= 0 && $bEstado <= 5){
            $this->dtGrabar[$this->contarDatos]["bEstado"]  = $bEstado;
            $this->dtGrabar[$this->contarDatos]["sbEstado"] = $this->vEstado($bEstado,"bEstado");
        } else {
            $this->dtGrabar[$this->contarDatos]["bEstado"]  = 1;
            $this->dtGrabar[$this->contarDatos]["sbEstado"] = "No definido";
        }

        if ($crear==TRUE){
            $this->dtGrabar[$this->contarDatos]["Crear"]    = TRUE;
            $this->dtGrabar[$this->contarDatos]["Cambiar"]  = FALSE;
            $this->dtGrabar[$this->contarDatos]["Eliminar"] = FALSE;
        } else {
            if ($modificar==TRUE){
                $this->dtGrabar[$this->contarDatos]["Crear"]    = FALSE;
                $this->dtGrabar[$this->contarDatos]["Cambiar"]  = TRUE;
                $this->dtGrabar[$this->contarDatos]["Eliminar"] = FALSE;
            } else {
                if ($eliminar==TRUE){
                    $this->dtGrabar[$this->contarDatos]["Crear"]    = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Cambiar"]  = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Eliminar"] = TRUE;
                } else {
                    $this->dtGrabar[$this->contarDatos]["Crear"]    = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Cambiar"]  = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Eliminar"] = FALSE;
                }
            }
        }


        $this->contarDatos=$this->contarDatos+1;
        $this->existenDatos=TRUE;

        unset($utileria);

        return TRUE;

    }

    /**
     ************** Función setInformacionObj **************
     * Esta función permite darle valor al objeto para despues
     * retornar datos.
     * 
     * @param [object] $obj
     * @return void
     */
    public function setInformacionObj($obj)
    {
        $registros  = 0;
        $registros  = $obj->registrosCargados();

        if($registros > 0){
            
            $i=0;

            for($i=0; $i < $registros - 1; $i = $i + 1){

                $dato = $obj->getInformacionObj();

                $this->setInformacion(
                    $dato[$i]["nIDDispositivoComando"],
                    $dato[$i]["nIDDispositivo"],
                    $dato[$i]["Accion"],
                    $dato[$i]["Comando"],
                    $dato[$i]["Separador"],
                    $dato[$i]["Parametro1"],
                    $dato[$i]["Parametro2"],
                    $dato[$i]["Parametro3"],
                    $dato[$i]["Parametro4"],
                    $dato[$i]["Parametro5"],
                    $dato[$i]["FechaModificacion"],
                    $dato[$i]["FechaCreacion"],
                    $dato[$i]["Observaciones"],
                    $dato[$i]["bEstado"],
                    $dato[$i]["Crear"],
                    $dato[$i]["Cambiar"],
                    $dato[$i]["Eliminar"]
                );
            }

        }
        return TRUE;
    }

    /**
     ************** Función getInformacionObj **************
     * Esta función graba los datos traídos de la base de datos
     * para despues agregarlos a una variable u objeto.
     * 
     * @return void
     */
    public function getInformacionObj()
    {
        return $this->dtGrabar;
    }

    /**
     ************** Función getInformacion **************
     * Esta función permite devolver el valor de las propiedades
     * que se encuentan en las variables.
     * 
     * @param [int] $numero
     * @return void
     */
    public function getInformacion($numero)
    {
        if($this->existenDatos == TRUE){
            if($numero >= 0){
                return $this->dtGrabar[$numero];
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    /**
     ************** Función getIndice **************
     *
     * @param [int] $nID
     * @return void
     */
    public function getIndice($nID)
    {
        if($this->contarDatos>0){

            $indice = -1;

            for($i=0; $i < $this->contarDatos - 1; $i = $i + 1){
                if($this->dtgrabar[$i][$this->id] == $nID){
                    $indice = $i;
                    break;
                }
            }
            return $indice;
        } else {
            return -1;
        }
    }

    /**
     ************** Función OrdenarTabla **************
     * Ordena la tabla con respecto al campo que se le
     * pasa por parametro.
     * 
     * @param [string] $campos
     * @return void
     */
    public function OrdenarTabla($campos)
    {
        $this->campoOrdenar = $campos;
    }

    /**
     ************** Función formaOrdenamiento **************
     * Ordena la tabla de forma ascendente o descendente según
     * el parametro.
     * 
     * @param [type] $forma
     * @return void
     */
    public function formaOrdenamiento($forma)
    {
        $this->ordenamiento = $forma;
    }

    /**
     ************** Función dtBase **************
     * Graba los registros de la base de datos.
     * 
     * @return void
     */
    public function dtBase()
    {
        return $this->dtGrabar;
    }

    /**
     ************** Función errorObjeto **************
     * La funcion retorna un error en caso de que este
     * exista.
     * 
     * @return void
     */
    public function errorObjeto()
    {
        if($this->existeError==TRUE){
            return $this->error;
        } else {
            return "No existe ningún mensaje de error.";
        }
    }

    /**
     ************** Función registrosCargados **************
     * Función para retornar el número de registros cargados.
     * @return void
     */
    public function registrosCargados()
    {
        return $this->contarDatos;
    }

    /**
     ************** Función formaDeOrdenamiento **************
     * Función para retornar la forma de ordenar la tabla.
     * 
     * @return void
     */
    public function formaDeOrdenamiento()
    {
        return $this->ordenamiento;
    }

    /**
     ************** Función errorDeObjeto **************
     * Función para retornar el error de un objeto en caso
     * de que este exista.
     * 
     * @return void
     */
    public function errorDeObjeto()
    {
        return $this->existeError;
    }

    /**
     ************** Función datosCargados **************
     *
     * @return void
     */
    public function datosCargados()
    {
        return $this->existenDatos;
    }

    /**
     ************** Función setlimiteInferior **************
     *
     * @param [int] $limite
     * @return void
     */
    public function setlimiteInferior($limite)
    {
        $this->limiteInferior = $limite;
    }

    /**
     ************** Función setlimiteSuperior **************
     *
     * @param [int] $limite
     * @return void
     */
    public function setlimiteSuperior($limite)
    {
        $this->limiteSuperior = $limite;
    }

    /**
     *************** Función Inicializacion ***************
     * Esta función permite inicializar atributos y contenido.
     * 
     * @return void
     */
    public function Inicializacion()
    {
        $this->InicializaAtributos();
        $this->InicializacionContenido();
    }

    /**
     *************** Función InicializaAtributos ***************
     * Inicializa los atributos.
     * 
     * @return void
     */
    private function InicializaAtributos()
    {
        $this->existeError      = FALSE;
        $this->existenDatos     = FALSE;
        $this->datosConexion    = FALSE;

        $this->error        = "No hay error";
        $this->ordenamiento = $this->ascendente;
        $this->contarDatos  = 0;
    }

    /**
     *************** Función InicializacionContenido ***************
     * Inicializa el contenido.
     * @return void
     */
    public function InicializacionContenido()
    {
        $this->contarDatos  = 0;
        $this->existeError  = FALSE;
    }

    /**
     *************** Función existeID ***************
     *
     * @param [int] $nID
     * @return void
     */
    public function existeID($nID)
    {

        // Verifica si existe algún error con la conexión
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        ="Error No tiene los datos para conectarse a la base de 
                                 datos [Cargar]";
            return FALSE;
        }

        // Abre la conexion
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT * ";
        $txtConsulta = $txtConsulta . "FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . $this->id ."=" . $nID ;

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Cargar la informacion
        if($res){
            if($registro=mysqli_fetch_array($res, MYSQLI_ASSOC)){
                return TRUE;
            } else {

                return FALSE;
            }

        } else {
            return FALSE;
        }
    }

   

    /**
     *************** Función vEstado ***************
     * Verifica el estado del registro.
     * 0 = Eliminado; 1 = Activo; 2 = Cancelado; 
     * 3 = No definido;
     * 
     * @param [string] $estado
     * @param [string] $nombreCampo
     * @return void
     */
    private function vEstado($estado, $nombreCampo)
    {
        $cadena = "";
        switch($estado){
            case 0: // Eliminado
                $cadena = "Eliminado";
                break;
            case 1: // Activo
                $cadena = "Activo";
                break;
            case 2: // Cancelado
                $cadena = "Cancelado";
                break;
            case 3: // No definido
                $cadena = "No definido";
                break;
        }
        return $cadena;
    }

    /**
     *************** Funcion especialesHTML ***************
     *
     * @param [string] $str
     * @return void
     */
    public function especialesHTML($str)
    {
        $str = mb_convert_encoding($str,  'UTF-8');
        return $str;
    }

    /**
     ***************  Función consultar *************** 
     * Realiza una consulta simple.
     * 
     * @return void
     */
    private function consultar()
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Genera la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT *";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

        if($this->formaDeOrdenamiento()==$this->ascendente){
            $txtConsulta = $txtConsulta . " ASC";
        } else {
            $txtConsulta = $txtConsulta . " DESC";
        }

        if($this->limiteInferior>=0){
            $txtConsulta = $txtConsulta . " LIMIT " . $this->limiteInferior . "," . $this->limiteSuperior;
        }

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli, $txtConsulta);

        // Carga la informacion
        $contador = 0;
        $this->contarDatos = 0;

        if($res){

            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $nIDDispositivoComando     = 0;
                $nIDDispositivo            = 0;
                $Accion                    = "";
                $Comando                   = "";
                $Separador                 = "";
                $Parametro1                = "";
                $Parametro2                = "";
                $Parametro3                = "";
                $Parametro4                = "";
                $Parametro5                = "";

                // Campos base           
                $FechaModificacion  = "";     
                $FechaCreacion      = "";                
                $Observaciones      = "";
                $bEstado            = 3;

                // Carga la información del registro leído
                if($registro['nIDDispositivoComando'] != NULL){
                    $nIDDispositivoComando = $registro['nIDDispositivoComando'];
                }
                if($registro['nIDDispositivo'] != NULL){
                    $nIDDispositivo = $registro['nIDDispositivo'];
                }
                if($registro['Accion'] != NULL){
                    $Accion = $registro['Accion'];
                }
                if($registro['Comando'] != NULL){
                    $Comando = $registro['Comando'];
                }
                if($registro['Separador'] != NULL){
                    $Separador = $registro['Separador'];
                }
                if($registro['Parametro1'] != NULL){
                    $Parametro1 = $registro['Parametro1'];
                }
                if($registro['Parametro2'] != NULL){
                    $Parametro2 = $registro['Parametro2'];
                }
                if($registro['Parametro3'] != NULL){
                    $Parametro3 = $registro['Parametro3'];
                }
                if($registro['Parametro4'] != NULL){
                    $Parametro4 = $registro['Parametro4'];
                }
                if($registro['Parametro5'] != NULL){
                    $Parametro5 = $registro['Parametro5'];
                }

                // Campos base
                if($registro['FechaModificacion']!= NULL){
                    $FechaModificacion = $registro['FechaModificacion'];
                } 
                if($registro['FechaCreacion']!= NULL){
                    $FechaCreacion = $registro['FechaCreacion'];
                }
                if($registro['FechaModificacion']!= NULL){
                    $FechaModificacion = $registro['FechaModificacion'];
                }                

                if($registro['Observaciones']!= NULL){
                    $Observaciones = $registro['Observaciones'];
                }

                if($registro['bEstado']!= NULL){
                    $bEstado = $registro['bEstado'];
                }

                // Carga la información en el objeto
                $this->setInformacion(
                        $nIDDispositivoComando,
                        $nIDDispositivo,
                        $Accion,
                        $Comando,
                        $Separador,
                        $Parametro1,
                        $Parametro2,
                        $Parametro3,
                        $Parametro4,
                        $Parametro5,
                        $FechaModificacion,
                        $FechaCreacion,
                        $Observaciones,
                        $bEstado,
                        FALSE,
                        TRUE,
                        FALSE
                );
            }

        } else {
            $this->existenDatos = FALSE;
        }
        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     ***************  Función consultarCondicion *************** 
     * Realiza una consulta con condición.
     * 
     * @param [string] $condicion
     * @return void
     */
    public function consultarCondicion($condicion)
    {

      
            // Abre la conexión
            $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

            if(mysqli_connect_errno()){
                $this->existeError  = TRUE;
                $this->error        = "Error No se puede conectar a la base de datos";
                return FALSE;
            }

            $txtConsulta = "";
            $txtConsulta = "SELECT *";
            $txtConsulta = $txtConsulta . " FROM ";
            $txtConsulta = $txtConsulta . $this->nombreTabla;
            $txtConsulta = $txtConsulta . " WHERE ";
            $txtConsulta = $txtConsulta . $condicion;
            $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

            if($this->limiteInferior >= 0){
                $txtConsulta = $txtConsulta . " LIMIT " . $this->limiteInferior . "," . $this->limiteSuperior;
            }

            // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli, $txtConsulta);

        // Carga la informacion
        $contador = 0;
        $this->contarDatos = 0;

        if($res){

            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $nIDDispositivoComando     = 0;
                $nIDDispositivo            = 0;
                $Accion                    = "";
                $Comando                   = "";
                $Separador                 = "";
                $Parametro1                = "";
                $Parametro2                = "";
                $Parametro3                = "";
                $Parametro4                = "";
                $Parametro5                = "";

                // Campos base
                $FechaModificacion  = "";                
                $FechaCreacion      = "";
                $Observaciones      = "";
                $bEstado            = 3;

                // Carga la información del registro leído
                if($registro['nIDDispositivoComando'] != NULL){
                    $nIDDispositivoComando = $registro['nIDDispositivoComando'];
                }
                if($registro['nIDDispositivo'] != NULL){
                    $nIDDispositivo = $registro['nIDDispositivo'];
                }
                if($registro['Accion'] != NULL){
                    $Accion = $registro['Accion'];
                }
                if($registro['Comando'] != NULL){
                    $Comando = $registro['Comando'];
                }
                if($registro['Separador'] != NULL){
                    $Separador = $registro['Separador'];
                }
                if($registro['Parametro1'] != NULL){
                    $Parametro1 = $registro['Parametro1'];
                }
                if($registro['Parametro2'] != NULL){
                    $Parametro2 = $registro['Parametro2'];
                }
                if($registro['Parametro3'] != NULL){
                    $Parametro3 = $registro['Parametro3'];
                }
                if($registro['Parametro4'] != NULL){
                    $Parametro4 = $registro['Parametro4'];
                }
                if($registro['Parametro5'] != NULL){
                    $Parametro5 = $registro['Parametro5'];
                }

                // Campos base

                if($registro['FechaModificacion']!= NULL){
                    $FechaModificacion = $registro['FechaModificacion'];
                }                
                if($registro['FechaCreacion']!= NULL){
                    $FechaCreacion = $registro['FechaCreacion'];
                }
                if($registro['Observaciones']!= NULL){
                    $Observaciones = $registro['Observaciones'];
                }

                if($registro['bEstado']!= NULL){
                    $bEstado = $registro['bEstado'];
                }

                // Carga la información en el objeto
                $this->setInformacion(
                        $nIDDispositivoComando,
                        $nIDDispositivo,
                        $Accion,
                        $Comando,
                        $Separador,
                        $Parametro1,
                        $Parametro2,
                        $Parametro3,
                        $Parametro4,
                        $Parametro5,
                        $FechaModificacion,
                        $FechaCreacion,
                        $Observaciones,
                        $bEstado,
                        FALSE,
                        TRUE,
                        FALSE
                );
            }

        } else {
            $this->existenDatos = FALSE;
        }
        mysqli_close($mysqli);
        return TRUE;
    }

    public function buscarComando($nIDDispositivoAuto,$Accion){

        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){

            $this->existeError = TRUE;
            $this->error       = "Error no se puede conecta a la base de datos";
            return FALSE;
        }

        $txtConsulta = "";
        $txtConsulta = " SELECT ";
        $txtConsulta = $txtConsulta . " tbl_dispositivos_comandos.nIDDispositivoComando,tbl_autos_dispositivos.nIDDispositivoAuto,";
        $txtConsulta = $txtConsulta . "tbl_dispositivos_comandos.nIDDispositivo,tbl_dispositivos_comandos.Accion,";
        $txtConsulta = $txtConsulta . "tbl_dispositivos_comandos.Comando,tbl_dispositivos_comandos.Separador,tbl_dispositivos_comandos.Parametro1,";
        $txtConsulta = $txtConsulta . "tbl_dispositivos_comandos.Parametro2,tbl_dispositivos_comandos.Parametro3,";
        $txtConsulta = $txtConsulta . "tbl_dispositivos_comandos.Parametro4,tbl_dispositivos_comandos.Parametro5,";
        $txtConsulta = $txtConsulta . "tbl_autos_dispositivos.Identificador,tbl_autos_dispositivos.Imei,";
        $txtConsulta = $txtConsulta . "tbl_autos_dispositivos.Lada,tbl_autos_dispositivos.Password,";
        $txtConsulta = $txtConsulta . "tbl_autos_dispositivos.Numero_Confianza,tbl_autos_dispositivos.Celular,";
        $txtConsulta = $txtConsulta . "tbl_dispositivos.Nombre,tbl_dispositivos.Tipo"; 
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " INNER JOIN tbl_autos_dispositivos ON tbl_autos_dispositivos.nIDDispositivo = tbl_dispositivos_comandos.nIDDispositivo ";
        $txtConsulta = $txtConsulta . " INNER JOIN tbl_dispositivos ON tbl_dispositivos.nIDDispositivo = tbl_dispositivos_comandos.nIDDispositivo ";
        $txtConsulta = $txtConsulta . " WHERE tbl_autos_dispositivos.nIDDispositivoAuto = ".$nIDDispositivoAuto." and tbl_dispositivos_comandos.Accion = '".$Accion. "'";

        //echo $txtConsulta;

        mysqli_query($mysqli,"SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        $regs = array();

        while($r = mysqli_fetch_assoc($res)){

            $regs[] = $r;

        }return $regs;

    }

    /**
     *************** Función registrosTabla ***************
     *
     * @return void
     */
    private function registrosTabla()
    {

        // Abre la conexión
        $mysqli=mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT *";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

        if($this->formaDeOrdenamiento()==$this->ascendente){
            $txtConsulta=$txtConsulta . " ASC";
        } else {
            $txtConsulta=$txtConsulta . " DESC";
        }

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Carga la informacion
        $contador=0;

        // Cuenta los registros
        if($res){
            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $contador = $contador + 1;
            }
        }

        mysqli_close($mysqli);
        return $contador;
    }

    /**
     *************** Función contarRegistros ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function contarRegistros($condicion)
    {
        
        $contador = 0;

        // Abrea la conexion
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error No se puede conectar a la base de datos";
            return 0;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT count(nIDDispositivoComando) as Total";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . $condicion;

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Cargar la informacion


        //$this->contarDatos=0;
        if($res){
            if($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                if($registro['Total']!=NULL){
                    $contador=$registro['Total'];
                }
            }
        }

        mysqli_close($mysqli);
        return $contador;
    }

    /**
     *************** Función consultarID ***************
     *
     * @param [int] $nIDEmpresa
     * @return void
     */
    private function consultarID($nIdComando)
    {

        // Abrea la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return 3;
        }

        $txtConsulta = "";
        $txtConsulta = "SELECT *";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . "nIDDispositivoComando ='" . $nIdComando . "'";

        //' Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);
        $nid = 3;

        // Cargar la informacion
        if($res){
            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $nid = 3;

                if($registro['nIdComando']!= NULL){
                    $nid = $registro['nIdComando'];
                    break;
                }
            }
            mysqli_close($mysqli);
            return $nid;
        } else {
            mysqli_close($mysqli);
            return 3;
        }
    }

    /**
     *************** Función borrar ***************
     *
     * @return void
     */
    private function borrar()
    {
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return 3;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "DELETE FROM " . $this->nombreTabla;
        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función borrarID ***************
     *
     * @param [int] $nID
     * @return void
     */
    private function borrarID($nID)
    {
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return 3;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "DELETE FROM " . $this->nombreTabla . " WHERE " . $this->id . "=" . $nID;
        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función borrarCondicion ***************
     *
     * @param [tstring] $condicion
     * @return void
     */
    private function borrarCondicion($condicion)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "DELETE FROM " . $this->nombreTabla . " WHERE " . $condicion;
        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función borrarTemporal ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @return void
     */
    private function borrarTemporal($nID, $observaciones)
    {
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return 3;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " bEstado= 0" . ",";

        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res=mysqli_query($mysqli,$txtInsercion);
        if($res==FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarEstado ***************
     *
     * @param [int] $nID
     * @param [int] $bEstado
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarEstado($nID, $bEstado, $observaciones)
    {
        
        // Abre la conexión
        $mysqli=mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " bEstado=" . $bEstado . ",";

        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res=mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarPassword ***************
     *
     * @param [int] $nID
     * @param [string] $password
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarPassword($nID, $password, $observaciones)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " Password='" . $this->especialesHTML($password) . "',";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res=mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }
        mysqli_close($mysqli);
        return TRUE;
    }

    
    /**
     *************** Función ActualizarNombre ***************
     * 
     * @param [int] $nID
     * @param [string] $nombre
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarNombre($nID, $nombre, $observaciones)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " Nombre=" . $this->especialesHTML($nombre) . ",";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarVIP ***************
     *
     * @param [int] $nIDEmpresa
     * @param [string] $RazonSocial
     * @param [string] $Calle
     * @param [string] $NoExterior
     * @param [string] $NoInterior
     * @param [string] $Colonia
     * @param [string] $Ciudad
     * @param [string] $Estado
     * @param [string] $Pais
     * @param [string] $Cp
     * @param [string] $RFC
     * @param [int] $NumeroDeTarjeta
     * @param [string] $Tipo
     * @param [string] $FechaVencimiento
     * @param [string] $Telefono
     * @param [string] $Email
     * @param [type] $observaciones
     * @return void
     */
    private function actualizarVIP($nIDDispositivoComando,$nIDDispositivo,$Accion,$Comando,$Separador,$Parametro1,$Parametro2,
        $Parametro3,$Parametro4,$Parametro5,$observaciones)
    {

        // Abre la conexón
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion   = "UPDATE " .  $this->nombreTabla;
        $txtInsercion   = $txtInsercion . " SET ";
        $txtInsercion   = $txtInsercion . " Accion ='" . $this->especialesHTML($Accion) . "',";
        $txtInsercion   = $txtInsercion . " Comando ='" . $this->especialesHTML($Comando) . "',";
        $txtInsercion   = $txtInsercion . " Separador ='" . $this->especialesHTML($Separador) . "',";
        $txtInsercion   = $txtInsercion . " Parametro1 = '" . $this->especialesHTML($Parametro1) . "',";
        $txtInsercion   = $txtInsercion . " Parametro2 = '" . $this->especialesHTML($Parametro2) . "',";
        $txtInsercion   = $txtInsercion . " Parametro3 = '" . $this->especialesHTML($Parametro3) . "',";
        $txtInsercion   = $txtInsercion . " Parametro4 = '" . $this->especialesHTML($Parametro4) . "',";
        $txtInsercion   = $txtInsercion . " Parametro5 = '" . $this->especialesHTML($Parametro5) . "',";


        

        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nIDDispositivoComando;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarNivel ***************
     *
     * @param [int] $nID
     * @param [int] $nIDNivel
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarNivel($nID, $nIDNivel, $observaciones)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion   = "UPDATE " .  $this->nombreTabla;
        $txtInsercion   = $txtInsercion . " SET ";
        $txtInsercion   = $txtInsercion . " nIDNivel=" . $nIDNivel . ",";


        $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion   = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion   = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion   = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarCodigoUnico ***************
     *
     * @param [int] $nID
     * @param [int] $codigoUnico
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarCodigoUnico($nID, $codigoUnico, $observaciones)
    {
        
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion   = "UPDATE " .  $this->nombreTabla;
        $txtInsercion   = $txtInsercion . " SET ";
        $txtInsercion   = $txtInsercion . " CodigoUnico='" . $codigoUnico . "',";


        $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion   = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion   = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion   = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarTerminos ***************
     *
     * @param [int] $nID
     * @param [string] $aceptoTerminos
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarTerminos($nID, $aceptoTerminos, $observaciones)
    {
        
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion   = "UPDATE " .  $this->nombreTabla;
        $txtInsercion   = $txtInsercion . " SET ";
        $txtInsercion   = $txtInsercion . " AceptoTerminos='" . $aceptoTerminos . "',";


        $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion   = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion   = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion   = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función grabar ***************
     *
     * @param [array] $tabla
     * @return void
     */
    private function grabar($tabla)
    {

        // Abre la conexion
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        for ($i = 0;$i<$this->contarDatos; $i++){
            if($tabla[$i]["Crear"]==TRUE){
                $txtInsercion = "INSERT INTO " . $this->nombreTabla;
                $txtInsercion = $txtInsercion . "(";

                $txtInsercion = $txtInsercion . "nIDDispositivoComando,";
                $txtInsercion = $txtInsercion . "nIDDispositivo,";
                $txtInsercion = $txtInsercion . "Accion,";
                $txtInsercion = $txtInsercion . "Comando,";
                $txtInsercion = $txtInsercion . "Separador,";
                $txtInsercion = $txtInsercion . "Parametro1,";
                $txtInsercion = $txtInsercion . "Parametro2,";
                $txtInsercion = $txtInsercion . "Parametro3,";
                $txtInsercion = $txtInsercion . "Parametro4,";
                $txtInsercion = $txtInsercion . "Parametro5,";
                $txtInsercion = $txtInsercion . "FechaModificacion,";
                $txtInsercion = $txtInsercion . "FechaCreacion,";                
                $txtInsercion = $txtInsercion . "Observaciones,";
                $txtInsercion = $txtInsercion . "bEstado";


                $txtInsercion = $txtInsercion . ")";
                $txtInsercion = $txtInsercion . " VALUES";
                $txtInsercion = $txtInsercion . "(";

                $txtInsercion = $txtInsercion . $tabla[$i]["nIDDispositivoComando"] . ",";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["nIDDispositivo"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Accion"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Comando"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Separador"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Parametro1"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Parametro2"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Parametro3"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Parametro4"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Parametro5"] . "',";

                // Campos bases
                $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
                $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
                $txtInsercion = $txtInsercion . "'" . $fechaLocal . "',";
                $txtInsercion = $txtInsercion . "'" . $fechaLocal . "',";

                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Observaciones"] . "',";

                $txtInsercion = $txtInsercion . $tabla[$i]["bEstado"];

                $txtInsercion = $txtInsercion . ")";

            } else {

                if($tabla[$i]["Cambiar"]==TRUE){

                $txtInsercion = "UPDATE " .  $this->nombreTabla;
                $txtInsercion = $txtInsercion . " SET ";

                   
              $txtInsercion = $txtInsercion . "Accion ='" . $tabla[$i]["Accion"] . "',";
              $txtInsercion = $txtInsercion . "Comando ='"       . $tabla[$i]["Comando"] . "',";
              $txtInsercion = $txtInsercion . "Separador ='" . $tabla[$i]["Separador"] . "',";
              $txtInsercion = $txtInsercion . "Parametro1 ='"  . $tabla[$i]["Parametro1"] . "',";
              $txtInsercion = $txtInsercion . "Parametro2 ='"  . $tabla[$i]["Parametro2"] . "',";
              $txtInsercion = $txtInsercion . "Parametro3 ='"  . $tabla[$i]["Parametro3"] . "',";
              $txtInsercion = $txtInsercion . "Parametro4 ='"  . $tabla[$i]["Parametro4"] . "',";
              $txtInsercion = $txtInsercion . "Parametro5 ='"  . $tabla[$i]["Parametro5"] . "',";


                    // Basicas
                    $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
                    $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
                    $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

                    $txtInsercion = $txtInsercion . " Observaciones='". $tabla[$i]["Observaciones"] . "'";
                    $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$tabla[$i]["nIDDispositivoComando"];

                    

                } else {

                    if($tabla[$i]["Eliminar"]==TRUE){
                        $txtInsercion = "DELETE FROM " . $this->nombreTabla . " WHERE " . $this->id . "=" .$tabla[$i]["nIdComando"];

                    }
                }

            }

           // echo $txtInsercion;

            mysqli_query($mysqli, "SET NAMES UTF8");
            $res=mysqli_query($mysqli,$txtInsercion);

           
            $this->last_id = mysqli_insert_id($mysqli);
          
            if($res==FALSE){
                mysqli_close($mysqli);
                return FALSE;
            }

        }
        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función limpiarTabla ***************
     *
     * @return void
     */
    public function limpiarTabla()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->borrar() == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función eliminarPorID ***************
     *
     * @param [int] $nID
     * @return void
     */
    public function eliminarPorID($nID)
    {

        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->borrarID($nID) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función eliminarConCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function eliminarConCondicion($condicion)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if(strlen($condicion)<=0){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene condicion";
            return FALSE;
        }

        if ($this->borrarCondicion($condicion) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función ocultar ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @return void
     */
    public function ocultar($nID, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->borrarTemporal($nID,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarEstado ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @param [int] $bEstado
     * @return void
     */
    public function cambiarEstado($nID, $observaciones, $bEstado)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarEstado($nID,$bEstado,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }


     /**
     *************** Función cambiarPassword ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @param [string] $password
     * @return void
     */
    public function cambiarPassword($nID, $observaciones, $password)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarPassword($nID,$password,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarNombre ***************
     *
     * @param [int] $nID
     * @param [string] $nombre
     * @param [string] $observaciones
     * @return void
     */
    public function cambiarNombre($nID, $nombre, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarNombre($nID,$nombre,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarVIP ***************
     *
     * @param [int] $nIDEmpresa
     * @param [string] $RazonSocial
     * @param [string] $Calle
     * @param [string] $NoExterior
     * @param [string] $NoInterior
     * @param [string] $Colonia
     * @param [string] $Ciudad
     * @param [string] $Estado
     * @param [string] $Pais
     * @param [string] $Cp
     * @param [string] $RFC
     * @param [string] $NumeroTarjeta
     * @param [string] $Tipo
     * @param [string] $FechaVencimiento
     * @param [string] $Telefono
     * @param [string] $Email
     * @param [string] $observaciones
     * @return void
     */
    public function cambiarVIP($nIDDispositivoComando,$nIDDispositivo,$Accion,$Comando,$Separador,$Parametro1,$Parametro2,
        $Parametro3,$Parametro4,$Parametro5,$observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarVIP($nIDDispositivoComando,$nIDDispositivo,$Accion,$Comando,$Separador,$Parametro1,$Parametro2,
        $Parametro3,$Parametro4,$Parametro5,$observaciones)==TRUE){ 
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarNivel ***************
     *
     * @param [type] $nID
     * @param [type] $nIDNivel
     * @param [type] $observaciones
     * @return void
     */
    public function cambiarNivel($nID, $nIDNivel, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarNivel($nID,$nIDNivel, $observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarCodigoUnico ***************
     *
     * @param [int] $nID
     * @param [int] $codigoUnico
     * @param [string] $observaciones
     * @return void
     */
    public function cambiarCodigoUnico($nID, $codigoUnico, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarCodigoUnico($nID,$codigoUnico, $observaciones)==TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función leer ***************
     *
     * @return void
     */
    public function leer()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if($this->consultar() == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función leerCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function leerCondicion($condicion)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if(strlen($condicion) <= 0){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene Condicion";
            return FALSE;
        }

        if($this->consultarCondicion($condicion) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función totalRegistros ***************
     *
     * @return void
     */
    public function totalRegistros()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError = TRUE;
            $this->error       = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->registrosTabla();
    }

    /**
     *************** Función numeroTotalRegistrosCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function numeroTotalRegistrosCondicion($condicion)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->contarRegistrosCondicion($condicion);
    }

    /**
     *************** Función getEstado ***************
     *
     * @param [int] $nID
     * @return void
     */
    public function getEstado($nID)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->ConsultarEstado($nID);
    }

    /**
     *************** Función getID ***************
     *
     * @param [int] $usuario
     * @return void
     */
    public function getID($usuario)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->consultarID($usuario);
    }

    /**
     *************** Función ejecutar ***************
     *
     * @return void
     */
    public function ejecutar()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }


        if($this->contarDatos<=0){
            return FALSE;
        }

        if($this->grabar($this->dtGrabar) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
?>