<?php

	error_reporting(E_ALL ^ E_NOTICE);
    ini_set('display_errors','1');

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Request-Methods');

      include_once '../Configuraciones/Conexion.php';
      include_once '../Dispositivos/clComando.php';
      include_once '../Utilerias/clHerramientas_v2011.php';

      
      $method = $_SERVER['REQUEST_METHOD'];

      $UtileriasDatos   = new clHerramientasv2011();
      $fechaLocal       = $UtileriasDatos->getFechaYHoraActual_General();
      $fechaLocal       = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
      
      $comando = new clComando();

      
      $comando->conexion($SERVER,$USER,$PASSWORD,$BD);

      switch($method){

            case 'GET' : 

               $nIDDispositivoAuto = $_GET['nIDDispositivoAuto'];
               $Accion = $_GET['Accion'];




                     if($comando->buscarComando($nIDDispositivoAuto,$Accion)){

                          echo json_encode($comando->buscarComando($nIDDispositivoAuto,$Accion));

                     }else{

                          echo json_encode('null');

                     }
                      
                   break;

      			  

      	    case 'POST' : 

      	    	 $action = $_POST['accion'];

      	    	 if($action == 'agregar'){

      	    	 		$object = $_POST['objeto'];

      	    	 		$data = json_decode($object);

      	    	 		$comando->setInformacion(
      	    	 			$data->nIDDispositivoComando,
      	    	 			$data->nIDDispositivo,
      	    	 			$data->Accion,
      	    	 			$data->Comando,
                    $data->Separador,
                    $data->Parametro1,
                    $data->Parametro2,
                    $data->Parametro3,
                    $data->Parametro4,
                    $data->Parametro5,
      	    	 			'',
      	    	 			'',
      	    	 			'Se ha agregado un nuevo comando al dispositivo - ' . $fechaLocal,
      	    	 			'1',
      	    	 			TRUE,
      	    	 			FALSE,
      	    	 			FALSE
      	    	 		
      	    	 		);

      	    	 		if($comando->ejecutar('tbl_dispositivos_comandos')){

      	    	 			echo "INSERTED";

      	    	 		}else{

      	    	 			    echo "NOT INSERTED";

      	    	 		}
      	    	 
      	    	 }else if($action == 'buscar'){

                              $id = $_POST['id'];


                  $comando->leerCondicion(" tbl_dispositivos_comandos.nIDDispositivo =". $id ." and  tbl_dispositivos_comandos.bEstado = 1 ");

                                    $registro = $comando->dtBase();

                                    echo json_encode($registro);

                                    break;

                  }else{

                        $request = json_decode(file_get_contents('php://input'),true);

                        $comando->setInformacion(

                                $request['nIDDispositivoComando'],
                                $request['nIDDispositivo'],
                                $request['Accion'],
                                $request['Comando'],
                                $request['Parametro1'],
                                $request['Parametro2'],
                                $request['Parametro3'],
                                $request['Parametro4'],
                                $request['Parametro5'],
                                '',
                                '',
                                'Se ha agregado un nuevo comando al dispositivo - ' . $fechaLocal,
                                '1',
                                TRUE,
                                FALSE,
                                FALSE

                          );

                          if($comando->ejecutar('tbl_dispositivos_comandos')){

                                    echo json_encode("INSERTED",JSON_UNESCAPED_UNICODE);

                          }else{

                                    echo json_encode("NOT INSERTED",JSON_UNESCAPED_UNICODE);
                          }
                  }
      }

?>