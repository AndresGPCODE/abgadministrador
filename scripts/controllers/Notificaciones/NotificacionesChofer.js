'use strict';

angular.module('abgpassportApp')
	   
	   .controller('NotificationChoferCtrl',['$scope','notificationChoferServices','choferServices','typeNotificationServices',

	   	function($scope,notificationChoferServices,choferServices,typeNotificationServices){

	   		$scope.notificacion = {

	   			  nIDNotificacionesChofer  : '0',
	   			  nIDChofer 			   : '0',
	   			  nIDTipoNotificacion	   : '0',
	   			  Encabezado	 		   : '',
	   			  Body 					   : '',
	   			  Permiso 				   : ''

	   		};

	   		$scope.clean = function(){

	   			  $scope.notificacion = {

	   			  		nIDNotificacionesChofer : '0',
	   			  		nIDChofer 			    : '0',
	   			  		nIDTipoNotificacion	 	: '0',
	   			  		Encabezado 				: '',
	   			  		Body 					: '',
	   			  		Permiso 			    : ''

	   			  };

	   			  $("add_modal").modal('hide');

	   		};

	   		$scope.get = function(){

	   			  $scope.getChofer();

	   			  $scope.tipos();

	   			  $scope.notificaciones = '';

	   			  notificationChoferServices.get().then(function(resp){ console.log(resp.data);

	   			  							if(resp.data == 'null'){

	   			  								   console.log('Not foun data');
	   			  								   $scope.Titulo = 'No tienes notificaciones registradas';
	   			  							}else{

	   			  								  $scope.notificaciones = resp.data;

	   			  							}

	   			  },function(err){

	   			  		console.log('error' + JSON.stringify(err));

	   			  });

	   		};

	   		$scope.tipos = function(){

                  typeNotificationServices.get().then(function(resp){

                                          if(resp.data == 'null'){

                                                 console.log('Not Found Data');

                                          }else{

                                                $scope.tipos = resp.data;

                                          }

                  },function(err){

                        console.log('error' + JSON.Stringify(err));

                  });

            };

	   		$scope.getChofer = function(){

	   			  choferServices.get().then(function(resp){

	   			  				if(resp.data == 'null'){

	   			  					   console.log('not foun data');

	   			  				}else{

	   			  					$scope.choferes = resp.data;

	   			  				}

	   			  },function(err){

	   			  		console.log('error' + JSON.stringify(err));

	   			  });

	   		};

	   		$scope.add = function(objeto){

	   				 if(objeto.Encabezado.length == 0){

	   				 		  swal.fire({

       				  				title 						: 'Oops!',

       				  				text 						: 'Encabezado se encuentra vacio completalo',

       				  				imageUrl 					: 'assets/img/advertencia.png',

       				  				showCloseButton 		    : true,

       				  				confirmButtonColor 		    : '#044BFD'

       				  		  });

	   				 }else if(objeto.Body.length == 0){

	   				 		  swal.fire({

       				  				title 						: 'Oops!',

       				  				text 						: 'Cuerpo de la notificación se encuentra vacio completalo',

       				  				imageUrl 					: 'assets/img/advertencia.png',

       				  				showCloseButton 		    : true,

       				  				confirmButtonColor 		    : '#044BFD'

       				  		  });

	   				 }else{

       				  notificationChoferServices.add(objeto).then(function(resp){

       				  							  if(resp.data == "INSERTED"){

       				  							  		 swal.fire({

       				  							  		 	title 						: 'Registro exitoso!',

       				  							  		 	imageUrl 					: 'assets/img/completo.png',

       				  							  		 	showCloseButton 		    : true,

       				  							  		 	confirmButtonColor 		    : '#044BFD'

       				  							  		 });

       				  							  		 $scope.get();

       				  							  		 $scope.clean();

       				  							  		 

       				  							  }else{

       				  							  		swal.fire({

       				  							  		 	title 						: 'Oops!',

       				  							  		 	text 						: 'No se completo el registro, Inténtalo de nuevo',

       				  							  		 	imageUrl 					: 'assets/img/error.png',

       				  							  		 	showCloseButton 		    : true,

       				  							  		 	confirmButtonColor 		    : '#044BFD'

       				  							  		 });

       				  							  }
       				  }, function(err){

       				  			console.log('error' + JSON.Stringify(err));

       				  });

       				}

       			};


	   }]);