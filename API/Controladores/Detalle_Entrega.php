<?php
  error_reporting(E_ALL ^ E_NOTICE);  

  ini_set('display_errors', '1');

  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Request-Methods');

  include_once '../Configuraciones/Conexion.php';
  include_once '../Cliente/clDetalleEntrega.php';
  include_once '../Utilerias/clHerramientas_v2011.php';

  $method         = $_SERVER['REQUEST_METHOD'];

  $UtileriasDatos = new clHerramientasv2011();
  $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
  $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

  $edoCuenta         = new clDetalleEntrega();

  $edoCuenta->conexion($SERVER,$USER,$PASSWORD,$BD);

  switch($method){

        case 'GET' : 

              $action = $_GET['accion'];

              if($action == 'buscar'){
                  
                  echo json_encode($edoCuenta->consultarCondicion('tbl_detalle_entrega.bEstado = 1'));
              }

       
  }

  ?>