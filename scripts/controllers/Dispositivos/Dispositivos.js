'use strict';

angular.module('abgpassportApp')

	   .controller('DispositivoCtrl',['$scope','dispositivoServices','commandServices',

	   	function($scope,dispositivoServices,commandServices){


	   		$scope.comando = {

	   			  nIDDispositivoComando	: '0',
	   			  nIDDispositivo        : '',
	   			  Accion   	    		: '',
	   			  Comando  				: '',
	   			  Parametro1 			: '',
	   			  Parametro2    		: '',
	   			  Parametro3 			: '',
	   			  Parametro4    		: '',
	   			  Parametro5 			: ''

	   		};

	   		$scope.cleanComando = function(){

	   			  $scope.comando = {

	   			  nIDDispositivoComando	: '0',
	   			  nIDDispositivo        : '',
	   			  Accion   	    		: '',
	   			  Comando  				: '',
	   			  Parametro1 			: '',
	   			  Parametro2    		: '',
	   			  Parametro3 			: '',
	   			  Parametro4    		: '',
	   			  Parametro5 			: ''

	   			  };

	   			  $('#add_commands_modal').modal('hide');

	   		}

	   		$scope.dispositivo = {

	   			  nIDDispositivo : '0',
	   			  Nombre     	 : '',
	   			  Tipo 			 : '',
	   			  Directo 		 : ''

	   		};

	   		$scope.clean = function(){

	   			  $scope.dispositivo = {

	   			  		nIDDispositivo : '0',

	   			  		Nombre 		   : '',

	   			  		Tipo 		   : '',

	   			  		Directo 	   : ''

	   			  };

	   			  $('#add_modal').modal('hide');

	   		};


	   		$scope.dispositivoEdit = {

	   			  nIDDispositivo : '0',

	   			  Nombre 		 : '',

	   			  Tipo 			 : '',

	   			  Directo 		: ''

	   		};


	   		$scope.cleanEdit = function(){

	   			  $scope.dispositivoEdit = {

	   			  		nIDDispositivo : '0',

	   			  		Nombre 		   : '',

	   			  		Tipo 		   : '',

	   			  		Directo 	   : ''

	   			  };

	   			  $('#update_modal').modal('hide');

	   		};

	   		$scope.get = function(){

	   			 $scope.mensaje = '';
	   			 $scope.dispositivos = '';

	   			  dispositivoServices.get().then(function(resp){

	   			  					 if(resp.data == 'null'){

	   			  					 		$scope.mensaje = 'No hay dispositivos registrados';

	   			  					 }else{

	   			  					 		$scope.dispositivos = resp.data;

	   			  					 }

	   			  },function(err){

	   			  		console.log('error' + JSON.stringify(err));

	   			  });

	   		};

	   		$scope.add = function(objeto){

	   			  if(objeto.Nombre.length == 0){

	   			  		    swal.fire({

	   			  		    	title 					: 'Oops!',

	   			  		    	text					: 'Nombre des dipositivo se encuentra vacio, completalo',

	   			  		    	imageUrl 				: 'assets/img/advertencia.png',

	   			  		    	showCloseButton 		: true,

	   			  		    	confirmButtonColor 		: '#044BFD'

	   			  		    });

	   			  }else if(objeto.Tipo.length == 0){

	   			  				 swal.fire({

	   			  				 	 title 							: 'Oops!',

	   			  				 	 text							: 'Tipo de dispositivo se encuentra vacio, completalo',

	   			  				 	 imageUrl 						: 'assets/img/advertencia.png',

	   			  				 	 showCloseButton 				: true,

	   			  				 	 confirmButtonColor 			: '#044BFD'

	   			  				 });

	   			  }else{

	   			  		dispositivoServices.add(objeto).then(function(resp){

	   			  						   if(resp.data == 'INSERTED'){

	   			  						   		  swal.fire({

	   			  						   		  	  title 					: 'Registro exitoso!',

	   			  						   		  	  imageUrl 					: 'assets/img/completo.png',

	   			  						   		  	  showCloseButton 		    : true,

	   			  						   		  	  confirmButtonColor 		: '#044BFD'

	   			  						   		  });

	   			  						   		  $scope.get();

	   			  						   		  $scope.clean();

	   			  						   }else{

	   			  						   		 swal.fire({

	   			  						   		 	 title 						 : 'Oops!',

	   			  						   		 	 text	 					 : 'No se completo el registro, Inténtalo de nuevo',

	   			  						   		 	 imageUrl 				     : 'assets/img/error.png',

	   			  						   		 	 showCloseButton 			 : true,

	   			  						   		 	 confirmButtonColor 	     : '#044BFD'

	   			  						   		 });

	   			  						   }
	   			  		
	   			  		},function(err){

	   			  				console.log('error' + JSON.stringify(err));

	   			  		});

	   			  }	   			  

	   		};

	   		$scope.show = function(objeto){ console.log(objeto); 



	   			  $scope.dispositivoEdit.nIDDispositivo = objeto.nIDDispositivo;

	   			  $scope.dispositivoEdit.Nombre			= objeto.Nombre;

	   			  $scope.dispositivoEdit.Tipo  			= objeto.Tipo;

	   			  if(objeto.directo == 0){

	   			  			$scope.dispositivoEdit.Directo        = false;

	   			  }else{

	   			  			$scope.dispositivoEdit.Directo        = true;
	   			  }
	   			  


	   		};


	   		$scope.update = function(objeto){

	   			  if(objeto.Nombre.length == 0){

	   			  		   swal.fire({

	   			  		   	   title 					: 'Oops!',

	   			  		   	   text 					: 'Nombre de dispositivo se encuentra vacio, completalo',

	   			  		   	   imageUrl 				: 'assets/img/advertencia.png',

	   			  		   	   showCloseButton 			: true,

	   			  		   	   confirmButtonColor 		: '#044BFD'

	   			  		   });

	   			  }else if(objeto.Tipo.length == 0){

	   			  				 swal.fire({

	   			  				 	 title 						: 'Oops!',

	   			  				 	 text 						: 'Tipo de dispositivo se encuentra vacio, completalo',

	   			  				 	 imageUrl 					: 'assets/img/advertencia.png',

	   			  				 	 showCloseButton 			: true,

	   			  				 	 confirmButtonColor 		: '#044BFD'

	   			  				 });

	   			  }else{

	   			  		dispositivoServices.update(objeto).then(function(resp){

	   			  						   if(resp.data == "UPDATED"){

	   			  						   		  swal.fire({

	   			  						   		  	  title 					: 'Modificación exitosa!',

	   			  						   		  	  imageUrl 					: 'assets/img/completo.png',

	   			  						   		  	  showCloseButton 			: true,

	   			  						   		  	  confirmButtonColor	 	: '#044BFD'

	   			  						   		  });

	   			  						   		  $scope.get();

	   			  						   		  $scope.cleanEdit();

	   			  						   }else{

	   			  						   		  swal.fire({

	   			  						   		  	  title 						: 'Oops!',

	   			  						   		  	  text 							: 'No se completo el registro, Intétalo de nuevo',

	   			  						   		  	  imageUrl 						: 'assets/img/error.png',

	   			  						   		  	  showCloseButton 				: true,

	   			  						   		  	  confirmButtonColor 			: '#044BFD'

	   			  						   		  });

	   			  						   }

	   			  		}, function(err){

	   			  				console.log('error' + JSON.stringify(err));

	   			  		});

	   			  }

	   		};

	   		$scope.destroy = function(id){

	   			  swal.fire({

	   			  	  title 					: 'ADVERTENCIA',

	   			  	  text 						: '¿Estás seguro de eliminar este registro?',

	   			  	  imageUrl 					: 'assets/img/advertencia.png',

	   			  	  showCancelButton 			: true,

	   			  	  showCloseButton 			: true,

	   			  	  cancelButtonColor 		: 'rgba(4,75,253,0.5)',

	   			  	  confirmButtonColor        : '#044BFD',

	   			  	  cancelButtonText 			: 'Cancelar',

	   			  	  confirmButtonText 		: 'Si',

	   			  }).then((result) => {

	   			  	if(result.value){

	   			  			 dispositivoServices.destroy(id).then(function(resp){

	   			  			 					if(resp.data == "DELETED"){

	   			  			 							swal.fire({

	   			  			 								title 						: 'Registro eliminado con exito!',

	   			  			 								imageUrl 					: 'assets/img/completo.png',

	   			  			 								showCloseButton 			: true,

	   			  			 								confirmButtonColor  		: '#044BFD'

	   			  			 							});

	   			  			 							$scope.get();

	   			  			 					}else{

	   			  			 							swal.fire({

	   			  			 								title 						: 'Oops!',

	   			  			 								text 						: 'No se pudo eliminar el registro inténtalo de nuevo',

	   			  			 								imageUrl 					: 'assets/img/error.png',

	   			  			 								showCloseButton 			: true,

	   			  			 								confirmButtonColor 			: '#044BFD'

	   			  			 							});

	   			  			 					}
	   			  			 
	   			  			 },function(err){

	   			  			 		console.log('error' + JSON.stringify(err));

	   			  			 });
	   			  	
	   			  	}else{

	   			  			swal.fire({

	   			  				title 					: 'Se cancelo la eliminación!',

	   			  				imageUrl 				: 'assets/img/error.png',

	   			  				showCloseButton 		: true,

	   			  				confirmButtonColor 		: '#044BFD'

	   			  			});

	   			  	}

	   			  });

	   		};



	   		/////////////////////////////////// ALTA COMANDOS //////////////////////////////////
	   		$scope.comand = function(id){

	   			  $scope.comando.nIDDispositivo = id;

	   		};

	   		$scope.add_command = function(objeto){ 

	   			  if(objeto.Accion.length == 0){

	   			  			swal.fire({

	   			  				title 				 : 'Oops!',

	   			  				text 				 : 'Acción se encuentra vacio, completalo',

	   			  				imageUrl 			 : 'assets/img/advertencia.png',

	   			  				showCloseButton 	 : true,

	   			  				confirmButtonColor   : '#044BFD'

	   			  			});

	   			  }else if (objeto.Comando.length == 0){

	   			  			swal.fire({

	   			  				title 		 			: 'Oops!',

	   			  				text 					: 'Comando se encuentra vacio, completalo',

	   			  				imageUrl 				: 'assets/img/advertencia.png',

	   			  				showCloseButton 		: true,

	   			  				confirmButtonColor 		: '#044BFD'

	   			  			});

	   			  }else{

	   			  		commandServices.add(objeto).then(function(resp){

	   			  					   if(resp.data == "INSERTED"){

	   			  					   		  swal.fire({

	   			  					   		  	  title 				: 'Registro exitoso!',

	   			  					   		  	  imageUrl 				: 'assets/img/completo.png',

	   			  					   		  	  showCloseButton 		: true,

	   			  					   		  	  confirmButtonColor    : '#044BFD'

	   			  					   		  });

	   			  					   		  $scope.get();

	   			  					   		  $scope.cleanComando();
	   			  					   
	   			  					   }else{

	   			  					   		 swal.fire({

	   			  					   		 	 title 					: 'Oops!',

	   			  					   		 	 text 					: 'No se completo el registro, Inténtalo de nuevo',

	   			  					   		 	 imageUrl 				: 'assets/img/error.png',

	   			  					   		 	 showCloseButton 		: true,

	   			  					   		 	 confirmButtonColor 	: '#044BFD'

	   			  					   		 });

	   			  					   }
	   			  		
	   			  		},function(err){

	   			  				console.log('error' + JSON.stringify(err));

	   			  		});
	   			  
	   			  }

	   		};


	   		$scope.devices_comandos = function(id){

	   			 $scope.mensajeD = '';

	   			 $scope.devicecomandos = '';

	   			  commandServices.get(id).then(function(resp){

	   			  				 if(resp.data == 'null'){

	   			  				 		console.log('Not Found Data');
	   			  				 		$scope.mensajeD = 'No tienes comandos registrados';

	   			  				 }else{

	   			  				 		$scope.devicecomandos = resp.data;

	   			  				 }

	   			  },function(err){

	   			  		console.log('error' + JSON.stringify(err));

	   			  });

	   		};




	   	}]);