'use strict';

angular.module('abgpassportApp')
       
       .service('accountstatuServices',['$http','configService',

        function($http,configService){

        	return{

        		get : function(){

        			return $http.get(configService.apiUrl + 'Detalle_Entrega.php?accion=buscar',

        				{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

        			);

        		},

        		getDetalles : function(id){ console.log(id);

        			return $http.post(configService.apiUrl + 'EstadoCuenta.php','accion=buscarDetalles&id=' + JSON.stringify(id),

        				{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

        			);

        		},

        		add : function(objeto){

        			return $http.post(configService.apiUrl + 'EstadoCuenta.php','accion=agregar&objeto=' + JSON.stringify(objeto),

        				{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

        			);
        			
        		},

                getEstado : function(id){

                    return $http.post(configService.apiUrl + 'EstadoCuenta.php','accion=buscarEstado&id=' + JSON.stringify(id),

                        { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                    );

                },

                update : function(id){

                        return $http.post(configService.apiUrl + 'EstadoCuenta.php','accion=modificar&id=' + JSON.stringify(id),

                            { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                        );
                        
                }

        	};


        }]);