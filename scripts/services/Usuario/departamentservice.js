'use strict';

angular.module('abgpassportApp')
       
       .service('departamentServices',['$http','configService',function($http,configService){

            return{

                  get : function(){

                      return $http.get(configService.apiUrl + 'Departamento.php?accion=consultar',

                        { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                      );

                  },

                  add : function(departamento){

                      return $http.post(configService.apiUrl + 'Departamento.php','accion=agregar&departamento=' + JSON.stringify(departamento),

                        { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                      );

                  },

                  update : function(departamento){ 

                      return $http.post(configService.apiUrl + 'Departamento.php','accion=modificar&departamento=' + JSON.stringify(departamento),

                        { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                      );

                  },

                  destroy : function(id){

                      return $http.post(configService.apiUrl + 'Departamento.php','accion=eliminar&id=' + JSON.stringify(id),

                        { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                      );
                      
                  }
            };

       }]);