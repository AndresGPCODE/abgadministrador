<?php
/*
------ Registro del historial de ubicaciones del cliente
*/
header('Content-Type: application/json');
date_default_timezone_set('America/Mexico_City');
$method = $_SERVER["REQUEST_METHOD"];
//$method = 'POST';
include 'conexion.php';

switch ($method) {
    case 'POST':
        $request = json_decode(file_get_contents('php://input'), true);
        //$request['nIDDispositivo'] = 8;
        if (is_null($request['nIDDispositivo'])) {
            $request['resp'] = 'Los Datos enviados están Incompletos||' . $request . " || " . file_get_contents('php://input');
            $request['status'] = false;
            echo json_encode($request);
            return json_encode($request);
        } else {
            $resp = obtenerUbicaciones($request['nIDDispositivo']);
            if ($resp[0]['status']) {
                echo json_encode($resp);
                return json_encode($resp);
            } else {
                $resp[0]['resp'] = 'No se pudo realizar el proceso';
                $resp[0]['status'] = false;
                echo json_encode($resp);
                return json_encode($resp);
            }
        }
        break;
    default:
        $resp[0]['resp'] = 'No se puede procesar la informacion';
        $resp[0]['status'] = false;
        echo json_encode($resp);
        return json_encode($resp);
}



function obtenerUbicaciones($id){
    $pdo = Conexion();
    // $select = "SELECT his.* FROM tbl_historial AS his INNER JOIN tbl_solicitud AS sol ON sol.nIDCliente = $idCliente AND his.nIDSolicitud = sol.nIDSolicitud";
    //$select = "SELECT aut.Placas as Direccion, his.* from tbl_historial as his inner join tbl_solicitud as sol inner join tbl_automoviles as aut on sol.nIDCliente = $idCliente AND his.nIDSolicitud = sol.nIDSolicitud AND aut.nIDAutomovil = sol.nIDAutomovil order by his.FechaCreacion desc Limit 1;";
    $select = "SELECT dis.Identificador as Direccion, his.* from tbl_historial as his inner join tbl_solicitud as sol inner join tbl_autos_dispositivos as dis on his.nIDDispositivoAuto = $id AND dis.nIDDispositivoAuto = $id order by his.FechaCreacion desc Limit 1;";
    $ejecutar = $pdo->prepare($select);
    $ejecutar->execute();
    $result = $ejecutar->fetchAll(PDO::FETCH_ASSOC);
    $arr = [];
    if (count($result) > 0) {
        foreach ($result as $row) {
            $row['status'] = true;
            $row['resp'] = "Registro de ubicaciones";
            array_push($arr, $row);
        }
        return $arr;
    } else {
        $row['status'] = false;
        $row['resp'] = "No hay ubicaciones regitradas";
        array_push($arr, $row);
        return $arr;
    }
}


// inge, cree que sea posible que pida el lunes y el martes de la proxima semana?