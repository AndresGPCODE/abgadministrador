<?php
    
  error_reporting(E_ALL ^ E_NOTICE);
  ini_set('display_errors','1');

  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Request-Methods');

  include_once '../Configuraciones/Conexion.php';
  include_once '../Usuario/clDepartamentos.php';
  include_once '../Utilerias/clHerramientas_v2011.php';

  $method = $_SERVER['REQUEST_METHOD'];

  $UtileriasDatos    = new clHerramientasv2011();
  $fechaLocal        = $UtileriasDatos->getFechaYHoraActual_General();
  $fechaLocal        = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

  $departament = new clDepartamentos();

  $departament->conexion($SERVER,$USER,$PASSWORD,$BD);

  switch($method){

      case 'GET' : 

            $action = $_GET['accion'];

            if($action == 'consultar'){

                      $departament->leerCondicion("tbl_cat_departamento.bEstado = 1 ");

                      $registros = $departament->dtBase();

                  echo json_encode($registros);

                  break;

            }

      case 'POST' :

            $action = $_POST['accion'];

            if($action == 'agregar'){

                      $object = $_POST['departamento'];

                      $data   = json_decode($object);

                      $departament->setInformacion(

                                  $data->nIDDepartamento, 
                                  $data->Departamento,
                                  $data->Descripcion,
                                  '',
                                  '',
                                  'Nuevo departamento - ' . $fechaLocal,
                                  '1',
                                  TRUE,
                                  FALSE,
                                  FALSE

                      );

                      if($departament->ejecutar('tbl_cat_departamento')){

                                      echo "INSERTED";

                      }else{

                                      echo "NOT INSERTED";
                      
                      }
            
            }else if($action == 'modificar'){

                      $object = $_POST['departamento'];

                      $data   = json_decode($object);

                      $departament->setInformacion(

                                  $data->nIDDepartamento, 
                                  $data->Departamento,
                                  $data->Descripcion,
                                  '',
                                  '',
                                  'Modificación al departamento - ' . $fechaLocal,
                                  '1',
                                  FALSE,
                                  TRUE,
                                  FALSE

                      );

                      if($departament->ejecutar('tbl_cat_departamento')){

                                      echo "UPDATED";

                      }else{

                                      echo "NOT UPDATED";
                      
                      }
            
            }else if($action == 'eliminar'){
                      
                      $id = $_POST['id'];

                      $observacion = 'departamento eliminado temporalmente - ' . $fechaLocal;

                      if($departament->ocultar($id,$observacion)){

                            echo "DELETED";
                      }else{

                            echo "NOT DELETED";
                      }
            
            }
  }
?>