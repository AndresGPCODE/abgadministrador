<?php
  error_reporting(E_ALL ^ E_NOTICE);  

  ini_set('display_errors', '1');

  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Request-Methods');

  include_once '../Configuraciones/Conexion.php';
  include_once '../Usuario/clChofer.php';
  include_once '../Utilerias/clHerramientas_v2011.php';

  $method         = $_SERVER['REQUEST_METHOD'];

  $UtileriasDatos = new clHerramientasv2011();
  $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
  $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

  $chofer         = new clChofer();

  $chofer->conexion($SERVER,$USER,$PASSWORD,$BD);

  switch($method){

        case 'GET' : 

              $action = $_GET['accion'];

              if($action == 'consultar'){

                  $chofer->leerCondicion(" tbl_choferes.bEstado = 1 ");

                  $registros = $chofer->dtBase();

                  echo json_encode($registros);
                  break;
              }

        case 'POST' : 

             $action = $_POST['accion'];

             if($action == 'agregar'){

                  $object = $_POST['chofer'];

                  $data   = json_decode($object);

                  $Archivo= str_replace(' ', '+', $data->Imagen);

                  $email  = str_replace('"','',$data->Email); 

                      if($chofer->consultaGeneralEmail(' tbl_choferes ',' tbl_choferes.Email ="' . $email . '" ' )){

                              echo 'EXISTE';

                      }else if($chofer->consultaGeneralEmail(' tbl_usuarios ',' tbl_usuarios.Email ="' . $email . '" ' )){

                              echo 'EXISTE';

                      }else if($chofer->consultaGeneralEmail(' tbl_clientes ',' tbl_clientes.Email ="' . $email . '" ')){

                              echo 'EXISTE';
                      
                      }else{

                  $chofer->setInformacion(

                        $data->nIDChofer,
                        $data->nIDEmpresa,
                        '',
                        $data->Nombre,
                        $data->Usuario,
                        $data->Password,
                        '',
                        $data->Direccion,
                        $data->RFC,
                        $data->Celular,
                        $data->Email,
                        $Archivo,
                        $data->Activo,
                        $data->NSS,
                        $data->NoLicencia,
                        $data->FechaVigencia_Licencia,
                        '',
                        '',
                        'Nuevo chofer - ' . $fechaLocal,
                        '1',
                        TRUE,
                        FALSE,
                        FALSE

                  );

                  if($chofer->ejecutar('tbl_choferes')){

                          echo "INSERTED";

                  }else{

                          echo "NOT INSERTED";

                  }

              }

             }else if($action == 'modificar'){

                      $object = $_POST['chofer'];

                      $data   = json_decode($object);

                      $Archivo= str_replace(' ', '+', $data->Imagen);

                      $chofer->setInformacion(
                             $data->nIDChofer,
                             $data->nIDEmpresa,
                             '',
                             $data->Nombre,
                             $data->Usuario,
                             $data->Password,
                             '',
                             $data->Direccion,
                             $data->RFC,
                             $data->Celular,
                             $data->Email,
                             $Archivo,
                             $data->Activo,
                             $data->NSS,
                             $data->NoLicencia,
                             $data->FechaVigencia_Licencia,
                             '',
                             '',
                             'Actualización al chofer - ' . $fechaLocal,
                             '1',
                              FALSE,
                              TRUE,
                              FALSE
                      );

                      if($chofer->ejecutar('tbl_choferes')){

                                echo "UPDATED";

                      }else{

                                echo "NOT UPDATED";

                      }

             }else if($action == 'eliminar'){

                      $id = $_POST['id'];

                      $observacion = 'Chofer eliminado temporalmente - ' . $fechaLocal;

                      if($chofer->ocultar($id,$observacion)){

                            echo "DELETED";
                      }else{

                            echo "NOT DELETED";
                      }
             }else if($action == 'consultarID'){

                      $id = $_POST['id'];

                      $chofer->leerCondicion(" tbl_choferes.bEstado = 1 and tbl_choferes.nIDChofer =" . $id );

                      $registros = $chofer->dtBase();

                      echo json_encode($registros);
             }
  }

  ?>