<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app = new \Slim\App;

/* TABLA USUARIOS */

// LISTA DE TODOS LOS USUARIOS ACTIVOS

$app->get('/users', function(Request $request, Response $response){

    $sql = "SELECT * FROM tbl_usuarios where bEstado = 1 ";

    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
        
          $users = $result->fetchAll(PDO::FETCH_OBJ);    
          echo json_encode($users, JSON_UNESCAPED_UNICODE);
      
      }else {
        
        echo json_encode("No existe el usuario en la base de datos.", JSON_UNESCAPED_UNICODE);
      
      }
      
      $result = null;
      $db = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

}); 

// TRAER USUARIO POR IDENTIFICADOR UNICO

$app->get('/users/{id}', function(Request $request, Response $response){
    
    $id  = $request->getAttribute('id');
    
    $sql = "SELECT * FROM tbl_usuarios WHERE nIDUsuario = $id";

    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $user = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($user, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("Don't exist user with this ID.");
      
      }
      
      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

}); 


// CREAR NUEVO USUARIO IBEROIL 
$app->post('/users/add', function(Request $request, Response $response){
    $Usuario                 = $request->getParam('Usuario');
    $Password                = $request->getParam('Password');
    $Clave                   = $request->getParam('Clave');
    $Nombre                  = $request->getParam('Nombre');
    $Direccion               = $request->getParam('Direccion');
    $RFC                     = $request->getParam('RFC');
    $Telefono                = $request->getParam('Telefono');
    $Celular                 = $request->getParam('Celular');
    $Email                   = $request->getParam('Email');
    $Imagen                  = $request->getParam('Imagen');
    $Activo                  = "SI";
    $Fecha                   = $request->getParam('Fecha');
    $nIDRol                  = $request->getParam('nIDRol');
    $nIDPuesto               = $request->getParam('nIDPuesto');
    $nIDDepartamento         = $request->getParam('nIDDepartamento');
    $Sueldo                  = $request->getParam('Sueldo');
    $Maquina                 = $request->getParam('Maquina');
    $Firma                   = $request->getParam('Firma');
    $Estatus                 = "ACTIVO";
    $nIDExtension            = $request->getParam('nIDExtension');
    $nIDEmpresa              = $request->getParam('nIDEmpresa');
    $FechaModificacion       = date('Y-m-d H:i:s');
    $FechaCreacion           = date('Y-m-d H:i:s');
    $Observaciones           = 'Se agregó un nuevo usuario - ' . date('Y-m-d H:i:s');
    $bEstado                 = 1;
    
    
   
   $sql = "INSERT INTO tbl_usuarios (Usuario, Password, Clave, Nombre, Direccion, RFC, Telefono, Celular, Email, Imagen, Activo, Fecha, nIDRol, nIDPuesto, nIDDepartamento, Sueldo, Maquina, Firma, Estatus, nIDExtension,nIDEmpresa, FechaModificacion,FechaCreacion,Observaciones,bEstado) VALUES 
           (:Usuario, :Password, :Clave, :Nombre, :Direccion, :RFC, :Telefono, :Celular, :Email, :Imagen, :Activo, :Fecha, :nIDRol, :nIDPuesto, :nIDDepartamento, :Sueldo, :Maquina, :Firma, :Estatus, :nIDExtension, :nIDEmpresa, :FechaModificacion, :FechaCreacion, :Observaciones, :bEstado)";
   try{
     $db     = new db();
     $db     = $db->connection();
     $result = $db->prepare($sql);
 
     $result->bindParam(':Usuario', $Usuario);
     $result->bindParam(':Password', $Password);
     $result->bindParam(':Clave', $Clave);
     $result->bindParam(':Nombre', $Nombre);
     $result->bindParam(':Direccion', $Direccion);
     $result->bindParam(':RFC', $RFC);
     $result->bindParam(':Telefono', $Telefono);
     $result->bindParam(':Celular', $Celular);
     $result->bindParam(':Email', $Email);
     $result->bindParam(':Imagen', $Imagen);
     $result->bindParam(':Activo', $Activo);
     $result->bindParam(':Fecha', $Fecha);
     $result->bindParam(':nIDRol', $nIDRol);
     $result->bindParam(':nIDPuesto', $nIDPuesto);
     $result->bindParam(':nIDDepartamento', $nIDDepartamento);
     $result->bindParam(':Sueldo', $Sueldo);
     $result->bindParam(':Maquina', $Maquina);
     $result->bindParam(':Firma', $Firma);
     $result->bindParam(':Estatus', $Estatus);
     $result->bindParam(':nIDExtension', $nIDExtension);
     $result->bindParam(':nIDEmpresa', $nIDEmpresa);
     $result->bindParam('FechaModificacion', $FechaModificacion);
     $result->bindParam('FechaCreacion', $FechaCreacion);
     $result->bindParam('Observaciones', $Observaciones);
     $result->bindParam('bEstado', $bEstado);
 
     $result->execute();
     echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  
 
     $result = null;
     $db     = null;

   }catch(PDOException $e){
     echo '{"error" : {"text":'.$e->getMessage().'}';
   }
}); 


// ACTUALIZAR USUARIO IBEROIL
$app->post('/users/update/{id}', function(Request $request, Response $response){
    
    $nIdUsuario              = $request->getAttribute('id');
    $Usuario                 = $request->getParam('Usuario');
    $Password                = $request->getParam('Password');
    $Nombre                  = $request->getParam('Nombre');
    $Direccion               = $request->getParam('Direccion');
    $RFC                     = $request->getParam('RFC');
    $Telefono                = $request->getParam('Telefono');
    $Celular                 = $request->getParam('Celular');
    $Email                   = $request->getParam('Email');
    $Imagen                  = $request->getParam('Imagen');
    $nIDRol                  = $request->getParam('nIDRol');
    $nIDPuesto               = $request->getParam('nIDPuesto');
    $nIDDepartamento         = $request->getParam('nIDDepartamento');
    $Estatus                 = $request->getParam('Estatus');
    $nIDEmpresa              = $request->getParam('nIDEmpresa');
    $FechaModificacion       = date('Y-m-d H:i:s');
    $Observaciones           = 'Modificación al usuario - ' . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_usuarios SET
     Usuario                 = :Usuario,
     Password                = :Password,
     Nombre                  = :Nombre,
     Direccion               = :Direccion,
     RFC                     = :RFC,
     Telefono                = :Telefono,
     Celular                 = :Celular,
     Email                   = :Email,
     Imagen                  = :Imagen,
     nIDRol                  = :nIDRol,
     nIDPuesto               = :nIDPuesto,
     nIDDepartamento         = :nIDDepartamento,
     Estatus                 = :Estatus,
     nIDEmpresa              = :nIDEmpresa,
     FechaModificacion       = :FechaModificacion,
     Observaciones           = :Observaciones

  WHERE nIDUsuario = $nIdUsuario";

  try{
  $db     = new db();
  $db     = $db->connection();
  $result = $db->prepare($sql);

  $result->bindParam(':Usuario', $Usuario);
  $result->bindParam(':Password', $Password);
  $result->bindParam(':Nombre', $Nombre);
  $result->bindParam(':Direccion', $Direccion);
  $result->bindParam(':RFC', $RFC);
  $result->bindParam(':Telefono', $Telefono);
  $result->bindParam(':Celular', $Celular);
  $result->bindParam(':Email', $Email);
  $result->bindParam(':Imagen', $Imagen);
  $result->bindParam(':nIDRol', $nIDRol);
  $result->bindParam(':nIDPuesto', $nIDPuesto);
  $result->bindParam(':nIDDepartamento', $nIDDepartamento);
  $result->bindParam(':Estatus', $Estatus);
  $result->bindParam(':nIDEmpresa', $nIDEmpresa);
  $result->bindParam(':FechaModificacion', $FechaModificacion);
  $result->bindParam(':Observaciones', $Observaciones);

  $result->execute();
  echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

  $result = null;
  $db     = null;

  }catch(PDOException $e){

  echo '{"error" : {"text":'.$e->getMessage().'}';

  }

}); 

//ELIMINACION TEMPORAL DE UN USUARIO
$app->get('/users/delete/{id}', function(Request $request, Response $response){
  
  $nIDUsuario       = $request->getAttribute('id');
  $bEstado          = 0;
  $Estatus          = "INACTIVO";
  $Activo           = "NO";
  $Observaciones    = "El usuario ha sido eliminado temporalmente - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_usuarios SET
         Activo        = :Activo,
         Estatus       = :Estatus,
         Observaciones = :Observaciones
         bEstado       = :bEstado
      WHERE nIDUsuario = $nIDUsuario";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':Activo', $Activo);
   $result->bindParam(':Estatus', $Estatus);
   $result->bindParam(':bEstado', $bEstado);
   $result->bindParam(':Observaciones', $Observaciones);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 

// ACTIVAR USUARIO
$app->get('/users/active/{id}', function(Request $request, Response $response){
  $nIDUsuario       = $request->getAttribute('id');
  $Estatus          = "ACTIVO";
  $bEstado          = 1;
  $Activo           = "SI";
  $Observaciones    = "El usuario ha sido activado nuevamente - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_usuarios SET
         Activo   = :Activo,
         Estatus  = :Estatus,
         bEstado  = :bEstado
      WHERE nIDUsuario = $nIDUsuario";
    
  try{
   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':Activo', $Activo);
   $result->bindParam(':Estatus', $Estatus);
   $result->bindParam(':bEstado', $bEstado);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){

   echo '{"error" : {"text":'.$e->getMessage().'}';

  }

}); 


//BUSCAR USUARIO POR NOMBRE
$app->get('/users/search/{search}', function(Request $request, Response $response){
  
  $search         = $request->getAttribute('search');
  
  $sql            = "SELECT * FROM tbl_usuarios WHERE Nombre LIKE '%$search%'";

  try{

    $db     = new db();
    $db     = $db->connection();
    $result = $db->query($sql);

    if ($result->rowCount() > 0){
     
      $usuario = $result->fetchAll(PDO::FETCH_OBJ);
      echo json_encode($usuario, JSON_UNESCAPED_UNICODE);
    
    }else {
    
      echo json_encode("FALSE");
    
    }
    
    $result = null;
    $db     = null;
  
  }catch(PDOException $e){
  
    echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 


// LOGIN USUARIOS
$app->post('/users/login', function(Request $request, Response $response){

   $user     = $request->getParam('usuario');
   $password = $request->getParam('password');

   $sql = "SELECT tbl_usuarios.nIDUsuario,tbl_usuarios.Nombre,tbl_usuarios.Usuario,tbl_roles.TipoDeRol,tbl_roles.nIDRol 
   FROM tbl_usuarios 
   INNER JOIN tbl_roles ON tbl_roles.nIDRol = tbl_usuarios.nIDRol 
   WHERE tbl_usuarios.Usuario = " . "'" . $user . "'" . " AND tbl_usuarios.Password = " . "'" . $password . "' AND tbl_usuarios.bEstado    = 1";

   	$sql2 = "SELECT tbl_cat_contacto_cliente.nIDContactoCliente,tbl_cat_contacto_cliente.Nombre,tbl_cat_contacto_cliente.Usuario,tbl_roles.TipoDeRol,tbl_roles.nIDRol 
   FROM tbl_cat_contacto_cliente 
   INNER JOIN tbl_roles ON tbl_roles.nIDRol = tbl_cat_contacto_cliente.nIDRol 
   WHERE tbl_cat_contacto_cliente.Usuario = " . "'" . $user . "'" . " AND tbl_cat_contacto_cliente.Password = " . "'" . $password . "' AND tbl_cat_contacto_cliente.bEstado    = 1";
   try{
     $db     = new db();
     $db     = $db->connection();
     $result = $db->query($sql);
     $result2 = $db->query($sql2);
 
     if ($result->rowCount() > 0){

      $user = $result->fetchAll(PDO::FETCH_OBJ);
      echo json_encode($user, JSON_UNESCAPED_UNICODE);

   }else {
   
      

      if($result2->rowCount() > 0){
      		$user = $result2->fetchAll(PDO::FETCH_OBJ);
      		echo json_encode($user, JSON_UNESCAPED_UNICODE);
      }else{
      	echo json_encode("FALSE");
      }
      
   
   } 
 
     $result = null;
     $db     = null;
   
   }catch(PDOException $e){
   
     echo '{"error" : {"text":'.$e->getMessage().'}';
   
   }

});

/* Login admin */

$app->get('/api/admins/login', function(Request $request, Response $response){
   $user = $request->getParam('user');
   $password = $request->getParam('password');

   $sql = "SELECT id,name,user FROM admins WHERE user = " . "'" . $user . "'" . " AND password = " . "'" . $password . "' AND status = 1";
   try{
     $db     = new db();
     $db     = $db->connection();
     $result = $db->query($sql);
 
     if ($result->rowCount() > 0){
      $user = $result->fetchAll(PDO::FETCH_OBJ);
      echo json_encode($user, JSON_UNESCAPED_UNICODE);
   }else {
      echo json_encode("Usuario incorrecto.");
   } 
 
     $result = null;
     $db     = null;
   }catch(PDOException $e){
     echo '{"error" : {"text":'.$e->getMessage().'}';
   }
});

//VALIDACIONES DE LA EXISTENCIA A LOS USUARIOS
$app->get('/validation/{user}', function(Request $request, Response $response){

    $usuario = $request->getAttribute('user');

    $sql     = "SELECT Usuario FROM tbl_usuarios WHERE Usuario = '$usuario'";
    $sql2    = "SELECT Usuario FROM tbl_cat_contacto_cliente WHERE Usuario = '$usuario'";

    try{
      $db          = new db();
      $db          = $db->connection();
      $result      = $db->query($sql);
      $result2     = $db->query($sql2);

      if($result->rowCount() > 0){

        echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);        

      }else{

          if($result2->rowCount() > 0){
             echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);
          }else{
            echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
          }
      }
      $result = null;
      $result2 = null;
      $db = null;
    }catch(PDOException $e){
      echo '{"error" : {"text":' . $e->getMessage() . '}';
    }

});

 /* EMPRESAS */

// LISTA DE TODAS LAS EMPRESAS

$app->get('/company', function(Request $request, Response $response){
    
    $sql = "SELECT * FROM tbl_empresa WHERE bEstado = 1";

    try{
      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
        
        $company = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($company, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
      
      }
      
      $result = null;
      $db = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

}); 


// CREAR NUEVA EMPRESA
$app->post('/company/add', function(Request $request, Response $response){
    $RazonSocial            = $request->getParam('RazonSocial');
    $Calle                  = $request->getParam('Calle');
    $NoExterior             = $request->getParam('NoExterior');
    $NoInterior             = $request->getParam('NoInterior');
    $Colonia                = $request->getParam('Colonia');
    $Ciudad                 = $request->getParam('Ciudad');
    $Pais                   = $request->getParam('Pais');
    $Cp                     = $request->getParam('Cp');
    $RFC                    = $request->getParam('RFC');
    $NumeroDeTarjeta        = $request->getParam('NumeroDeTarjeta');
    $Tipo                   = $request->getParam('Tipo');
    $FechaVencimiento       = $request->getParam('FechaVencimiento');
    $Telefono               = $request->getParam('Telefono');
    $Email                  = $request->getParam('Email');
    $FechaModificacion      = date('Y-m-d H:i:s');
    $FechaCreacion          = date('Y-m-d H:i:s');
    $Observaciones          = 'Se agregó una nueva empresa - ' . date('Y-m-d H:i:s');
    $bEstado                = 1;
   
   $sql = "INSERT INTO tbl_empresa (RazonSocial, Calle, NoExterior, NoInterior, Colonia, Ciudad, Pais, Cp, RFC, NumeroDeTarjeta, Tipo, FechaVencimiento,Telefono, Email,FechaModificacion,FechaCreacion,Observaciones,bEstado) VALUES 
           (:RazonSocial, :Calle, :NoExterior, :NoInterior, :Colonia, :Ciudad, :Pais, :Cp, :RFC, :NumeroDeTarjeta, :Tipo, :FechaVencimiento, :Telefono, :Email, :FechaModificacion, :FechaCreacion, :Observaciones, :bEstado)";
   try{
     $db     = new db();
     $db     = $db->connection();
     $result = $db->prepare($sql);
 
     $result->bindParam(':RazonSocial', $RazonSocial);
     $result->bindParam(':Calle', $Calle);
     $result->bindParam(':NoExterior', $NoExterior);
     $result->bindParam(':NoInterior', $NoInterior);
     $result->bindParam(':Colonia', $Colonia);
     $result->bindParam(':Ciudad', $Ciudad);
     $result->bindParam(':Pais', $Pais);
     $result->bindParam(':Cp', $Cp);
     $result->bindParam(':RFC', $RFC);
     $result->bindParam(':NumeroDeTarjeta', $NumeroDeTarjeta);
     $result->bindParam(':Tipo', $Tipo);
     $result->bindParam(':FechaVencimiento', $FechaVencimiento);
     $result->bindParam(':Telefono', $Telefono);
     $result->bindParam(':Email', $Email);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':FechaCreacion', $FechaCreacion);
     $result->bindParam(':Observaciones', $Observaciones);
     $result->bindParam(':bEstado', $bEstado);
 
     $result->execute();
     echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  
 
     $result = null;
     $db     = null;

   }catch(PDOException $e){
     
     echo '{"error" : {"text":'.$e->getMessage().'}';
   
   }

}); 

// TRAER EMPRESA POR IDENTIFICADOR UNICO
$app->get('/getcompany/{id}', function(Request $request, Response $response){
    
    $nIDEmpresa  = $request->getAttribute('id');
    
    $sql = "SELECT * FROM tbl_empresa WHERE nIDEmpresa = $nIDEmpresa AND bEstado = 1";
   
    try{
   
      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
   
        $company = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($company, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE");
      
      }
      
      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

}); 

// BUSCAR EMPRESA POR RAZON SOCIAL
$app->get('/company/search/{search}', function(Request $request, Response $response){
  
  $search  = $request->getAttribute('search');
  
  $sql = "SELECT * FROM tbl_empresas WHERE RazonSocial LIKE '%$search%' and bEstado = 1";

  try{
    
    $db     = new db();
    $db     = $db->connection();
    $result = $db->query($sql);

    if ($result->rowCount() > 0){
    
      $company = $result->fetchAll(PDO::FETCH_OBJ);
      echo json_encode($company, JSON_UNESCAPED_UNICODE);
    
    }else {
     
      echo json_encode("FALSE");
    
    }
    
    $result = null;
    $db     = null;
  
  }catch(PDOException $e){
  
    echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 

// ACTUALIZAR EMPRESA

$app->post('/companyIberoil/update/{id}', function(Request $request, Response $response){ 

    $nIDEmpresa              = $request->getAttribute('id');
    $RazonSocial             = $request->getParam('RazonSocial');
    $Calle                   = $request->getParam('Calle');
    $NoExterior              = $request->getParam('NoExterior');
    $NoInterior              = $request->getParam('NoInterior');
    $Colonia                 = $request->getParam('Colonia');
    $Ciudad                  = $request->getParam('Ciudad');
    $Pais                    = $request->getParam('Pais');
    $Cp                      = $request->getParam('Cp');
    $RFC                     = $request->getParam('RFC');
    $NumeroDeTarjeta         = $request->getParam('NumeroDeTarjeta');
    $Tipo                    = $request->getParam('Tipo');
    $FechaVencimiento        = $request->getParam('FechaVencimiento');
    $Telefono                = $request->getParam('Telefono');
    $Email                   = $request->getParam('Email');
    $FechaModificacion       = date('Y-m-d H:i:s');
    $Observaciones           = "Se modificó la empresa - " . date('Y-m-d H:i:s');

    $sql = "UPDATE tbl_empresa SET
            RazonSocial          = :RazonSocial,
            Calle                = :Calle,
            NoExterior           = :NoExterior,
            NoInterior           = :NoInterior,
            Colonia              = :Colonia,
            Ciudad               = :Ciudad,
            Pais                 = :Pais,
            Cp                   = :Cp,
            RFC                  = :RFC,
            NumeroDeTarjeta      = :NumeroDeTarjeta,
            Tipo                 = :Tipo,
            FechaVencimiento     = :FechaVencimiento,
            Telefono             = :Telefono,
            Email                = :Email,
            FechaModificacion    = :FechaModificacion,
            Observaciones        = :Observaciones
            WHERE nIDEmpresa     = $nIDEmpresa";
    
  try{
   
   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':RazonSocial', $RazonSocial);
   $result->bindParam(':Calle', $Calle);
   $result->bindParam(':NoExterior', $NoExterior);
   $result->bindParam(':NoInterior', $NoInterior);
   $result->bindParam(':Colonia', $Colonia);
   $result->bindParam(':Ciudad', $Ciudad);
   $result->bindParam(':Pais', $Pais);
   $result->bindParam(':Cp', $Cp);
   $result->bindParam(':RFC', $RFC);
   $result->bindParam(':NumeroDeTarjeta', $NumeroDeTarjeta);
   $result->bindParam(':Tipo', $Tipo);
   $result->bindParam('FechaVencimiento', $FechaVencimiento);
   $result->bindParam('Telefono', $Telefono);
   $result->bindParam('Email', $Email);
   $result->bindParam('FechaModificacion', $FechaModificacion);
   $result->bindParam('Observaciones', $Observaciones);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;
  
  }catch(PDOException $e){
  
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

});  

//ELIMINAR EMPRESA TEMPORALMENTE
$app->get('/company/delete/{id}', function(Request $request, Response $response){
  
  $nIDEmpresa       = $request->getAttribute('id');
  $bEstado          = 0;
  $Observaciones    = "La empresa fue eliminada temporalmente - ". date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_empresa SET
         Observaciones   = :Observaciones,         
         bEstado         = :bEstado

      WHERE nIDEmpresa = $nIDEmpresa";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 

// ACTIVAR EMPRESA
$app->get('/company/active/{id}', function(Request $request, Response $response){
  $nIDEmpresa       = $request->getAttribute('id');  
  $bEstado          = 1;
  $Observaciones    = "La empresa ha sido activada de nuevo - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_empresas SET
         Observaciones  = :Observaciones,
         bEstado        = :bEstado
      WHERE nIDEmpresa = $nIDEmpresa";
    
  try{
   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){

   echo '{"error" : {"text":'.$e->getMessage().'}';

  }

});  

//FILTRADO DE DATOS PORempresa
$app->get('/company/filter_search/{element}', function(Request $request, Response $response){
    $elemento = $request->getAttribute('element');
    
    $sql = "SELECT * FROM tbl_empresa WHERE RazonSocial LIKE '%$elemento%' and bEstado = 1";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $rol = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($rol, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

}); 

// CONTACTO EMPRESA
$app->get('/contact_company/{id}', function(Request $request, Response $response){

    $empresa = $request->getAttribute('id');
    $sql = "SELECT tbl_cat_contacto.nIDContacto,tbl_cat_contacto.Nombre,tbl_cat_contacto.Telefono,tbl_cat_contacto.Email,tbl_cat_contacto.FechaModificacion,tbl_cat_contacto.FechaCreacion,tbl_cat_contacto.Observaciones,tbl_cat_contacto.bEstado,tbl_empresa.RazonSocial,tbl_cat_contacto.nIDEmpresa FROM tbl_cat_contacto
    INNER JOIN tbl_empresa ON tbl_empresa.nIDEmpresa = tbl_cat_contacto.nIDEmpresa where tbl_cat_contacto.bEstado = 1 and tbl_cat_contacto.nIDEmpresa = $empresa";

    try {
       $db      = new db();
       $db      = $db->connection();
       $result  = $db->query($sql);

       if($result->rowCount() > 0){
          $contact_company = $result->fetchAll(PDO::FETCH_OBJ);
          echo json_encode($contact_company, JSON_UNESCAPED_UNICODE);
       }else{
          echo json_encode('FALSE');
       }

       $result = null;
       $db     = null;
    }catch(PDOException $e){
      echo '{"error" : {"text" : ' . $e->getMessage() . '}';
    }
});

// CREAR UN NUEVO CONTACTO DE LA EMPRESA
$app->post('/contact_company/add', function(Request $request, Response $response){

    $Nombre            = $request->getParam('Nombre');
    $Telefono          = $request->getParam('Telefono');
    $Email             = $request->getParam('Email');
    $nIDEmpresa        = $request->getParam('nIDEmpresa');
    $FechaModificacion = date('Y-m-d H:i:s');
    $FechaCreacion     = date('Y-m-d H:i:s');
    $Observaciones     = 'Se agregó un nuevo contacto a la empresa - ' . date('Y-m-d H:i:s');
    $bEstado           = 1;
   
   $sql = "INSERT INTO tbl_cat_contacto (Nombre, Telefono, Email, nIDEmpresa, FechaModificacion, FechaCreacion, Observaciones, bEstado) VALUES 
           (:Nombre, :Telefono, :Email, :nIDEmpresa, :FechaModificacion, :FechaCreacion, :Observaciones, :bEstado)";
   try{

     $db     = new db();
     $db     = $db->connection();
     $result = $db->prepare($sql);
 
     $result->bindParam(':Nombre', $Nombre);
     $result->bindParam(':Email', $Email);
     $result->bindParam(':Telefono', $Telefono);
     $result->bindParam(':nIDEmpresa', $nIDEmpresa);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':FechaCreacion', $FechaCreacion);
     $result->bindParam(':Observaciones', $Observaciones);
     $result->bindParam(':bEstado', $bEstado);
 
     if($result->execute()){
        echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);   
        $result = null;
        $db     = null;
     }else{
        echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
     }
     
 
     //echo $result->execute();

   }catch(PDOException $e){

     echo '{"error" : {"text":'.$e->getMessage().'}';
    
   }

}); 

// ELIMINAR TEMPORALMENTE UN FORMATO
$app->get('/contact_company/deleted/{id}', function(Request $request, Response $response){
  $nIDContacto       = $request->getAttribute('id');
  $bEstado          = 0;
  $FechaModificacion= date('Y-m-d H:i:s');
  $Observaciones    = "El contacto ha sido eliminado temporalmente - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_cat_contacto SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDContacto           = $nIDContacto";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);


   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 

// CONTACTO EMPRESA
$app->get('/roles', function(Request $request, Response $response){

    
    $sql = "SELECT * FROM tbl_roles WHERE tbl_roles.bEstado = 1";

    try {
       $db      = new db();
       $db      = $db->connection();
       $result  = $db->query($sql);

       if($result->rowCount() > 0){
          $roles = $result->fetchAll(PDO::FETCH_OBJ);
          echo json_encode($roles, JSON_UNESCAPED_UNICODE);
       }else{
          echo json_encode('FALSE');
       }

       $result = null;
       $db     = null;
    }catch(PDOException $e){
      echo '{"error" : {"text" : ' . $e->getMessage() . '}';
    }
});

// ROL CLIENTE
$app->get('/roles/searchCust', function(Request $request, Response $response){

    
    $sql = "SELECT * FROM tbl_roles WHERE tbl_roles.bEstado = 1 and tbl_roles.TipoDeRol LIKE '%Cliente%'";

    try {
       $db      = new db();
       $db      = $db->connection();
       $result  = $db->query($sql);

       if($result->rowCount() > 0){
          $roles = $result->fetchAll(PDO::FETCH_OBJ);
          echo json_encode($roles, JSON_UNESCAPED_UNICODE);
       }else{
          echo json_encode('FALSE');
       }

       $result = null;
       $db     = null;
    }catch(PDOException $e){
      echo '{"error" : {"text" : ' . $e->getMessage() . '}';
    }
});

// CONTACTO EMPRESA
$app->get('/rolesid/{id}', function(Request $request, Response $response){
    $id = $request->getAttribute('id');
    
    $sql = "SELECT * FROM tbl_roles WHERE tbl_roles.bEstado = 1 and tbl_roles.nIDRol = $id";

    try {
       $db      = new db();
       $db      = $db->connection();
       $result  = $db->query($sql);

       if($result->rowCount() > 0){
          $roles = $result->fetchAll(PDO::FETCH_OBJ);
          echo json_encode($roles, JSON_UNESCAPED_UNICODE);
       }else{
          echo json_encode('FALSE');
       }

       $result = null;
       $db     = null;
    }catch(PDOException $e){
      echo '{"error" : {"text" : ' . $e->getMessage() . '}';
    }
});
// CREAR UN NUEVO CONTACTO DE LA EMPRESA
$app->post('/roles/add', function(Request $request, Response $response){

    $TipoDeRol         = $request->getParam('TipoDeRol');
    $Descripcion       = $request->getParam('Descripcion');
    $Email1            = 'N/A';
    $Email2            = 'N/A';
    $Email3            = 'N/A';
    $Clasificacion     = $request->getParam('Clasificacion');
    $FechaModificacion = date('Y-m-d H:i:s');
    $FechaCreacion     = date('Y-m-d H:i:s');
    $Observaciones     = 'Se agregó un nuevo tipo de rol - ' . date('Y-m-d H:i:s');
    $bEstado           = 1;
   
   $sql = "INSERT INTO tbl_roles (TipoDeRol,Descripcion,Email1, Email2, Email3, Clasificacion,  FechaModificacion, FechaCreacion, Observaciones, bEstado) VALUES 
           (:TipoDeRol, :Descripcion, :Email1, :Email2, :Email3, :Clasificacion,  :FechaModificacion, :FechaCreacion, :Observaciones, :bEstado)";
   try{

     $db     = new db();
     $db     = $db->connection();
     $result = $db->prepare($sql);
 
     $result->bindParam(':TipoDeRol', $TipoDeRol);
     $result->bindParam(':Descripcion', $Descripcion);
     $result->bindParam(':Email1', $Email1);
     $result->bindParam(':Email2', $Email2);
     $result->bindParam(':Email3', $Email3);
     $result->bindParam(':Clasificacion', $Clasificacion);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':FechaCreacion', $FechaCreacion);
     $result->bindParam(':Observaciones', $Observaciones);
     $result->bindParam(':bEstado', $bEstado);
 
     if($result->execute()){
        echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);   
        $result = null;
        $db     = null;
     }else{
        echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
     }
     
 
     //echo $result->execute();

   }catch(PDOException $e){

     echo '{"error" : {"text":'.$e->getMessage().'}';
    
   }

}); 

// ELIMINAR TEMPORALMENTE UN FORMATO
$app->get('/roles/deleted/{id}', function(Request $request, Response $response){
  $nIDRol           = $request->getAttribute('id');
  $bEstado          = 0;
  $FechaModificacion= date('Y-m-d H:i:s');
  $Observaciones    = "El rol ha sido eliminado temporalmente - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_roles SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDRol           = $nIDRol";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);


   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 

// ACTUALIZAR CONFIGURACION DEL  FORMATO
$app->post('/roles/update/{id}', function(Request $request, Response $response){
    $nIDRol                 = $request->getAttribute('id');
    $TipoDeRol              = $request->getParam('TipoDeRol');
    $Descripcion            = $request->getParam('Descripcion');
    $Clasificacion          = $request->getParam('Clasificacion');
    $FechaModificacion      = date('Y-m-d H:i:s');
    $Observaciones          = 'El rol ha sido ha sido actualizado - ' . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_roles SET
         TipoDeRol          = :TipoDeRol,
         Descripcion        = :Descripcion,
         Clasificacion      = :Clasificacion,
         FechaModificacion  = :FechaModificacion,
         Observaciones      = :Observaciones
      WHERE nIDRol          = $nIDRol";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

     $result->bindParam(':TipoDeRol', $TipoDeRol);
     $result->bindParam(':Descripcion', $Descripcion);
     $result->bindParam(':Clasificacion', $Clasificacion);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':Observaciones', $Observaciones);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 

//FILTRADO DE DATOS POR Tipo de rol
$app->get('/roles/filter_search/{element}', function(Request $request, Response $response){
    $elemento = $request->getAttribute('element');
    
    $sql = "SELECT * FROM tbl_roles WHERE TipoDeRol LIKE '%$elemento%' and bEstado = 1";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $rol = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($rol, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

}); 
 /* FORMATOS */

// LISTA DE TODOS LOS FORMATOS

$app->get('/formats', function(Request $request, Response $response){
    
    $sql = "SELECT * FROM tbl_formato
    INNER JOIN tbl_usuarios ON tbl_usuarios.nIDUsuario = tbl_formato.nIDUsuario WHERE tbl_formato.bEstado = 1";
    
    try{
    
      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
    
        $format = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($format, JSON_UNESCAPED_UNICODE);
    
      }else {
    
        echo json_encode("FALSE");
    
      }
    
      $result = null;
      $db     = null;
   
    }catch(PDOException $e){
   
      echo '{"error" : {"text":'.$e->getMessage().'}';
   
    }

});

 // CREAR UN NUEVO FORMATO
$app->post('/formats/add', function(Request $request, Response $response){

    $Folio             = $request->getParam('Folio');
    $Formato           = $request->getParam('Formato');
    $Descripcion       = $request->getParam('Descripcion');
    $Titulo1           = $request->getParam('Titulo1');
    $Titulo2           = $request->getParam('Titulo2');
    $Titulo3           = $request->getParam('Titulo3');
    $FechaPiePagina    = $request->getParam('FechaPiePagina');
    $Logo              = $request->getParam('Logo');
    $nIDUsuario        = $request->getParam('nIDUsuario');
    $FechaModificacion = date('Y-m-d H:i:s');
    $FechaCreacion     = date('Y-m-d H:i:s');
    $Observaciones     = 'Se agregó un nuevo formato - ' . date('Y-m-d H:i:s');
    $bEstado           = 1;
   
   $sql = "INSERT INTO tbl_formato (Folio, Formato, Descripcion, Titulo1, Titulo2, Titulo3, FechaPiePagina, Logo, nIDUsuario, FechaModificacion, FechaCreacion, Observaciones, bEstado) VALUES 
           (:Folio, :Formato, :Descripcion, :Titulo1, :Titulo2, :Titulo3, :FechaPiePagina, :Logo, :nIDUsuario, :FechaModificacion, 
           :FechaCreacion, :Observaciones, :bEstado)";
   try{

     $db     = new db();
     $db     = $db->connection();
     $result = $db->prepare($sql);
 
     $result->bindParam(':Folio', $Folio);
     $result->bindParam(':Formato', $Formato);
     $result->bindParam(':Descripcion', $Descripcion);
     $result->bindParam(':Titulo1', $Titulo1);
     $result->bindParam(':Titulo2', $Titulo2);
     $result->bindParam(':Titulo3', $Titulo3);
     $result->bindParam(':FechaPiePagina', $FechaPiePagina);
     $result->bindParam(':Logo', $Logo);
     $result->bindParam(':nIDUsuario', $nIDUsuario);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':FechaCreacion', $FechaCreacion);
     $result->bindParam(':Observaciones', $Observaciones);
     $result->bindParam(':bEstado', $bEstado);
 
     if($result->execute()){
        echo json_encode($db->lastInsertId(), JSON_UNESCAPED_UNICODE);   
        $result = null;
        $db     = null;
     }else{
        echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
     }
     
 
     //echo $result->execute();

   }catch(PDOException $e){

     echo '{"error" : {"text":'.$e->getMessage().'}';
   
   }

}); 

// ACTUALIZAR FORMATO

$app->post('/formats/update/{id}', function(Request $request, Response $response){ 

    $db = new db();
    $db = $db->connection();
  
    $nIDFormato        = $request->getAttribute('id');
    $Folio             = $request->getParam('Folio');
    $Formato           = $request->getParam('Formato');
    $Descripcion       = $request->getParam('Descripcion');
    $Titulo1           = $request->getParam('Titulo1');
    $Titulo2           = $request->getParam('Titulo2');
    $Titulo3           = $request->getParam('Titulo3');
    $FechaPiePagina    = $request->getParam('FechaPiePagina');
    $Logo              = $request->getParam('Logo');
    $nIDUsuario        = $request->getParam('nIDUsuario');
    $Nombre            = $request->getParam('Nombre');
    $FechaModificacion = date('Y-m-d H:i:s');
    $Observaciones     = 'El formato ha sido actualizado - ' . date('Y-m-d H:i:s');

   $sql = "UPDATE tbl_formato SET
         Folio              = '$Folio',
         Formato            = '$Formato',
         Descripcion        = '$Descripcion',
         Titulo1            = '$Titulo1',
         Titulo2            = '$Titulo2',
         Titulo3            = '$Titulo3',
         FechaPiePagina     = '$FechaPiePagina',
         nIDUsuario         = '$nIDUsuario',
         FechaModificacion  = '$FechaModificacion',
         Observaciones      = '$Observaciones'";

   if($Logo === ""){


     }else{

        $LOGO       = base64_decode($Logo); 
        $route      = $_SERVER['DOCUMENT_ROOT'] . "/iberoil/formatos/Logos/" . $Nombre;
        //$route= "C:/xampp2/htdocs/api/src/Formatos/Logos/" . $Nombre;
        file_put_contents($route, $LOGO);
        chmod($route . $Nombre, 0777);

        $sql        = $sql . ",Logo   ='" .$Nombre."'"; 

     }
    
  
     $sql = $sql . " WHERE nIDFormato = '$nIDFormato'";

      
     
     $result = $db->query($sql);
      if($result->execute()){
        echo json_encode('TRUE', JSON_UNESCAPED_UNICODE); 
        $result = null; 
        $db = null;
      }else{
        echo json_encode('FALSE', JSON_UNESCAPED_UNICODE);
      }

      

      
      

}); 

// ELIMINAR TEMPORALMENTE UN FORMATO
$app->get('/formats/deleted/{id}', function(Request $request, Response $response){
  $nIDFormato       = $request->getAttribute('id');
  $bEstado          = 0;
  $FechaModificacion= date('Y-m-d H:i:s');
  $Observaciones    = "El formato ha sido eliminado temporalmente - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_formato SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDFormato            = $nIDFormato";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);


   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 



// ACTIVAR EL FORMATO
$app->get('/formats/active/{id}', function(Request $request, Response $response){
  $nIDFormato       = $request->getAttribute('id');
  $bEstado          = 1;
  $FechaModificacion= date('Y-m-d H:i:s');
  $Observaciones    = "El formato ha sido activado - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_formato SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDFormato            = $nIDFormato";
    
  try{
   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

});


// MOSTRAR FORMATO POR ID

$app->get('/formats/search/{id}', function(Request $request, Response $response){
    $nIDFormato = $request->getAttribute('id');
    
    $sql = "SELECT * FROM tbl_formato WHERE nIDFormato = $nIDFormato and bEstado = 1";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $formats = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($formats, JSON_UNESCAPED_UNICODE);

      
      }else {
      
        echo json_encode("FALSE");
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

}); 

//FILTRADO DE DATOS POR NOMBRE DE USUARIO O NOMBRE DE PLANTILLA
$app->get('/formats/filter_search/{element}', function(Request $request, Response $response){
    $elemento = $request->getAttribute('element');
    
    $sql = "SELECT * FROM tbl_formato 
    INNER JOIN tbl_usuarios ON tbl_usuarios.nIDUsuario = tbl_formato.nIDUsuario 
    WHERE (tbl_formato.bEstado = 1 and tbl_formato.Formato LIKE '%$elemento%') OR (tbl_usuarios.Nombre LIKE '%$elemento%'  and tbl_formato.bEstado = 1)";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $formats = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($formats, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

}); 

// MOSTRAR FORMATO POR NOMBRE DE FORMATO
$app->get('/formats/searchformats/{search}', function(Request $request, Response $response){
    $search = $request->getAttribute('search');
    
    $sql = "SELECT * FROM tbl_formato WHERE Formato LIKE '%$search%' and bEstado = 1";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $formats = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($formats, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE");
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

}); 


 /* CONFIGURACION DE LOS FORMATOS */

// LISTA DE TODOS LOS FORMATOS

$app->get('/formats_conf', function(Request $request, Response $response){
    
    $sql = "SELECT * FROM tbl_formato_configuracion WHERE bEstado = 1";
    
    try{
    
      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
    
        $formats_conf = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($formats_conf, JSON_UNESCAPED_UNICODE);
    
      }else {
    
        echo json_encode("FALSE");
    
      }
    
      $result = null;
      $db     = null;
   
    }catch(PDOException $e){
   
      echo '{"error" : {"text":'.$e->getMessage().'}';
   
    }

});

 // CREAR NUEVA CONFIGURACION DEL FORMATO
$app->post('/formats_conf/add', function(Request $request, Response $response){ 

    
    $nIDFormato             = $request->getParam('nIDFormato');
    $Nombre                 = $request->getParam('Nombre');
    $Tipo                   = $request->getParam('Tipo');
    $Columna                = $request->getParam('Columna');
    $Fila                   = $request->getParam('Fila');
    $TipoDeDato             = $request->getParam('TipoDeDato');
    $Longitud               = $request->getParam('Longitud');
    $ValorMaximo            = $request->getParam('ValorMaximo');
    $ValorMinimo            = $request->getParam('ValorMinimo');
    $Obligatorio            = $request->getParam('Obligatorio');
    $MensajeDeError         = $request->getParam('MensajeDeError');
    $FechaModificacion      = date('Y-m-d H:i:s');
    $FechaCreacion          = date('Y-m-d H:i:s');
    $Observaciones          = 'Se agregó una nueva configuración al formato - ' . date('Y-m-d H:i:s');
    $bEstado                = 1;
   
   $sql = "INSERT INTO tbl_formato_configuracion (nIDFormato, Nombre, Tipo, Columna, Fila,TipoDeDato, Longitud, ValorMaximo, ValorMinimo, Obligatorio, MensajeDeError,FechaModificacion, FechaCreacion, Observaciones, bEstado) VALUES 
           (:nIDFormato, :Nombre, :Tipo, :Columna, :Fila,:TipoDeDato, :Longitud, :ValorMaximo, :ValorMinimo, :Obligatorio, :MensajeDeError,:FechaModificacion, :FechaCreacion, :Observaciones, :bEstado)";
   try{

     $db     = new db();
     $db     = $db->connection();
     $result = $db->prepare($sql);
 
     
     $result->bindParam(':nIDFormato', $nIDFormato);
     $result->bindParam(':Nombre', $Nombre);
     $result->bindParam(':Tipo', $Tipo);
     $result->bindParam(':Columna', $Columna);
     $result->bindParam(':Fila', $Fila);
     $result->bindParam(':TipoDeDato', $TipoDeDato);
     $result->bindParam(':Longitud', $Longitud);
     $result->bindParam(':ValorMaximo', $ValorMaximo);
     $result->bindParam(':ValorMinimo', $ValorMinimo);
     $result->bindParam(':Obligatorio', $Obligatorio);
     $result->bindParam(':MensajeDeError', $MensajeDeError);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':FechaCreacion', $FechaCreacion);
     $result->bindParam(':Observaciones', $Observaciones);
     $result->bindParam(':bEstado', $bEstado);
 
     $result->execute();
     echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  
 
     $result = null;
     $db     = null;

   }catch(PDOException $e){

     echo '{"error" : {"text":'.$e->getMessage().'}';
   
   }

}); 

// ACTUALIZAR CONFIGURACION DEL  FORMATO
$app->post('/formats_conf/update/{id}', function(Request $request, Response $response){
    $nIDFormato_Conf        = $request->getAttribute('id');
    $nIDFormato             = $request->getParam('nIDFormato');
    $Nombre                 = $request->getParam('Nombre');
    $Tipo                   = $request->getParam('Tipo');
    $Columna                = $request->getParam('Columna');
    $Fila                   = $request->getParam('Fila');
    $TipoDeDato             = $request->getParam('TipoDeDato');
    $Longitud               = $request->getParam('Longitud');
    $ValorMaximo            = $request->getParam('ValorMaximo');
    $ValorMinimo            = $request->getParam('ValorMinimo');
    $Obligatorio            = $request->getParam('Obligatorio');
    $MensajeDeError         = $request->getParam('MensajeDeError');
    $FechaModificacion      = date('Y-m-d H:i:s');
    $Observaciones          = 'La configuración del formato ha sido actualizado - ' . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_formato_configuracion SET
         nIDFormato         = :nIDFormato,
         Nombre             = :Nombre,
         Tipo               = :Tipo,
         Columna            = :Columna,
         Fila               = :Fila,
         TipoDeDato         = :TipoDeDato,
         Longitud           = :Longitud,
         ValorMaximo        = :ValorMaximo,
         ValorMinimo        = :ValorMinimo,
         Obligatorio        = :Obligatorio,
         MensajeDeError     = :MensajeDeError,
         FechaModificacion  = :FechaModificacion,
         Observaciones      = :Observaciones
      WHERE nIDFormato_Conf = $nIDFormato_Conf";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

     $result->bindParam(':nIDFormato', $nIDFormato);
     $result->bindParam(':Nombre', $Nombre);
     $result->bindParam(':Tipo', $Tipo);
     $result->bindParam(':Columna', $Columna);
     $result->bindParam(':Fila', $Fila);
     $result->bindParam(':TipoDeDato', $TipoDeDato);
     $result->bindParam(':Longitud', $Longitud);
     $result->bindParam(':ValorMaximo', $ValorMaximo);
     $result->bindParam(':ValorMinimo', $ValorMinimo);
     $result->bindParam(':Obligatorio', $Obligatorio);
     $result->bindParam(':MensajeDeError', $MensajeDeError);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':Observaciones', $Observaciones);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 

// ELIMINAR TEMPORALMENTE UNA CONFIGURACION DEL FORMATO
$app->get('/formats_conf/deleted/{id}', function(Request $request, Response $response){
  $nIDFormato_Conf       = $request->getAttribute('id');
  $bEstado               = 0;
  $FechaModificacion     = date('Y-m-d H:i:s');
  $Observaciones         = "La configuración del formato ha sido eliminada temporalmente - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_formato_configuracion SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDFormato_Conf       = $nIDFormato_Conf";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);


   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 

// ACTIVAR LA CONFIGURACION DEL FORMATO
$app->get('/formats_conf/active/{id}', function(Request $request, Response $response){
  $nIDFormato_Conf       = $request->getAttribute('id');
  $bEstado               = 1;
  $FechaModificacion     = date('Y-m-d H:i:s');
  $Observaciones         = "La configuración del formato ha sido activada - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_formato_configuracion SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDFormato_Conf       = $nIDFormato_Conf";
    
  try{
   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

});


// MOSTRAR LA CONFIGURACION DEL FORMATO POR ID

$app->get('/formats_conf/search/{id}', function(Request $request, Response $response){
    $nIDFormato = $request->getAttribute('id');
    
    $sql = "SELECT * FROM tbl_formato_configuracion WHERE nIDFormato = $nIDFormato and bEstado = 1";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $formats_conf = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($formats_conf, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE");
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

}); 

// MOSTRAR LA CONFIGURACION DEL FORMATO POR NOMBRE DE LA CONFIGURACION
$app->get('/formats_conf/searchformats_conf/{search}', function(Request $request, Response $response){
    $search = $request->getAttribute('search');
    
    $sql = "SELECT * FROM tbl_formato_configuracion WHERE Nombre LIKE '%$search%' and bEstado = 1";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $formats_conf = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($formats_conf, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE");
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

});

//BUSCAR CAMPOS DE ACUERDO AL NUMERO DE DOCUMENTO QUE ESTA LIGADO CON LA PLANTILLA
$app->get('/formats_conf/searchFields/{id}', function(Request $request, Response $response){
    $id = $request->getAttribute('id');
    
    $sql = "SELECT fc.nIDFormato_Conf,fc.Nombre,fc.Tipo,fc.Columna,fc.Fila,fc.TipoDeDato,fc.Longitud,fc.ValorMaximo,fc.ValorMinimo,
    fc.Obligatorio,fc.MensajeDeError,d.nIDDocumento,fc.bEstado,fc.nIDFormato FROM tbl_formato_configuracion as fc
    INNER JOIN tbl_documentos as d ON d.nIDFormato = fc.nIDFormato
    WHERE d.nIDDocumento = $id and fc.bEstado = 1 ORDER BY fc.Fila,fc.Columna";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $formats_conf = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($formats_conf, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

});

//BUSCAR CAMPOS DE ACUERDO AL NUMERO DE DOCUMENTO QUE ESTA LIGADO CON LA PLANTILLA
$app->get('/formats_conf/searchFile/{id}', function(Request $request, Response $response){
    $id = $request->getAttribute('id');
    
    $sql = "SELECT MAX(tbl_formato_configuracion.Fila) as Fila FROM tbl_formato_configuracion
    INNER JOIN tbl_documentos ON tbl_documentos.nIDFormato = tbl_formato_configuracion.nIDFormato
    INNER JOIN tbl_formato ON tbl_formato.nIDFormato = tbl_formato_configuracion.nIDFormato
    WHERE tbl_documentos.nIDDocumento = $id and tbl_formato_configuracion.bEstado = 1 ORDER BY tbl_formato_configuracion.Fila";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $formats_conf = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($formats_conf, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

});

//BUSCAR CAMPOS DE ACUERDO AL NUMERO DE DOCUMENTO QUE ESTA LIGADO CON LA PLANTILLA
/*$app->get('/formats_conf/searchFields/{id}', function(Request $request, Response $response){
    $id = $request->getAttribute('id');
    
    $sql = "SELECT fc.nIDFormato_Conf,fc.Nombre,fc.Tipo,fc.Columna,fc.Fila,fc.TipoDeDato,fc.Longitud,fc.ValorMaximo,fc.ValorMinimo,
    fc.Obligatorio,fc.MensajeDeError,d.nIDDocumento,fc.bEstado,fc.nIDFormato FROM tbl_formato_configuracion as fc
    INNER JOIN tbl_documentos as d ON d.nIDFormato = fc.nIDFormato
    WHERE d.nIDDocumento = $id and fc.bEstado = 1 ORDER BY fc.Fila,fc.Columna";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $formats_conf = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($formats_conf, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

});*/



/*function CamposConfiguracion($id_documento){

    $sql = "SELECT fc.nIDFormato_Conf,fc.Nombre,fc.Tipo,fc.Columna,fc.Fila,fc.TipoDeDato,fc.Longitud,fc.ValorMaximo,fc.ValorMinimo,
    fc.Obligatorio,fc.MensajeDeError,d.nIDDocumento,fc.bEstado,fc.nIDFormato FROM tbl_formato_configuracion as fc
    INNER JOIN tbl_documentos as d ON d.nIDFormato = fc.nIDFormato
    WHERE d.nIDDocumento = $id_documento and fc.bEstado = 1 ORDER BY fc.Fila,fc.Columna";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
      
      if ($result->rowCount() > 0){
        $regs = array();
        while($formats_conf = $result->fetch(PDO::FETCH_ASSOC)){
            $regs[] = $formats_conf;
        }return $regs;
        //return $formats_conf;
      
      }else {
      
        return json_encode("FALSE", JSON_UNESCAPED_UNICODE);
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

}


//BUSCAR CAMPOS DE ACUERDO AL NUMERO DE DOCUMENTO QUE ESTA LIGADO CON LA PLANTILLA
$app->get('/formats_conf/searchFile/{id}', function(Request $request, Response $response){
    $id = $request->getAttribute('id');
    $COLUMNAS = array(0);
    $FILAS    = array(0);
    $NIDS     = array(0);
    $CAMPOS   = array("");
    $FORMATO;


    $COUNT_COLUMNAS = count($COLUMNAS);
    $COUNT_FILAS    = count($FILAS);
    $COUNT_NIDS     = count($NIDS);
    $COUNT_CAMPOS   = 0;

    $BANDENCONTRADO  = 0;
    $BANDENCONTRADO2 = 0;

    $Fila_Array = array();
    $campos     = array();

    $campos = CamposConfiguracion($id);  
    $nCampo =count($campos, 0);
    echo json_encode($campos[1]["Longitud"]);
    $Fila_Array = str_replace('"', "", numeroFilas($id)); 

    //numero de filas
   for( $i = 0; $i < $Fila_Array; $i++){
      $BANDENCONTRADO = 0; 
      
      for( $j = 0; $j < $COUNT_FILAS; $j++){
        
        if($FILAS[$j] == $campos[$i]["Fila"]){
          $BANDENCONTRADO = 1;
          break;
        }

      }

      if ($BANDENCONTRADO == 0) {
        array_push($FILAS, $campos[$i]["Fila"]);
        $FORMATO =  "<tr id='"+$i+"' class='filasnew' name='filasdinamicas'></tr>";

        for( $j = 1; $j < 13; $j++){

              $BANDENCONTRADO2 = 0;

              for( $k = 0; $k < $nCampo; $k++){
                  echo $campos[$i]["Fila"];
                if($campos[$i]["Fila"] == $campos[$k]["Fila"]){

                  if($j == $campos[$k]["Columna"]){

                        $BANDENCONTRADO2 = 1;

                        switch ($campos[$k]["Tipo"]) {
                          case 'Grupo':
                              //echo json_encode($campos[$k]["Longitud"]);
                            break;

                          case 'Etiqueta':
                              //echo json_encode($campos[$k]["Longitud"]);
                            break;

                          case 'Text':
                              //echo json_encode($campos[$k]["Longitud"]);
                            break;
                          
                          
                        }
                  }
                }
              }
        }
      }
    
  }

    
    

});

function numeroFilas($id_documento){
  
    $sql = "SELECT MAX(tbl_formato_configuracion.Fila) as Fila FROM tbl_formato_configuracion
    INNER JOIN tbl_documentos ON tbl_documentos.nIDFormato = tbl_formato_configuracion.nIDFormato
    INNER JOIN tbl_formato ON tbl_formato.nIDFormato = tbl_formato_configuracion.nIDFormato
    WHERE tbl_documentos.nIDDocumento = $id_documento and tbl_formato_configuracion.bEstado = 1 ORDER BY tbl_formato_configuracion.Fila";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
      //echo json_encode($result->rowCount());
      if ($result->rowCount() > 0){
      
        $formats_conf = $result->fetchAll(PDO::FETCH_COLUMN, 0);
        return json_encode($formats_conf[0]);
      }else {
      
        return json_encode("FALSE", JSON_UNESCAPED_UNICODE);
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }
}*/

// CREAR UN NUEVO DOCUMENTO
$app->post('/permission/add', function(Request $request, Response $response){

    
    $nIDRol                 = $request->getParam('nIDRol');
    $Permiso                = $request->getParam('Permiso');
    $FechaModificacion      = date('Y-m-d H:i:s');
    $FechaCreacion          = date('Y-m-d H:i:s');
    $Observaciones          = 'Se agregó un nuevo permiso - ' . date('Y-m-d H:i:s');
    $bEstado                = 1;
   
   $sql = "INSERT INTO tbl_permisos (nIDRol, Permiso, FechaModificacion, FechaCreacion, Observaciones, bEstado) VALUES 
           (:nIDRol, :Permiso,:FechaModificacion, :FechaCreacion, :Observaciones, :bEstado)";
   try{

     $db     = new db();
     $db     = $db->connection();
     $result = $db->prepare($sql);
 
     
     $result->bindParam(':nIDRol', $nIDRol);
     $result->bindParam(':Permiso', $Permiso);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':FechaCreacion', $FechaCreacion);
     $result->bindParam(':Observaciones', $Observaciones);
     $result->bindParam(':bEstado', $bEstado);
 
     $result->execute();
     echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  
 
     $result = null;
     $db     = null;

   }catch(PDOException $e){

     echo '{"error" : {"text":'.$e->getMessage().'}';
   
   }

}); 


$app->get('/permission/{id}', function(Request $request, Response $response){
    $id = $request->getAttribute('id');

    $sql = "SELECT * FROM tbl_permisos WHERE bEstado = 1 and nIDRol = $id";
    
    try{
    
      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
    
        $permiso = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($permiso, JSON_UNESCAPED_UNICODE);
    
      }else {
    
        echo json_encode("FALSE", JSON_UNESCAPED_UNICOD);
    
      }
    
      $result = null;
      $db     = null;
   
    }catch(PDOException $e){
   
      echo '{"error" : {"text":'.$e->getMessage().'}';
   
    }

});



// ELIMINAR TEMPORALMENTE UN CLIENTE
$app->get('/permission/deleted/{id}', function(Request $request, Response $response){
  $nIDPermiso            = $request->getAttribute('id');
  $bEstado               = 0;
  $FechaModificacion     = date('Y-m-d H:i:s');
  $Observaciones         = "El permiso ha sido desactivado - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_permisos SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDPermiso            = $nIDPermiso";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);


   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

});













 /* DOCUMENTOS */

// LISTA DE TODOS LOS DOCUMENTOS

$app->get('/documents', function(Request $request, Response $response){
    
    $sql = "SELECT doc.nIDDocumento,doc.nIDFormato,doc.Folio,doc.Fecha,doc.Documento,doc.nIDUsuario,doc.Estatus,doc.FechaModificacion,doc.FechaCreacion,doc.Observaciones,doc.bEstado,us.Nombre,f.Formato FROM tbl_documentos as doc 
        INNER JOIN tbl_usuarios as us ON us.nIDUsuario = doc.nIDUsuario
        INNER JOIN tbl_formato as f ON f.nIDFormato = doc.nIDFormato WHERE doc.bEstado = 1";
    
    try{
    
      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
    
        $documents = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($documents, JSON_UNESCAPED_UNICODE);
    
      }else {
    
        echo json_encode("FALSE", JSON_UNESCAPED_UNICOD);
    
      }
    
      $result = null;
      $db     = null;
   
    }catch(PDOException $e){
   
      echo '{"error" : {"text":'.$e->getMessage().'}';
   
    }

});

 // CREAR UN NUEVO DOCUMENTO
$app->post('/documents/add', function(Request $request, Response $response){

    
    $nIDFormato             = $request->getParam('nIDFormato');
    $Folio                  = $request->getParam('Folio');
    $Fecha                  = $request->getParam('Fecha');
    $Documento              = $request->getParam('Documento');
    $Descripcion            = $request->getParam('Descripcion');
    $nIDUsuario             = $request->getParam('nIDUsuario');
    $Estatus                = 'NO CAPTURADO';
    $FechaModificacion      = date('Y-m-d H:i:s');
    $FechaCreacion          = date('Y-m-d H:i:s');
    $Observaciones          = 'Se agregó una nuevo documento - ' . date('Y-m-d H:i:s');
    $bEstado                = 1;
   
   $sql = "INSERT INTO tbl_documentos (nIDFormato, Folio, Fecha, Documento, Descripcion, nIDUsuario, Estatus, FechaModificacion, FechaCreacion, Observaciones, bEstado) VALUES 
           (:nIDFormato, :Folio, :Fecha, :Documento, :Descripcion, :nIDUsuario, :Estatus,:FechaModificacion, :FechaCreacion, :Observaciones, :bEstado)";
   try{

     $db     = new db();
     $db     = $db->connection();
     $result = $db->prepare($sql);
 
     
     $result->bindParam(':nIDFormato', $nIDFormato);
     $result->bindParam(':Folio', $Folio);
     $result->bindParam(':Fecha', $Fecha);
     $result->bindParam(':Documento', $Documento);
     $result->bindParam(':Descripcion', $Descripcion);
     $result->bindParam(':nIDUsuario', $nIDUsuario);
     $result->bindParam(':Estatus', $Estatus);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':FechaCreacion', $FechaCreacion);
     $result->bindParam(':Observaciones', $Observaciones);
     $result->bindParam(':bEstado', $bEstado);
 
     $result->execute();
     echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  
 
     $result = null;
     $db     = null;

   }catch(PDOException $e){

     echo '{"error" : {"text":'.$e->getMessage().'}';
   
   }

}); 

// ACTUALIZAR DOCUMENTO
$app->post('/documents/update/{id}', function(Request $request, Response $response){
    $nIDDocumento           = $request->getAttribute('id');
    $nIDFormato             = $request->getParam('nIDFormato');
    $Documento              = $request->getParam('Documento');
    $Descripcion            = $request->getParam('Descripcion');
    $FechaModificacion      = date('Y-m-d H:i:s');
    $Observaciones          = 'El documento ha sido actualizado - ' . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_documentos SET

         nIDFormato         = :nIDFormato,
         Documento          = :Documento,
         Descripcion        = :Descripcion,
         FechaModificacion  = :FechaModificacion,
         Observaciones      = :Observaciones
      WHERE nIDDocumento    = $nIDDocumento";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

     $result->bindParam(':nIDFormato', $nIDFormato);
     $result->bindParam(':Documento', $Documento);
     $result->bindParam(':Descripcion', $Descripcion);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':Observaciones', $Observaciones);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 

// ELIMINAR TEMPORALMENTE UN DOCUMENTO
$app->get('/documents/deleted/{id}', function(Request $request, Response $response){
  $nIDDocumento          = $request->getAttribute('id');
  $bEstado               = 0;
  $FechaModificacion     = date('Y-m-d H:i:s');
  $Observaciones         = "El documento ha sido eliminado temporalmente - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_documentos SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDDocumento          = $nIDDocumento";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);


   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 

// ACTIVAR EL DOCUMENTO
$app->get('/documents/active/{id}', function(Request $request, Response $response){
  $nIDDocumento          = $request->getAttribute('id');
  $bEstado               = 1;
  $FechaModificacion     = date('Y-m-d H:i:s');
  $Observaciones         = "La configuración del formato ha sido activada - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_documentos SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDDocumento          = $nIDDocumento";
    
  try{
   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

});


// MOSTRAR DOCUMENTO POR ID

$app->get('/documents/search/{id}', function(Request $request, Response $response){
    $nIDDocumento = $request->getAttribute('id');
    
    $sql = "SELECT * FROM tbl_documentos WHERE nIDDocumento = $nIDDocumento and bEstado = 1";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $documents = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($documents, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE");
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

}); 

// MOSTRAR DOCUMENTO POR NOMBRE DEL DOCUMENTO
$app->get('/documents/searchname/{search}', function(Request $request, Response $response){
    $search = $request->getAttribute('search');
    
    $sql = "SELECT doc.nIDDocumento,doc.nIDFormato,doc.Folio,doc.Fecha,doc.Documento,doc.nIDUsuario,doc.Estatus,
    doc.FechaModificacion,doc.FechaCreacion,doc.Observaciones,doc.bEstado,us.Nombre,f.Formato
    FROM tbl_documentos as doc
    INNER JOIN tbl_usuarios as us ON us.nIDUsuario = doc.nIDUsuario
    INNER JOIN tbl_formato as f ON f.nIDFormato = doc.nIDFormato 
    WHERE doc.Documento LIKE '%$search%' 
    OR f.Formato LIKE '%$search%'
    OR us.Nombre LIKE '%$search%' and doc.bEstado = 1";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $documents = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($documents, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE");
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

});


//CLIENTES
// LISTA DE TODOS LOS CLIENTES
$app->get('/customers', function(Request $request, Response $response){
    
    $sql = "SELECT nIDCliente,Codigo,Denominacion,RazonSocial,Email,RFC,Domicilio,Telefono,
    FechaModificacion,FechaCreacion,Observaciones,bEstado FROM tbl_clientes WHERE bEstado = 1";
    
    try{
    
      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
    
        $customers = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($customers, JSON_UNESCAPED_UNICODE);
    
      }else {
    
        echo json_encode("FALSE", JSON_UNESCAPED_UNICOD);
    
      }
    
      $result = null;
      $db     = null;
   
    }catch(PDOException $e){
   
      echo '{"error" : {"text":'.$e->getMessage().'}';
   
    }

});
// CREAR UN NUEVO CLIENTE
$app->post('/customers/add', function(Request $request, Response $response){

    
    $nIDCliente             = $request->getParam('nIDCliente');
    $Codigo                 = $request->getParam('Codigo');
    $Denominacion           = $request->getParam('Denominacion');
    $RazonSocial            = $request->getParam('Nombre');
    $Email                  = $request->getParam('Email');
    $RFC                    = $request->getParam('RFC');
    $Domicilio              = $request->getParam('Domicilio');
    $Telefono               = $request->getParam('Telefono');
    $FechaModificacion      = date('Y-m-d H:i:s');
    $FechaCreacion          = date('Y-m-d H:i:s');
    $Observaciones          = 'Se agregó un nuevo cliente - ' . date('Y-m-d H:i:s');
    $bEstado                = 1;
   
   $sql = "INSERT INTO tbl_clientes (nIDCliente, Codigo, Denominacion, RazonSocial, Email, RFC, Domicilio, Telefono, FechaModificacion, FechaCreacion, Observaciones, bEstado) VALUES   
           (:nIDCliente, :Codigo, :Denominacion, :Nombre, :Email, :RFC, :Domicilio, :Telefono, :FechaModificacion, :FechaCreacion, :Observaciones, :bEstado)";
   try{

     $db     = new db();
     $db     = $db->connection();
     $result = $db->prepare($sql);
 
     
     $result->bindParam(':nIDCliente', $nIDCliente);
     $result->bindParam(':Codigo', $Codigo);
     $result->bindParam(':Denominacion', $Denominacion);
     $result->bindParam(':Nombre', $RazonSocial);
     $result->bindParam(':Email', $Email);
     $result->bindParam(':RFC', $RFC);
     $result->bindParam(':Domicilio', $Domicilio);
     $result->bindParam(':Telefono', $Telefono);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':FechaCreacion', $FechaCreacion);
     $result->bindParam(':Observaciones', $Observaciones);
     $result->bindParam(':bEstado', $bEstado);
 
     $result->execute();
     echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  
 
     $result = null;
     $db     = null;

   }catch(PDOException $e){

     echo '{"error" : {"text":'.$e->getMessage().'}';
   
   }

}); 

// ACTUALIZAR CLIENTE
$app->post('/customers/update/{id}', function(Request $request, Response $response){
    $nIDCliente             = $request->getAttribute('id');
    $Codigo                 = $request->getParam('Codigo');
    $Denominacion           = $request->getParam('Denominacion');
    $RazonSocial            = $request->getParam('Nombre');
    $Email                  = $request->getParam('Email');
    $RFC                    = $request->getParam('RFC');
    $Domicilio              = $request->getParam('Domicilio');
    $Telefono               = $request->getParam('Telefono');
    $FechaModificacion      = date('Y-m-d H:i:s');
    $Observaciones          = 'El cliente ha sido actualizado - ' . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_clientes SET

         Codigo             = :Codigo,
         Denominacion       = :Denominacion,
         RazonSocial        = :Nombre,
         Email              = :Email,
         RFC                = :RFC,
         Domicilio          = :Domicilio,
         Telefono           = :Telefono,  
         FechaModificacion  = :FechaModificacion,
         Observaciones      = :Observaciones
      WHERE nIDCliente      = $nIDCliente";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

     $result->bindParam(':Codigo', $Codigo);
     $result->bindParam(':Denominacion', $Denominacion);
     $result->bindParam(':Nombre', $RazonSocial);
     $result->bindParam(':Email', $Email);
     $result->bindParam(':RFC', $RFC);
     $result->bindParam(':Domicilio', $Domicilio);
     $result->bindParam(':Telefono', $Telefono);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':Observaciones', $Observaciones);
 
   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 

// ELIMINAR TEMPORALMENTE UN CLIENTE
$app->get('/customers/deleted/{id}', function(Request $request, Response $response){
  $nIDCliente            = $request->getAttribute('id');
  $bEstado               = 0;
  $FechaModificacion     = date('Y-m-d H:i:s');
  $Observaciones         = "El cliente ha sido eliminado temporalmente - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_clientes SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDCliente            = $nIDCliente";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);


   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 

// ACTIVAR EL CLIENTE
$app->get('/customers/active/{id}', function(Request $request, Response $response){
  $nIDCliente            = $request->getAttribute('id');
  $bEstado               = 1;
  $FechaModificacion     = date('Y-m-d H:i:s');
  $Observaciones         = "El cliente ha sido activado - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_cliente SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDCliente            = $nIDCliente";
    
  try{
   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

});


// MOSTRAR CLIENTE POR ID
$app->get('/customers/search/{id}', function(Request $request, Response $response){
    $nIDCliente = $request->getAttribute('id');
    
    $sql = "SELECT nIDCliente,Codigo,Denominacion,RazonSocial,Email,RFC,Domicilio,Telefono,
    FechaModificacion,FechaCreacion,Observaciones,bEstado FROM tbl_clientes WHERE nIDCliente = $nIDCliente and bEstado = 1";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $customers = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($customers, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE");
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

}); 

// MOSTRAR CLIENTE POR NOMBRE DEL CLIENTE
$app->get('/customers/searchElement/{search}', function(Request $request, Response $response){
    $search = $request->getAttribute('search');
    
    $sql = "SELECT * FROM tbl_clientes WHERE RazonSocial LIKE '%$search%' or RFC LIKE '%$search%'
    or Domicilio LIKE '%$search%' or Telefono LIKE '%$search%' or Email LIKE '%$search%' and bEstado = 1";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $customers = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($customers, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE");
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

});



//CONTACTS OF CUSTOMER
// LIST OF CONTACT CUSTOMER
$app->get('/contact_customer/{id}', function(Request $request, Response $response){
  
  $id = $request->getAttribute('id');

  $sql = "SELECT nIDContactoCliente,Nombre,Usuario,Telefono,Celular,Email,nIDCliente,
    FechaModificacion,FechaCreacion,Observaciones,bEstado FROM tbl_cat_contacto_cliente WHERE nIDCliente = $id and bEstado = 1";
      
    try{
    
      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
    
        $contacts_customers = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($contacts_customers, JSON_UNESCAPED_UNICODE);
    
      }else {
    
        echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
    
      }
    
      $result = null;
      $db     = null;
   
    }catch(PDOException $e){
   
      echo '{"error" : {"text":'.$e->getMessage().'}';
   
    }
  });

// LIST OF CONTACT CUSTOMER ID
$app->get('/contact_customer/select/{id}', function(Request $request, Response $response){
  
  $id = $request->getAttribute('id');

  $sql = "SELECT nIDContactoCliente,Nombre,Usuario,Telefono,Celular,Email,nIDCliente,
    FechaModificacion,FechaCreacion,Observaciones,bEstado FROM tbl_cat_contacto_cliente WHERE nIDContactoCliente = $id and bEstado = 1";
      
    try{
    
      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
    
        $contacts_customers = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($contacts_customers, JSON_UNESCAPED_UNICODE);
    
      }else {
    
        echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
    
      }
    
      $result = null;
      $db     = null;
   
    }catch(PDOException $e){
   
      echo '{"error" : {"text":'.$e->getMessage().'}';
   
    }
  });


//ADD CONTACT
$app->post('/contact_customer/add', function(Request $request, Response $response){
    $Nombre         = $request->getParam('Nombre');
    $Usuario        = $request->getParam('Usuario');
    $Password       = $request->getParam('Password');
    $Telefono       = $request->getParam('Telefono');
    $Celular        = $request->getParam('Celular');
    $Email          = $request->getParam('Email');
    $nIDCliente     = $request->getParam('nIDCliente');
    $nIDRol         = $request->getParam('nIDRol');
    $bEstado        = 1;
    $FechaCreacion = date('Y-m-d H:i:s');
    $FechaModificacion = date('Y-m-d H:i:s');
    $Observaciones     = 'Nuevo contacto de la empresa registrado - ' . date('Y-m-d H:i:s');
   
   $sql = "INSERT INTO tbl_cat_contacto_cliente (Nombre, Usuario, Password, Telefono, Celular, Email, nIDCliente, nIDRol, FechaModificacion, FechaCreacion, Observaciones, bEstado) VALUES 
           (:Nombre, :Usuario, :Password, :Telefono, :Celular, :Email, :nIDCliente, :nIDRol, :FechaModificacion, :FechaCreacion, :Observaciones, 
           :bEstado)";
   try{
     $db     = new db();
     $db     = $db->connection();
     $result = $db->prepare($sql);
 
     $result->bindParam(':Nombre', $Nombre);
     $result->bindParam(':Usuario', $Usuario);
     $result->bindParam(':Password', $Password);
     $result->bindParam(':Telefono', $Telefono);
     $result->bindParam(':Celular', $Celular);
     $result->bindParam(':Email', $Email);
     $result->bindParam(':nIDCliente', $nIDCliente);
     $result->bindParam(':nIDRol', $nIDRol);
     $result->bindParam(':FechaCreacion', $FechaCreacion);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':Observaciones', $Observaciones);
     $result->bindParam(':bEstado', $bEstado);
 
     $result->execute();
     echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);    
 
     $result = null;
     $db     = null;
   }catch(PDOException $e){
     echo '{"error" : {"text":'.$e->getMessage().'}';
   }
});

// UPDATE CONTACT CUSTOMER
$app->post('/contact_customer/update/{id}', function(Request $request, Response $response){
    $id       = $request->getAttribute('id');
    $Nombre         = $request->getParam('Nombre');
    $Usuario        = $request->getParam('Usuario');
    $Telefono       = $request->getParam('Telefono');
    $Celular        = $request->getParam('Celular');
    $Email          = $request->getParam('Email');
    $nIDRol  	    = $request->getParam('nIDRol');
    $FechaModificacion = date('Y-m-d H:i:s');
    $Observaciones     = 'Contacto de la empresa modificado - ' . date('Y-m-d H:i:s');
   

  $sql = "UPDATE tbl_cat_contacto_cliente SET
              Nombre   = :Nombre,
              Usuario  = :Usuario,
              Telefono = :Telefono,
              Celular  = :Celular,
              Email    = :Email,
              nIDRol   = :nIDRol,
              FechaModificacion = :FechaModificacion,
              Observaciones = :Observaciones
      WHERE nIDContactoCliente = $id";
    
  try{
   $db = new db();
   $db = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':Nombre', $Nombre);
   $result->bindParam(':Usuario', $Usuario);
   $result->bindParam(':Telefono', $Telefono);
   $result->bindParam(':Celular', $Celular);
   $result->bindParam(':Email', $Email);
   $result->bindParam(':nIDRol', $nIDRol);
   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   echo '{"error" : {"text":'.$e->getMessage().'}';
  }
});

// DELETE CONTACT CUSTOMER
$app->get('/contact_customer/delete/{id}', function(Request $request, Response $response){
    $id             = $request->getAttribute('id');
    $bEstado           = 0;
    $FechaModificacion = date('Y-m-d H:i:s');
    $Observaciones     = 'Contacto de la empresa eliminado temporalmente - ' . date('Y-m-d H:i:s');
   

  $sql = "UPDATE tbl_cat_contacto_cliente SET
              bEstado  = :bEstado,
              FechaModificacion = :FechaModificacion,
              Observaciones = :Observaciones
      WHERE nIDContactoCliente = $id";
    
  try{
   $db = new db();
   $db = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':bEstado', $bEstado);
   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);

   $result = null;
   $db     = null;
   
  }catch(PDOException $e){
   echo '{"error" : {"text":'.$e->getMessage().'}';
  }
});

// FORMATS COMBOS
// add of formats combos
$app->post('/formatscombo/add', function(Request $request, Response $response){
    $nIDFormato_Conf            = $request->getParam('nIDFormato_Conf');
    $ID                         = $request->getParam('ID');
    $Valor                      = $request->getParam('Valor');
    $Tipo                       = $request->getParam('Tipo');
    $Bd                         = $request->getParam('Bd');
    $Host                       = $request->getParam('Host');
    $Usuario                    = $request->getParam('Usuario');
    $Password                   = $request->getParam('Password');
    $Tabla                      = $request->getParam('Tabla');
    $FechaModificacion          = date('Y-m-d H:i:s');
    $FechaCreacion              = date('Y-m-d H:i:s');
    $Observaciones              = "Se agrego nuevo combo - " . date('Y-m-d H:i:s');
    $bEstado                    = 1;

    $sql = "INSERT INTO tbl_formato_combo (nIDFormato_Conf, ID, Valor, Tipo, Bd, Host, Usuario, Password, Tabla, FechaModificacion, FechaCreacion, Observaciones, bEstado) VALUES (:nIDFormato_Conf, :ID, :Valor, :Tipo, :Bd, :Host, :Usuario, :Password, :Tabla, :FechaModificacion, :FechaCreacion, :Observaciones, :bEstado)";

    try{
      $db         = new db();
      $db         = $db->connection();
      $result     = $db->prepare($sql);

      $result->bindParam(':nIDFormato_Conf', $nIDFormato_Conf);
      $result->bindParam(':ID', $ID);
      $result->bindParam(':Valor', $Valor);
      $result->bindParam(':Tipo', $Tipo);
      $result->bindParam(':Bd', $Bd);
      $result->bindParam(':Host', $Host);
      $result->bindParam(':Usuario', $Usuario);
      $result->bindParam(':Password', $Password);
      $result->bindParam(':Tabla', $Tabla);
      $result->bindParam(':FechaModificacion', $FechaModificacion);
      $result->bindParam(':FechaCreacion', $FechaCreacion);
      $result->bindParam(':Observaciones', $Observaciones);
      $result->bindParam(':bEstado', $bEstado);

      $result->execute();
      echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);

      $result = null;
      $db     = null;

    }catch(PDOException $e){
      echo '{"error" : {"text":' . $e->getMessage() . '}';
    }
});

$app->get('/formatscombo/select/{id}', function(Request $request, Response $response){

    $id = $request->getAttribute('id');
    $sql = "SELECT * FROM tbl_formato_combo WHERE bEstado = 1 and nIDFormato_Conf = $id";

    try{
      $db         = new db();
      $db         = $db->connection();
      $result     = $db->query($sql);

      if($result->rowCount() > 0){
          $formatscombo = $result->fetchAll(PDO::FETCH_OBJ);
          echo json_encode($formatscombo, JSON_UNESCAPED_UNICODE);          
      }else{
        echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
      }

      $result = null;
      $db     = null;

    }catch(PDOException $e){
          echo '{"error" : {"text":'.$e->getMessage().'}';
    }

});

$app->get('/formatscombo/delete/{id}', function(Request $request, Response $response){
    $id = $request->getAttribute('id');
    $FechaModificacion = date('Y-m-d H:i:s');
    $Observaciones     = "La configuración ha sido eliminada temporalmente - " . date('Y-m-d H:i:s');
    $bEstado           = 0;
    $sql = "UPDATE tbl_formato_combo set 
            FechaModificacion = :FechaModificacion,
            Observaciones     = :Observaciones,
            bEstado           = :bEstado
            WHERE nIDFormato_Combo = $id";

    try{
      $db         = new db();
      $db         = $db->connection();
      $result     = $db->prepare($sql);

      $result->bindParam(':FechaModificacion', $FechaModificacion);
      $result->bindParam(':Observaciones', $Observaciones);
      $result->bindParam(':bEstado', $bEstado);

      $result->execute();
      echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);


      $result = null;
      $db     = null;

    }catch(PDOException $e){
          echo '{"error" : {"text":'.$e->getMessage().'}';
    }

});

// SERVICES
// LISTA DE TODOS LOS SERVICIOS

$app->get('/services', function(Request $request, Response $response){
    
    $sql = "SELECT * FROM tbl_cat_servicio WHERE bEstado = 1";
    
    try{
    
      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
    
        $services = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($services, JSON_UNESCAPED_UNICODE);
    
      }else {
    
        echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
    
      }
    
      $result = null;
      $db     = null;
   
    }catch(PDOException $e){
   
      echo '{"error" : {"text":'.$e->getMessage().'}';
   
    }

});

 // CREAR UN NUEVO SERVICIO
$app->post('/services/add', function(Request $request, Response $response){

    
    $nIDServicio            = $request->getParam('nIDServicio');
    $Servicio               = $request->getParam('Servicio');
    $Descripcion            = $request->getParam('Descripcion');
    $FechaModificacion      = date('Y-m-d H:i:s');
    $FechaCreacion          = date('Y-m-d H:i:s');
    $Observaciones          = 'Se agregó un nuevo servicio - ' . date('Y-m-d H:i:s');
    $bEstado                = 1;
   
   $sql = "INSERT INTO tbl_cat_servicio (nIDServicio, Servicio, Descripcion, FechaModificacion, FechaCreacion, Observaciones, bEstado) VALUES 
           (:nIDServicio, :Servicio, :Descripcion, :FechaModificacion, :FechaCreacion, :Observaciones, :bEstado)";
   try{

     $db     = new db();
     $db     = $db->connection();
     $result = $db->prepare($sql);
 
     
     $result->bindParam(':nIDServicio', $nIDServicio);
     $result->bindParam(':Servicio', $Servicio);
     $result->bindParam(':Descripcion', $Descripcion);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':FechaCreacion', $FechaCreacion);
     $result->bindParam(':Observaciones', $Observaciones);
     $result->bindParam(':bEstado', $bEstado);
 
     $result->execute();
     echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  
 
     $result = null;
     $db     = null;

   }catch(PDOException $e){

     echo '{"error" : {"text":'.$e->getMessage().'}';
   
   }

}); 

// ACTUALIZAR SERVICIO
$app->post('/services/update/{id}', function(Request $request, Response $response){
    $nIDServicio            = $request->getAttribute('id');
    $Servicio               = $request->getParam('Servicio');
    $Descripcion            = $request->getParam('Descripcion');
    $FechaModificacion      = date('Y-m-d H:i:s');
    $Observaciones          = 'El servicio ha sido actualizado - ' . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_cat_servicio SET
         
         Servicio           = :Servicio,
         Descripcion        = :Descripcion,
         FechaModificacion  = :FechaModificacion,
         Observaciones      = :Observaciones
      WHERE nIDServicio     = $nIDServicio";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

     $result->bindParam(':Servicio', $Servicio);
     $result->bindParam(':Descripcion', $Descripcion);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':Observaciones', $Observaciones);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 

// ELIMINAR TEMPORALMENTE UN SERVICIO
$app->get('/services/deleted/{id}', function(Request $request, Response $response){
  $nIDServicio           = $request->getAttribute('id');
  $bEstado               = 0;
  $FechaModificacion     = date('Y-m-d H:i:s');
  $Observaciones         = "El servicio ha sido eliminado temporalmente - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_cat_servicio SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDServicio           = $nIDServicio";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);


   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 

// ACTIVAR EL DOCUMENTO
$app->get('/services/active/{id}', function(Request $request, Response $response){
  $nIDServicio           = $request->getAttribute('id');
  $bEstado               = 1;
  $FechaModificacion     = date('Y-m-d H:i:s');
  $Observaciones         = "El servicio ha sido activada - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_cat_servicio SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDServicio           = $nIDServicio";
    
  try{
   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

});


// MOSTRAR SERVICIO POR ID

$app->get('/services/search/{id}', function(Request $request, Response $response){
    $nIDServicio = $request->getAttribute('id');
    
    $sql = "SELECT * FROM tbl_cat_servicio WHERE nIDServicio = $nIDServicio and bEstado = 1";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $services = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($services, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE");
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

}); 

// MOSTRAR SERVICIO POR NOMBRE DEL SERVICIO
$app->get('/services/searchname/{search}', function(Request $request, Response $response){
    $search = $request->getAttribute('search');
    
    $sql = "SELECT * FROM tbl_cat_servicio WHERE Servicio LIKE '%$search%' and bEstado = 1";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $services = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($services, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE");
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

});

//USUARIOS
// LISTA DE USUARIOS
$app->get('/usersIberoil', function(Request $request, Response $response){
    
    $sql = "SELECT nIDUsuario,Usuario,Clave,Nombre,Direccion,RFC,Telefono,Celular,Email,Activo,Extension,nIDEmpresa,FechaModificacion,
    FechaCreacion,Observaciones,bEStado  FROM tbl_usuarios WHERE bEstado = 1";
    
    try{
    
      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
    
        $users = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($users, JSON_UNESCAPED_UNICODE);
    
      }else {
    
        echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
    
      }
    
      $result = null;
      $db     = null;
   
    }catch(PDOException $e){
   
      echo '{"error" : {"text":'.$e->getMessage().'}';
   
    }

});

 // CREAR UN NUEVO USUARIO
$app->post('/usersIberoil/add', function(Request $request, Response $response){

    
    $nIDUsuario             = $request->getParam('nIDUsuario');
    $Usuario                = $request->getParam('Usuario');
    $Password               = $request->getParam('Password');
    $Clave                  = $request->getParam('Clave');
    $Nombre                 = $request->getParam('Nombre');
    $Direccion              = $request->getParam('Direccion');
    $RFC                    = $request->getParam('RFC');
    $Telefono               = $request->getParam('Telefono');
    $Celular                = $request->getParam('Celular');
    $Email                  = $request->getParam('Email');
    $Imagen                 = $request->getParam('Imagen');
    $Activo                 = $request->getParam('Activo');
    $nIDRol                 = $request->getParam('nIDRol');
    $nIDPuesto              = $request->getParam('nIDPuesto');
    $nIDDepartamento        = $request->getParam('nIDDepartamento');
    $Extension              = $request->getParam('Extension');
    $nIDEmpresa             = $request->getParam('nIDEmpresa');
    $FechaModificacion      = date('Y-m-d H:i:s');
    $FechaCreacion          = date('Y-m-d H:i:s');
    $Observaciones          = 'Se agregó un nuevo usuario iberoil - ' . date('Y-m-d H:i:s');
    $bEstado                = 1;
   
   $sql = "INSERT INTO tbl_usuarios (nIDUsuario, Usuario, Password, Clave, Nombre, Direccion, RFC, Telefono, Celular, Email,Imagen, Activo, nIDRol, nIDPuesto, nIDDepartamento, Extension, nIDEmpresa, FechaModificacion, FechaCreacion, Observaciones, bEstado) VALUES 
           (:nIDUsuario, :Usuario, :Password, :Clave, :Nombre, :Direccion, :RFC, :Telefono, :Celular, :Email, :Imagen, :Activo, :nIDRol, :nIDPuesto, :nIDDepartamento, :Extension, :nIDEmpresa, :FechaModificacion, :FechaCreacion, :Observaciones, :bEstado)";
   try{

     $db     = new db();
     $db     = $db->connection();
     $result = $db->prepare($sql);
 
     
     $result->bindParam(':nIDUsuario', $nIDUsuario);
     $result->bindParam(':Usuario', $Usuario);
     $result->bindParam(':Password', $Password);
     $result->bindParam(':Clave', $Clave);
     $result->bindParam(':Nombre', $Nombre);
     $result->bindParam(':Direccion', $Direccion);
     $result->bindParam(':RFC', $RFC);
     $result->bindParam(':Telefono', $Telefono);
     $result->bindParam(':Celular', $Celular);
     $result->bindParam(':Email', $Email);
     $result->bindParam(':Imagen', $Imagen);
     $result->bindParam(':Activo', $Activo);
     $result->bindParam(':nIDRol', $nIDRol);
     $result->bindParam(':nIDPuesto', $nIDPuesto);
     $result->bindParam(':nIDDepartamento', $nIDDepartamento);
     $result->bindParam(':Extension', $Extension);
     $result->bindParam(':nIDEmpresa', $nIDEmpresa);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':FechaCreacion', $FechaCreacion);
     $result->bindParam(':Observaciones', $Observaciones);
     $result->bindParam(':bEstado', $bEstado);
 
     $result->execute();
     echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  
 
     $result = null;
     $db     = null;

   }catch(PDOException $e){

     echo '{"error" : {"text":'.$e->getMessage().'}';
   
   }

}); 

// ACTUALIZAR USUARIO
$app->post('/usersIberoil/update/{id}', function(Request $request, Response $response){
    $nIDUsuario             = $request->getAttribute('id');
    $Usuario                = $request->getParam('Usuario');
    $Clave                  = $request->getParam('Clave');
    $Nombre                 = $request->getParam('Nombre');
    $Direccion              = $request->getParam('Direccion');
    $RFC                    = $request->getParam('RFC');
    $Telefono               = $request->getParam('Telefono');
    $Celular                = $request->getParam('Celular');
    $Email                  = $request->getParam('Email');
    $Imagen                 = $request->getParam('Imagen');
    $Activo                 = $request->getParam('Activo');
    $nIDRol                 = $request->getParam('nIDRol');
    $nIDPuesto              = $request->getParam('nIDPuesto');
    $nIDDepartamento        = $request->getParam('nIDDepartamento');
    $Extension              = $request->getParam('Extension');
    $nIDEmpresa             = $request->getParam('nIDEmpresa');
    $FechaModificacion      = date('Y-m-d H:i:s');
    $Observaciones          = 'El usuario iberoil ha sido actualizado - ' . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_usuarios SET
         
         Usuario            = :Usuario,
         Clave              = :Clave,
         Nombre             = :Nombre,
         Direccion          = :Direccion,
         RFC                = :RFC,
         Telefono           = :Telefono,
         Celular            = :Celular,
         Email              = :Email,
         Imagen             = :Imagen,
         Activo             = :Activo,
         nIDRol             = :nIDRol,
         nIDPuesto          = :nIDPuesto,
         nIDDepartamento    = :nIDDepartamento,
         Extension          = :Extension,
         nIDEmpresa         = :nIDEmpresa,
         FechaModificacion  = :FechaModificacion,
         Observaciones      = :Observaciones
      WHERE nIDUsuario     = $nIDUsuario";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

     $result->bindParam(':Usuario', $Usuario);
     $result->bindParam(':Clave', $Clave);
     $result->bindParam(':Nombre', $Nombre);
     $result->bindParam(':Direccion', $Direccion);
     $result->bindParam(':RFC', $RFC);
     $result->bindParam(':Telefono', $Telefono);
     $result->bindParam(':Celular', $Celular);
     $result->bindParam(':Email', $Email);
     $result->bindParam(':Imagen', $Imagen);
     $result->bindParam(':Activo', $Activo);
     $result->bindParam(':nIDRol', $nIDRol);
     $result->bindParam(':nIDPuesto', $nIDPuesto);
     $result->bindParam(':nIDDepartamento', $nIDDepartamento);
     $result->bindParam(':Extension', $Extension);
     $result->bindParam(':nIDEmpresa', $nIDEmpresa);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':Observaciones', $Observaciones);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 

// ELIMINAR TEMPORALMENTE UN USUARIO
$app->get('/usersIberoil/deleted/{id}', function(Request $request, Response $response){
  $nIDUsuario            = $request->getAttribute('id');
  $bEstado               = 0;
  $FechaModificacion     = date('Y-m-d H:i:s');
  $Observaciones         = "El usuario iberoil ha sido eliminado temporalmente - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_usuarios SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDUsuario            = $nIDUsuario";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);


   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 

// ACTIVAR EL USUARIO
$app->get('/usersIberoil/active/{id}', function(Request $request, Response $response){
  $nIDUsuario            = $request->getAttribute('id');
  $bEstado               = 1;
  $FechaModificacion     = date('Y-m-d H:i:s');
  $Observaciones         = "El usuario iberoil ha sido activado - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_usuarios SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDUsuario            = $nIDUsuario";
    
  try{
   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

});


// MOSTRAR USUARIO POR ID

$app->get('/usersIberoil/search/{id}', function(Request $request, Response $response){
    $nIDUsuario = $request->getAttribute('id');
    
    $sql = "SELECT nIDUsuario,Usuario,Clave,Nombre,Direccion,RFC,Telefono,Celular,Email,Activo,Extension,nIDEmpresa,FechaModificacion,
    FechaCreacion,Observaciones,bEStado FROM tbl_usuarios WHERE nIDUsuario = $nIDUsuario and bEstado = 1";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $users = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($users, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE");
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

}); 

// MOSTRAR USUARIO POR NOMBRE
$app->get('/usersIberoil/searchname/{search}', function(Request $request, Response $response){
    $search = $request->getAttribute('search');
    
    $sql = "SELECT nIDUsuario,Usuario,Clave,Nombre,Direccion,RFC,Telefono,Celular,Email,Activo,Extension,nIDEmpresa,FechaModificacion,
    FechaCreacion,Observaciones,bEStado FROM tbl_usuarios WHERE Nombre LIKE '%$search%' and bEstado = 1";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $users = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($users, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE");
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

});



// FLUJOS
// LISTADO DE TODOS LOS FLUJOS
$app->get('/flow', function(Request $request, Response $response){

    $sql = "SELECT flu.nIDFlujo,flu.nIDFormato,flu.TipoOrden,tbl_formato.Formato, flu.FechaModificacion,flu.FechaCreacion,flu.Observaciones,flu.bEstado 
    FROM tbl_flujo as flu INNER JOIN tbl_formato ON tbl_formato.nIDFormato = flu.nIDFormato WHERE flu.bEstado = 1";

    try{

        $db             = new db();
        $db             = $db->connection();
        $result         = $db->query($sql);

        if( $result->rowCount() > 0){
            $flow       = $result->fetchAll(PDO::FETCH_OBJ);
            echo json_encode($flow, JSON_UNESCAPED_UNICODE);
        }else{
            echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
        }

        $result = null;
        $db     = null;
    }catch(PDOException $e){
       echo '{"error" : {"text" : '.$e->getMessage().'}';
    }
});

//CREAR UN NUEVO FLUJO
$app->post('/flow/add', function(Request $request, Response $response){

    $nIDFlujo            = $request->getParam('nIDFlujo');
    $nIDFormato          = $request->getParam('nIDFormato');
    $TipoOrden           = $request->getParam('TipoOrden');
    $FechaModificacion   = date('Y-m-d H:i:s');
    $FechaCreacion       = date('Y-m-d H:i:s');
    $Observaciones       = 'Se ha agregado un nuevo flujo - ' . date('Y-m-d H:i:s');
    $bEstado             = 1;

    $sql = "INSERT INTO tbl_flujo (nIDFlujo, nIDFormato, TipoOrden, FechaModificacion, FechaCreacion, Observaciones, bEstado) 
    VALUES (:nIDFlujo, :nIDFormato, :TipoOrden, :FechaModificacion, :FechaCreacion, :Observaciones, :bEstado)";

    try{

        $db         = new db();
        $db         = $db->connection();
        $result     = $db->prepare($sql);


        $result->bindParam(':nIDFlujo', $nIDFlujo);
        $result->bindParam(':nIDFormato', $nIDFormato);
        $result->bindParam(':TipoOrden', $TipoOrden);
        $result->bindParam(':FechaModificacion', $FechaModificacion);
        $result->bindParam(':FechaCreacion', $FechaCreacion);
        $result->bindParam(':Observaciones', $Observaciones);
        $result->bindParam(':bEstado', $bEstado);

        $result->execute();
        echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);

        $result = null;
        $db     = null;

    }catch(PDOException $e){

        echo '{"error" : {"text":'.$e->getMessage().'}';
    }
});

// ACTUALIZAR FLUJO
/*$app->post('/flow/update/{id}', function(Request $request, Response $response){
    $nIDFlujo             = $request->getAttribute('id');
    $nIDFormato           = $request->getParam('nIDFormato');
    $TipoOrden            = $request->getParam('TipoOrden');
    $FechaModificacion    = date('Y-m-d H:i:s');
    $Observaciones        = "Actualización del flujo - " . date('Y-m-d H:i:s');

    $sql = "UPDATE tbl_flujo SET 
            nIDFormato        = :nIDFormato,
            TipoOrden         = :TipoOrden,
            FechaModificacion = :FechaModificacion,
            Observaciones     = :Observaciones
            WHERE nIDFlujo    = $nIDFlujo";

    try{

        $db       = new db() ;
        $db       = $db->connection();
        $result   = $db->prepare($sql);

        $result->bindParam(':nIDFormato', $nIDFormato);
        $result->bindParam(':TipoOrden', $TipoOrden);
        $result->bindParam(':FechaModificacion', $FechaModificacion);
        $result->bindParam(':Observaciones', $Observaciones);

        $result->execute();
        echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);

        $result = null;
        $db     = null;        

    }catch (PDOException $e){
           <echo '{"error" : {"text":'.$e->getMessage().'}';
    }

});*/

// ELIMINAR TEMPORALMENTE UN FLUJO
$app->get('/flow/deleted/{id}', function(Request $request, Response $response){
  $nIDFlujo             = $request->getAttribute('id');
  $bEstado               = 0;
  $FechaModificacion     = date('Y-m-d H:i:s');
  $Observaciones         = "El flujo ha sido eliminado temporalmente - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_flujo SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDFlujo              = $nIDFlujo";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);


   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 

// ACTIVAR EL FLUJO
/*$app->get('/flow/active/{id}', function(Request $request, Response $response){
  $nIDFlujo              = $request->getAttribute('id');
  $bEstado               = 1;
  $FechaModificacion     = date('Y-m-d H:i:s');
  $Observaciones         = "El flujo ha sido activado - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_flujo SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDFlujo              = $nIDFlujo";
    
  try{
   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

});

// MOSTRAR FLUJO POR ID
$app->get('/flow/search/{id}', function(Request $request, Response $response){
    $nIDFlujo = $request->getAttribute('id');
    
    $sql = "SELECT * FROM tbl_flujo WHERE nIDFlujo = $nIDFlujo and bEstado = 1";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $flow = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($flow, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE");
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

}); 

// MOSTRAR FLUJO POR NOMBRE
$app->get('/flow/searchname/{search}', function(Request $request, Response $response){
    $search = $request->getAttribute('search');
    
    $sql = "SELECT * FROM tbl_flujo WHERE nIDFlujo LIKE '%$search%' and bEstado = 1";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $flow = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($flow, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE");
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

});*/

//CONFIGURACIÓN DE FLUJO
// LISTA DE USUARIOS
$app->get('/conf_flow/getFlowID/{id}', function(Request $request, Response $response){
    
    $id = $request->getAttribute('id');
    $sql = "SELECT tbl_conf_flujo.nIDFlujo,tbl_conf_flujo.nIDConf_Flujo,tbl_conf_flujo.nIDFormato,
    tbl_conf_flujo.FechaModificacion,tbl_conf_flujo.FechaCreacion,tbl_conf_flujo.Observaciones,tbl_conf_flujo.bEstado,tbl_formato.Formato  FROM tbl_conf_flujo INNER JOIN tbl_formato ON tbl_formato.nIDFormato = tbl_conf_flujo.nIDFormato WHERE tbl_conf_flujo.nIDFlujo = $id and
     tbl_conf_flujo.bEstado = 1";
    
    try{
    
      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
    
        $conf_flow = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($conf_flow, JSON_UNESCAPED_UNICODE);
    
      }else {
    
        echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
    
      }
    
      $result = null;
      $db     = null;
   
    }catch(PDOException $e){
   
      echo '{"error" : {"text":'.$e->getMessage().'}';
   
    }

});

 // CREAR UN NUEVO USUARIO
$app->post('/conf_flow/add', function(Request $request, Response $response){

    
    $nIDConf_Flujo          = $request->getParam('nIDConf_Flujo');
    $nIDFlujo               = $request->getParam('nIDFlujo');
    $nIDFormato             = $request->getParam('nIDFormato');
    $FechaModificacion      = date('Y-m-d H:i:s');
    $FechaCreacion          = date('Y-m-d H:i:s');
    $Observaciones          = 'Se agregó una nueva configuración al flujo - ' . date('Y-m-d H:i:s');
    $bEstado                = 1;
   
   $sql = "INSERT INTO tbl_conf_flujo (nIDConf_Flujo, nIDFlujo, nIDFormato, FechaModificacion, FechaCreacion, Observaciones, bEstado) 
   VALUES (:nIDConf_Flujo, :nIDFlujo, :nIDFormato, :FechaModificacion, :FechaCreacion, :Observaciones, :bEstado)";
   try{

     $db     = new db();
     $db     = $db->connection();
     $result = $db->prepare($sql);
 
     
     $result->bindParam(':nIDConf_Flujo', $nIDConf_Flujo);
     $result->bindParam(':nIDFlujo', $nIDFlujo);
     $result->bindParam(':nIDFormato', $nIDFormato);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':FechaCreacion', $FechaCreacion);
     $result->bindParam(':Observaciones', $Observaciones);
     $result->bindParam(':bEstado', $bEstado);
 
     $result->execute();
     echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  
 
     $result = null;
     $db     = null;

   }catch(PDOException $e){

     echo '{"error" : {"text":'.$e->getMessage().'}';
   
   }

}); 

// ACTUALIZAR CONFIGURACION DEL FLUJO
/*$app->post('/conf_flow/update/{id}', function(Request $request, Response $response){
    $nIDConf_Flujo          = $request->getAttribute('id');
    $nIDFlujo               = $request->getParam('nIDFlujo');
    $nIDFormato             = $request->getParam('nIDFormato');
    $FechaModificacion      = date('Y-m-d H:i:s');
    $Observaciones          = 'La configuración del flujo ha sido actualizado - ' . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_conf_flujo SET
         
         nIDFlujo           = :nIDFlujo,
         nIDFormato         = :nIDFormato,
         FechaModificacion  = :FechaModificacion,
         Observaciones      = :Observaciones
      WHERE nIDConf_Flujo   = $nIDConf_Flujo";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

     $result->bindParam(':nIDFlujo', $nIDFlujo);
     $result->bindParam(':nIDFormato', $nIDFormato);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':Observaciones', $Observaciones);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   <echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); */

// ELIMINAR TEMPORALMENTE UNA CONFIGURACIÓN
$app->get('/conf_flow/deleted/{id}', function(Request $request, Response $response){
  $nIDConf_Flujo         = $request->getAttribute('id');
  $bEstado               = 0;
  $FechaModificacion     = date('Y-m-d H:i:s');
  $Observaciones         = "La configuración del flujo ha sido eliminado temporalmente - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_conf_flujo SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDConf_Flujo         = $nIDConf_Flujo";
    
  try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);


   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

}); 

// ACTIVAR LA CONFIGURACIÓN DEL FLUJO
/*$app->get('/conf_flow/active/{id}', function(Request $request, Response $response){
  $nIDConf_Flujo         = $request->getAttribute('id');
  $bEstado               = 1;
  $FechaModificacion     = date('Y-m-d H:i:s');
  $Observaciones         = "La configuración del flujo ha sido activado - " . date('Y-m-d H:i:s');

  $sql = "UPDATE tbl_conf_flujo SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDConf_Flujo         = $nIDConf_Flujo";
    
  try{
   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);

   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }

});*/


// MOSTRAR CONFIGURACION DEL FLUJO POR ID

$app->post('/conf_flow/search/date', function(Request $request, Response $response){
    $nIDDocumento_Captura = $request->getParam('nIDDocumento_Captura');
    $TipoOrden = $request->getParam('TipoOrden');
    
    $sql = "SELECT docCapt.nIDDocumento_Captura,confFlu.nIDConf_Flujo,doc.nIDDocumento,flu.nIDFlujo,form.Formato,flu.TipoOrden,confFlu.nIDFormato
    FROM tbl_documentos_captura as docCapt
    INNER JOIN tbl_documentos as doc ON doc.nIDDocumento = docCapt.nIDDocumento
    INNER JOIN tbl_formato as form ON form.nIDFormato = doc.nIDFormato
    INNER JOIN tbl_flujo as flu ON flu.nIDFormato = form.nIDFormato
    INNER JOIN tbl_conf_flujo as confFlu ON confFlu.nIDFlujo = flu.nIDFlujo
    WHERE docCapt.nIDDocumento_Captura = $nIDDocumento_Captura and flu.TipoOrden like '%$TipoOrden%' and confFlu.bEstado = 1";
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
      if ($result->rowCount() > 0){
      
        $conf_flow = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($conf_flow, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE");
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

}); 

// MOSTRAR CONFIGURACIÓN DEL FLUJO POR NOMBRE
$app->get('/conf_flow/searchflow/{id}', function(Request $request, Response $response){
    
    $nIDFlujo = $request->getAttribute('id');
    
    $sql = "SELECT conFlu.nIDFormato,conFlu.nIDFlujo,doc.Documento,doc.nIDDocumento 
    FROM tbl_conf_flujo as conFlu 
    INNER JOIN tbl_documentos as doc ON doc.nIDFormato = conFlu.nIDFormato
    WHERE conFlu.nIDFlujo = $nIDFlujo and conFlu.bEstado = 1";
    
    try{

      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);
  
      if ($result->rowCount() > 0){
      
        $conf_flow = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($conf_flow, JSON_UNESCAPED_UNICODE);
      
      }else {
      
        echo json_encode("FALSE");
      
      }

      $result = null;
      $db     = null;
    
    }catch(PDOException $e){
    
      echo '{"error" : {"text":'.$e->getMessage().'}';
    
    }

});

//ALTA DE NUEVA CAPTURA DEL DOCUMENTO
$app->post('/document_Capt/add', function(Request $request, Response $response){
    $nIDDocumento_Captura       = $request->getParam('nIDDocumento_Captura');
    $nIDDocumento               = $request->getParam('nIDDocumento');
    $Folio                      = $request->getParam('Folio');
    $Fecha                      = date('Y-m-d H:i:s');
    $nIDUsuario                 = $request->getParam('nIDUsuario');
    $FechaModificacion          = date('Y-m-d H:i:s');
    $FechaCreacion              = date('Y-m-d H:i:s');
    $Observaciones              = 'Nueva captura del documento ' . date('Y-m-d H:i:s');
    $bEstado                    = 1;

    $sql = "INSERT INTO tbl_documentos_captura (nIDDocumento_Captura, nIDDocumento, Folio, Fecha, nIDUsuario, FechaModificacion, FechaCreacion, Observaciones, bEstado) VALUES (:nIDDocumento_Captura, :nIDDocumento, :Folio, :Fecha, :nIDUsuario, :FechaModificacion, :FechaCreacion, :Observaciones, :bEstado)";

    try{
      $db     = new db();
      $db     = $db->connection();
      $result  = $db->prepare($sql);

      $result->bindParam(':nIDDocumento_Captura', $nIDDocumento_Captura);
      $result->bindParam(':nIDDocumento', $nIDDocumento);
      $result->bindParam(':Folio', $Folio);
      $result->bindParam(':Fecha', $Fecha);
      $result->bindParam(':nIDUsuario', $nIDUsuario);
      $result->bindParam(':FechaModificacion', $FechaModificacion);
      $result->bindParam(':FechaCreacion', $FechaCreacion);
      $result->bindParam(':Observaciones', $Observaciones);
      $result->bindParam(':bEstado', $bEstado);

      if($result->execute()){
        echo json_encode($db->lastInsertId(), JSON_UNESCAPED_UNICODE);   
     }else{
        echo json_encode("FALSE", JSON_UNESCAPED_UNICODE);
     }

      $result = null;
      $db     = null;

    }catch(PDOException $e){
        echo '{"error" : { "text" :'. $e->getMessage() . '}';
    }
});


// MOSTRAR LAS CAPTURAS DEL DOCUMENTO
$app->get('/document_Capt', function(Request $request, Response $response){

    $sql = "SELECT doc.nIDDocumento_Captura,doc.nIDDocumento,doc.Folio,doc.Fecha,doc.nIDUsuario,doc.FechaModificacion,doc.FechaCreacion,
    doc.Observaciones,doc.bEstado,d.Documento,user.Nombre,f.Formato,f.nIDFormato FROM tbl_documentos_captura as doc
    Inner Join tbl_documentos as d ON d.nIDDocumento = doc.nIDDocumento
    Inner Join tbl_usuarios as user ON user.nIDUsuario = doc.nIDUsuario
    Inner Join tbl_formato as f ON f.nIDFormato = d.nIDFormato 
    WHERE doc.bEstado = 1";

    try{
      $db     = new db();
      $db     = $db->connection();
      $result = $db->query($sql);

      if($result->rowCount() > 0){
          $doc_capt = $result->fetchAll(PDO::FETCH_OBJ);
          echo json_encode($doc_capt, JSON_UNESCAPED_UNICODE);          
      }else{
          echo json_encode("FALSE");
      }

      $result  = null;
      $db      = null;
    }catch(PDOException $e){
      echo '{"error" : {"text" : ' . $e->getMessage() . '}';
    }
});

// Mostrar documento por vista de documents capture
$app->get('/document_Capt/getId/{id}', function(Request $request, Response $response){
  $id       = $request->getAttribute('id');

  $sql      = "SELECT f.nIDFormato, f.Folio,f.Formato,f.Descripcion,f.Titulo1,f.Titulo2,f.Titulo3,f.FechaPiePagina,
              f.nIDUsuario,f.bEstado,f.Logo FROM tbl_formato as f 
              INNER JOIN tbl_documentos as d ON d.nIDFormato = f.nIDFormato
              INNER JOIN tbl_documentos_captura as dc ON dc.nIDDocumento = d.nIDDocumento
              WHERE dc.nIDDocumento_Captura = $id ";

  try{
    $db         = new db();
    $db         = $db->connection();
    $result     = $db->query($sql);

    if( $result->rowCount() > 0 ){
        $doc_Cap = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($doc_Cap, JSON_UNESCAPED_UNICODE);
    }else{
        echo json_encode("FALSE");
    }

    $result = null;
    $db     = null;
  }catch(PDOException $e){
    echo '{"error" : {"text" : ' . $e->getMessage() . '}';
  }
});

//Eliminar temporalmente la captura del documento
$app->get('/document_Capt/delete/{id}', function(Request $request, Response $response){
    $nIDDocumento_Captura          = $request->getAttribute('id');
    $FechaModificacion             = date('Y-m-d H:i:s');
    $Observaciones                 = "Eliminacion temporal del documento de captura - " . date('Y-m-d H:i:s');
    $bEstado                       = 0;


    $sql = "UPDATE tbl_documentos_captura SET
         FechaModificacion        = :FechaModificacion,
         Observaciones            = :Observaciones,
         bEstado                  = :bEstado
      WHERE nIDDocumento_Captura  = $nIDDocumento_Captura";

    try{

   $db     = new db();
   $db     = $db->connection();
   $result = $db->prepare($sql);

   $result->bindParam(':FechaModificacion', $FechaModificacion);
   $result->bindParam(':Observaciones', $Observaciones);
   $result->bindParam(':bEstado', $bEstado);


   $result->execute();
   echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

   $result = null;
   $db     = null;

  }catch(PDOException $e){
   
   echo '{"error" : {"text":'.$e->getMessage().'}';
  
  }
});

//ALTA DE VALORES DE LAS CONFIGURACIONES
$app->post('/document_Capt_Datos/add', function(Request $request, Response $response){
  $nIDDocumento_Datos         = $request->getParam('nIDDocumento_Datos');
  $nIDDocumento_Captura       = $request->getParam('nIDDocumento_Captura');
  $nIDFormato_conf            = $request->getParam('nIDFormato_conf');
  $Valor                      = $request->getParam('Valor');
  $FechaModificacion          = date('Y-m-d H:i:s');
  $FechaCreacion              = date('Y-m-d H:i:s');
  $Observaciones              = "Registro del campo - " . date('Y-m-d H:i:s');
  $bEstado                    = 1;

  $sql = "INSERT INTO tbl_documentos_captura_datos (nIDDocumento_Datos,nIDDocumento_Captura,nIDFormato_conf,Valor,
         FechaModificacion,FechaCreacion,Observaciones,bEstado) VALUES (:nIDDocumento_Datos,:nIDDocumento_Captura,:nIDFormato_conf,
         :Valor,:FechaModificacion,:FechaCreacion,:Observaciones,:bEstado)";

  try{
     $db       = new db();
     $db       = $db->connection();
     $result   = $db->prepare($sql);

     $result->bindParam(':nIDDocumento_Datos', $nIDDocumento_Datos);
     $result->bindParam(':nIDDocumento_Captura', $nIDDocumento_Captura);
     $result->bindParam(':nIDFormato_conf', $nIDFormato_conf);
     $result->bindParam(':Valor', $Valor);
     $result->bindParam(':FechaModificacion', $FechaModificacion);
     $result->bindParam(':FechaCreacion', $FechaCreacion);
     $result->bindParam(':Observaciones', $Observaciones);
     $result->bindParam(':bEstado', $bEstado);

     $result->execute();
     echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);

     $result = null;
     $db     = null;

  }catch(PDOException $e){
    echo '{"error" : {"text" :'. $e->getMessage() . '}';
  }
});

// Mostrar documento por vista de documents capture
$app->get('/document_Capt_Datos/getId/{id}', function(Request $request, Response $response){
  $id       = $request->getAttribute('id');

  $sql      = "SELECT * FROM tbl_documentos_captura_datos
              WHERE bEstado =1 and nIDDocumento_Captura = $id ";

  try{
    $db         = new db();
    $db         = $db->connection();
    $result     = $db->query($sql);

    if( $result->rowCount() > 0 ){
        $doc_Cap = $result->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($doc_Cap, JSON_UNESCAPED_UNICODE);
    }else{
        echo json_encode("FALSE");
    }

    $result = null;
    $db     = null;
  }catch(PDOException $e){
    echo '{"error" : {"text" : ' . $e->getMessage() . '}';
  }
});

$app->post('/document_Capt_Datos/update/{id}', function(Request $request, Response $response){

    $id = $request->getAttribute('id');
    $Valor             = $request->getParam('Valor');
    $FechaModificacion = date('Y-m-d H:i:s');
    $Observaciones     = 'Se ha modificado la captura de datos - ' . date('Y-m-d H:i:s');

    $sql  = "UPDATE tbl_documentos_captura_datos SET
    Valor = :Valor,
    FechaModificacion = :FechaModificacion,
    Observaciones     = :Observaciones
    WHERE nIDDocumento_Datos = $id";


    try{
      $db = new db();
      $db = $db->connection();
      $result = $db->prepare($sql);

      $result->bindParam(':Valor', $Valor);
      $result->bindParam(':FechaModificacion', $FechaModificacion);
      $result->bindParam(':Observaciones', $Observaciones);

      $result->execute();
      echo json_encode("TRUE", JSON_UNESCAPED_UNICODE);  

      $result = null;
      $db     = null;
    }catch(PDOException $e){
      echo '{"error" : { "text" : ' . $e->getMessage() . '}';
    }
});
