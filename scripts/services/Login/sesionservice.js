'use strict';

angular.module('abgpassportApp')

       .service('sesionServices',['$http','configService', function($http,configService){

       return {

              add : function(objeto){

                  return $http.post(configService.apiUrl + 'RelaUxS.php','accion=agregar&objeto=' + JSON.stringify(objeto),

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                  );

              },

              get : function(correo){

                   return $http.post(configService.apiUrl + 'Usuario.php','accion=buscarUsuarioCorreo&correo=' + JSON.stringify(correo),

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                   );

              },

              logout : function(GUID){

                      return $http.post(configService.apiUrl + 'RelaUxS.php','accion=modificar&GUID=' + JSON.stringify(GUID),

                        { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                      );
              }
       };

       }]);