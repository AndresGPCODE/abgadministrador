'use strict';

angular.module('abgpassportApp')
       
       .controller('SolicitadosCtrl', ['$scope','solicitudesServices','choferServices','notificationclientesServices','notificationChoferServices',

        function($scope,solicitudesServices,choferServices,notificationclientesServices,notificationChoferServices){


        	$scope.solicitud = {

        		  nIDSolicitud : '0',
        		  nIDChofer : '0'

        	};

            $scope.notificacion = {

                  nIDNotificacion : '0',
                  nIDTipoNotificacion : '10',
                  nIDCliente : '0',
                  Encabezado : 'Tu chofer ha sido asignado',
                  Body       : '',
                  Permiso    : 'SI'

            };

            $scope.notificacionC = {

                  nIDNotificacionesChofer : '0',
                  nIDTipoNotificacion : '8',
                  nIDChofer : '0',
                  Encabezado : 'Se te ha asignado un nuevo vehículo para entrega',
                  Body       : '',
                  Permiso    : 'SI'

            };

        	$scope.clean = function(){

        		  $scope.solicitud = {

        		  		nIDSolicitud : '0',
        		  		nIDChofer : '0'

        		  };

        		  $("#add_modal").modal('hide');
        	};

        	$scope.get = function(){

        		 $scope.getChoferes();

        		 $scope.solicitudes = '';

        		 $scope.Titulo = '';

        		  solicitudesServices.get().then(function(resp){

        		  				     if(resp.data == 'null'){

        		  				     			console.log('Not Found Data');
        		  				     			$scope.Titulo = 'No tienes solicitudes';

        		  				     }else{

        		  				     			$scope.solicitudes = resp.data;

        		  				     }

        		  },function(err){

        		  			console.log('error' + JSON.stringify(err));

        		  });

        	};


        	$scope.getChoferes = function(){

        		  choferServices.get().then(function(resp){

        		  				if(resp.data == 'null'){

        		  					   console.log('Not Found Data');

        		  				}else{

        		  					   $scope.choferes = resp.data;

        		  				}

        		  },function(err){

        		  		console.log('error' + JSON.stringigy(err));

        		  });

        	};

        	$scope.asignar = function(solicitud){

        		$scope.solicitud.nIDSolicitud = solicitud.nIDSolicitud;

                $scope.notificacion.nIDCliente = solicitud.nIDCliente;

                $scope.notificacionC.Body = 'Tu cliente es : ' + solicitud.Nombre + ' la dirección es ' +  solicitud.Calle + ' ' + solicitud.NoExterior + ' Fecha y Hora de entrega es  : ' + solicitud.FechaHora;

        	};

            $scope.traerChofer = function(identificador,solicitud){

                  choferServices.getChofer(identificador).then(function(resp){
                               
                               $scope.notificacion.Body     = 'Tu chofer es : ' + resp.data[0]['Nombre'];

                               $scope.notificacionC.nIDChofer = resp.data[0]['nIDChofer'];

                               $scope.update(solicitud);

                        
                  },function(err){

                        console.log('error' + JSON.stringigy(err));

                  });
            };


        	$scope.update = function(objeto){ console.log(objeto.nIDChofer); 

        		  if(objeto.nIDChofer == 'null'){

        		  		   swal.fire({

        		  		   	   title 							: 'Oops!',


        		  		   	   text								: 'Selecciona un chofer',

        		  		   	   imageUrl							: 'assets/img/advertencia.png',

        		  		   	   showCloseButton					: true,

        		  		   	   confirmButtonColor 				: '044BFD'

        		  		   });

        		  }else{

                        
                        


        		  		solicitudesServices.update(objeto).then(function(resp){

        		  							if(resp.data == "UPDATED"){

        		  								   swal.fire({

        		  								   	   title 					: 'Chofer Asignado!', 

        		  								   	   imageUrl 				: 'assets/img/completo.png',

        		  								   	   showCloseButton 			: true,

        		  								   	   confirmButtonColor 		: '#044BFD'

        		  								   });

        		  								   $scope.get();

        		  								   $scope.clean();

                                                   $scope.addNotificacion($scope.notificacion);

                                                   $scope.addNotificacionChofer($scope.notificacionC);
        		  							}else{

        		  									swal.fire({

        		  										title 				   : 'Oops!' ,

        		  										text 				   : 'No se completo la asignación, Inténtalo de nuevo',

        		  										imageUrl 			   : 'assets/img/error.png',

        		  										showCloseButton 	   : true,

        		  										confirmButtonColor     : '#044BFD'

        		  									});

        		  							}

        		  		},function(err){

        		  				console.log('error' + JSON.stringigy(err));

        		  		});

        		  }

        	};


            $scope.addNotificacion = function(objeto){

                  notificationclientesServices.add(objeto).then(function(resp){


                  },function(err){

                        console.log('error' + JSON.stringigy(err));

                  });

            };

            $scope.addNotificacionChofer = function(objeto){

                  notificationChoferServices.add(objeto).then(function(resp){


                  },function(err){

                        console.log('error' + JSON.stringigy(err));
                        
                  });

            };

        

        }]);