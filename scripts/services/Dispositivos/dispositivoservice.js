'use strict';

angular.module('abgpassportApp')

	   .service('dispositivoServices',['$http','configService',

	   	function($http,configService){

	   		return{

	   			get : function(){

	   				return $http.get(configService.apiUrl + 'Dispositivo.php?accion=buscar',

	   					{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   				);

	   			},

	   			getDispositivo : function(){

	   				return $http.get(configService.apiUrl + 'Dispositivo.php?accion=buscarDispositivo',

	   					{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   				);

	   			},

	   			add : function(objeto){

	   				return $http.post(configService.apiUrl + 'Dispositivo.php','accion=agregar&objeto=' + JSON.stringify(objeto),

	   					{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   				);

	   			},

	   			update : function(objeto){

	   				return $http.post(configService.apiUrl + 'Dispositivo.php','accion=modificar&objeto=' + JSON.stringify(objeto),

	   					{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   				);

	   			},

	   			destroy : function(id){

	   					return $http.post(configService.apiUrl + 'Dispositivo.php','accion=eliminar&id=' + JSON.stringify(id),

	   						{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   					);

	   			}

	   		};

	   	}]);