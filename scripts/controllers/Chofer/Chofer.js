'use strict';

angular.module('abgpassportApp')
       
       .controller('ChoferCtrl',['$scope','choferServices','agencyServices',

        function($scope,choferServices,agencyServices){

        var expRFC      = /^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\d]{3})?$/;

        var expEmail    = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;

        var expTelefono = /^[0-9]{10}$/;

        $scope.chofer = {

              nIDChofer             : '0',
              nIDEmpresa            : '',
              Nombre                : '',
              Usuario               : '',
              Password              : '',
              Direccion             : '',
              RFC                   : '',
              Celular               : '',
              Email                 : '',
              Imagen                : '',
              Activo                : 'Si',
              NSS                   : '',
              NoLicencia            : '',
              FechaVigencia_Licencia: ''

        };

        $scope.choferEdit = {

              nIDChofer             : '0',
              nIDEmpresa            : '',
              Nombre                : '',
              Usuario               : '',
              Password              : '',
              Direccion             : '',
              RFC                   : '',
              Celular               : '',
              Email                 : '',
              Imagen                : '',
              Activo                : 'Si',
              NSS                   : '',
              NoLicencia            : '',
              FechaVigencia_Licencia: ''

        };

        $scope.clean = function(){

              $scope.chofer = {

                    nIDChofer             : '0',
                    nIDEmpresa            : '',
                    Nombre                : '',
                    Usuario               : '',
                    Password              : '',
                    Direccion             : '',
                    RFC                   : '',
                    Celular               : '',
                    Email                 : '',
                    Imagen                : '',
                    Activo                : 'Si',
                    NSS                   : '',
                    NoLicencia            : '',
                    FechaVigencia_Licencia: ''

              };

              $('#add_modal').modal('hide');
              document.getElementById('Imagen').value = '';
              document.getElementById("doc").src = '';

        
        };

        $scope.cleanEdit = function(){

              $scope.choferEdit = {

                    nIDChofer             : '0',
                    nIDEmpresa            : '',
                    Nombre                : '',
                    Usuario               : '',
                    Password              : '',
                    Direccion             : '',
                    RFC                   : '',
                    Celular               : '',
                    Email                 : '',
                    Imagen                : '',
                    Activo                : 'Si',
                    NSS                   : '',
                    NoLicencia            : '',
                    FechaVigencia_Licencia: ''

              };

              $('#update_modal').modal('hide');
              document.getElementById('ImagenEdit').value = '';
              document.getElementById("docEdit").src = '';

        
        };

        $scope.get = function(){

              $scope.choferes = '';
              $scope.mensaje  = '';
              $scope.getAgencia();
              choferServices.get().then(function(resp){

                            if(resp.data == 'null'){

                                   $scope.mensaje = 'No tienes choferes registrados';

                            }else{

                                  $scope.choferes = resp.data;

                            }

              },function(err){

                  console.log('error' + JSON.stringify(err));

              });

        };


        $scope.getAgencia = function(){

              agencyServices.get().then(function(resp){

                            if(resp.data == 'null'){


                            }else{

                                  $scope.empresas = resp.data;

                            }

              },function(err){

                    console.log('error' + JSON.stringify(err));

              });

        };

        function ValidarExt() {

              var archivoInput = document.getElementById('Imagen');

              var archivoRuta  = archivoInput.value;

              var expExtensiones = /(.PNG)|(.JPG)|(.JPEG)|(.PDF)$/i;

              if(!expExtensiones.test(archivoRuta)){

                    swal.fire({

                      title           : 'Oops!',

                      text            : 'Solo puedes subir imagenes con extension .png, .jpg, .jpeg',

                      imageUrl        : 'assets/img/advertencia.png',

                      showCloseButton : true,

                      confirmButtonColor: '#044BFD'

                    });

                    archivoRuta.value = '';

                    return false;

              }else{ 

                    if(archivoInput.files && archivoInput.files[0]){

                        var visor = new FileReader();

                        visor.addEventListener("load" , function(e){ console.log(e.target.result);

                            document.getElementById("doc").src = e.target.result;

                            $scope.chofer.Imagen = e.target.result;

                        });

                        visor.readAsDataURL(archivoInput.files[0]);
                    }

              }

        }(function(){

              document.getElementById('Imagen').addEventListener("change", ValidarExt);
        })();

        function ValidarExtEdit() {

              var archivoInputEdit = document.getElementById('ImagenEdit');

              var archivoRutaEdit  = archivoInputEdit.value;

              var expExtensionesEdit = /(.PNG)|(.JPG)|(.JPEG)|(.PDF)$/i;

              if(!expExtensionesEdit.test(archivoRutaEdit)){

                    swal.fire({

                      title           : 'Oops!',

                      text            : 'Solo puedes subir imagenes con extension .png, .jpg, .jpeg',

                      imageUrl        : 'assets/img/advertencia.png',

                      showCloseButton : true,

                      confirmButtonColor: '#044BFD'

                    });

                    archivoRutaEdit.value = '';

                    return false;

              }else{ 

                    if(archivoInputEdit.files && archivoInputEdit.files[0]){

                        var visorEdit = new FileReader();

                        visorEdit.addEventListener("load" , function(e){ 

                            document.getElementById("docEdit").src = e.target.result;

                            $scope.choferEdit.Imagen = e.target.result;

                        });

                        visorEdit.readAsDataURL(archivoInputEdit.files[0]);
                    }

              }

        }(function(){

              document.getElementById('ImagenEdit').addEventListener("change", ValidarExtEdit);
        })();


        $scope.add =  function(cliente){

              if(cliente.Nombre.length == 0){

                        swal.fire({

                            title                  : 'Oops!',

                            text                   : 'Nombre vacio completalo',

                            imageUrl               : 'assets/img/advertencia.png',

                            showCloseButton        : true,

                            confirmButtonColor     : '#044BFD',

                        });

              }else if(!expRFC.test(cliente.RFC)){

                              swal.fire({

                                   title                : 'Oops!',

                                   text                 : 'RFC no cumple con las especificaciones, Inténtalo de nuevo!',

                                   imageUrl             : 'assets/img/advertencia.png',

                                   showCloseButton      : true,

                                   confirmButtonColor   : '#044BFD'

                              });

              }else if(!expEmail.test(cliente.Email)){

                              swal.fire({

                                  title                   : 'Oops!',

                                  text                    : 'Correo electrónico no cumple con las especificaciones, Inténtalo de nuevo!',

                                  imageUrl                : 'assets/img/advertencia.png',

                                  showCloseButton         : true,

                                  confirmButtonColor      : '#044BFD'

                              });

              }else if(cliente.Usuario.length == 0){

                              swal.fire({

                                  title                 : 'Oops!',

                                  text                  : 'Usuario vacio completalo',

                                  imageUrl              : 'assets/img/advertencia.png',

                                  showCloseButton       : true,

                                  confirmButtonColor    : '#044BFD'

                              });

              }else if(cliente.Password.length == 0){

                              swal.fire({

                                  title                 : 'Oops!',

                                  text                  : 'Contraseña vacio completalo',

                                  imageUrl              : 'assets/img/advertencia.png',

                                  showCloseButton       : true,

                                  confirmButtonColor    : '#044BFD'

                              });

              }else if(!expTelefono.test(cliente.Celular)){

                              swal.fire({

                                  title                 : 'Oops!',

                                  text                  : 'Número de celular no cumple con las especificaciones, Inténtalo de nuevo!',

                                  imageUrl              : 'assets/img/advertencia.png',

                                  showCloseButton       : true,

                                  confirmButtonColor    : '#044BFD'

                              });

              }else if(cliente.nIDEmpresa.length == 0){

                              swal.fire({

                                  title                   : 'Oops!',

                                  text                    : 'Razon Social vacio completalo',

                                  imageUrl                : 'assets/img/advertencia.png',

                                  showCloseButton         : true,

                                  confirmButtonColor      : '#044BFD'

                              });

              }else if(cliente.NSS.length == 0){

                              swal.fire({

                                  title                  : 'Oops!',

                                  text                   : 'Número de seguro social vacio completalo',

                                  imageUrl               : 'assets/img/advertencia.png',

                                  showCloseButton        : true,

                                  confirmButtonColor     : '#044BFD'

                              });

              }else if(cliente.NoLicencia.length == 0){

                              swal.fire({

                                  title                   : 'Oops!',

                                  text                    : 'Número de licencia vacio completalo',

                                  imageUrl                : 'assets/img/advertencia.png',

                                  showCloseButton         : true,

                                  confirmButtonColor      : '#044BFD'

                              });

              }else if(cliente.FechaVigencia_Licencia.length == 0){

                              swal.fire({

                                  title                   : 'Oops!',

                                  text                    : 'Ingresa la vigencia de la licencia',

                                  imageUrl                : 'assets/img/advertencia.png',

                                  showCloseButton         : true,

                                  confirmButtonColor      : '#044BFD'

                              });

              }else{


                  choferServices.add(cliente).then(function(resp){

                                if(resp.data == 'INSERTED'){

                                       swal.fire({

                                           title                : 'Registro exitoso!',

                                           imageUrl             : 'assets/img/completo.png',

                                           showCloseButton      : true,

                                           confirmButtonColor   : '#044BFD'

                                       });

                                       $scope.get();

                                       $scope.clean();

                                }else if(resp.data == 'EXISTE'){

                                              swal.fire({

                                                  title                 : 'Correo electrónico existente!',

                                                  text                  : 'Ingresa un correo electrónico diferente',

                                                  imageUrl              : 'assets/img/advertencia.png',

                                                  showCloseButton       : true,

                                                  confirmButtonColor    : '#044BFD'

                                              });

                                }else{

                                       swal.fire({

                                           title                : 'Lo siento!',

                                           text                 : 'No se pudo completar el registro, Inténtalo de nuevo!',

                                           imageUrl             : 'assets/img/error.png',

                                           showCloseButton      : true,

                                           confirmButtonColor   : '#044BFD'

                                       });                                       

                                }

                  },function(err){

                        console.log('error' + JSON.stringify(err));

                  });

              }

        };


        $scope.show = function(chofer){

              $scope.choferEdit.nIDChofer                                  = chofer.nIDChofer;
              $scope.choferEdit.nIDEmpresa                                 = chofer.nIDEmpresa;
              $scope.choferEdit.Nombre                                     = chofer.Nombre;
              $scope.choferEdit.Usuario                                    = chofer.Usuario;
              $scope.choferEdit.Password                                   = chofer.Password;
              $scope.choferEdit.Direccion                                  = chofer.Direccion;
              $scope.choferEdit.RFC                                        = chofer.RFC;
              $scope.choferEdit.Celular                                    = chofer.Celular;
              $scope.choferEdit.Email                                      = chofer.Email;              
              $scope.choferEdit.Activo                                     = chofer.Activo;
              $scope.choferEdit.NSS                                        = chofer.NSS;
              $scope.choferEdit.NoLicencia                                 = chofer.NoLicencia;
              $scope.choferEdit.Imagen                                     = chofer.Imagen;
              document.getElementById("docEdit").src                       = chofer.Imagen;
              document.getElementById("FechaVigencia_LicenciaEdit").value  = chofer.FechaVigencia_Licencia.substring(0,10);
              $scope.choferEdit.FechaVigencia_Licencia                     = chofer.FechaVigencia_Licencia.substring(0,10);
        };


        $scope.update = function(chofer){

              if(chofer.Nombre.length == 0){

                        swal.fire({

                            title                  : 'Oops!',

                            text                   : 'Nombre vacio completalo',

                            imageUrl               : 'assets/img/advertencia.png',

                            showCloseButton        : true,

                            confirmButtonColor     : '#044BFD',

                        });

              }else if(!expRFC.test(chofer.RFC)){

                              swal.fire({

                                   title                : 'Oops!',

                                   text                 : 'RFC no cumple con las especificaciones, Inténtalo de nuevo!',

                                   imageUrl             : 'assets/img/advertencia.png',

                                   showCloseButton      : true,

                                   confirmButtonColor   : '#044BFD'

                              });

              }else if(!expEmail.test(chofer.Email)){

                              swal.fire({

                                  title                   : 'Oops!',

                                  text                    : 'Correo electrónico no cumple con las especificaciones, Inténtalo de nuevo!',

                                  imageUrl                : 'assets/img/advertencia.png',

                                  showCloseButton         : true,

                                  confirmButtonColor      : '#044BFD'

                              });

              }else if(chofer.Usuario.length == 0){

                              swal.fire({

                                  title                 : 'Oops!',

                                  text                  : 'Usuario vacio completalo',

                                  imageUrl              : 'assets/img/advertencia.png',

                                  showCloseButton       : true,

                                  confirmButtonColor    : '#044BFD'

                              });

              }else if(chofer.Password.length == 0){

                              swal.fire({

                                  title                 : 'Oops!',

                                  text                  : 'Contraseña vacio completalo',

                                  imageUrl              : 'assets/img/advertencia.png',

                                  showCloseButton       : true,

                                  confirmButtonColor    : '#044BFD'

                              });

              }else if(chofer.Direccion.length == 0){

                              swal.fire({

                                  title               : 'Oops!',

                                  text                : 'Dirección vacio completalo',

                                  imageUrl            : 'assets/img/advertencia.png',

                                  showCloseButton     : true,

                                  confirmButtonColor  : '#044BFD'

                              });

              }else if(!expTelefono.test(chofer.Celular)){

                              swal.fire({

                                  title                 : 'Oops!',

                                  text                  : 'Número de celular no cumple con las especificaciones, Inténtalo de nuevo!',

                                  imageUrl              : 'assets/img/advertencia.png',

                                  showCloseButton       : true,

                                  confirmButtonColor    : '#044BFD'

                              });

              }else if(chofer.nIDEmpresa.length == 0){

                              swal.fire({

                                  title                   : 'Oops!',

                                  text                    : 'Razon Social vacio completalo',

                                  imageUrl                : 'assets/img/advertencia.png',

                                  showCloseButton         : true,

                                  confirmButtonColor      : '#044BFD'

                              });

              }else if(chofer.NSS.length == 0){

                              swal.fire({

                                  title                  : 'Oops!',

                                  text                   : 'Número de seguro social vacio completalo',

                                  imageUrl               : 'assets/img/advertencia.png',

                                  showCloseButton        : true,

                                  confirmButtonColor     : '#044BFD'

                              });

              }else if(chofer.NoLicencia.length == 0){

                              swal.fire({

                                  title                   : 'Oops!',

                                  text                    : 'Número de licencia vacio completalo',

                                  imageUrl                : 'assets/img/advertencia.png',

                                  showCloseButton         : true,

                                  confirmButtonColor      : '#044BFD'

                              });

              }else if(chofer.FechaVigencia_Licencia.length == 0){

                              swal.fire({

                                  title                   : 'Oops!',

                                  text                    : 'Ingresa la vigencia de la licencia',

                                  imageUrl                : 'assets/img/advertencia.png',

                                  showCloseButton         : true,

                                  confirmButtonColor      : '#044BFD'

                              });

              }else{ 

                    choferServices.update(chofer).then(function(resp){

                                  if(resp.data == "UPDATED"){

                                         swal.fire({

                                             title                  : 'Modificación exitosa!',

                                             imageUrl               : 'assets/img/completo.png',

                                             showCloseButton        : true,

                                             confirmButtonColor     : '#044BFD'

                                         });

                                         $scope.get();

                                         $scope.cleanEdit();

                                  }else{

                                        swal.fire({

                                            title             : 'Lo siento!',

                                            text              : 'No se pudo completar el registro, Inténtalo de nuevo!',

                                            imageUrl          : 'assets/img/error.png',

                                            showCloseButton   : true,

                                            confirmButtonColor: '#044BFD'

                                        });

                                  }

                    }, function(err){

                                console.log('error' + JSON.stringify(err));

                    });

              }

        };

        $scope.destroy = function(id){

              swal.fire({

                  title               : 'ADVERTENCIA',

                  text                : '¿Estás seguro de eliminar este registro?',

                  imageUrl            : 'assets/img/advertencia.png',

                  showCancelButton    : true,

                  showCloseButton     : true,                  

                  cancelButtonColor   : 'rgba(4,75,253,0.5)',                  

                  confirmButtonColor  : '#044BFD',

                  cancelButtonText    : 'Cancelar',

                  confirmButtonText   : 'Si',

              }).then((result) => {

                       if(result.value){

                                choferServices.destroy(id).then(function(resp){

                                              if(resp.data == "DELETED"){

                                                      swal.fire({

                                                          title              : 'Registro eliminado con exito!',

                                                          imageUrl           : 'assets/img/completo.png',

                                                          showCloseButton    : true,

                                                          confirmButtonColor : '#044BFD'

                                                      });

                                                      $scope.get();

                                              }else{

                                                      swal.fire({

                                                          title             : 'Oops!',

                                                          text              : 'No se pudo eliminar el registro inténtalo de nuevo',

                                                          imageUrl          : 'assets/img/error.png',

                                                          showCloseButton   : true,

                                                          confirmButtonColor: '#044BFD'

                                                      });

                                              }

                                },function(err){

                                      console.log('error' + JSON.stringify(err));

                                });

                       }else{

                                swal.fire({

                                    title               : 'Se cancelo la eliminicación!',

                                    imageUrl            : 'assets/img/error.png',

                                    showCloseButton     : true,

                                    confirmButtonColor  : '#044BFD'

                                });
                 
                       }
             
              });
        
        };
        
            
        }]);