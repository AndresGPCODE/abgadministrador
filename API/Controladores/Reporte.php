<?php
  
  error_reporting(E_ALL ^ E_NOTICE);
  ini_set('display_errors','1');

  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Request-Methods');

  include_once '../Configuraciones/Conexion.php';
  include_once '../Dispositivos/clReporte.php';
  include_once '../Utilerias/clHerramientas_v2011.php';

  $method = $_SERVER['REQUEST_METHOD'];

  $UtileriasDatos    = new clHerramientasv2011();
  $fechaLocal        = $UtileriasDatos->getFechaYHoraActual_General();
  $fechaLocal        = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

  $report = new clReporte();

  $report->conexion($SERVER,$USER,$PASSWORD,$BD);

  switch ($method) {
    
    case 'GET':

          $action = $_GET['accion'];

          if($action == 'consultar'){

                $valores = " COUNT(tbl_reportes.nIDReporte) AS nIDReporte,tbl_reportes.tipoReporte,tbl_reportes.nIDCliente,tbl_reportes.fechaCreacion, ";

                $valores = $valores . " date_format(tbl_reportes.fechaCreacion, '%m') AS Mes ";

                $condicion = " date_format(now(),'%m') = date_format(tbl_reportes.fechaCreacion, '%m') GROUP BY tbl_reportes.tipoReporte ";                

                if($report->consultarCondicion($valores,$condicion)){

                          echo json_encode($report->consultarCondicion($valores,$condicion));
                }else{

                          echo "null";
                }
                break;
          }

      
  }
?>