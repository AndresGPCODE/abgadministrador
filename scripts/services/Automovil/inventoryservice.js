'use strict';

angular.module('abgpassportApp')
       
       .service('inventoryServices',['$http','configService', function($http,configService){

            return {

                    get : function(){
                      
                        return $http.get(configService.apiUrl + 'Automovil.php?accion=consultar',

                          { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                        );

                    },

                    getDisponibles : function(){

                        return $http.get(configService.apiUrl + 'Automovil.php?accion=disponibles',

                          { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                        );

                    },

                    getMantenimiento : function(){

                        return $http.get(configService.apiUrl + 'Automovil.php?accion=mantenimiento',

                          { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                        );

                    },

                    getOcupado : function(){

                        return $http.get(configService.apiUrl + 'Automovil.php?accion=ocupado',

                          { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                        );

                    },

                    Historial : function(id){

                        return $http.post(configService.apiUrl + 'Automovil.php','accion=buscar&id=' + JSON.stringify(id),

                          { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                        );

                    },

                    getGps : function(placa){

                           return $http.post(configService.apiUrl + 'Automovil.php','accion=buscarGps&placa=' + JSON.stringify(placa),

                            { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                          );

                    },

                    getVehiculos : function(){

                       return $http.get(configService.apiUrl + 'Automovil.php?accion=listado',

                        { headers : {'Content-Type' : 'apllication/x-www-form-urlencoded' } }

                       );

                    },

                    add : function(Automovil){

                        return $http.post(configService.apiUrl + 'Automovil.php','accion=agregar&Automovil=' + JSON.stringify(Automovil),

                              { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                        );
                        
                    },

                    update : function(Automovil){

                        return $http.post(configService.apiUrl + 'Automovil.php','accion=modificar&Automovil=' + JSON.stringify(Automovil),

                          { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                        );

                    },

                    destroy : function(id){

                            return $http.post(configService.apiUrl + 'Automovil.php','accion=eliminar&id=' + JSON.stringify(id),

                              { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                            );
                            
                    }

            };

       }]);