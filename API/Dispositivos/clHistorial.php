<?php

    //Clase Automovil
    //Esta clase es la que controlara los datos de la empresa.

    //Herramienta externa
    include_once "../Utilerias/clHerramientas_v2011.php";

    //tbl_automoviles


/**
 * 
 */
class clHistorial
{
  
    //Atributos de conexion a la base de datos
    private $servidor;
    private $usuario;
    private $password;
    private $basededatos;
    private $contarDatos   = 0;

    //Constantes generales
    private $ascendente     = "Ascendente";
    private $descendente    = "Descendente";

    //Constantes para la base de datos
    private $nombreTabla    = "tbl_historial";
    private $nombreVista    = "";
    private $id             = "nIDHistorial";
    private $campoOrdenar   = "nIDHistorial";
    private $ordenamiento   = "Descendente";
    private $limiteInferior = -1;
    private $limiteSuperior = -1;
    public  $last_id = 0;
    private $dt;
    private $dtGrabar;

    //Constantes de estado
    private $existeError    = FALSE;
    private $existenDatos   = FALSE;
    private $datosConexion  = FALSE;

    //Mensajes
    private $error          = "";

    //Constructor
  function __construct()
  {
    $this->Inicializacion();
  }

  /**
     *************** Función de conexión ***************
     * La función realiza la conexión a la base de datos 
     * mediante las credenciales previamente establecidas. 
     * 
     * @param [string] $servidor
     * @param [string] $usuario
     * @param [string] $password
     * @param [string] $basededatos
     * @return void
     */
    public function conexion($servidor, $usuario, $password, $basededatos)
    {
        $this->servidor     = $servidor;
        $this->usuario      = $usuario;
        $this->password     = $password;
        $this->basededatos  = $basededatos;

        $this->datosConexion    = TRUE;
    }

    /**
     ************** Función setInfomacion **************
     * La función realiza la asignación de los datos almacenados
     * en la base de datos a las respectivas variables, los datos
     * que se encuentran como parametro son obligatorios para cada
     * tabla. 
     * 
     * @param [datetime] $FechaModificacion
     * @param [datetime] $FechaCreacion
     * @param [string] $Observaciones
     * @param [int] $bEstado
     * @param [boolean] $crear
     * @param [boolean] $modificar
     * @param [boolean] $eliminar
     * @return void
     */
    public function setInformacion($nIDHistorial,$nIDDispositivoAuto,$nIDSolicitud,$Link,$Respuesta,$Latitud,$Longitud,
        $Velocidad,$Hora,$Fecha,$FechaModificacion,$FechaCreacion,$Observaciones,$bEstado, $crear, $modificar, $eliminar) 
    {
        $utileria = new clHerramientasv2011();
        
        $this->dtGrabar[$this->contarDatos]["nIDHistorial"]              = $nIDHistorial;
        $this->dtGrabar[$this->contarDatos]["nIDDispositivoAuto"]        = $nIDDispositivoAuto;
        $this->dtGrabar[$this->contarDatos]["nIDSolicitud"]              = $nIDSolicitud;
        $this->dtGrabar[$this->contarDatos]["Link"]                      = $Link;
        $this->dtGrabar[$this->contarDatos]["Respuesta"]                 = $Respuesta;
        $this->dtGrabar[$this->contarDatos]["Latitud"]                   = $Latitud;
        $this->dtGrabar[$this->contarDatos]["Longitud"]                  = $Longitud;
        $this->dtGrabar[$this->contarDatos]["Velocidad"]                 = $Velocidad;
        $this->dtGrabar[$this->contarDatos]["Hora"]                      = $Hora;
        $this->dtGrabar[$this->contarDatos]["Fecha"]                     = $Fecha;

         /*
         ** Estos datos son obligatorios para todas las tablas
        */
        $fecha = "";

        if( strlen($FechaModificacion) > 0 )
        {
            $fecha = $utileria->ConvertirFechaYHora_General($FechaModificacion);
            $this->dtGrabar[$this->contarDatos]["FechaModificacion"] = $FechaModificacion;
        } else {
            $this->dtGrabar[$this->contarDatos]["FechaModificacion"] = "01/01/1980";
        }

        $fecha = "";

        if(strlen($FechaCreacion)>0){
            $fecha=$utileria->ConvertirFechaYHora_General($FechaCreacion);
            $this->dtGrabar[$this->contarDatos]["FechaCreacion"] = $fecha;
        } else {
            $this->dtGrabar[$this->contarDatos]["FechaCreacion"] = "01/01/1980";
        }

        $this->dtGrabar[$this->contarDatos]["Observaciones"] = $Observaciones;

        if($bEstado >= 0 && $bEstado <= 5){
            $this->dtGrabar[$this->contarDatos]["bEstado"]  = $bEstado;
            $this->dtGrabar[$this->contarDatos]["sbEstado"] = $this->vEstado($bEstado,"bEstado");
        } else {
            $this->dtGrabar[$this->contarDatos]["bEstado"]  = 1;
            $this->dtGrabar[$this->contarDatos]["sbEstado"] = "No definido";
        }

        if ($crear==TRUE){
            $this->dtGrabar[$this->contarDatos]["Crear"]    = TRUE;
            $this->dtGrabar[$this->contarDatos]["Cambiar"]  = FALSE;
            $this->dtGrabar[$this->contarDatos]["Eliminar"] = FALSE;
        } else {
            if ($modificar==TRUE){
                $this->dtGrabar[$this->contarDatos]["Crear"]    = FALSE;
                $this->dtGrabar[$this->contarDatos]["Cambiar"]  = TRUE;
                $this->dtGrabar[$this->contarDatos]["Eliminar"] = FALSE;
            } else {
                if ($eliminar==TRUE){
                    $this->dtGrabar[$this->contarDatos]["Crear"]    = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Cambiar"]  = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Eliminar"] = TRUE;
                } else {
                    $this->dtGrabar[$this->contarDatos]["Crear"]    = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Cambiar"]  = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Eliminar"] = FALSE;
                }
            }
        }


        $this->contarDatos=$this->contarDatos+1;
        $this->existenDatos=TRUE;

        unset($utileria);

        return TRUE;

    }

    /**
     ************** Función setInformacionObj **************
     * Esta función permite darle valor al objeto para despues
     * retornar datos.
     * 
     * @param [object] $obj
     * @return void
     */
    public function setInformacionObj($obj)
    {
        $registros  = 0;
        $registros  = $obj->registrosCargados();

        if($registros > 0){
            
            $i=0;

            for($i=0; $i < $registros - 1; $i = $i + 1){

                $dato = $obj->getInformacionObj();

                $this->setInformacion(
                    $dato[$i]["nIDHistorial"],
                    $dato[$i]["nIDDispositivoAuto"],
                    $dato[$i]["nIDSolicitud"],
                    $dato[$i]["Link"],
                    $dato[$i]["Respuesta"],
                    $dato[$i]["Latitud"],
                    $dato[$i]["Longitud"],
                    $dato[$i]["Velocidad"],
                    $dato[$i]["Hora"],
                    $dato[$i]["Fecha"],
                    $dato[$i]["FechaModificacion"],
                    $dato[$i]["FechaCreacion"],
                    $dato[$i]["Observaciones"],
                    $dato[$i]["bEstado"],
                    $dato[$i]["Crear"],
                    $dato[$i]["Cambiar"],
                    $dato[$i]["Eliminar"]
                );
            }

        }
        return TRUE;
    }

    /**
     ************** Función getInformacionObj **************
     * Esta función graba los datos traídos de la base de datos
     * para despues agregarlos a una variable u objeto.
     * 
     * @return void
     */
    public function getInformacionObj()
    {
        return $this->dtGrabar;
    }

    /**
     ************** Función getInformacion **************
     * Esta función permite devolver el valor de las propiedades
     * que se encuentan en las variables.
     * 
     * @param [int] $numero
     * @return void
     */
    public function getInformacion($numero)
    {
        if($this->existenDatos == TRUE){
            if($numero >= 0){
                return $this->dtGrabar[$numero];
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    /**
     ************** Función getIndice **************
     *
     * @param [int] $nID
     * @return void
     */
    public function getIndice($nID)
    {
        if($this->contarDatos>0){

            $indice = -1;

            for($i=0; $i < $this->contarDatos - 1; $i = $i + 1){
                if($this->dtgrabar[$i][$this->id] == $nID){
                    $indice = $i;
                    break;
                }
            }
            return $indice;
        } else {
            return -1;
        }
    }

    /**
     ************** Función OrdenarTabla **************
     * Ordena la tabla con respecto al campo que se le
     * pasa por parametro.
     * 
     * @param [string] $campos
     * @return void
     */
    public function OrdenarTabla($campos)
    {
        $this->campoOrdenar = $campos;
    }

    /**
     ************** Función formaOrdenamiento **************
     * Ordena la tabla de forma ascendente o descendente según
     * el parametro.
     * 
     * @param [type] $forma
     * @return void
     */
    public function formaOrdenamiento($forma)
    {
        $this->ordenamiento = $forma;
    }

    /**
     ************** Función dtBase **************
     * Graba los registros de la base de datos.
     * 
     * @return void
     */
    public function dtBase()
    {
        return $this->dtGrabar;
    }

    /**
     ************** Función errorObjeto **************
     * La funcion retorna un error en caso de que este
     * exista.
     * 
     * @return void
     */
    public function errorObjeto()
    {
        if($this->existeError==TRUE){
            return $this->error;
        } else {
            return "No existe ningún mensaje de error.";
        }
    }

    /**
     ************** Función registrosCargados **************
     * Función para retornar el número de registros cargados.
     * @return void
     */
    public function registrosCargados()
    {
        return $this->contarDatos;
    }

    /**
     ************** Función formaDeOrdenamiento **************
     * Función para retornar la forma de ordenar la tabla.
     * 
     * @return void
     */
    public function formaDeOrdenamiento()
    {
        return $this->ordenamiento;
    }

    /**
     ************** Función errorDeObjeto **************
     * Función para retornar el error de un objeto en caso
     * de que este exista.
     * 
     * @return void
     */
    public function errorDeObjeto()
    {
        return $this->existeError;
    }

    /**
     ************** Función datosCargados **************
     *
     * @return void
     */
    public function datosCargados()
    {
        return $this->existenDatos;
    }

    /**
     ************** Función setlimiteInferior **************
     *
     * @param [int] $limite
     * @return void
     */
    public function setlimiteInferior($limite)
    {
        $this->limiteInferior = $limite;
    }

    /**
     ************** Función setlimiteSuperior **************
     *
     * @param [int] $limite
     * @return void
     */
    public function setlimiteSuperior($limite)
    {
        $this->limiteSuperior = $limite;
    }

    /**
     *************** Función Inicializacion ***************
     * Esta función permite inicializar atributos y contenido.
     * 
     * @return void
     */
    public function Inicializacion()
    {
        $this->InicializaAtributos();
        $this->InicializacionContenido();
    }

    /**
     *************** Función InicializaAtributos ***************
     * Inicializa los atributos.
     * 
     * @return void
     */
    private function InicializaAtributos()
    {
        $this->existeError      = FALSE;
        $this->existenDatos     = FALSE;
        $this->datosConexion    = FALSE;

        $this->error        = "No hay error";
        $this->ordenamiento = $this->ascendente;
        $this->contarDatos  = 0;
    }

    /**
     *************** Función InicializacionContenido ***************
     * Inicializa el contenido.
     * @return void
     */
    public function InicializacionContenido()
    {
        $this->contarDatos  = 0;
        $this->existeError  = FALSE;
    }

    /**
     *************** Función existeID ***************
     *
     * @param [int] $nID
     * @return void
     */
    public function existeID($nID)
    {

        // Verifica si existe algún error con la conexión
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        ="Error No tiene los datos para conectarse a la base de 
                                 datos [Cargar]";
            return FALSE;
        }

        // Abre la conexion
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT * ";
        $txtConsulta = $txtConsulta . "FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . $this->id ."=" . $nID ;

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Cargar la informacion
        if($res){
            if($registro=mysqli_fetch_array($res, MYSQLI_ASSOC)){
                return TRUE;
            } else {

                return FALSE;
            }

        } else {
            return FALSE;
        }
    }

   

    /**
     *************** Función vEstado ***************
     * Verifica el estado del registro.
     * 0 = Eliminado; 1 = Activo; 2 = Cancelado; 
     * 3 = No definido;
     * 
     * @param [string] $estado
     * @param [string] $nombreCampo
     * @return void
     */
    private function vEstado($estado, $nombreCampo)
    {
        $cadena = "";
        switch($estado){
            case 0: // Eliminado
                $cadena = "Eliminado";
                break;
            case 1: // Activo
                $cadena = "Activo";
                break;
            case 2: // Cancelado
                $cadena = "Cancelado";
                break;
            case 3: // No definido
                $cadena = "No definido";
                break;
        }
        return $cadena;
    }

    /**
     *************** Funcion especialesHTML ***************
     *
     * @param [string] $str
     * @return void
     */
    public function especialesHTML($str)
    {
        $str = mb_convert_encoding($str,  'UTF-8');
        return $str;
    }

    /**
     ***************  Función consultar *************** 
     * Realiza una consulta simple.
     * 
     * @return void
     */
    private function consultar()
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Genera la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT *";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

        if($this->formaDeOrdenamiento()==$this->ascendente){
            $txtConsulta = $txtConsulta . " ASC";
        } else {
            $txtConsulta = $txtConsulta . " DESC";
        }

        if($this->limiteInferior>=0){
            $txtConsulta = $txtConsulta . " LIMIT " . $this->limiteInferior . "," . $this->limiteSuperior;
        }

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli, $txtConsulta);

        // Carga la informacion
        $contador = 0;
        $this->contarDatos = 0;

        if($res){

            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $nIDHistorial                   = 0;
                $nIDDispositivoAuto             = 0;
                $nIDSolicitud                   = 0;
                $Link                           = "";
                $Respuesta                      = "";
                $Latitud                        = "";
                $Longitud                       = "";
                $Velocidad                      = "";
                $Hora                           = "";
                $Fecha                          = "";

                // Campos base
                $FechaModificacion  = "";                
                $FechaCreacion      = "";
                $Observaciones      = "";
                $bEstado            = 3;

                // Carga la información del registro leído
                if($registro['nIDHistorial'] != NULL){
                    $nIDHistorial = $registro['nIDHistorial'];
                }
                if($registro['nIDDispositivoAuto'] != NULL){
                    $nIDDispositivoAuto = $registro['nIDDispositivoAuto'];
                }
                if($registro['nIDSolicitud'] != NULL){
                    $nIDSolicitud = $registro['nIDSolicitud'];
                }
                if($registro['Link'] != NULL){
                    $Link = $registro['Link'];
                }
                if($registro['Respuesta'] != NULL){
                    $Respuesta = $registr['Respuesta'];
                }
                if($registro['Latitud'] != NULL){
                    $Latitud = $registro['Latitud'];
                }
                if($registro['Longitud'] != NULL){
                    $Longitud = $registro['Longitud'];
                }
                if($registro['Velocidad'] != NULL){
                    $Velocidad = $registro['Velocidad'];
                }
                if($registro['Hora'] != NULL){
                    $Hora = $registro['Hora'];
                }
                if($registro['Fecha'] != NULL){
                    $Fecha = $registro['Fecha'];
                }

                // Campos base
                if($registro['FechaModificacion']!= NULL){
                    $FechaModificacion = $registro['FechaModificacion'];
                }   
                if($registro['FechaCreacion']!= NULL){
                    $FechaCreacion = $registro['FechaCreacion'];
                }             

                if($registro['Observaciones']!= NULL){
                    $Observaciones = $registro['Observaciones'];
                }

                if($registro['bEstado']!= NULL){
                    $bEstado = $registro['bEstado'];
                }

                // Carga la información en el objeto
                $this->setInformacion(
                        $nIDHistorial,
                        $nIDDispositivoAuto,
                        $nIDSolicitud,
                        $Link,
                        $Respuesta,
                        $Latitud,
                        $Longitud,
                        $Velocidad,
                        $Hora,
                        $Fecha,
                        $FechaModificacion,
                        $FechaCreacion,
                        $Observaciones,
                        $bEstado,
                        FALSE,
                        TRUE,
                        FALSE
                );
            }

        } else {
            $this->existenDatos = FALSE;
        }
        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     ***************  Función consultarCondicion *************** 
     * Realiza una consulta con condición.
     * 
     * @param [string] $condicion
     * @return void
     */
    public function consultarCondicion($valores,$condicion)
    {

      
            // Abre la conexión
            $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

            if(mysqli_connect_errno()){
                $this->existeError  = TRUE;
                $this->error        = "Error No se puede conectar a la base de datos";
                return FALSE;
            }

            $txtConsulta = "";
            $txtConsulta = "SELECT ";
            $txtConsulta = $txtConsulta . $valores;
            $txtConsulta = $txtConsulta . " FROM ";
            $txtConsulta = $txtConsulta . $this->nombreTabla;
            $txtConsulta = $txtConsulta . " INNER JOIN tbl_autos_dispositivos ON tbl_autos_dispositivos.nIDDispositivoAuto = tbl_historial.nIDDispositivoAuto ";
            $txtConsulta = $txtConsulta . " WHERE ";
            $txtConsulta = $txtConsulta . $condicion;
            $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

            if($this->limiteInferior >= 0){
                $txtConsulta = $txtConsulta . " LIMIT " . $this->limiteInferior . "," . $this->limiteSuperior;
            }
            //echo $txtConsulta;
            // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli, $txtConsulta);

        $regs = array();

        while($r = mysqli_fetch_assoc($res)){

            $regs[] = $r;

        }return $regs;

       /*// Carga la informacion
        $contador = 0;
        $this->contarDatos = 0;

        if($res){

            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $nIDHistorial                   = 0;
                $nIDDispositivoAuto             = 0;
                $nIDSolicitud                   = 0;
                $Link                           = "";
                $Latitud                        = "";
                $Longitud                       = "";
                $Velocidad                      = "";
                $Hora                           = "";
                $Fecha                          = "";

                // Campos base           
                $FechaModificacion  = "";     
                $FechaCreacion      = "";
                $Observaciones      = "";
                $bEstado            = 3;

                // Carga la información del registro leído
                if($registro['nIDHistorial'] != NULL){
                    $nIDHistorial = $registro['nIDHistorial'];
                }
                if($registro['nIDDispositivoAuto'] != NULL){
                    $nIDDispositivoAuto = $registro['nIDDispositivoAuto'];
                }
                if($registro['nIDSolicitud'] != NULL){
                    $nIDSolicitud = $registro['nIDSolicitud'];
                }
                if($registro['Link'] != NULL){
                    $Link = $registro['Link'];
                }
                if($registro['Latitud'] != NULL){
                    $Latitud = $registro['Latitud'];
                }
                if($registro['Longitud'] != NULL){
                    $Longitud = $registro['Longitud'];
                }
                if($registro['Velocidad'] != NULL){
                    $Velocidad = $registro['Velocidad'];
                }
                if($registro['Hora'] != NULL){
                    $Hora = $registro['Hora'];
                }
                if($registro['Fecha'] != NULL){
                    $Fecha = $registro['Fecha'];
                }

                // Campos base
                if($registro['FechaModificacion']!= NULL){
                    $FechaModificacion = $registro['FechaModificacion'];
                } 
                if($registro['FechaCreacion']!= NULL){
                    $FechaCreacion = $registro['FechaCreacion'];
                }               

                if($registro['Observaciones']!= NULL){
                    $Observaciones = $registro['Observaciones'];
                }

                if($registro['bEstado']!= NULL){
                    $bEstado = $registro['bEstado'];
                }

                // Carga la información en el objeto
                $this->setInformacion(
                        $nIDHistorial,
                        $nIDDispositivoAuto,
                        $nIDSolicitud,
                        $Link,
                        $Latitud,
                        $Longitud,
                        $Velocidad,
                        $Hora,
                        $Fecha,
                        $FechaModificacion,
                        $FechaCreacion,
                        $Observaciones,
                        $bEstado,
                        FALSE,
                        TRUE,
                        FALSE
                );
            }

        } else {
            $this->existenDatos = FALSE;
        }*/
        mysqli_close($mysqli);
        //return TRUE;
    }

    public function buscarTodosDispositivos(){

         $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

         if(mysqli_connect_errno()){
            
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;

         }

         $txtConsulta = "SELECT * FROM (SELECT nIDHistorial AS ID, Latitud, Longitud, Identificador, his.FechaCreacion, his.nIDDispositivoAuto";
         $txtConsulta = $txtConsulta . " FROM tbl_historial as his ";
         $txtConsulta = $txtConsulta . " INNER JOIN tbl_autos_dispositivos ";
         $txtConsulta = $txtConsulta . " ON tbl_autos_dispositivos.nIDDispositivoAuto = his.nIDDispositivoAuto ";
         $txtConsulta = $txtConsulta . " WHERE tbl_autos_dispositivos.bEstado = 1 ";
         $txtConsulta = $txtConsulta . " ORDER BY his.FechaCreacion DESC) as sub ";
         $txtConsulta = $txtConsulta . " GROUP BY sub.nIDDispositivoAuto  ";
         $txtConsulta = $txtConsulta . " order by sub.FechaCreacion ";

         mysqli_query($mysqli,"SET NAMES UTF8");
         $res = mysqli_query($mysqli,$txtConsulta);

         $regs = array();
         while($r = mysqli_fetch_assoc($res)){

                $regs[] = $r;

         }return $regs;

         mysqli_close($mysqli);
    }


    public function consultarSolicitud($nIDDispositivoAuto)
    {
      
            // Abre la conexión
            $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

            if(mysqli_connect_errno()){
                $this->existeError  = TRUE;
                $this->error        = "Error No se puede conectar a la base de datos";
                return FALSE;
            }

           $txtConsulta = "";
           $txtConsulta = "SELECT ";
           $txtConsulta = $txtConsulta . " nIDSolicitud ";
           $txtConsulta = $txtConsulta . " FROM tbl_solicitud WHERE nIDAutomovil = ";
           $txtConsulta = $txtConsulta . " (SELECT nIDAutomovil FROM tbl_automoviles WHERE Placas = (SELECT Identificador FROM tbl_autos_dispositivos WHERE nIDDispositivoAuto = ".$nIDDispositivoAuto."))";
           $txtConsulta = $txtConsulta . " and (Estatus = 'ENTREGADO' OR Estatus = 'EN RUTA')";
   //echo $txtConsulta;
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli, $txtConsulta);

        $regs = array();

        while($r = mysqli_fetch_assoc($res)){

            $regs[] = $r;

        }return $regs[0]['nIDSolicitud'];  


        mysqli_close($mysqli);

    }

    public function SepararCadena($respuesta)
    {
           $arreglo = array();          

           $arreglo = explode(",", $respuesta);

           $arregloTiempo = explode(" ", $arreglo[0]);

           $arregloLatitud = explode(" ", $arreglo[1]); 

           $arregloLongitud = explode(" ", $arreglo[2]);
           
           $arregloFinal = ["Hora" => $arregloTiempo[1] , "Fecha" => $arreglo[0]  , "Latitud" => $arregloLatitud[2] , "Longitud" => $arregloLongitud[2], "Link" => $arreglo[0] ];

           return $arregloFinal;

           //echo "Hora : " . $arregloTiempo[1] . "Fecha: " . $arreglo[0] . "Lat: " . $arregloLatitud[2] . "Lon: " . $arregloLongitud[2] . "link" . $arreglo[0hdop];       

           /*$arreglo = explode(",", $respuesta);

           $arregloL1 = explode("?", $arreglo[0]);

           $arregloL2 = explode("&", $arregloL1[1]);

           $arregloLatitud = explode("=", $arregloL2[0]);

           $arregloLongitud = explode("=", $arregloL2[1]);*/

    }

    public function consultarFechas($valores,$inner,$condicion)
    {

      
            // Abre la conexión
            $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

            if(mysqli_connect_errno()){
                $this->existeError  = TRUE;
                $this->error        = "Error No se puede conectar a la base de datos";
                return FALSE;
            }

           $txtConsulta = "";
           $txtConsulta = "SELECT ";
           $txtConsulta = $txtConsulta . $valores;
           $txtConsulta = $txtConsulta . " FROM ";
           $txtConsulta = $txtConsulta . $this->nombreTabla;
           $txtConsulta = $txtConsulta . $inner;
           $txtConsulta = $txtConsulta . $condicion;
   //echo $txtConsulta;
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli, $txtConsulta);

        $regs = array();

        while($r = mysqli_fetch_assoc($res)){

            $regs[] = $r;

        }return $regs;  


        mysqli_close($mysqli);

    }

    /**
     *************** Función registrosTabla ***************
     *
     * @return void
     */
    private function registrosTabla()
    {

        // Abre la conexión
        $mysqli=mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT *";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

        if($this->formaDeOrdenamiento()==$this->ascendente){
            $txtConsulta=$txtConsulta . " ASC";
        } else {
            $txtConsulta=$txtConsulta . " DESC";
        }

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Carga la informacion
        $contador=0;

        // Cuenta los registros
        if($res){
            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $contador = $contador + 1;
            }
        }

        mysqli_close($mysqli);
        return $contador;
    }

    /**
     *************** Función contarRegistros ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function contarRegistros($condicion)
    {
        
        $contador = 0;

        // Abrea la conexion
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error No se puede conectar a la base de datos";
            return 0;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT count(nIDHistorial) as Total";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . $condicion;

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Cargar la informacion


        //$this->contarDatos=0;
        if($res){
            if($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                if($registro['Total']!=NULL){
                    $contador=$registro['Total'];
                }
            }
        }

        mysqli_close($mysqli);
        return $contador;
    }

    /**
     *************** Función consultarID ***************
     *
     * @param [int] $nIDEmpresa
     * @return void
     */
    private function consultarID($nIDAutomovil)
    {

        // Abrea la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return 3;
        }

        $txtConsulta = "";
        $txtConsulta = "SELECT *";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . "nIDHistorial='" . $nIDAutomovil . "'";

        //' Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);
        $nid = 3;

        // Cargar la informacion
        if($res){
            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $nid = 3;

                if($registro['nIDHistorial']!= NULL){
                    $nid = $registro['nIDHistorial'];
                    break;
                }
            }
            mysqli_close($mysqli);
            return $nid;
        } else {
            mysqli_close($mysqli);
            return 3;
        }
    }

    /**
     *************** Función borrar ***************
     *
     * @return void
     */
    private function borrar()
    {
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return 3;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "DELETE FROM " . $this->nombreTabla;
        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función borrarID ***************
     *
     * @param [int] $nID
     * @return void
     */
    private function borrarID($nID)
    {
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return 3;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "DELETE FROM " . $this->nombreTabla . " WHERE " . $this->id . "=" . $nID;
        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función borrarCondicion ***************
     *
     * @param [tstring] $condicion
     * @return void
     */
    private function borrarCondicion($condicion)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "DELETE FROM " . $this->nombreTabla . " WHERE " . $condicion;
        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función borrarTemporal ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @return void
     */
    private function borrarTemporal($nID, $observaciones)
    {
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return 3;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " bEstado= 0" . ",";

        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res=mysqli_query($mysqli,$txtInsercion);
        if($res==FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarEstado ***************
     *
     * @param [int] $nID
     * @param [int] $bEstado
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarEstado($nID, $bEstado, $observaciones)
    {
        
        // Abre la conexión
        $mysqli=mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " bEstado=" . $bEstado . ",";

        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res=mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarPassword ***************
     *
     * @param [int] $nID
     * @param [string] $password
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarPassword($nID, $password, $observaciones)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " Password='" . $this->especialesHTML($password) . "',";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res=mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }
        mysqli_close($mysqli);
        return TRUE;
    }

    
    /**
     *************** Función ActualizarNombre ***************
     * 
     * @param [int] $nID
     * @param [string] $nombre
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarNombre($nID, $nombre, $observaciones)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " Nombre=" . $this->especialesHTML($nombre) . ",";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarVIP ***************
     *
     * @param [int] $nIDEmpresa
     * @param [string] $RazonSocial
     * @param [string] $Calle
     * @param [string] $NoExterior
     * @param [string] $NoInterior
     * @param [string] $Colonia
     * @param [string] $Ciudad
     * @param [string] $Estado
     * @param [string] $Pais
     * @param [string] $Cp
     * @param [string] $RFC
     * @param [int] $NumeroDeTarjeta
     * @param [string] $Tipo
     * @param [string] $FechaVencimiento
     * @param [string] $Telefono
     * @param [string] $Email
     * @param [type] $observaciones
     * @return void
     */
    private function actualizarVIP($nIDHistorial,$nIDDispositivoAuto,$nIDSolicitud,$Link,$Respuesta,
        $Latitud,$Longitud,$Velocidad,$Hora,$Fecha,$observaciones)
    {

        // Abre la conexón
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion   = "UPDATE " .  $this->nombreTabla;
        $txtInsercion   = $txtInsercion . " SET ";
        $txtInsercion   = $txtInsercion . " nIDDispositivoAuto ='" . $this->especialesHTML($nIDDispositivoAuto) . "',";
        $txtInsercion   = $txtInsercion . " Link ='" . $this->especialesHTML($Link) . "',";
        $txtInsercion   = $txtInsercion . " Respuesta ='" . $this->especialesHTML($Respuesta) . "',";
        $txtInsercion   = $txtInsercion . " Latitud ='" . $this->especialesHTML($Latitud) . "',";
        $txtInsercion   = $txtInsercion . " Longitud ='" . $this->especialesHTML($Longitud) . "',";
        $txtInsercion   = $txtInsercion . " Velocidad ='" . $this->especialesHTML($Velocidad) . "',";
        $txtInsercion   = $txtInsercion . " Hora ='" . $this->especialesHTML($Hora) . "',";
        $txtInsercion   = $txtInsercion . " Fecha ='" . $this->especialesHTML($Fecha) . "',";

        

        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nIDHistorial;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarNivel ***************
     *
     * @param [int] $nID
     * @param [int] $nIDNivel
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarNivel($nID, $nIDNivel, $observaciones)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion   = "UPDATE " .  $this->nombreTabla;
        $txtInsercion   = $txtInsercion . " SET ";
        $txtInsercion   = $txtInsercion . " nIDNivel=" . $nIDNivel . ",";


        $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion   = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion   = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion   = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarCodigoUnico ***************
     *
     * @param [int] $nID
     * @param [int] $codigoUnico
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarCodigoUnico($nID, $codigoUnico, $observaciones)
    {
        
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion   = "UPDATE " .  $this->nombreTabla;
        $txtInsercion   = $txtInsercion . " SET ";
        $txtInsercion   = $txtInsercion . " CodigoUnico='" . $codigoUnico . "',";


        $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion   = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion   = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion   = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarTerminos ***************
     *
     * @param [int] $nID
     * @param [string] $aceptoTerminos
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarTerminos($nID, $aceptoTerminos, $observaciones)
    {
        
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion   = "UPDATE " .  $this->nombreTabla;
        $txtInsercion   = $txtInsercion . " SET ";
        $txtInsercion   = $txtInsercion . " AceptoTerminos='" . $aceptoTerminos . "',";


        $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion   = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion   = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion   = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función grabar ***************
     *
     * @param [array] $tabla
     * @return void
     */
    private function grabar($tabla)
    {

        // Abre la conexion
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        for ($i = 0;$i<$this->contarDatos; $i++){
            if($tabla[$i]["Crear"]==TRUE){
                $txtInsercion = "INSERT INTO " . $this->nombreTabla;
                $txtInsercion = $txtInsercion . "(";

                $txtInsercion = $txtInsercion . "nIDHistorial,";
                $txtInsercion = $txtInsercion . "nIDDispositivoAuto,";
                $txtInsercion = $txtInsercion . "nIDSolicitud,";
                $txtInsercion = $txtInsercion . "Link,";
                $txtInsercion = $txtInsercion . "Respuesta,";
                $txtInsercion = $txtInsercion . "Latitud,";
                $txtInsercion = $txtInsercion . "Longitud,";
                $txtInsercion = $txtInsercion . "Velocidad,";
                $txtInsercion = $txtInsercion . "Hora,";
                $txtInsercion = $txtInsercion . "Fecha,";
                $txtInsercion = $txtInsercion . "FechaModificacion,";
                $txtInsercion = $txtInsercion . "FechaCreacion,";                
                $txtInsercion = $txtInsercion . "Observaciones,";
                $txtInsercion = $txtInsercion . "bEstado";


                $txtInsercion = $txtInsercion . ")";
                $txtInsercion = $txtInsercion . " VALUES";
                $txtInsercion = $txtInsercion . "(";

                $txtInsercion = $txtInsercion .       $tabla[$i]["nIDHistorial"]       . ",";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["nIDDispositivoAuto"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["nIDSolicitud"]       . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Link"]               . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Respuesta"]          . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Latitud"]            . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Longitud"]           . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Velocidad"]          . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Hora"]               . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Fecha"]              . "',";

                // Campos bases
                $fechaLocal   = $UtileriasDatos->getFechaYHoraActual_General();
                $fechaLocal   = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
                $txtInsercion = $txtInsercion . "'" . $fechaLocal                      . "',";
                $txtInsercion = $txtInsercion . "'" . $fechaLocal                      . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Observaciones"]      . "',";
                $txtInsercion = $txtInsercion .       $tabla[$i]["bEstado"];

                $txtInsercion = $txtInsercion . ")";

            } else {

                if($tabla[$i]["Cambiar"]==TRUE){

                $txtInsercion = "UPDATE " .  $this->nombreTabla;
                $txtInsercion = $txtInsercion . " SET ";

                $txtInsercion = $txtInsercion . "nIDDispositivoAuto ='" . $tabla[$i]["nIDDispositivoAuto"] . "',";
                $txtInsercion = $txtInsercion . "nIDSolicitud ='"       . $tabla[$i]["nIDSolicitud"]       . "',";
                $txtInsercion = $txtInsercion . "Link ='"               . $tabla[$i]["Link"]               . "',";
                $txtInsercion = $txtInsercion . "Respuesta ='"          . $tabla[$i]["Respuesta"]          . "',";
                $txtInsercion = $txtInsercion . "Latitud ='"            . $tabla[$i]["Latitud"]            . "',";
                $txtInsercion = $txtInsercion . "Longitud ='"           . $tabla[$i]["Longitud"]           . "',";
                $txtInsercion = $txtInsercion . "Velocidad ='"          . $tabla[$i]["Velocidad"]          . "',";
                $txtInsercion = $txtInsercion . "Hora ='"               . $tabla[$i]["Hora"]               . "',";
                $txtInsercion = $txtInsercion . "Fecha = '"             . $tabla[$i]["Fecha"]              . "',";

                // Basicas
                $fechaLocal   = $UtileriasDatos->getFechaYHoraActual_General();
                $fechaLocal   = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
                $txtInsercion = $txtInsercion . " FechaModificacion='"  . $fechaLocal                      . "',";
                $txtInsercion = $txtInsercion . " Observaciones='"      . $tabla[$i]["Observaciones"]      . "'";
                $txtInsercion = $txtInsercion . " WHERE "               . $this->id                        . "=" .$tabla[$i]["nIDHistorial"];

                    

                } else {

                    if($tabla[$i]["Eliminar"]==TRUE){
                        $txtInsercion = "DELETE FROM " . $this->nombreTabla . " WHERE " . $this->id . "=" .$tabla[$i]["nIDHistorial"];

                    }
                }

            }

           // echo $txtInsercion;

            mysqli_query($mysqli, "SET NAMES UTF8");
            $res=mysqli_query($mysqli,$txtInsercion);

           
            $this->last_id = mysqli_insert_id($mysqli);
          
            if($res==FALSE){
                mysqli_close($mysqli);
                return FALSE;
            }

        }
        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función limpiarTabla ***************
     *
     * @return void
     */
    public function limpiarTabla()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->borrar() == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función eliminarPorID ***************
     *
     * @param [int] $nID
     * @return void
     */
    public function eliminarPorID($nID)
    {

        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->borrarID($nID) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función eliminarConCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function eliminarConCondicion($condicion)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if(strlen($condicion)<=0){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene condicion";
            return FALSE;
        }

        if ($this->borrarCondicion($condicion) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función ocultar ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @return void
     */
    public function ocultar($nID, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->borrarTemporal($nID,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarEstado ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @param [int] $bEstado
     * @return void
     */
    public function cambiarEstado($nID, $observaciones, $bEstado)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarEstado($nID,$bEstado,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }


     /**
     *************** Función cambiarPassword ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @param [string] $password
     * @return void
     */
    public function cambiarPassword($nID, $observaciones, $password)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarPassword($nID,$password,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarNombre ***************
     *
     * @param [int] $nID
     * @param [string] $nombre
     * @param [string] $observaciones
     * @return void
     */
    public function cambiarNombre($nID, $nombre, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarNombre($nID,$nombre,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarVIP ***************
     *
     * @param [int] $nIDEmpresa
     * @param [string] $RazonSocial
     * @param [string] $Calle
     * @param [string] $NoExterior
     * @param [string] $NoInterior
     * @param [string] $Colonia
     * @param [string] $Ciudad
     * @param [string] $Estado
     * @param [string] $Pais
     * @param [string] $Cp
     * @param [string] $RFC
     * @param [string] $NumeroTarjeta
     * @param [string] $Tipo
     * @param [string] $FechaVencimiento
     * @param [string] $Telefono
     * @param [string] $Email
     * @param [string] $observaciones
     * @return void
     */
    public function cambiarVIP($nIDHistorial,$nIDDispositivoAuto,$nIDSolicitud,$Link,$Respuesta,$Latitud,
        $Longitud,$Velocidad,$Hora,$Fecha,$observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarVIP($nIDHistorial,$nIDDispositivoAuto,$nIDSolicitud,$Link,$Respuesta,$Latitud,
        $Longitud,$Velocidad,$Hora,$Fecha,$observaciones)==TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarNivel ***************
     *
     * @param [type] $nID
     * @param [type] $nIDNivel
     * @param [type] $observaciones
     * @return void
     */
    public function cambiarNivel($nID, $nIDNivel, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarNivel($nID,$nIDNivel, $observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarCodigoUnico ***************
     *
     * @param [int] $nID
     * @param [int] $codigoUnico
     * @param [string] $observaciones
     * @return void
     */
    public function cambiarCodigoUnico($nID, $codigoUnico, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarCodigoUnico($nID,$codigoUnico, $observaciones)==TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función leer ***************
     *
     * @return void
     */
    public function leer()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if($this->consultar() == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función leerCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function leerCondicion($condicion)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if(strlen($condicion) <= 0){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene Condicion";
            return FALSE;
        }

        if($this->consultarCondicion($condicion) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función totalRegistros ***************
     *
     * @return void
     */
    public function totalRegistros()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError = TRUE;
            $this->error       = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->registrosTabla();
    }

    /**
     *************** Función numeroTotalRegistrosCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function numeroTotalRegistrosCondicion($condicion)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->contarRegistrosCondicion($condicion);
    }

    /**
     *************** Función getEstado ***************
     *
     * @param [int] $nID
     * @return void
     */
    public function getEstado($nID)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->ConsultarEstado($nID);
    }

    /**
     *************** Función getID ***************
     *
     * @param [int] $usuario
     * @return void
     */
    public function getID($usuario)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->consultarID($usuario);
    }

    /**
     *************** Función ejecutar ***************
     *
     * @return void
     */
    public function ejecutar()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }


        if($this->contarDatos<=0){
            return FALSE;
        }

        if($this->grabar($this->dtGrabar) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
?>