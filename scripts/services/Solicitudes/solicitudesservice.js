'use strict';

angular.module('abgpassportApp')

	   .service('solicitudesServices',['$http','configService',

	   	function($http,configService){

	   		return {

	   				get : function(){

	   					return $http.get(configService.apiUrl + 'Solicitud.php?accion=buscar',

	   						{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   					); 

	   				},

	   				getBuscar : function(){

	   					return $http.get(configService.apiUrl + 'Solicitud.php?accion=buscarCuenta',

	   						{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   					);

	   				},

	   				getRestantes : function(){

	   					return $http.get(configService.apiUrl + 'Solicitud.php?accion=buscarFechasRestantes',

	   						{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   					);

	   				},

	   				getEnRutas : function(){

	   					return $http.get(configService.apiUrl + 'Solicitud.php?accion=buscarRutas',

	   						{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   					);

	   				},

	   				getPorAtender : function(){

	   					return $http.get(configService.apiUrl + 'Solicitud.php?accion=buscarAsignados',

	   						{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   					);

	   				},

	   				getSolicitudes : function(){

	   					return $http.get(configService.apiUrl + 'Solicitud.php?accion=buscarSolicitudes',

	   						{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   					);

	   				},

	   				update : function(objeto){ console.log(objeto);

                    	return $http.post(configService.apiUrl + 'Solicitud.php','accion=modificar&objeto=' + JSON.stringify(objeto),

                      		{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                    	);

                	},

                	updateEstado : function(id){

                		return $http.post(configService.apiUrl + 'Solicitud.php','accion=modificarEstado&id=' + JSON.stringify(id),

                			{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                		);

                	},

                	getCalendario : function(){ 

                		return $http.get(configService.apiUrl + 'Solicitud.php?accion=fechasEnCalendario',

                			{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } } 

                		);

                	},

                	getRecepcion : function(){ 

                		return $http.get(configService.apiUrl + 'Solicitud.php?accion=Recepcion',

                			{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } } 

                		);

                	},

                	getImagenes : function(id){

                		return $http.post(configService.apiUrl + 'Solicitud.php','accion=Imagenes&id=' + JSON.stringify(id),
                			{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                		);
                		
                	}
	   		};


	   	}]);