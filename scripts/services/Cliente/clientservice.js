'use strict';

angular.module('abgpassportApp')
  
       .service('clientServices', ['$http','configService', function($http,configService){

            return{

                get : function(){

                    return $http.get(configService.apiUrl + 'Cliente.php?accion=consultar',

                      { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                    );

                },

                add : function(cliente){

                    return $http.post(configService.apiUrl + 'Cliente.php','accion=agregar&cliente=' + JSON.stringify(cliente),

                      { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                    );

                },

                update : function(cliente){

                    return $http.post(configService.apiUrl + 'Cliente.php','accion=modificar&cliente=' + JSON.stringify(cliente),

                      { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                    );

                },

                destroy : function(id){

                    return $http.post(configService.apiUrl + 'Cliente.php','accion=eliminar&id=' + JSON.stringify(id),

                      { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                    );
                    
                }

            };
         
       }]);