'use strict';

angular.module('abgpassportApp')

       .controller('MarketStallCtrl',['$scope','marketstallServices',function($scope,marketstallServices){

       $scope.puesto = {
             nIDPuesto     : '0',
             Puesto        : '',
             Descripcion   : ''

       };

       $scope.puestoEdit = {

             nIDPuesto     : '0',
             Puesto        : '',
             Descripcion   : ''

       };


       $scope.clean = function(){

             $scope.puesto = {

                   nIDPuesto        : '0',
                   Puesto           : '',
                   Descripcion      : ''

             };

             $('#add_modal').modal('hide');

       };


       $scope.cleanEdit = function(){

              $scope.puestoEdit = {

                    nIDPuesto        : '0',
                    Puesto           : '',
                    Descripcion      : ''

              };

              $('#update_modal').modal('hide');

       };

       $scope.get = function(){

             marketstallServices.get().then(function(resp){

                         if(resp.data == 'null'){

                                $scope.icono   = 'assets/img/hecho.png';
                                $scope.mensaje = 'No tienes puestos registrados';

                         }else{

                              $scope.puestos =  resp.data;

                         }

             },function(err){

                console.log('error' + JSON.stringify(err));

             });

       };

       $scope.add = function(puesto){

             if(puesto.Puesto.length == 0){

                   swal.fire({

                       title              : 'Oops!',

                       text               : 'Puesto vacio completalo',

                       imageUrl           : 'assets/img/advertencia.png',

                       showCloseButton    : true,

                       confirmButtonColor : '#044BFD'

                   });

             }else if(puesto.Descripcion.length == 0){

                         swal.fire({

                             title                : 'Oops!',

                             text                 : 'Descripcion vacio completalo',

                             imageUrl             : 'assets/img/advertencia.png',

                             showCloseButton      : true,

                             confirmButtonColor   : '#044BFD'

                         });

             }else{


                      marketstallServices.add(puesto).then(function(resp){

                                          if(resp.data == 'INSERTED'){

                                                  swal.fire({

                                                      title            : 'Registro exitoso!',

                                                      imageUrl         : 'assets/img/completo.png',

                                                      showCloseButton  : true,

                                                      confirmButtonColor : '#044BFD'

                                                  });

                                                  $scope.get();

                                                  $scope.clean();

                                          }else{

                                                  swal.fire({

                                                      title                 : 'Oops!',
 
                                                      text                  : 'No se logro el registro,inténtalo de nuevo',

                                                      imageUrl              : 'assets/img/error.png',

                                                      showCloseButton       : true,

                                                      confirmButtonColor    : '#044BFD'

                                                  });

                                          }

                      },function(err){

                                console.log('error' + JSON.stringify(err));

                      });

             }

       };


       $scope.show = function(puesto){
        
             $scope.puestoEdit.nIDPuesto     = puesto.nIDPuesto;
             $scope.puestoEdit.Puesto        = puesto.Puesto;
             $scope.puestoEdit.Descripcion   = puesto.Descripcion;
       };

       $scope.update = function(puesto){

              if(puesto.Puesto.length == 0){

                   swal.fire({

                       title              : 'Oops!',

                       text               : 'Puesto vacio completalo',

                       imageUrl           : 'assets/img/advertencia.png',

                       showCloseButton    : true,

                       confirmButtonColor : '#044BFD'

                   });

             }else if(puesto.Descripcion.length == 0){

                         swal.fire({

                             title                : 'Oops!',

                             text                 : 'Descripcion vacio completalo',

                             imageUrl             : 'assets/img/advertencia.png',

                             showCloseButton      : true,

                             confirmButtonColor   : '#044BFD'

                         });

             }else{

                        marketstallServices.update(puesto).then(function(resp){

                                    if(resp.data == 'UPDATED'){

                                           swal.fire({

                                                title              : 'Modificación exitosa!',

                                                imageUrl           : 'assets/img/hecho.png',

                                                showCloseButton    : true,

                                                confirmButtonColor : '#044BFD'

                                           });

                                           $scope.cleanEdit();

                                           $scope.get();

                                    }else{

                                           swal.fire({

                                               title                : 'Oops!',

                                               text                 : 'No se completo la modificación, inténtalo de nuevo',

                                               imageUrl             : 'assets/img/error.png',

                                               showCloseButton      : true,

                                               confirmButtonColor   : '#044BFD'

                                           });

                                    }

                        },function(err){

                                  console.log('error' + JSON.stringify(err));

                        });

             }
       
       };


       $scope.destroy = function(id){

              swal.fire({

                  title               : 'ADVERTENCIA',

                  text                : '¿Estás seguro de eliminar este registro?',

                  imageUrl            : 'assets/img/advertencia.png',

                  showCancelButton    : true,

                  showCloseButton     : true,                  

                  cancelButtonColor   : 'rgba(4,75,253,0.5)',                  

                  confirmButtonColor  : '#044BFD',

                  cancelButtonText    : 'Cancelar',

                  confirmButtonText   : 'Si',

              }).then((result) => {

                       if(result.value){

                                marketstallServices.destroy(id).then(function(resp){

                                              if(resp.data == "DELETED"){

                                                      swal.fire({

                                                          title              : 'Registro eliminado con exito!',

                                                          imageUrl           : 'assets/img/completo.png',

                                                          showCloseButton    : true,

                                                          confirmButtonColor : '#044BFD'

                                                      });

                                                      $scope.get();

                                              }else{

                                                      swal.fire({

                                                          title             : 'Oops!',

                                                          text              : 'No se pudo eliminar el registro inténtalo de nuevo',

                                                          imageUrl          : 'assets/img/error.png',

                                                          showCloseButton   : true,

                                                          confirmButtonColor: '#044BFD'

                                                      });

                                              }

                                },function(err){

                                      console.log('error' + JSON.stringify(err));

                                });

                       }else{

                                swal.fire({

                                    title               : 'Se cancelo la eliminicación!',

                                    imageUrl            : 'assets/img/error.png',

                                    showCloseButton     : true,

                                    confirmButtonColor  : '#044BFD'

                                });
                 
                       }
             
              });
        
        };

       }]);