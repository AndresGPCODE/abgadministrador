'use strict';

angular.module('abgpassportApp')

       .controller('typeDocumentCtrl',['$scope','typedocumentServices',

        function($scope,typedocumentServices){

        $scope.tipodoc = {

              nIDTipoDocumento : '0',
              TipoDocumento    : '',
              Descripcion      : '',
              Tipo             : ''

        };

        $scope.tipodocEdit = {

              nIDTipoDocumento : '0',
              TipoDocumento    : '',
              Descripcion      : '',
              Tipo             : ''

        };

        $scope.clean = function(){

              $scope.tipodoc = {

                    nIDTipoDocumento  : '0',
                    TipoDocumento     : '',
                    Descripcion       : '',
                    Tipo              : ''

              };

              $('#add_modal').modal('hide');

        };

        $scope.cleanEdit = function(){

              $scope.tipodocEdit = {

                    nIDTipoDocumento  : '0',
                    TipoDocumento     : '',
                    Descripcion       : '',
                    Tipo              : ''

              };

              $('#update_modal').modal('hide');

        };

        $scope.get = function(){

              $scope.getA();

              $scope.types = '';

              typedocumentServices.get().then(function(resp){

                                  if(resp.data == 'null'){

                                         console.log('Not Faound Data');

                                  }else{

                                         $scope.types = resp.data;

                                  }

              },function(err){

                        console.log('error' + JSON.stringify(err));

              });

        };

        $scope.getA = function(){

              $scope.typeAs = '';

              typedocumentServices.getA().then(function(resp){

                                  if(resp.data == 'null'){

                                         console.log('Not Found Data');

                                  }else{

                                         $scope.typeAs = resp.data;

                                  }
              
              },function(err){

                    console.log('error' + JSON.stringify(err));

              });

        };

        $scope.add = function(tipodedocumento){

              if(tipodedocumento.TipoDocumento.length == 0){

                                swal.fire({

                                    title               : 'Oops!',

                                    text                : 'Tipo de documento vacio completalo',

                                    imageUrl            : 'assets/img/advertencia.png',

                                    showCloseButton     : true,

                                    confirmButtonColor  : '#044BFD'

                                });

              }else if(tipodedocumento.Descripcion.length == 0){

                                      swal.fire({

                                          title                 : 'Oops!',

                                          text                  : 'Descripción vacio completalo',

                                          imageUrl              : 'assets/img/advertencia.png',

                                          showCloseButton       : true,

                                          confirmButtonColor    : '#044BFD'

                                      });

              }else{

                        typedocumentServices.add(tipodedocumento).then(function(resp){

                                            if(resp.data == "INSERTED"){

                                                   swal.fire({

                                                       title                : 'Registro exitoso!',

                                                       imageUrl             : 'assets/img/completo.png',

                                                       showCloseButton      : true,

                                                       confirmButtonColor   : '#044BFD'

                                                   });

                                                   $scope.clean();

                                                   $scope.get();
                                            }else{

                                                   swal.fire({

                                                       title              : 'Oops!',

                                                       text               : 'Lo siento no se logro el registro, inténtalo de nuevo',

                                                       imageUrl           : 'assets/img/error.png',

                                                       showCloseButton    : true,

                                                       confirmButtonColor : '#044BFD'

                                                   });

                                            }

                        },function(err){

                                  console.log('error' + JSON.stringify(err));

                        });

              }

        };

        $scope.show = function(tipo){

              $scope.tipodocEdit.nIDTipoDocumento = tipo.nIDTipoDocumento;
              $scope.tipodocEdit.TipoDocumento    = tipo.TipoDocumento;
              $scope.tipodocEdit.Descripcion      = tipo.Descripcion;
              $scope.tipodocEdit.Tipo             = tipo.Tipo;


        };

        $scope.showA = function(tipo){

              $scope.tipodocEdit.nIDTipoDocumento = tipo.nIDTipoDocumento;
              $scope.tipodocEdit.TipoDocumento    = tipo.TipoDocumento;
              $scope.tipodocEdit.Descripcion      = tipo.Descripcion;
              $scope.tipodocEdit.Tipo             = tipo.Tipo;


        };

        $scope.update = function(tipodedocumento){

              if(tipodedocumento.TipoDocumento.length == 0){

                                swal.fire({

                                    title               : 'Oops!',

                                    text                : 'Tipo de documento vacio completalo',

                                    imageUrl            : 'assets/img/advertencia.png',

                                    showCloseButton     : true,

                                    confirmButtonColor  : '#044BFD'

                                });

              }else if(tipodedocumento.Descripcion.length == 0){

                                      swal.fire({

                                          title                 : 'Oops!',

                                          text                  : 'Descripción vacio completalo',

                                          imageUrl              : 'assets/img/advertencia.png',

                                          showCloseButton       : true,

                                          confirmButtonColor    : '#044BFD'

                                      });

              }else{

                        typedocumentServices.update(tipodedocumento).then(function(resp){

                                            if(resp.data == "UPDATED"){

                                                   swal.fire({

                                                       title                : 'Modificación exitosa!',

                                                       imageUrl             : 'assets/img/completo.png',

                                                       showCloseButton      : true,

                                                       confirmButtonColor   : '#044BFD'

                                                   });

                                                   $scope.cleanEdit();

                                                   $scope.get();

                                                  $scope.getA(); 
                                            }else{

                                                   swal.fire({

                                                       title              : 'Oops!',

                                                       text               : 'Lo siento no se logro la modificación, inténtalo de nuevo',

                                                       imageUrl           : 'assets/img/error.png',

                                                       showCloseButton    : true,

                                                       confirmButtonColor : '#044BFD'

                                                   });
                                                   
                                            }

                        },function(err){

                                  console.log('error' + JSON.stringify(err));

                        });

              }

        };

        $scope.destroy = function(id){

              swal.fire({

                  title                 : 'ADVERTENCIA',

                  text                  : '¿Estás seguro de eliminar este registro?',

                  imageUrl              : 'assets/img/advertencia.png',

                  showCancelButton      : true,

                  showCloseButton       : true,

                  confirmButtonColor    : '#044BFD',

                  cancelButtonColor     : 'rgba(4,75,253,0.5)',

                  confirmButtonText     : 'Si',

                  cancelButtonText      : 'Cancelar'

              }).then((result) => {

                      if(result.value){

                                typedocumentServices.destroy(id).then(function(resp){

                                                if(resp.data == "DELETED"){

                                                        $scope.get();

                                                        swal.fire({

                                                            title             : 'Registro eliminado con exito!',

                                                            imageUrl          : 'assets/img/completo.png',

                                                            showCloseButton   : true,

                                                            confirmButtonColor: '#044BFD'

                                                        });

                                                }else{

                                                        swal.fire({
 
                                                            title                 : 'Oops!',

                                                            text                  : 'No se pudo eliminar el registro, inténtalo de nuevo',

                                                            imageUrl              : 'assets/img/error.png',

                                                            showCloseButton       : true,

                                                            confirmButtonColor    : '#044BFD'

                                                        });

                                                }

                                }, function(err){

                                    console.log('error' + JSON.stringify(err));

                                });

                      }else{

                            swal.fire({

                                title               : 'Se cancelo la eliminación!',

                                imageUrl            : 'assets/img/error.png',

                                showCloseButton     : true,

                                confirmButtonColor  : '#044BFD'

                            });

                      }

              });


        };

        }]);