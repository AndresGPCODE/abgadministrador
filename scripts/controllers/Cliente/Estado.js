'use strict';

angular.module('abgpassportApp')

	   .controller('EstadoCtrl',['$scope','accountstatuServices','clientServices',

	   	function($scope,accountstatuServices,clientServices){
	   		$scope.identificador ={

	   			  id : '0'
	   		};


	   		$scope.get = function(){

	   			  clientServices.get().then(function(resp){

	   			  				if(resp.data == 'null'){

	   			  						console.log('Not Found Data');


	   			  				}else{

	   			  						$scope.clientes = resp.data;

	   			  				}

	   			  },function(err){

	   			  		console.log('error' + JSON.stringify(err));

	   			  });

	   		};

	   		$scope.estado = function(id){

	   			  $scope.mensaje = '';
	   			  $scope.cuentas = '';
	   			  var suma = 0;
	   			  $scope.Total = '';
	   			  $scope.identificador.id = id;


	   			   accountstatuServices.getEstado(id).then(function(resp){

	   			   					   if(resp.data == 'null'){

	   			   					   		  console.log('Not Found data');
	   			   					   		  $scope.mensaje = 'No tienes adeudos';
	   			   					   		  $scope.Total = '0.00';

	   			   					   }else{

	   			   					   		  $scope.cuentas = resp.data;
	   			   					   		   for(var i = 0; i < resp.data.length; i++){

        		  					  		 		 suma = parseInt(suma) + parseInt(resp.data[i]['Costo']); console.log(suma);

        		  					  		 }

        		  					  		 $scope.Total = suma;
	   			   					   
	   			   					   }
	   			   
	   			   },function(err){

	   			   		console.log('error' + JSON.stringify(err));

	   			   });

	   		};

	   		$scope.cambiar = function(){ console.log($scope.identificador.id);

	   			swal.fire({

                  title               : 'ADVERTENCIA',

                  text                : '¿Estás seguro que se realizó el pago correspondiente?',

                  imageUrl            : 'assets/img/advertencia.png',

                  showCancelButton    : true,

                  showCloseButton     : true,                  

                  cancelButtonColor   : 'rgba(4,75,253,0.5)',                  

                  confirmButtonColor  : '#044BFD',

                  cancelButtonText    : 'Cancelar',

                  confirmButtonText   : 'Si',

              }).then((result) => {

                       if(result.value){

                                accountstatuServices.update($scope.identificador.id).then(function(resp){

	   			  					  if(resp.data == 'UPDATED'){

	   			  					  		 swal.fire({

	   			  					  		 	 title 					: 'No tienes adeudos',

	   			  					  		 	 imageUrl 				: 'assets/img/completo.png',

	   			  					  		 	 showCloseButton		: true,

	   			  					  		 	 confirmButtonColor     : '#044BFD'

	   			  					  		 });

	   			  					  		 $scope.estado($scope.identificador.id);

	   			  					  }else{

	   			  					  		 swal.fire({

	   			  					  		 	 title 					 : 'Oops!',

	   			  					  		 	 text 					 : 'No se completo el pago, Inténtalo de nuevo',

	   			  					  		 	 imageUrl 				 : 'assets/img/error.png',

	   			  					  		 	 showCloseButton 		 : true,

	   			  					  		 	 confirmButtonColor 	 : '#044BFD'

	   			  					  		 });

	   			  					  }

	   			  },function(err){

	   			  	console.log('error' + JSON.stringify(err));

	   			  });

                       }else{

                                swal.fire({

                                    title               : 'Se cancelo la eliminación!',

                                    imageUrl            : 'assets/img/error.png',

                                    showCloseButton     : true,

                                    confirmButtonColor  : '#044BFD'

                                });
                 
                       }
             
              });

	   			  
	   		};



	   	}]);