<?php
/*
------ Obtencion y manejo de chats 
*/
header('Content-Type: application/json');
date_default_timezone_set('America/Mexico_City');
$method = $_SERVER["REQUEST_METHOD"];
$method = 'POST';
include 'conexion.php';
// $token = getUserToken(3, 0, 10, 'CLIENTE');
// sendPushNotification($token['Token'], 'Mensaje de CLIENTE' , 'Hola que tal   como te va' , ['ACCION' => 'CARGAR_CHAT', 'TIPO' => 'CHOFER']);

// return;

switch ($method) {
    case 'POST':
        $request = json_decode(file_get_contents('php://input'), true);
        // $request['nIDCliente'] = 3;
        // $request['nIDChofer'] = 0;
        // $request['nIDUsuario'] = 0;
        // $request['option'] = 2;
        // $request['Remitente'] = "CLIENTE";

        $request['nIDCliente'] = isset($request['nIDCliente']) ? $request['nIDCliente'] : 0;
        $request['nIDChofer'] = isset($request['nIDChofer']) ? $request['nIDChofer'] : 0;
        $request['nIDUsuario'] = isset($request['nIDUsuario']) ? $request['nIDUsuario'] : 0;
        $request['option'] = isset($request['option']) ? $request['option'] : 0;
        $request['Remitente'] = isset($request['Remitente']) ? $request['Remitente'] : '';
        $request['Texto'] = isset($request['Texto']) ? $request['Texto'] : '';
        $request['ids'] = isset($request['ids']) ? $request['ids'] : '0'; 

        if (is_null($request['nIDCliente']) || is_null($request['nIDChofer']) || is_null($request['nIDUsuario']) || is_null($request['Remitente'])) {
            $request['resp'] = 'Los Datos enviados están Incompletos||' . $request . " || " . file_get_contents('php://input');
            $request['status'] = false;
            return json_encode($request);
        } else {
            switch ($request['option']){
                case 1:
                    $resp = obternerChat($request['nIDCliente'], $request['nIDChofer'], $request['nIDUsuario'], $request['Remitente']);
                    //var_dump($resp);
                    if ($resp[0]['status']) {
                        echo json_encode($resp);
                        return json_encode($resp);
                    } else {
                        $request['resp'] = 'No se pudo realizar el proceso';
                        $request['status'] = false;
                        echo json_encode($request);
                        return json_encode($request);
                    }
                break;
                case 2:
                    $resp = enviarMensaje($request);
                    // var_dump($resp);
                    if ($resp[0]['status']) {
                        echo json_encode($resp);
                        return json_encode($resp);
                    } else {
                        $request['resp'] = 'No se pudo realizar el proceso';
                        $request['status'] = false;
                        echo json_encode($request);
                        return json_encode($request);
                    }
                break;
                case 3:
                    if  ($request['ids'] != '0'){
                        $resp = leerTodos($request['ids']);
                        // var_dump($resp);
                        if ($resp[0]['status']) {
                            echo json_encode($resp);
                            return json_encode($resp);
                        } else {
                            $request['resp'] = $resp['resp'];
                            $request['status'] = false;
                            echo json_encode($request);
                            return json_encode($request);
                        }
                    } else {
                        $request['resp'] = 'Sin mensajes para procesar';
                        $request['status'] = false;
                        echo json_encode($request);
                        return json_encode($request);
                    }
                    
                    break;
                default:
                    $request['resp'] = 'No se pudo realizar el proceso';
                    $request['status'] = false;
                    echo json_encode($request);
                    return json_encode($request);
                break;
            }
        }
        break;

    default:
        $request['resp'] = 'No se puede procesar la informacion';
        $request['status'] = false;
        return json_encode($request);
}
return;

function obternerChat($idCliente, $idChofer, $idUsuario, $remitente){
    // Consulta para obtencion de los mensajes entre cliente-chofer o cliente-usuario
    // el usuario es el gerente asignado a ese cliente
    $pdo = Conexion();
    $select = "SELECT * FROM tbl_chats WHERE nIDCliente = $idCliente AND nIDChofer = $idChofer AND nIDUsuario = $idUsuario ORDER BY FechaCreacion";
    $ejecutar = $pdo->prepare($select);
    $ejecutar->execute();
    $result = $ejecutar->fetchAll(PDO::FETCH_ASSOC);
    $arr = [];
    if (count($result) > 0) {
        foreach ($result as $row) {
            // Por cada fila de resultado
            if ($row['Remitente'] == $remitente){
                $row['Estatus'] = 'LEIDO';
                $row['isIncoming'] = false;
            } else {
                // $row['Estatus'] = 'NO LEIDO';
                // $row['Remitente'] = false;
                $row['isIncoming'] = true;
            }
            $row['status'] = true;
            array_push($arr, $row);
        }
        return $arr;
    } else {
        $row['status'] = false;
        $row['resp'] = "No hay estados de cuenta pendientes";
        array_push($arr, $row);
        return $arr;
    }
}

function enviarMensaje($datos){
    // Insertar proximo cambio del cliente
    $pdo = Conexion();
    $fechaActual = date('Y-m-d H:i:s');
    // nIDCliente, nIDChofer, nIDUsuario, Texto, Remitente, Estatus, FechaModificacion, FechaCreacion, Observaciones, bEstado
    $insert = "INSERT INTO tbl_chats(nIDCliente, nIDChofer, nIDUsuario, Texto, Remitente, Estatus, FechaModificacion, FechaCreacion, Observaciones, bEstado) 
    VALUES (" . $datos['nIDCliente'] . "," . $datos['nIDChofer'] . "," . $datos['nIDUsuario'] . ",'" . $datos['Texto'] . "','" . $datos['Remitente'] . "','NO LEIDO','$fechaActual','$fechaActual','Nuevo mensaje',1)";
    $ejecutar = $pdo->prepare($insert);
    $ejecutar->execute();
    $result = $pdo->lastInsertId();
    $arr = [];
    if ($result > 0) {
        // $token = getUserToken($datos['nIDCliente'], $datos['nIDUsuario'], $datos['nIDChofer'], $datos['Remitente']);
        // if(!is_null($token)){
        //     sendPushNotification($token, ucwords(strtolower($datos['Remitente'])), $datos['Texto'], ['Data' => 'Mensaje recibido']);
        // }
        $row['lastID'] = $result;
        $row['status'] = true;
        array_push($arr, $row);
        return $arr;
    } else {
        $row['status'] = false;
        array_push($arr, $row);
        return $arr;
    }
}

function leerTodos($datos){
    $pdo = Conexion();
    $date = date('Y-m-d H:i:s');
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);
    $update = '';
    if ($datos == '0'){
        $row['status'] = false;
        $row['resp'] = 'Sin mensajes que procesar';
        array_push($arr, $row);
        return $arr;
    } else {
        $arrIds = explode(',', $datos);
        foreach($arrIds as $id){
            $update = $update . "
            UPDATE tbl_chats SET Estatus = 'LEIDO', FechaModificacion = '$date' WHERE nIDChat = $id;";
        }
        $ejecutar = $pdo->prepare($update);
        $ejecutar->execute();
        $arr = [];
        if ($ejecutar->rowCount() > 0) {
            $row['status'] = true;
            array_push($arr, $row);
            return $arr;
        } else {
            $row['status'] = false;
            $row['resp'] = 'No se pudieron procesar los mensajes';
            array_push($arr, $row);
            return $arr;
        }
    }
    
}

function getUserToken($idCliente,$idUsuario,$idChofer, $remitente){
    $id = 0;
    switch($remitente){
        case 'CLIENTE':
            $id = $idChofer;
            $select = "SELECT Token FROM tbl_choferes WHERE nIDChofer = $id LIMIT 1";
        break;
        case 'CHOFER':
            $id = $idCliente;
            $select = "SELECT Token FROM tbl_clientes WHERE nIDCLiente = $id LIMIT 1";
        break;
        default:
            $id = 0;
            $select = "";
        break;
    }
    //var_dump($select);
    if($id != 0){
        $pdo = Conexion();
        $ejecutar = $pdo->prepare($select);
        $ejecutar->execute();
        $result = $ejecutar->fetch(PDO::FETCH_ASSOC);
        $arr = [];
        if ($result) {
            $arr = $result;
            return $arr;
        } else {
            return null;
        }
    } else {
        return null;
    }
   
}

function sendPushNotification($userToken, $title, $message, $data){
    $request = [
        'to'            => $userToken,
        'notification'  => [
            'title' => $title,
            'body'  => $message
        ],
        'data'          => $data
    ];
    //var_dump($request);
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
    // esta direccion poniendola directamnte en el buscador te manda a la documentacion (https://fcm.googleapis.com/fcm/send) 
    // ó directamente esta otra (https://firebase.google.com/docs/cloud-messaging/http-server-ref).
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($request));
    // No cambies el valor de Authorization: key = 'KEY_SERVER'
    curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Authorization: key=AAAATEJtwAc:APA91bEtufJgtkm1NCVUZakkRnwXQTlzKcSeoGQ8imnKn0RmFZUIAPwnfLmO8S6PR0BC1ilvAz0sayA95Tm-xJ2Q1MsT358g8Q0Zp7gI7J4inMrrs9Z73QFLRsWqH4Lco2IZeiQNIKcK']);
    $resp = curl_exec($curl);
    curl_close($curl);
    return $resp;
}