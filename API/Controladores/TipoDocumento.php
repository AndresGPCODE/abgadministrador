<?php
    
  error_reporting(E_ALL ^ E_NOTICE);
  ini_set('display_errors','1');

  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Request-Methods');

  include_once '../Configuraciones/Conexion.php';
  include_once '../Documento/clTipoDocumento.php';
  include_once '../Utilerias/clHerramientas_v2011.php';

  $method = $_SERVER['REQUEST_METHOD'];

  $UtileriasDatos    = new clHerramientasv2011();
  $fechaLocal        = $UtileriasDatos->getFechaYHoraActual_General();
  $fechaLocal        = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

  $typedocument = new clTipoDocumento();

  $typedocument->conexion($SERVER,$USER,$PASSWORD,$BD);

  switch($method){

      case 'GET' : 

            $action = $_GET['accion'];

            if($action == 'consultar'){

                      $typedocument->leerCondicion('bEstado = 1 and Tipo = 0');

                      $registro = $typedocument->dtBase();

                      echo json_encode($registro);


            }else if($action == 'consultarA'){

                     $typedocument->leerCondicion('bEstado = 1 and Tipo = 1');

                      $registro = $typedocument->dtBase();

                      echo json_encode($registro);

                      break;
            }



      case 'POST' : 

            $action = $_POST['accion'];

            if($action == 'consultarTipos'){

                      $id = $_POST['id'];

                      $typedocument->leerCondicion(' Tipo = 0 and bEstado = 1 and nIDTipoDocumento NOT IN(SELECT nIDTipoDocumento from tbl_documentos Inner join tbl_cat_documentos_cliente ON tbl_cat_documentos_cliente.nIDDocumento = tbl_documentos.nIDDocumento where tbl_cat_documentos_cliente.nIDCliente = '.$id.' and tbl_cat_documentos_cliente.bEstado = 1)');

                      $registro = $typedocument->dtBase();

                      echo json_encode($registro);

                      break;

            }else if($action == 'consultarTiposAutos'){

                      $id = $_POST['id'];

                      $typedocument->leerCondicion(' Tipo = 1 and bEstado = 1 and nIDTipoDocumento NOT IN(SELECT nIDTipoDocumento from tbl_documentos Inner join tbl_cat_documento_automovil ON tbl_cat_documento_automovil.nIDDocumento = tbl_documentos.nIDDocumento where tbl_cat_documento_automovil.nIDAutomovil = '.$id.' and tbl_cat_documento_automovil.bEstado = 1)');

                      $registro = $typedocument->dtBase();

                      echo json_encode($registro);

                      break;

            }else if($action == 'agregar'){

                      $objeto = $_POST['tipo'];

                      $data   = json_decode($objeto);

                      $typedocument->setInformacion(

                                  $data->nIDTipoDocumento,
                                  $data->TipoDocumento,
                                  $data->Descripcion,
                                  $data->Tipo,
                                  '',
                                  '',
                                  'Nuevo Tipo de documento registrado - ' . $fechaLocal,
                                  '1',
                                  TRUE,
                                  FALSE,
                                  FALSE

                      );

                      if($typedocument->ejecutar('tbl_cat_tipo_documento')){

                                      echo "INSERTED";

                      }else{

                                      echo "NOT INSERTED";

                      }

            }else if($action == 'modificar'){

                      $objeto = $_POST['tipo'];

                      $data   = json_decode($objeto);

                      $typedocument->setInformacion(

                                  $data->nIDTipoDocumento,
                                  $data->TipoDocumento,
                                  $data->Descripcion,
                                  $data->Tipo,
                                  '',
                                  '',
                                  'Modificación de Tipo de documento - ' . $fechaLocal,
                                  '1',
                                  FALSE,
                                  TRUE,
                                  FALSE

                      );

                      if($typedocument->ejecutar('tbl_cat_tipo_documento')){

                                      echo "UPDATED";

                      }else{

                                      echo "NOT UPDATED";

                      }

            }else if($action == 'eliminar'){

                            $id = $_POST['id'];

                            $observacion = 'Tipo de documento eliminado temporalmente - ' . $fechaLocal;

                            if($typedocument->ocultar($id,$observacion)){

                                            echo "DELETED";

                            }else{

                                            echo "NOT DELETED";
                            }
            } 

     
  }
?>