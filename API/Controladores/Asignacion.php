<?php
  error_reporting(E_ALL ^ E_NOTICE);  

  ini_set('display_errors', '1');

  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Request-Methods');

  include_once '../Configuraciones/Conexion.php';
  include_once '../Dispositivos/clUsuarioDispositivos.php';
  include_once '../Utilerias/clHerramientas_v2011.php';

  $method         = $_SERVER['REQUEST_METHOD'];

  $UtileriasDatos = new clHerramientasv2011();
  $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
  $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

  $userDevices    = new clUsuarioDispositivos();

  $userDevices->conexion($SERVER,$USER,$PASSWORD,$BD);

  switch($method){

        case 'GET' : 

              $action = $_GET['accion'];

              if($action == 'buscar'){                                  

                  echo json_encode($userDevices->consultarCondicion('tbl_autos_dispositivos.bEstado = 1'));
              }

        case 'POST' : 

             $action = $_POST['accion'];

             if($action == 'agregar'){

                  $object = $_POST['objeto'];

                  $data   = json_decode($object);


                  $userDevices->setInformacion(

                        $data->nIDDispositivoAuto,
                        '0',
                        $data->nIDDispositivo,
                        $data->Identificador,
                        $data->Imei,
                        $data->Lada,
                        $data->Celular,
                        $data->Password,
                        $data->Numero_Confianza,
                        $data->Estatus,
                        '',
                        '',
                        'Nuevo dispositivo asignado - ' . $fechaLocal,
                        '1',
                        TRUE,
                        FALSE,
                        FALSE

                  );

                  if($userDevices->ejecutar('tbl_autos_dispositivos')){

                          echo "INSERTED";

                  }else{

                          echo "NOT INSERTED";

                  }

             }else if($action == 'modificar'){

                      $object = $_POST['objeto'];

                      $data   = json_decode($object);

                      $userDevices->setInformacion(
                        
                        $data->nIDDispositivoAuto,
                        '0',
                        $data->nIDDispositivo,
                        $data->Identificador,
                        $data->Imei,
                        $data->Lada,
                        $data->Celular,
                        $data->Password,
                        $data->Numero_Confianza,
                        $data->Estatus,
                        '',
                        '',
                        'Modificación del dispositivo asignado - ' . $fechaLocal,
                        '1',
                        FALSE,
                        TRUE,
                        FALSE
                      );

                      if($userDevices->ejecutar('tbl_autos_dispositivos')){

                                echo "UPDATED";

                      }else{

                                echo "NOT UPDATED";

                      }

             }else if($action == 'eliminar'){

                      $id = $_POST['id'];

                      $observacion = 'Dispositivo asignado eliminado temporalmente - ' . $fechaLocal;

                      if($userDevices->ocultar($id,$observacion)){

                            echo "DELETED";
                      }else{

                            echo "NOT DELETED";
                      }
             }
  }

  ?>