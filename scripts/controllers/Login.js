'use strict';


angular.module('abgpassportApp')
       
       .controller('LoginCtrl', ['$scope','$location','logServices','sesionServices', 

        function($scope,$location,logServices,sesionServices){

            document.getElementById('bodyAll').style.background =  "linear-gradient(#ffffff 90%, #000000)";

            $scope.logear = {

                  nIDRelaUxS : '0',

                  nIDUsuario : '',

                  GUID       : '',

                  nIDEmpresa : '',

                  email      : '',

                  password   : ''

            };

            $scope.clean = function(){

                  $scope.logear.email = '';
                  $scope.logear.password = '';

            }

            $scope.entrar = function(logear){

                  if(logear.email.length == 0){

                           swal.fire({

                               title              : 'Oops!',

                               text               : 'Ingresa tu correo electrónico',

                               imageUrl           : 'assets/img/advertencia.png',

                               showCloseButton    : true,

                               confirmButtonColor : "#044BFD"
                           
                           });

                  }else if(logear.password.length == 0){

                          swal.fire({

                              title                   : 'Oops!',

                              text                    : 'Ingresa tu contraseña',

                              imageUrl                : 'assets/img/advertencia.png',

                              showCloseButton         : true,

                              confirmButtonColor      : '#044BFD'

                          });

                  }else{

                        logServices.session(logear).then(function(resp){

                                   if(resp.data == 'null'){

                                          swal.fire({

                                              title                 : 'Oops!',

                                              text                  : 'No tenemos tu correo electrónico, inténtalo de nuevo',

                                              imageUrl              : 'assets/img/error.png',

                                              showCloseButton       : true,

                                              confirmButtonColor    : '#044BFD'

                                          });

                                   }else{

                                          $scope.logear.email      = resp.data[0]['Email'];
                                          $scope.logear.password   = resp.data[0]['Password'];
                                          $scope.logear.nIDEmpresa = resp.data[0]['nIDEmpresa'];
                                          $scope.logear.nIDUsuario = resp.data[0]['nIDUsuario'];
                                          $scope.logear.GUID       = uuid.v4();

                                           sessionStorage.setItem("GUID", $scope.logear.GUID);

                                          $scope.sesion($scope.logear);
                                   }
                                    
                        },function(err){

                          console.log('error' + JSON.stringify(err));
                        
                        });
                  }
                  

            };


            $scope.sesion = function(objeto){

                  sesionServices.add(objeto).then(function(resp){

                                if(resp.data == 'INSERTED'){

                                       $location.path('/home');
                                       document.getElementById('bodyAll').style =  "margin:0px;height:100%;width:100%;font-family:'Ubuntu';background : url('assets/img/fondo.png');background-size:400px 400px; background-position: center; background-repeat: no-repeat;background-attachment:fixed;display:grid;";

                                }else{

                                        swal.fire({

                                            title             : 'Problemas de acceso, inténtalo de nuevo',

                                            imageUrl          : 'assets/img/error.png',

                                            showCloseButton   : true,

                                            confirmButtonColor: '#044BFD'

                                        });

                                }
                  }, function(err){

                      console.log('error' + JSON.stringify(err));

                  });

            };


         
       }]);
