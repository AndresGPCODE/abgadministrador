<?php
    
  error_reporting(E_ALL ^ E_NOTICE);
  ini_set('display_errors','1');

  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Request-Methods');

  include_once '../Configuraciones/Conexion.php';
  include_once '../Documento/clDocumentoAutomovil.php';
  include_once '../Utilerias/clHerramientas_v2011.php';

  $method            = $_SERVER['REQUEST_METHOD'];

  $UtileriasDatos    = new clHerramientasv2011();
  $fechaLocal        = $UtileriasDatos->getFechaYHoraActual_General();
  $fechaLocal        = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

  $documentA = new clDocumentoAutomovil();

  $documentA->conexion($SERVER,$USER,$PASSWORD,$BD);

  switch($method){


      case 'POST' :

            $action = $_POST['accion'];

            if($action == 'buscar'){

                      $id = $_POST['id'];

                      $nIDAutomovil = str_replace('"', '', $id);

                      if($documentA->consultarCondicion("tbl_cat_documento_automovil.bEstado = 1 and tbl_cat_documento_automovil.nIDAutomovil = " . $nIDAutomovil)){

                            echo json_encode($documentA->consultarCondicion("tbl_cat_documento_automovil.bEstado = 1 and tbl_cat_documento_automovil.nIDAutomovil = " . $nIDAutomovil));
                        }else{

                            echo "null";
                        }

            }else if($action == 'agregar'){

                      $object = $_POST['objeto'];

                      $data   = json_decode($object);

                      $documentA->setInformacion(

                                  '0',
                                  $data->nIDDocumento, 
                                  '',
                                  '',
                                  '',
                                  '',
                                  $data->nIDAutomovil,
                                  '',
                                  '',
                                  'Nuevo documento del automovil ' . $data->nIDAutomovil . ' - ' . $fechaLocal,
                                  '1',
                                  TRUE,
                                  FALSE,
                                  FALSE

                      );

                      if($documentA->ejecutar('tbl_cat_documento_automovil')){

                                      echo "INSERTED";

                      }else{

                                      echo "NOT INSERTED";
                      
                      }
            
            }else if($action == 'eliminar'){

                              $id = $_POST['id'];

                              $observacion = 'Documento eliminado temporalmente - ' . $fechaLocal;

                              if($documentA->ocultar($id,$observacion)){

                                            echo "DELETED";

                              }else{

                                            echo "NOT DELETED";

                              }
                              
            }
  }
?>