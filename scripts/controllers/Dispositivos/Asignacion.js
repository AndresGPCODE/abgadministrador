'use strict'; 

angular.module('abgpassportApp')

	   .controller('AsignarCtrl',['$scope','asignacionServices','dispositivoServices','inventoryServices',

	   	function($scope,asignacionServices,dispositivoServices,inventoryServices){

	   		$scope.asignacion = {

	   			  nIDDispositivoAuto : '0',
	   			  nIDUsuario         : '',
	   			  nIDDispositivo     : '',
	   			  Identificador      : '',
	   			  Imei 				 : '',
	   			  Lada	 			 : '',
	   			  Celular  			 : '',
	   			  Password 			 : '',
	   			  Numero_Confianza   : '',
	   			  Estatus 			 : '1'

	   		};

	   		$scope.asignacionEdit = {

	   			  nIDDispositivoAuto : '0',
	   			  nIDUsuario         : '',
	   			  nIDDispositivo     : '',
	   			  Identificador      : '',
	   			  Imei 				 : '',
	   			  Lada	 			 : '',
	   			  Celular  			 : '',
	   			  Password 			 : '',
	   			  Numero_Confianza   : '',
	   			  Estatus 			 : '1'

	   		};

	   		$scope.clean = function(){

	   			  $scope.asignacion = {

	   			  		nIDDispositivoAuto   : '0',
	   			  		nIDUsuario           : '',
	   			  		nIDDispositivo       : '',
	   			  		Identificador        : '',
	   			  		Imei 				 : '',
	   			  		Lada	 			 : '',
	   			  		Celular  			 : '',
	   			  		Password 			 : '',
	   			  		Numero_Confianza     : '',
	   			  		Estatus 			 : '1'

	   		      };

	   		      $('#add_modal').modal('hide');

	   		};

	   		$scope.cleanEdit = function(){

	   			  $scope.asignacionEdit = {

	   			  		nIDDispositivoAuto   : '0',
	   			  		nIDUsuario           : '',
	   			  		nIDDispositivo       : '',
	   			  		Identificador        : '',
	   			  		Imei 				 : '',
	   			  		Lada	 			 : '',
	   			  		Celular  			 : '',
	   			  		Password 			 : '',
	   			  		Numero_Confianza     : '',
	   			  		Estatus 			 : '1'

	   		      };

	   		      $('#update_modal').modal('hide');

	   		};


	   		$scope.get = function(){

	   			$scope.getDispositivo();

	   			$scope.getVehiculos();

	   			$scope.mensaje = '';

	   			$scope.asignaciones = '';

	   			  asignacionServices.get().then(function(resp){ console.log(resp.data);

	   			  					if(resp.data == 'null'){

	   			  						   console.log('Not Found Data');
	   			  						   $scope.mensaje = 'No hay dispositivos asignados';

	   			  					}else{

	   			  						   $scope.asignaciones = resp.data;

	   			  					}

	   			  },function(err){

	   			  		console.log('error' + JSON.stringify(err));

	   			  });

	   		};

	   		$scope.getDispositivo = function(){

	   			  dispositivoServices.get().then(function(resp){

	   			  				     if(resp.data == 'null'){

	   			  				     		console.log('Not Found Data');

	   			  				     }else{

	   			  				     		$scope.tipos = resp.data;
	   			  				     }
	   			  },function(err){

	   			  	    console.log('error' + JSON.stringify(err));

	   			  });

	   		};

	   		$scope.getVehiculos = function(){

	   			  inventoryServices.getVehiculos().then(function(resp){ console.log(resp.data);

	   			  				   if(resp.data == 'null'){

	   			  				   		  console.log('Not Found Data');

	   			  				   }else{

	   			  				   		  $scope.vehiculos = resp.data;

	   			  				   }

	   			  },function(err){

	   			  		console.log('error' + JSON.stringify(err));


	   			  });

	   		};


	   		$scope.add = function(objeto){

	   			  if(objeto.Identificador.length == 0){

	   			  			swal.fire({

	   			  				title 					: 'Oops!',

	   			  				text					: 'Identificador se encuentra vacio, completalo',

	   			  				imageUrl 				: 'assets/img/advertencia.png',

	   			  				showCloseButton			: true,

	   			  				confirmButtonColor 		: '#044BFD'

	   			  			});

	   			  }else if(objeto.Imei.length == 0){

	   			  				 swal.fire({

	   			  				 	 title 						: 'Oops!',

	   			  				 	 text 						: 'Imei se encuentra vacio, completalo',

	   			  				 	 imageUrl 					: 'assets/img/advertencia.png',

	   			  				 	 showCloseButton 			: true,

	   			  				 	 confirmButtonColor 		: '#044BFD'

	   			  				 });

	   			  }else if(objeto.Lada.length == 0){

	   			  				swal.fire({

	   			  					title 						: 'Oops!',

	   			  					text 						: 'Lada se encuentra vacio, completalo',

	   			  					imageUrl 					: 'assets/img/advertencia.png',

	   			  					showCloseButton 			: true,

	   			  					confirmButtonColor 			: '#044BFD'

	   			  				});

	   			  }else if(objeto.Celular.length == 0){

	   			  				 swal.fire({

	   			  				 	 title 						: 'Oops!',

	   			  				 	 text 						: 'Celular se encuentra vacio, completalo',

	   			  				 	 imageUrl 					: 'assets/img/advertencia.png',

	   			  				 	 showCloseButton 			: true,

	   			  				 	 confirmButtonColor 		: '#044BFD'

	   			  				 });

	   			  }else if(objeto.Password.length == 0){

	   			  				 swal.fire({

	   			  				 	 title 						: 'Oops!',

	   			  				 	 text 						: 'Contraseña se encuentra vacio, completalo',

	   			  				 	 imageUrl 					: 'assets/img/advertencia.png',

	   			  				 	 showCloseButton 			: true,

	   			  				 	 confirmButtonColor 		: '#044BFD'

	   			  				 });

	   			  }else if(objeto.Numero_Confianza.length == 0){

	   			  				 swal.fire({

	   			  				 	 title 						: 'Oops!',

	   			  				 	 text	 					: 'Número de confianza se encuentra vacio, completalo',

	   			  				 	 imageUrl 					: 'assets/img/advertencia.png',

	   			  				 	 showCloseButton 			: true,

	   			  				 	 confirmButtonColor         : '#044BFD'

	   			  				 });

	   			  }else{
	   			  			asignacionServices.add(objeto).then(function(resp){

	   			  							   if(resp.data == "INSERTED"){

	   			  							   		  swal.fire({

	   			  							   		  	   title 						: 'Registro exitoso!',

	   			  							   		  	   imageUrl 					: 'assets/img/completo.png',

	   			  							   		  	   showCloseButton 				: true,

	   			  							   		  	   confirmButtonColor 			: '#044BFD'

	   			  							   		  });

	   			  							   		  $scope.get();

	   			  							   		  $scope.clean();

	   			  							   }else{

	   			  							   		swal.fire({

	   			  							   			title 							: 'Oops!',

	   			  							   			text 							: 'No se completo el registro, Inténtalo de nuevo',

	   			  							   			imageUrl 						: 'assets/img/error.png',

	   			  							   			showCloseButton 				: true,

	   			  							   			confirmButtonColor 				: '#044BFD'

	   			  							   		});

	   			  							   }

	   			  			},function(err){

	   			  					console.log('error' + JSON.stringify(err));

	   			  			});

	   			  }

	   		};

	   		$scope.show  = function(objeto){

	   			  $scope.asignacionEdit.nIDDispositivoAuto    = objeto.nIDDispositivoAuto;
	   			  $scope.asignacionEdit.nIDUsuario 			  = objeto.nIDUsuario;
	   			  $scope.asignacionEdit.nIDDispositivo   	  = objeto.nIDDispositivo;
	   			  $scope.asignacionEdit.Identificador         = objeto.Identificador;
	   			  $scope.asignacionEdit.Imei 				  = objeto.Imei;
	   			  $scope.asignacionEdit.Lada 				  = objeto.Lada;
	   			  $scope.asignacionEdit.Celular 			  = objeto.Celular;
	   			  $scope.asignacionEdit.Password 			  = objeto.Password;
	   			  $scope.asignacionEdit.Numero_Confianza      = objeto.Numero_Confianza;

	   		};

	   		$scope.update = function(objeto){

	   			  if(objeto.Identificador.length == 0){

	   			  			swal.fire({

	   			  				title 					: 'Oops!',

	   			  				text					: 'Identificador se encuentra vacio, completalo',

	   			  				imageUrl 				: 'assets/img/advertencia.png',

	   			  				showCloseButton			: true,

	   			  				confirmButtonColor 		: '#044BFD'

	   			  			});

	   			  }else if(objeto.Imei.length == 0){

	   			  				 swal.fire({

	   			  				 	 title 						: 'Oops!',

	   			  				 	 text 						: 'Imei se encuentra vacio, completalo',

	   			  				 	 imageUrl 					: 'assets/img/advertencia.png',

	   			  				 	 showCloseButton 			: true,

	   			  				 	 confirmButtonColor 		: '#044BFD'

	   			  				 });

	   			  }else if(objeto.Lada.length == 0){

	   			  				swal.fire({

	   			  					title 						: 'Oops!',

	   			  					text 						: 'Lada se encuentra vacio, completalo',

	   			  					imageUrl 					: 'assets/img/advertencia.png',

	   			  					showCloseButton 			: true,

	   			  					confirmButtonColor 			: '#044BFD'

	   			  				});

	   			  }else if(objeto.Celular.length == 0){

	   			  				 swal.fire({

	   			  				 	 title 						: 'Oops!',

	   			  				 	 text 						: 'Celular se encuentra vacio, completalo',

	   			  				 	 imageUrl 					: 'assets/img/advertencia.png',

	   			  				 	 showCloseButton 			: true,

	   			  				 	 confirmButtonColor 		: '#044BFD'

	   			  				 });

	   			  }else if(objeto.Password.length == 0){

	   			  				 swal.fire({

	   			  				 	 title 						: 'Oops!',

	   			  				 	 text 						: 'Contraseña se encuentra vacio, completalo',

	   			  				 	 imageUrl 					: 'assets/img/advertencia.png',

	   			  				 	 showCloseButton 			: true,

	   			  				 	 confirmButtonColor 		: '#044BFD'

	   			  				 });

	   			  }else if(objeto.Numero_Confianza.length == 0){

	   			  				 swal.fire({

	   			  				 	 title 						: 'Oops!',

	   			  				 	 text	 					: 'Número de confianza se encuentra vacio, completalo',

	   			  				 	 imageUrl 					: 'assets/img/advertencia.png',

	   			  				 	 showCloseButton 			: true,

	   			  				 	 confirmButtonColor         : '#044BFD'

	   			  				 });

	   			  }else{

	   			  			asignacionServices.update(objeto).then(function(resp){

	   			  							   if(resp.data == "UPDATED"){

	   			  							   		  swal.fire({

	   			  							   		  	   title 						: 'Modificación exitosa!',

	   			  							   		  	   imageUrl 					: 'assets/img/completo.png',

	   			  							   		  	   showCloseButton 				: true,

	   			  							   		  	   confirmButtonColor 			: '#044BFD'

	   			  							   		  });

	   			  							   		  $scope.get();

	   			  							   		  $scope.cleanEdit();

	   			  							   }else{

	   			  							   		swal.fire({

	   			  							   			title 							: 'Oops!',

	   			  							   			text 							: 'No se completo la modificación, Inténtalo de nuevo',

	   			  							   			imageUrl 						: 'assets/img/error.png',

	   			  							   			showCloseButton 				: true,

	   			  							   			confirmButtonColor 				: '#044BFD'

	   			  							   		});

	   			  							   }

	   			  			},function(err){

	   			  					console.log('error' + JSON.stringify(err));

	   			  			});

	   			  }

	   		};

	   		$scope.destroy = function(id){

	   			  swal.fire({

	   			  	  title 	 				: 'ADVERTENCIA',

	   			  	  text						: '¿Estás seguro de eliminar este registro?',

	   			  	  imageUrl 					: 'assets/img/advertencia.png',

	   			  	  showCloseButton 			: true,

	   			  	  showCancelButton			: true,

	   			  	  cancelButtonColor 		: 'rgba(4,75,253,0.5)',

	   			  	  confirmButtonColor 		: '#044BFD',

	   			  	  cancelButtonText 			: 'Cancelar',

	   			  	  confirmButtonText			: 'Si'

	   			  }).then((result)=> {

	   			  		if(result.value){

	   			  				 asignacionServices.destroy(id).then(function(resp){

	   			  				 				   if(resp.data == "DELETED"){

	   			  				 				   		  swal.fire({

	   			  				 				   		  	  title 					: 'Registro eliminado con exito!',

	   			  				 				   		  	  imageUrl 					: 'assets/img/completo.png',

	   			  				 				   		  	  showCloseButton 			: true,

	   			  				 				   		  	  confirmButtonColor 		: '#044BFD'

	   			  				 				   		  });

	   			  				 				   		  $scope.get();

	   			  				 				   }else{

	   			  				 				   		   swal.fire({

	   			  				 				   		   	   title 					: 'Oops!',

	   			  				 				   		   	   text 					: 'No se pudo eliminar el registro, Inténtalo de nuevo',

	   			  				 				   		   	   imageUrl 				: 'assets/img/error.png',

	   			  				 				   		   	   showCloseButton 			: true,

	   			  				 				   		   	   confirmButtonColor 		: '#044BFD'

	   			  				 				   		   });

	   			  				 				   }
	   			  				 
	   			  				 },function(err){

	   			  				 		console.log('error' + JSON.stringify(err));

	   			  				 });
	   			  		
	   			  		}else{

	   			  				swal.fire({

	   			  					 title 					: 'Se cancelo la eliminación!',

	   			  					 imageUrl 				: 'assets/img/error.png',

	   			  					 showCloseButton 		: true,

	   			  					 confirmButtonColor 	: '#044BFD'

	   			  				});

	   			  		}

	   			  });

	   		};





	   	}]);