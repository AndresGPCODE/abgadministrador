<?php

  error_reporting(E_ALL ^ E_NOTICE);  

  ini_set('display_errors', '1');

  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Request-Methods');

  include_once '../Configuraciones/Conexion.php';
  include_once '../Notificaciones/clChat.php';
  include_once '../Utilerias/clHerramientas_v2011.php';

  $method = $_SERVER['REQUEST_METHOD'];

  $UtileriasDatos = new clHerramientasv2011();
  $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
  $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

  $chat = new clChats();

  $chat->conexion($SERVER,$USER,$PASSWORD,$BD);

  switch($method){

        case 'GET' :
          $action = $_GET['accion'];

              if($action == 'contadorTodos'){

                if($chat->ContadorTodos()){

                      echo json_encode($chat->ContadorTodos());

                }

              }else if($action == 'leerTodos'){
                  if($chat->LeerChat('LEIDO','El mensaje a sido leido - ' . $fechaLocal)){

                   echo json_encode($chat->LeerChat('LEIDO','El mensaje a sido leido - ' . $fechaLocal));
                }
              }else if($action == 'chats'){
                  
                 if($chat->TodasLasConversaciones()){

                      echo json_encode($chat->TodasLasConversaciones());

                }else{

                      echo 'null';

                }
                break;
              }
           

        case 'POST' : 

           $action = $_POST['accion'];

           if($action == 'conversaciones'){

                $id = $_POST['usuario'];

                if($chat->buscarConversaciones($id)){

                      echo json_encode($chat->buscarConversaciones($id));

                }else{

                      echo 'null';

                }
           }else if($action == 'contador'){

                $id = $_POST['id'];

                if($chat->Contador($id)){

                      echo json_encode($chat->Contador($id));

                }
           }else if($action == 'leer'){

                $id = $_POST['id'];

                if($chat->actualizarVIP($id,'LEIDO','El mensaje a sido leido - ' . $fechaLocal)){

                   echo json_encode($chat->actualizarVIP($id,'LEIDO','El mensaje a sido leido - ' . $fechaLocal));
                }
           }else if($action == 'mensajes'){

                $id = $_POST['id'];

                if($chat->buscarMensajes($id)){

                   echo json_encode($chat->buscarMensajes($id));
                
                }else{

                  echo 'null';

                }

           }else if($action == 'enviar'){

                $nID = $_POST['idusuario']; 

                $nIDC = $_POST['idcliente'];

                $Texto = $_POST['texto'];

                $text = str_replace('"', '', $Texto);

                $id   = str_replace('"', '', $nID);
                $idc  = str_replace('"', '', $nIDC);

                $chat->setInformacion(
                   '0',
                   $idc,
                   '0',
                   $id,
                   $text,
                   'USUARIO',
                   'NO LEIDO',
                   '',
                   '',
                   'Nuevo mensaje',
                   1,
                   TRUE,
                   FALSE,
                   FALSE
                 
                 );

                if($chat->ejecutar('tbl_chats')){

                    echo 'INSERTED';

                }else{

                    echo 'NOT INSERTED';

                }
           }

  }


?>