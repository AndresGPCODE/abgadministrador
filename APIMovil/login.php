<?php
/*
------ LOGIN DE USUARIO Y CHOFERES
*/
header('Content-Type: application/json');
date_default_timezone_set('America/Mexico_City');
$method = $_SERVER["REQUEST_METHOD"];
include 'conexion.php';
// S1gU34lC0n3J0bL4nC0NEO
switch ($method) {
    case 'POST':
            $request = json_decode(file_get_contents('php://input'), true);
            $request['option'] = isset($request['option']) ? $request['option'] : 0;
            $request['nIDCliente'] = isset($request['nIDCliente']) ? $request['nIDCliente'] : 0;
            $request['email'] = isset($request['email']) ? $request['email'] : "";
            $request['password'] = isset($request['password']) ? $request['password'] : "";
            $request['token'] = isset($request['token']) ? $request['token'] : "";

        if (is_null($request['email']) || is_null($request['password']) || is_null($request['token'])) {
            $request['resp'] = 'Los Datos enviados están Incompletos||' . $request . " || " . file_get_contents('php://input');
            $request['status'] = false;
            $request['nIDCliente'] = 0;
            $request['Nombre'] = "";
            $request['Email'] = "";
            $request['kndUser'] = -1;
            $request['tokenUpdate'] = false;
            echo json_encode($request);
            return;
        }
        switch($request['option']){
            // Login de acceso
            case 1:

                $ret = login($request['email'], $request['password']);
                //echo json_encode($ret);
                if (!$ret['status']) {
                    $retChofer = loginChofer($request['email'], $request['password']);
                    if (!$retChofer['status']) {
                        $request['resp'] = '';
                        $request['status'] = $retChofer['status'];
                        $request['nIDCliente'] = 0;
                        $request['Nombre'] = "";
                        $request['kndUser'] = -1;
                        $request['Email'] = "";
                        $request['tokenUpdate'] = false;
                        echo json_encode($request);
                        return json_encode($request);
                    } else {
                        if ($retChofer['Token'] != $request['token']) {
                            // Actualizar Token
                            $retChofer['tokenUpdate'] = updateTokenChofer($request['token'], $retChofer['nIDCliente'], $retChofer['Observaciones'], $request['Dispositivo']);
                        } else {
                            $retChofer['tokenUpdate'] = false;
                        }
                        $retChofer['nIDCliente'] = (int) $retChofer['nIDCliente'];
                        $retChofer['Token'] = $request['token'];
                        $retChofer['kndUser'] = 1; // 0-cliente, 1-chofer
                        $retChofer['resp'] = "OK";
                        $retChofer['statusCambio'] = false;
                        echo json_encode($retChofer);
                        return json_encode($retChofer);
                    }
                } else {
                    //echo json_encode("entro");
                    if ($ret['Token'] != $request['token']) {
                        // Actualizar Token
                        $ret['tokenUpdate'] = updateToken($request['token'], $ret['nIDCliente'], $ret['Observaciones'], $request['Dispositivo']);
                    } else {
                        $ret['tokenUpdate'] = false;
                    }
                    $ret['nIDCliente'] = (int) $ret['nIDCliente'];
                    $ret['Token'] = $request['token'];
                    $ret['kndUser'] = 0; // 0-cliente, 1-chofer
                    $ret['resp'] = "OK";
                    echo json_encode($ret);
                    return json_encode($ret);
                }
            break;
            // 
            case 2:
                $resp = obtenerChoferEntregaId($request['nIDCliente']); 
                echo json_encode($resp);
                return json_encode($resp);
            
            break;
            default:
                $request['resp'] = 'Sin valores validos';
                echo json_encode($request);
                return;
            break;  
        }
        
        return;
        break;
    default:
        $request['resp'] = 'No se puede procesar la informacion';
        echo json_encode($request);
}
return;

function login($email, $password){
    $pdo = Conexion();
    $select = "SELECT * FROM tbl_clientes WHERE Email = '$email' AND Password = '$password' AND bEstado = 1 Limit 1";
    $ejecutar = $pdo->prepare($select);
    $ejecutar->execute();
    $result = $ejecutar->fetch(PDO::FETCH_ASSOC);
    
    $arr = [];
    if ($result) {
        $arr = $result;
        $resp = obtenerProximoCambio($result['nIDCliente']);
        $arr = $arr + $resp;
        $resp = obtenerContactoEmergencia($result['nIDCliente']);
        $arr = $arr + $resp;
        $arr['status'] = true;
        // json_encode($arr);
        return $arr;
    } else {
        $arr['status'] = false;
        // json_encode($arr);
        return $arr;
    }
}

function loginChofer($email, $password){
    $pdo = Conexion();
    $select = "SELECT * FROM tbl_choferes WHERE Email = '$email' AND Password = '$password' AND bEstado = 1 Limit 1";
    $ejecutar = $pdo->prepare($select);
    $ejecutar->execute();
    $result = $ejecutar->fetch(PDO::FETCH_ASSOC);
    // print_r($result);
    $arr = [];
    // var_dump($result);
    if ($result) {
        $arr = $result;
        $arr['status'] = true;
        $arr['nIDCliente'] = $arr['nIDChofer'];
        // json_encode($arr);
        return $arr;
    } else {
        $arr['status'] = false;
        $arr['nIDCliente'] = -1;
        // json_encode($arr);
        return $arr;
    }
}

function updateToken($tokenFCM, $usuario, $observaciones, $Dispositivo){
    $fechaActual = date('Y-m-d H:i:s');
    $pdo = Conexion();
    $update = "UPDATE tbl_clientes SET Token = '$tokenFCM', FechaModificacion = '$fechaActual', Observaciones = '$observaciones||Token Actualizado - ".$fechaActual."', TipoDispositivo = '$Dispositivo' where nIDCliente = $usuario";
    // echo $update;
    $ejecutar = $pdo->prepare($update);
    $ejecutar->execute();
    $result = $ejecutar->fetch(PDO::FETCH_ASSOC);
    if($result != null){
        return true;
    } else {
        return true;
    }
}

function updateTokenChofer($tokenFCM, $usuario, $observaciones, $Dispositivo) {
    $fechaActual = date('Y-m-d H:i:s');
    $pdo = Conexion();
    $update = "UPDATE tbl_choferes SET Token = '$tokenFCM', FechaModificacion = '$fechaActual', Observaciones = '$observaciones||Token Actualizado - " . $fechaActual . "', TipoDispositivo = '$Dispositivo' where nIDChofer = $usuario";
    $ejecutar = $pdo->prepare($update);
    $ejecutar->execute();
    $result = $ejecutar->fetch(PDO::FETCH_ASSOC);
    if ($result != null) {
        return true;
    } else {
        return true;
    }
}

function generateRandomString($length = 10){
    return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
}

function obtenerProximoCambio($idCliente){

    $pdo = Conexion();
    $select = "SELECT sol.*, aut.Imagen, aut.Placas, aut.Marca, aut.Linea_Submarca, aut.Modelo, aut.Color1, aut.Year, aut.Version FROM tbl_solicitud as sol, tbl_automoviles as aut WHERE nIDCLiente = $idCliente AND sol.nIDAutomovil = aut.nIDAutomovil AND sol.Estatus != 'ENTREGADO' AND sol.Estatus != 'DEVUELTO' AND sol.bEstado = 1 ORDER BY nIDSolicitud DESC LIMIT 1";
    
    $ejecutar = $pdo->prepare($select);
    
    $ejecutar->execute();
    //echo json_encode($ejecutar);
    $result = $ejecutar->fetch(PDO::FETCH_ASSOC);
    $arr = [];
    if ($result) {
        $arr = $result;
        $arr['statusCambio'] = true;
        $arr['fotoAuto'] = $arr['Imagen'];
        $arr['Imagen'] = '';
        // echo json_encode($arr);
        return $arr;
    } else {
        $arr['statusCambio'] = false;
        $arr['fotoAuto'] = "";
        // echo json_encode($arr);
        return $arr;
    }
}

function obtenerContactoEmergencia($idCliente){
    $pdo = Conexion();
    $select = "SELECT cont.nIDContactoEmergencia as contnIDContactoEmergencia, cont.Nombre as contNombre, cont.Telefono as contTelefono, cont.Calle as contCalle, cont.Noexterior as contNoexterior FROM tbl_cat_contacto_emergencia as cont WHERE nIDCLiente = $idCliente LIMIT 1";
    $ejecutar = $pdo->prepare($select);
    $ejecutar->execute();
    $result = $ejecutar->fetch(PDO::FETCH_ASSOC);
    $arr = [];
    if ($result) {
        $arr = $result;
        // echo json_encode($arr);
        return $arr;
    } else {
        $arr['statusContacto'] = false;
        $arr['contnIDContactoEmergencia'] = 0;
        $arr['contNombre'] = "";
        $arr['contTelefono'] = "";
        $arr['contCalle'] = "";
        $arr['contNoexterior'] = "";
        // echo json_encode($arr);
        return $arr;
    }
}
/// obtener imagen del automovil
function imgAuto($idAutomovil){
    $pdo = Conexion();
    $select = "SELECT Imagen FROM tbl_automoviles WHERE nIDAutomovil = $idAutomovil LIMIT 1";
    $ejecutar = $pdo->prepare($select);
    $ejecutar->execute();
    $result = $ejecutar->fetch(PDO::FETCH_ASSOC);
    if ($result) {
        // echo json_encode($arr);
        return $result['Imagen'];
    } else {
        return "";
    }
}


function obtenerChoferEntregaId($idCliente){
    $pdo = Conexion();
    $select = "SELECT cho.nIDChofer, cho.Celular FROM tbl_solicitud as sol, tbl_choferes as cho WHERE sol.nIDCliente = $idCliente AND sol.nIDChofer = cho.nIDChofer AND sol.Estatus = 'EN RUTA' LIMIT 1";
    $ejecutar = $pdo->prepare($select);
    $ejecutar->execute();
    $result = $ejecutar->fetch(PDO::FETCH_ASSOC);
    $arr = [];
    if ($result) {
        $arr['nIDChofer'] = $result['nIDChofer'];
        $arr['Celular'] = $result['Celular'];
        $arr['status'] = true;
        // echo json_encode($arr);
        return $arr;
    } else {
        $arr['status'] = false;
        $arr['nIDChofer'] = 0;
        $arr['Celular'] = 0;
        // echo json_encode($arr);
        return $arr;
    }
}
//obtenerProximoCambio(3);