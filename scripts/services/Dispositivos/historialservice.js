'use strict';

angular.module('abgpassportApp')

	   .service('historialService',['$http','configService',

	   	function($http,configService){

	   		return{

	   			get : function(id){

                           return $http.post(configService.apiUrl + 'Historial.php','accion=buscar&id=' + JSON.stringify(id),

                            { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                          );

                },

                getTodos : function(){

                		return $http.get(configService.apiUrl + 'Historial.php?accion=buscarTodos',

                			{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                		);
                		
                },

                getFechas : function(placas){

                        return $http.post(configService.apiUrl + 'Historial.php','accion=buscarRutas&placas=' + JSON.stringify(placas),

                            { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                        );

                },

                buscarRuta : function(id,fecha){console.log(id,fecha);

                        return $http.post(configService.apiUrl + 'Historial.php','accion=puntos&id=' + JSON.stringify(id) + '&fecha=' + JSON.stringify(fecha),

                            { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                        );

                }

                /*HistorialFechas : function(placas){console.log('hola');

                        return $http.post(configService.apiUrl + 'Historial.php','accion=buscarFechas&placas=' + JSON.stringify(placas),

                            { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                          );

                }*/

	   		};

	   	}]);