'use strict';

angular.module('abgpassportApp')

	   .controller('GraficaCtrl',['$scope','inventoryServices','solicitudesServices','reportServices',

	   	function($scope,inventoryServices,solicitudesServices,reportServices){

        var disponibles   = 0;

        var mantenimiento = 0;

        var Ocupado       = 0;

        var arreglo       = [];

        var arregloPlacas = [];

        var arregloResta  = [];

        var arregloColores = [];

             $scope.get = function(){

                $scope.getDisponibles();

                $scope.getRestantes();     

                $scope.getReportes();  

                $scope.getCalendario();                

              };


              $scope.getCalendario = function(){

       
                    solicitudesServices.getCalendario().then(function(resp){ 

                        event_data = {

                                  "events": [

                                  ]

                            };

                           for(var i = 0; i < resp.data.length; i++){

                                    var evento_local = {

                                          "occasion" : " Entrega de vehiculo "+ resp.data[i]["Marca"]+" " + resp.data[i]["Modelo"] + " con número de placa: " + resp.data[i]["Placas"] + 
                                          " al cliente " + resp.data[i]["Nombre"] + " en el domicilio " + resp.data[i]["Calle"] + " " + resp.data[i]["NoExterior"],

                                          "invited_count"    : resp.data[i]["FechaHora"],

                                          "year"     : resp.data[i]["año"],

                                          "month"     : resp.data[i]["mes"],

                                          "day" : resp.data[i]["dia"],

                                          "cancelled" :  false
                                    
                                    }

                                    event_data["events"].push(evento_local); 

                              }                             

                                //mostrar();

                                $scope.getRecepcion();

                   },function(err){

                          console.log('error' + JSON.stringify(err));

                    })



              };

              $scope.getRecepcion = function(){


                    solicitudesServices.getRecepcion().then(function(resp){ console.log(event_data.data);

                              for(var i = 0; i < resp.data.length; i++){

                                      var datos_locales = {

                                          "occasion" : "Recepción del vehiculo " + resp.data[i]["Marca"] + " " + resp.data[i]["Modelo"] + " con número de placa: " + resp.data[i]["Placas"]

                                          + " al cliente " + resp.data[i]["Nombre"],

                                          "invited_count" : resp.data[i]["Fechas_Fin"],

                                          "year" : resp.data[i]["año"],

                                          "month" :  resp.data[i]["mes"],

                                          "day" : resp.data[i]["dia"],

                                          "cancelled" : true

                                      }

                                      event_data["events"].push(datos_locales); console.log(event_data.data);

                              }

                              mostrar();


                    },function(err){

                            console.log('error' + JSON.stringify(err));

                    });

              };
              
              function mostrar(){
                    
                    var date = new Date();
                    
                    var today = date.getDate();
    
                    $(".right-button").click({date: date}, next_year);
                    
                    $(".left-button").click({date: date}, prev_year);
                    
                    $(".month").click({date: date}, month_click);
                    
                    $("#add-button").click({date: date}, new_event);
    
                    $(".months-row").children().eq(date.getMonth()).addClass("active-month");
                    
                    init_calendar(date);
                    
                    var events = check_events(today, date.getMonth()+1, date.getFullYear());
                
                    show_events(events, months[date.getMonth()], today);

              };

              function init_calendar(date) {
                
                    $(".tbody").empty();
                    
                    $(".events-container").empty();
                    
                    var calendar_days = $(".tbody");
                    
                    var month = date.getMonth();
                    
                    var year = date.getFullYear();
                    
                    var day_count = days_in_month(month, year);
                    
                    var row = $("<tr class='table-row'></tr>");
                    
                    var today = date.getDate();
    
                    date.setDate(1);
                    
                    var first_day = date.getDay();
    
                    for(var i=0; i<35+first_day; i++) {
        
                        var day = i-first_day+1;
        
                        if(i%7===0) {
                            
                            calendar_days.append(row);
                            
                            row = $("<tr class='table-row'></tr>");
                        
                        }
        
                        if(i < first_day || day > day_count) {
                            
                            var curr_date = $("<td class='table-date nil'>"+"</td>");
                            
                            row.append(curr_date);
                        
                        }else {
                            
                            var curr_date = $("<td class='table-date'>"+day+"</td>");

                            var events = check_events(day, month+1, year); 

                            if(today===day && $(".active-date").length===0) {
                                
                                curr_date.addClass("active-date");
                                
                                show_events(events, months[month], day);
                            
                            }
            
                            if(events.length!==0) {
                                
                                curr_date.addClass("event-date"); 
                            
                            }
            
                                curr_date.click({events: events, month: months[month], day:day}, date_click); 
                                
                                row.append(curr_date);
                        
                        }
                    
                    }
    
                      calendar_days.append(row);
                      
                      $(".year").text(year);
              
              }

              function days_in_month(month, year) {
                      
                      var monthStart = new Date(year, month, 1);
                      
                      var monthEnd = new Date(year, month + 1, 1);
                      
                      var dias = (monthEnd - monthStart) / (1000 * 60 * 60 * 24);    

                      return Math.round(dias);
              
              }

              function date_click(event) { console.log(event);
                      
                      $(".events-container").show(250);
                      
                      $("#dialog").hide(250);
                      
                      $(".active-date").removeClass("active-date");
                      
                      $(this).addClass("active-date");
                      
                      show_events(event.data.events, event.data.month, event.data.day);
              
              };

              function month_click(event) {
                      
                      $(".events-container").show(250);
                      
                      $("#dialog").hide(250);
                      
                      var date = event.data.date;
                      
                      $(".active-month").removeClass("active-month");
                      
                      $(this).addClass("active-month");
                      
                      var new_month = $(".month").index(this);
                      
                      date.setMonth(new_month);
                      
                      init_calendar(date);
              
              }

              function next_year(event) {
                      
                      $("#dialog").hide(250);
                      
                      var date = event.data.date;
                      
                      var new_year = date.getFullYear()+1;
                      
                      $("year").html(new_year);
                      
                      date.setFullYear(new_year);
                      
                      init_calendar(date);
              
              }

              function prev_year(event) {
                      
                      $("#dialog").hide(250);
                      
                      var date = event.data.date;
                      
                      var new_year = date.getFullYear()-1;
                      
                      $("year").html(new_year);
                      
                      date.setFullYear(new_year);
                      
                      init_calendar(date);
              
              }

              function new_event(event) {
    
                      if($(".active-date").length===0)
                          
                          return;
    
                          $("input").click(function(){
                              
                              $(this).removeClass("error-input");
                          
                          })
    
                          $("#dialog input[type=text]").val('');
                          
                          $("#dialog input[type=number]").val('');
                          
                          $(".events-container").hide(250);
                          
                          $("#dialog").show(250);
    
                          $("#cancel-button").click(function() {
                          
                              $("#name").removeClass("error-input");
                              
                              $("#count").removeClass("error-input");
                              
                              $("#dialog").hide(250);
                              
                              $(".events-container").show(250);
                          
                          });
    
                          $("#ok-button").unbind().click({date: event.data.date}, function() {
                                
                                var date = event.data.date;
                                
                                var name = $("#name").val().trim();
                                
                                var count = parseInt($("#count").val().trim());
                                
                                var day = parseInt($(".active-date").html());
        
                                if(name.length === 0) {
                                    
                                    $("#name").addClass("error-input");
                                
                                }else if(isNaN(count)) {
                                    
                                    $("#count").addClass("error-input");
                                
                                }else {
                                    
                                    $("#dialog").hide(250);

                                    new_event_json(name, count, date, day);
                                    
                                    date.setDate(day);
                                    
                                    init_calendar(date);
                                
                                }
                          
                          });
              
              }

              function new_event_json(name, count, date, day) {
                    
                    var event = {
                        
                        "occasion": name,
                        
                        "invited_count": count,
                        
                        "year": date.getFullYear(),
                        
                        "month": date.getMonth()+1,
                        
                        "day": day
                    
                    };
                    
                    event_data["events"].push(event);
              
              }

              function show_events(events, month, day) {
    
                    $(".events-container").empty();
                    
                    $(".events-container").show(250);
    
                    if(events.length===0) {
                        
                        var event_card = $("<div class='event-card'></div>");
                        
                        var event_name = $("<div class='event-name'>No tienes entregas ni recepciones en "+month+" "+day+".</div>");
                        
                        $(event_card).css({ "border-left": "10px solid #FF1744" });
                        
                        $(event_card).append(event_name);
                        
                        $(".events-container").append(event_card);
                    }else {
        
                        for(var i=0; i<events.length; i++) {
                            
                            var event_card = $("<div class='event-card'></div>");
                            
                            var event_name = $("<div class='event-name'>"+events[i]["occasion"]+":</div>");
                            
                            var event_count = $("<div class='event-count'>"+events[i]["invited_count"]+" </div>");
                            
                            if(events[i]["cancelled"]==true) {
                                
                                $(event_card).css({
                                      
                                      "border-left": "10px solid #FF1744"
                                
                                });
                                  
                                  event_count = $("<div class='event-cancelled'>"+ events[i]["invited_count"]+"</div>");
                            
                            }
                                
                                $(event_card).append(event_name).append(event_count);
                                
                                $(".events-container").append(event_card);
                        
                        }
                    
                    }
              
              }

              function check_events(day, month, year) {
                    
                    var events = [];
                    
                    for(var i=0; i<event_data["events"].length; i++) {
                            
                            var event = event_data["events"][i];
                            
                            if(event["day"]==day && event["month"]==month && event["year"]==year) { 
                                
                                events.push(event);
                            }
                    
                    }
    
                    return events;
              
              }

              var event_data = {
                  
                  "events": [
   
                  ]
              
              };

              const months = [ 
                  
                  "Enero", 
                  "Febrero", 
                  "Marzo", 
                  "Abril", 
                  "Mayo",  
                  "Junio", 
                  "Julio", 
                  "Agosto", 
                  "Septiembre", 
                  "Octubre", 
                  "Noviembre", 
                  "Diciembre" 
              
              ];



            $scope.getDisponibles = function(){

                  

                    inventoryServices.getDisponibles().then(function(resp){

                                     disponibles = resp.data[0]['Contador']; //console.log(disponibles);

                                     inventoryServices.getMantenimiento().then(function(resp){

                                            mantenimiento = resp.data[0]['Contador']; //console.log(mantenimiento);

                                            inventoryServices.getOcupado().then(function(resp){ 

                                                      Ocupado = resp.data[0]['Contador'];//console.log(Ocupado);

                                                      $scope.grafica();

                                            },function(err){

                                                      console.log('error' + JSON.stringify(err));

                                            });

                                     },function(err){

                                            console.log('error' + JSON.stringify(err));

                                     });

                    },function(err){

                          console.log('error' + JSON.stringify(err));

                    });

                  
            };


            $scope.getRestantes = function(){

                  solicitudesServices.getRestantes().then(function(resp){

                                      var array = JSON.parse(JSON.stringify(resp.data));                                       

                                      for(var i = 0; i < array.length; i++){

                                            arregloPlacas[i] = array[i]["Placas"];
                                            arregloResta[i] = array[i]["DIAS"];

                                            if(array[i]["DIAS"] <= 5){

                                                arregloColores[i] = 'rgb(202,5,17)';

                                            }else if(array[i]["DIAS"] > 5 && array[i]["DIAS"] <= 10){

                                                arregloColores[i] = 'rgb(236,206,13)';

                                            }else if(array[i]["DIAS"] > 10){

                                                arregloColores[i] = 'rgb(2,138,43)';
                                            }
                                              

                                      } 


                                            var popCanvas = document.getElementById("popChart");
                                            var barChart = new Chart(popCanvas, {
                                                
                                                type: 'horizontalBar',
                                                data: {
                                                    
                                                    labels: arregloPlacas,
                                                    datasets: [{
                                                            
                                                            label: 'Restantes',
                                                            data: arregloResta,
                                                            backgroundColor: arregloColores 
      
                                                    }]
                                                
                                                }
                                            
                                            });

                  },function(err){

                            console.log('error' + JSON.stringify(err));

                  });

            };


            $scope.getReportes = function(){
              
                  var Etiquetas = [];

                  var Valores = [];

                  var mes;

                  reportServices.get().then(function(resp){ console.log(resp.data); 

                      for(var i = 0; i < resp.data.length; i++){

                                Etiquetas[i] = resp.data[i]["tipoReporte"];
                                Valores[i]   = resp.data[i]["nIDReporte"];
                                mes          = resp.data[i]["Mes"];
                      }
                        // Bar chart
                    new Chart(document.getElementById("bar-chart"), {
            type: 'bar',
              data: {
                                                    
                            labels: Etiquetas,
                            datasets: [{
                                                            
                                    label: 'Reportes',
                                    data:Valores,
                                    backgroundColor: ["#3e95cd","#777978","#02723A"]
      
                            }]
                                                
                        }
          
          });


                  },function(err){

                        console.log('error' + JSON.stringify(err));

                  });

                    

            };



              $scope.grafica = function(){   

                   Morris.Donut({
                    element: 'donut',
                    data: [
                    {label: "DISPONIBLES", value: disponibles},
                    {label: "MANTENIMIENTO", value: mantenimiento},
                    {label: "OCUPADO", value:Ocupado}

                    ]
              });



              };




              


	   	}]);

