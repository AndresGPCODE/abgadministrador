<?php
/*
------ Registro del historial de kilometraje recorrido
*/
header('Content-Type: application/json');
date_default_timezone_set('America/Mexico_City');
error_reporting(E_ALL);
$method = $_SERVER["REQUEST_METHOD"];
$fechaDia = date('Y-m-d');
$fechaHora = date('H:i:s');

//$method = 'POST';
include 'conexion.php';

switch ($method) {
    case 'POST':
        $request = json_decode(file_get_contents('php://input'), true);
        if (is_null($request['nIDCliente'])) {
            $request['resp'] = 'Los Datos enviados están Incompletos ||' . $request['nIDCliente'] . " || " . file_get_contents('php://input');
            $request['status'] = false;
            echo json_encode($request);
            return json_encode($request);
        } else {
            $resp = obtenerHistorial($request['nIDCliente']);
            if ($resp[0]['status']) {
                echo json_encode($resp);
                return json_encode($resp);
            } else {
                $resp[0]['resp'] = 'No se pudo realizar el proceso';
                $resp[0]['status'] = false;
                echo json_encode($resp);
                return json_encode($resp);
            }
        }
        break;
    default:
        $resp[0]['resp'] = 'No se puede procesar la informacion';
        $resp[0]['status'] = false;
        echo json_encode($resp);
        return json_encode($resp);
}

/** ULTIMO DIA DEL MES ACTUAL **/
function _ultimo_dia_mes() { 
    $month = date('m');
    $year = date('Y');
    $day = date("d", mktime(0,0,0, $month+1, 0, $year));

    return date('Y-m-d', mktime(0,0,0, $month, $day, $year));
}

/** PRIMER DIA DEL MES ACTUAL **/
function _primer_dia_mes() {
    $month = date('m');
    $year = date('Y');
    return date('Y-m-d', mktime(0,0,0, $month, 1, $year));
}

function obtenerHistorial($id){
    $pdo = Conexion();
    $primerDia = _primer_dia_mes();
    $ultimoDia = _ultimo_dia_mes();

    $select  = "SELECT * FROM tbl_solicitud 
                inner join tbl_clientes on tbl_solicitud.nIDCliente = tbl_clientes.nIDCliente 
                inner join tbl_historial on tbl_historial.nIDSolicitud = tbl_solicitud.nIDSolicitud 
                where tbl_clientes.nIDCliente = $id 
                AND tbl_historial.FechaCreacion between '2020-08-01' AND '2020-08-31';";
                // AND tbl_historial.FechaCreacion between $primerDia AND $ultimoDia ;";

    $ejecutar = $pdo->prepare($select);
    $ejecutar->execute();
    $result = $ejecutar->fetchAll(PDO::FETCH_ASSOC);
    $arr = [];

    // Select conteo de cambios
        $Select2  = "SELECT count(nIDSolicitud) as Total FROM tbl_solicitud  
        where tbl_solicitud.nIDCliente = $id 
        AND tbl_solicitud.bEstado = 1 
        AND (tbl_solicitud.Estatus = 'ACEPTADO' OR tbl_solicitud.Estatus = 'DEVUELTO' ); ";
                // AND tbl_historial.FechaCreacion between $primerDia AND $ultimoDia ;";

        $ejecutar2 = $pdo->prepare($Select2);
        $ejecutar2->execute();
        $result2 = $ejecutar2->fetchAll(PDO::FETCH_ASSOC);

    if (count($result) > 0) {
        $anterior = [];
        $kilometros = 0;
        foreach ($result as $row => $index) {
            if($index == 0){
                $anterior[0] = $row['Latitud'];
                $anterior[1] = $row['Longitud'];
            } else {
                $kilometros = $kilometros + distance($anterior[0], $anterior[1], $row['Latitud'],$row['Longitud'], 'K');
                $anterior[0] = $row['Latitud'];
                $anterior[1] = $row['Longitud'];
            }
        }

        $resp['Kilometraje'] = $kilometros;
        $resp['Total'] = ($result2['Total'] !== null)? $result2['Total'] : '0';
        $resp['DiasTranscurridos'] = date('d');
        $resp['status'] = true;
        $resp['resp'] = "Registro de ubicaciones";
        array_push($arr, $resp);
        return $arr;
    } else {
        $resp['Kilometraje'] = '0';
        $resp['DiasTranscurridos'] = date('d');
        $resp['status'] = true;
        $resp['resp'] = "No hay ubicaciones regitradas";
        $resp['Total'] = ($result2['Total'] !== null)? $result2['Total'] : '0';
        array_push($arr, $resp);
        return $arr;
    }

}


function distance($lat1, $lon1, $lat2, $lon2, $unit = 'N') {
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
        return ($miles * 1.609344);
    } else if ($unit == "N") {
        return ($miles * 0.8684);
    } else {
        return $miles;
    }
}




