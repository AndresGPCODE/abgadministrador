<?php
/*
------PETICIONS REALIZADAS POR EL CHOFER
*/
header('Content-Type: application/json');
date_default_timezone_set('America/Mexico_City');
$method = $_SERVER["REQUEST_METHOD"];
// $method = 'POST';
include 'conexion.php';
// base64ToImage($image, "pruebas.jpeg");
switch ($method) {
    case 'POST':
        $request = json_decode(file_get_contents('php://input'), true);
        if (is_null($request['option']) || is_null($request['idCliente'])) {
            $request['resp'] = 'Los Datos enviados están Incompletos||' . $request . " || " . file_get_contents('php://input');
            $request['status'] = false;
            return json_encode($request);
        } else {
            switch ($request['option']){
                case 1:
                    $resp1 = obtenerAutoCliente($request['idCliente']);
                    $resp = obtenerDispositivoAuto($resp1['nIDAutomovil']);
                    //var_dump($resp1);
                    if ($resp['status']) {
                        echo json_encode($resp);
                        // base64ToImage($resp['Foto'], "foto.png");
                        return json_encode($resp);
                    } else {
                        $resp['resp'] = 'No hay dispositivo';
                        $resp['status'] = false;
                        echo json_encode($resp);
                        return json_encode($resp);
                    }
                break;
                default:
            break;
        }
    }
}

return;


function obtenerDispositivoAuto($id){
    $pdo = Conexion();
    $select = "SELECT * FROM tbl_automoviles as aut, tbl_autos_dispositivos as dis WHERE aut.placas = dis.Identificador AND aut.nIDAutomovil = $id LIMIT 1";
    $ejecutar = $pdo->prepare($select);
    $ejecutar->execute();
    $result = $ejecutar->fetchAll(PDO::FETCH_ASSOC);
    $arr = [];
    if (count($result) > 0) {
        foreach ($result as $row) {
           $row['status'] = true;
        }
        $arr = $row;
        return $arr;
    } else {
        $arr['status'] = false;
        $arr['nIDDispositivoAuto'] = 0;
        return $arr;
    }
}

function obtenerAutoCliente($id){
    //nIDCliente
    $pdo = Conexion();
    $select = "SELECT nIDAutomovil FROM tbl_solicitud where Estatus = 'ENTREGADO' AND nIDCliente = $id LIMIT 1";
    $ejecutar = $pdo->prepare($select);
    $ejecutar->execute();
    $result = $ejecutar->fetchAll(PDO::FETCH_ASSOC);
    $arr = [];
    if (count($result) > 0) {
        foreach ($result as $row) {
            $row['status'] = true;
        }
        $arr = $row;
        return $arr;
    } else {
        $arr['nIDAutomovil'] = 0;
        return $arr;
    }
}