'use strict';

angular.module('abgpassportApp')
  .service('documentServices', ['$http','configService',function($http,configService){
    return {

      get: function(){

          return $http.get(configService.apiUrl + 'Documentos.php?accion=consulta',

            { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }

        );

      },

      add : function(documento){

          return $http.post(configService.apiUrl + 'Documentos.php','accion=agregar&documento=' + JSON.stringify(documento),

            { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

          );

      },

      update : function(documento){

             return $http.post(configService.apiUrl + 'Documentos.php','accion=modificar&documento=' + JSON.stringify(documento),

              { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

            );

      },

      destroy : function(id){

              return $http.post(configService.apiUrl + 'Documentos.php','accion=eliminar&id=' + JSON.stringify(id),

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

              )

      }

      
    };

  }]);