'use strict';

angular.module('abgpassportApp')

       .service('roleServices',['$http','configService',function($http,configService){

                return{

                      get: function(){

                          return $http.get(configService.apiUrl + 'Rol.php?accion=consulta',

                            { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }

                          );

                      },


                      add : function(rol){

                          return $http.post(configService.apiUrl + 'Rol.php','accion=agregar&rol=' + JSON.stringify(rol),

                            { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                          );

                      },

                      update : function(rol){

                          return $http.post(configService.apiUrl + 'Rol.php','accion=modificar&rol=' + JSON.stringify(rol),

                            { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                          );

                      },

                      destroy : function(id){

                          return $http.post(configService.apiUrl + 'Rol.php','accion=eliminar&id=' + JSON.stringify(id),

                            { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                          );
                      }

                };
       }]);