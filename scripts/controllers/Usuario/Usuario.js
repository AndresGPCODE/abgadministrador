'use strict';

angular.module('abgpassportApp')

       .controller('UserCtrl',['$scope','userServices','agencyServices','roleServices','marketstallServices','departamentServices', 

        function($scope,userServices,agencyServices,roleServices,marketstallServices,departamentServices){

        var expRFC      = /^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\d]{3})?$/;

        var expEmail    = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;

        var expTelefono = /^[0-9]{10}$/;
        

        $scope.usuario = {

              nIDUsuario      : '0',
              Nombre          : '',
              RFC             : '',
              Usuario         : '',
              Password        : '',
              Telefono        : '',
              Celular         : '',
              Email           : '',
              Foto            : '',
              Direccion       : '',
              nIDEmpresa      : '',
              nIDRol          : ''

        };

        $scope.password = {
           Password : "",
           PasswordC : "",
           nIDUsuario : ""
        };

        $scope.usuarioEdit = {

              nIDUsuario      : '0',
              Nombre          : '',
              RFC             : '',
              Usuario         : '',
              Telefono        : '',
              Celular         : '',
              Email           : '',
              Foto            : '',
              Direccion       : '',
              nIDEmpresa      : '',
              nIDRol          : ''

        };


        $scope.clean = function(){

              $scope.usuario = {

                    nIDUsuario      : '0',
                    Nombre          : '',
                    RFC             : '',
                    Usuario         : '',
                    Password        : '',
                    Telefono        : '',
                    Celular         : '',
                    Email           : '',
                    Foto            : '',
                    Direccion       : '',
                    nIDEmpresa      : '',
                    nIDRol          : ''

              };

              $('#add_modal').modal('hide');
        
        };


        $scope.cleanEdit = function(){

              $scope.usuarioEdit = {

                    nIDUsuario      : '0',
                    Nombre          : '',
                    RFC             : '',
                    Usuario         : '',
                    Telefono        : '',
                    Celular         : '',
                    Email           : '',
                    Foto            : '',
                    Direccion       : '',
                    nIDEmpresa      : '',
                    nIDRol          : ''

              };

              $('#update_modal').modal('hide');
        };


        $scope.cleanPwd = function(){
              $scope.password = {
                    Password : "",
                    PasswordC : "",
                    nIDUsuario : ""
              };
              $("#change_Password").modal('hide');
        };

        $scope.get = function(){

          $scope.icono = '';
          $scope.mensaje = '';

          $scope.getEmpresa();
          //$scope.getPuestos();
          $scope.getRoles();
          //$scope.getDepartamento(); 
          console.log('Estoy entrando');

              userServices.get().then(function(resp){
                
                          if(resp.data == 'null'){

                              $scope.icono = 'assets/img/hecho.png';
                              $scope.mensaje = 'No tienes usuarios registrados';

                          }else{

                              $scope.usuarios = resp.data;

                          }

              },function(err){

                  console.log('error' + JSON.stringify(err));

              });

        };


        $scope.getEmpresa = function(){

              agencyServices.get().then(function(resp){

                            if(resp.data == 'null'){


                            }else{

                                  $scope.empresas = resp.data;

                            }
              
              },function(err){

                    console.log('error' + JSON.stringify(err));

              });

        };

        /*$scope.getDepartamento = function(){

              departamentServices.get().then(function(resp){

                            if(resp.data == 'null'){


                            }else{

                                  $scope.departamentos = resp.data;

                            }
              
              },function(err){

                    console.log('error' + JSON.stringify(err));

              });

        };*/

        /*$scope.getPuestos = function(){

              marketstallServices.get().then(function(resp){

                            if(resp.data == 'null'){


                            }else{

                                  $scope.puestos = resp.data;

                            }
              
              },function(err){

                    console.log('error' + JSON.stringify(err));

              });

        };*/

        $scope.getRoles = function(){

              roleServices.get().then(function(resp){

                            if(resp.data == 'null'){


                            }else{

                                  $scope.roles = resp.data;

                            }
              
              },function(err){

                    console.log('error' + JSON.stringify(err));

              });

        };

        function ValidarExt() {

              var archivoInput = document.getElementById('Foto');

              var archivoRuta  = archivoInput.value;

              var expExtensiones = /(.PNG)|(.JPG)|(.JPEG)|(.png)$/i;
               

              if(!expExtensiones.test(archivoRuta)){

                    swal.fire({

                      title           : 'Oops!',

                      text            : 'Solo puedes subir imagenes con extension .png, .jpg, .jpeg',

                      imageUrl        : 'assets/img/advertencia.png',

                      showCloseButton : true,

                      confirmButtonColor: '#044BFD'

                    });

                    archivoRuta.value = '';

                    return false;

              }else{ 

                    if(archivoInput.files && archivoInput.files[0]){

                        var visor = new FileReader();

                        visor.addEventListener("load" , function(e){ 

                            document.getElementById("doc").src = e.target.result;

                            $scope.usuario.Foto = e.target.result;

                        });

                        visor.readAsDataURL(archivoInput.files[0]);
                    }

              }

        }(function(){

              document.getElementById('Foto').addEventListener("change", ValidarExt);
        })();

        function ValidarExtEdit() {

              var archivoInput = document.getElementById('FotoEdit');

              var archivoRuta  = archivoInput.value;

              var expExtensiones = /(.PNG)|(.JPG)|(.JPEG)$/i;

              if(!expExtensiones.test(archivoRuta)){

                    swal.fire({

                      title           : 'Oops!',

                      text            : 'Solo puedes subir imagenes con extension .png, .jpg, .jpeg',

                      imageUrl        : 'assets/img/advertencia.png',

                      showCloseButton : true,

                      confirmButtonColor: '#044BFD'

                    });

                    archivoRuta.value = '';

                    return false;

              }else{ 

                    if(archivoInput.files && archivoInput.files[0]){

                        var visor = new FileReader();

                        visor.addEventListener("load" , function(e){ console.log(e.target.result);

                            document.getElementById("docEdit").src = e.target.result;

                            $scope.usuarioEdit.Foto = e.target.result;

                        });

                        visor.readAsDataURL(archivoInput.files[0]);
                    }

              }

        }(function(){

              document.getElementById('FotoEdit').addEventListener("change", ValidarExtEdit);
        })();


        $scope.add = function(usuario){ console.log('hola');

              if(usuario.Nombre.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Nombre vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });
                    

              }else if(!expRFC.test(usuario.RFC)){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'RFC no cumple con las especificaciones, inténtalo de nuevo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(usuario.Usuario.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Nombre de usuario vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(usuario.Password.length == 0){

                              swal.fire({

                                  title             : 'Oops!',

                                  text              : 'Password vacio completalo',

                                  imageUrl          : 'assets/img/advertencia.png',

                                  showCloseButton   : true,

                                  confirmButtonColor: '#044BFD'

                              });

              }else if(!expTelefono.test(usuario.Celular)){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Número de celular no cumple con las especificaciones, inténtalo de nuevo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(!expEmail.test(usuario.Email)){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Email no cumple con las especificaciones, inténtalo de nuevo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else{

                    userServices.add(usuario).then(function(resp){

                              

                                  if(resp.data == 'INSERTED'){

                                         swal.fire({

                                              title               : 'Registro exitoso!',

                                              imageUrl            : 'assets/img/completo.png',

                                              showCloseButton     : true,

                                              confirmButtonColor  : '#044BFD'

                                         });

                                         $scope.clean();

                                         $scope.get();

                                  }else if(resp.data == 'EXISTE'){

                                               swal.fire({

                                                    title                 : 'Correo electrónico existente!',

                                                    text                  : 'Ingresa un correo electrónico diferente',

                                                    imageUrl              : 'assets/img/advertencia.png',

                                                    showCloseButton       : true,

                                                    confirmButtonColor    : '#044BFD'

                                               });

                                  }else{

                                         swal.fire({

                                             title                : 'Oops!',

                                             text                 : 'No se logro el registro, inténtalo de nuevo',

                                             imageUrl             : 'assets/img/error.png',

                                             confirmButtonColor   : '#044BFD'

                                         });

                                  }

                    },function(err){

                          console.log('error' + JSON.stringify(err));

                    });

              }

        };


        $scope.show = function(usuario){ console.log('hola aqui sigo');

              $scope.usuarioEdit.nIDUsuario            = usuario.nIDUsuario;
              $scope.usuarioEdit.Nombre                = usuario.Nombre;
              $scope.usuarioEdit.RFC                   = usuario.RFC;
              $scope.usuarioEdit.Usuario               = usuario.Usuario;
              $scope.usuarioEdit.Telefono              = usuario.Telefono;
              $scope.usuarioEdit.Celular               = usuario.Celular;
              $scope.usuarioEdit.Email                 = usuario.Email;
              $scope.usuarioEdit.Foto                  = usuario.Imagen;
              document.getElementById("docEdit").src = usuario.Imagen;
              $scope.usuarioEdit.Direccion             = usuario.Direccion;
              $scope.usuarioEdit.nIDEmpresa            = usuario.nIDEmpresa;
              $scope.usuarioEdit.nIDRol                = usuario.nIDRol;
        
        };


        $scope.update = function(usuario){ console.log(usuario); 

              if(usuario.Nombre.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Nombre vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });
                    

              }else if(!expRFC.test(usuario.RFC)){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'RFC no cumple con las especificaciones, inténtalo de nuevo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         }); 

              }else if(usuario.Usuario.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Nombre de usuario vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(!expTelefono.test(usuario.Celular)){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Número de celular no cumple con las especificaciones, inténtalo de nuevo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(!expEmail.test(usuario.Email)){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Email no cumple con las especificaciones, inténtalo de nuevo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else{

                    userServices.update(usuario).then(function(resp){                              

                                  if(resp.data == 'UPDATED'){

                                         swal.fire({

                                              title               : 'Registro exitoso!',

                                              imageUrl            : 'assets/img/completo.png',

                                              showCloseButton     : true,

                                              confirmButtonColor  : '#044BFD'

                                         });

                                         $scope.cleanEdit();

                                         $scope.get();

                                  }else{

                                         swal.fire({

                                             title                : 'Oops!',

                                             text                 : 'No se logro el registro, inténtalo de nuevo',

                                             imageUrl             : 'assets/img/error.png',

                                             confirmButtonColor   : '#044BFD'

                                         });

                                  }

                    },function(err){

                          console.log('error' + JSO.stringify(err));

                    });

              }

        };

        $scope.contenedor = function(id){
            console.log(id);
            $scope.password.nIDUsuario = id;
           $("#update_modal").modal('hide');


        };

        $scope.change = function(cambio){
          if(cambio.Password == cambio.PasswordC){

            userServices.changeP(cambio).then(function(resp){

                if(resp.data === "true"){
                  swal.fire({
                    title      : 'Modificación exitosa!',
                    imageUrl            : 'assets/img/completo.png',
                    showCloseButton     : true,
                    confirmButtonColor  : '#044BFD'
                  });

                  $scope.cleanPwd(); 
                }else{
                  swal.fire({
                    title      : 'Inténtalo de nuevo!',
                    text       : 'No se completo el registro.',
                    imageUrl            : 'assets/img/error.png',
                    showCloseButton     : true,
                    confirmButtonColor  : '#044BFD'
                  });
                }
            });
          
          }else{
            swal.fire({
              title: 'Oops!',
              text : 'La confirmación de la contraseña no coincide intentalo de nuevo',
              imageUrl: 'assets/img/error.png',
              showCloseButton: true,
              confirmButtonColor: '#044BFD'
            });
          }

        };


        $scope.destroy = function(id){

              swal.fire({

                  title               : 'ADVERTENCIA',

                  text                : '¿Estás seguro de eliminar este registro?',

                  imageUrl            : 'assets/img/advertencia.png',

                  showCancelButton    : true,

                  showCloseButton     : true,                  

                  cancelButtonColor   : 'rgba(4,75,253,0.5)',                  

                  confirmButtonColor  : '#044BFD',

                  cancelButtonText    : 'Cancelar',

                  confirmButtonText   : 'Si',

              }).then((result) => {

                       if(result.value){

                                userServices.destroy(id).then(function(resp){

                                              if(resp.data == "DELETED"){

                                                      swal.fire({

                                                          title              : 'Registro eliminado con exito!',

                                                          imageUrl           : 'assets/img/completo.png',

                                                          showCloseButton    : true,

                                                          confirmButtonColor : '#044BFD'

                                                      });

                                                      $scope.get();

                                              }else{

                                                      swal.fire({

                                                          title             : 'Oops!',

                                                          text              : 'No se pudo eliminar el registro inténtalo de nuevo',

                                                          imageUrl          : 'assets/img/error.png',

                                                          showCloseButton   : true,

                                                          confirmButtonColor: '#044BFD'

                                                      });

                                              }

                                },function(err){

                                      console.log('error' + JSON.stringify(err));

                                });

                       }else{

                                swal.fire({

                                    title               : 'Se cancelo la eliminicación!',

                                    imageUrl            : 'assets/img/error.png',

                                    showCloseButton     : true,

                                    confirmButtonColor  : '#044BFD'

                                });
                 
                       }
             
              });
        
        };





       }]);