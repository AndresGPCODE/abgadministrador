'use strict';


angular.module('abgpassportApp')
       
       .controller('InventoryCtrl',['$scope','inventoryServices','recordsServices','historialService',

        function($scope,inventoryServices,recordsServices,historialService){

          mapboxgl.accessToken = 'pk.eyJ1IjoiYnJpYW5hZnJpYXMiLCJhIjoiY2s4dDl3dXNoMHk2bzNobnNxcjl1YmF0NyJ9.lgYrXbnGrogqmH65_RPobQ';

          var map;

          var marcador;

          var arreglo = [];

          map =  new mapboxgl.Map({
                      container: 'mapa', // container id
                      style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
                      center: [-103.3004668,20.6481645], // starting position [lng, lat]
                      zoom: 9 // starting zoom
                  });

                  var nav = new mapboxgl.NavigationControl();

                  map.addControl(nav, "top-left");

          var expPlaca = /^([A-Z]{3}-\d{3,4})$/;

          $scope.inventario = {

                 nIDAutomovil       : '0',
                 Placas             : '',
                 Marca              : '',
                 Submarca           : '',
                 Version            : '',
                 Year               : '',
                 Modelo             : '',
                 Color1             : '',
                 Color2             : '',
                 NoSerie            : '',
                 NoMotor            : '',
                 NumeroPuertas      : '',
                 Estatus            : '',
                 Imagen             : '',
                 MaxDiasRenta       : ''

          };

          $scope.inventarioEdit = {

                 nIDAutomovil       : '0',
                 Placas             : '',
                 Marca              : '',
                 Submarca           : '',
                 Version            : '',
                 Year               : '',
                 Modelo             : '',
                 Color1             : '',
                 Color2             : '',
                 NoSerie            : '',
                 NoMotor            : '',
                 NumeroPuertas      : '',
                 Estatus            : '',
                 Imagen             : '',
                 MaxDiasRenta       : ''

          };

          $scope.clean = function(){

                $scope.inventario = {

                      nIDAutomovil       : '0',
                      Placas             : '',
                      Marca              : '',
                      Submarca           : '',
                      Version            : '',
                      Year               : '',
                      Modelo             : '',
                      Color1             : '',
                      Color2             : '',
                      NoSerie            : '',
                      NoMotor            : '',
                      NumeroPuertas      : '',
                      Estatus            : '',
                      Imagen             : '',
                      MaxDiasRenta       : ''

                };

                $('#add_modal').modal('hide');

          };

          $scope.cleanEdit = function(){

                $scope.inventarioEdit = {

                      nIDAutomovil       : '0',
                      Placas             : '',
                      Marca              : '',
                      Submarca           : '',
                      Version            : '',
                      Year               : '',
                      Modelo             : '',
                      Color1             : '',
                      Color2             : '',
                      NoSerie            : '',
                      NoMotor            : '',
                      NumeroPuertas      : '',
                      Estatus            : '',
                      Imagen             : '',
                      MaxDiasRenta       : ''

                };

                $('#update_modal').modal('hide');

          };

          $scope.get = function(){

                $scope.inventarios = '';
                $scope.mensaje     = '';

                inventoryServices.get().then(function(resp){

                                 if(resp.data == 'null'){

                                        $scope.mensaje = 'No tienes automoviles registrados';

                                 }else{

                                        $scope.inventarios = resp.data;

                                 }

                },function(err){

                      console.log('error' + JSON.stringify(err));

                });

          };

          function CargarImagen() {

              var archivoInput = document.getElementById('Imagen');

              var archivoRuta  = archivoInput.value;

              
                    if(archivoInput.files && archivoInput.files[0]){

                        var visor = new FileReader();

                        visor.addEventListener("load" , function(e){ console.log(e.target.result);

                            document.getElementById("doc").src = e.target.result;

                            $scope.inventario.Imagen = e.target.result;

                        });

                        visor.readAsDataURL(archivoInput.files[0]);
                    }

              

        }(function(){

              document.getElementById('Imagen').addEventListener("change", CargarImagen);
        })();

        function CargarImagenEdit() {

              var archivoInput = document.getElementById('ImagenEdit');

              var archivoRuta  = archivoInput.value;

              
                    if(archivoInput.files && archivoInput.files[0]){

                        var visor = new FileReader();

                        visor.addEventListener("load" , function(e){ console.log(e.target.result);

                            document.getElementById("docEdit").src = e.target.result;

                            $scope.inventarioEdit.Imagen = e.target.result;

                        });

                        visor.readAsDataURL(archivoInput.files[0]);
                    }

              

        }(function(){

              document.getElementById('ImagenEdit').addEventListener("change", CargarImagenEdit);
        })();

          $scope.add = function(inventario){

                if(inventario.Marca.length == 0){

                             swal.fire({

                                 title              : 'Oops!',

                                 text               : 'Marca se encuentra vacio, completalo',

                                 imageUrl           : 'assets/img/advertencia.png',

                                 showCloseButton    : true,

                                 confirmButtonColor : '#044BFD'

                             });

                }else if(inventario.Submarca.length == 0){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Submarca se encuentra vacio, completalo',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });

                }else if(inventario.Modelo.length == 0){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Modelo se encuentra vacio, completalo',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });
                                   
                }else if(inventario.Version.length == 0){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Versión se encuentra vacio, completalo',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });
                                   
                }else if(inventario.Color1.length == 0){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Color1 se encuentra vacio, completalo',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });
                                   
                }else if(!expPlaca.test(inventario.Placas)){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Número de Placa no cumple con las especificaciones, ejemplo: XXX-0000 ó XXX-000',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });
                                   
                }else if(inventario.NoSerie.length < 17){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Número de serie esta incompleto debe tener 17 dígitos, completalo',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });
                                   
                }else if(inventario.NoMotor.length == 0){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Número de motor se encuentra vacio, completalo',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });
                                   
                }else if(inventario.NumeroPuertas.length == 0){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Número de puertas se encuentra vacio, completalo',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });
                                   
                }else if(inventario.Estatus.length == 0){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Estatus se encuentra vacio, completalo',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });
                                   
                }else if(inventario.Imagen.length == 0){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Imagen se encuentra vacio, completalo',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });
                                   
                }else if(inventario.MaxDiasRenta == 0){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Los días de renta del vehiculo se encuentra vacio, completalo',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   :'#044BFD'
                                   });
                }else{

                    inventoryServices.add(inventario).then(function(resp){

                                     if(resp.data == 'INSERTED'){

                                            swal.fire({

                                                title               : 'Registro exitoso!',

                                                imageUrl            : 'assets/img/completo.png',

                                                showCloseButton     : true,

                                                confirmButtonColor  : '#044BFD'

                                            });

                                            $scope.clean();

                                            $scope.get();
                                     }else{

                                          swal.fire({

                                              title                 : 'Oops!',

                                              text                  : 'No se logro el registro, inténtalo de nuevo!',

                                              imageUrl              : 'assets/img/error.png',

                                              showCloseButton       : true,

                                              confirmButtonColor    : '#044BFD'

                                          });

                                     }

                    },function(err){

                        console.log('error' + JSON.stringify(err));

                    });

                }

          };


          $scope.show = function(inventario){

                $scope.inventarioEdit.nIDAutomovil            = inventario.nIDAutomovil;
                $scope.inventarioEdit.Marca                   = inventario.Marca;
                $scope.inventarioEdit.Submarca                = inventario.Linea_Submarca;
                $scope.inventarioEdit.Modelo                  = inventario.Modelo;
                $scope.inventarioEdit.Version                 = inventario.Version;
                $scope.inventarioEdit.Color1                  = inventario.Color1;
                $scope.inventarioEdit.Color2                  = inventario.Color2;
                $scope.inventarioEdit.Placas                  = inventario.Placas;
                $scope.inventarioEdit.NoSerie                 = inventario.NoSerie;
                $scope.inventarioEdit.NoMotor                 = inventario.NoMotor;
                $scope.inventarioEdit.NumeroPuertas           = inventario.NumeroPuertas;
                $scope.inventarioEdit.Estatus                 = inventario.Estatus;
                $scope.inventarioEdit.MaxDiasRenta            = inventario.MaxDiasRenta;
                $scope.inventarioEdit.Imagen                  = inventario.Imagen;
                 document.getElementById("docEdit").src   = inventario.Imagen;

          };


          $scope.update = function(inventario){

                if(inventario.Marca.length == 0){

                             swal.fire({

                                 title              : 'Oops!',

                                 text               : 'Marca se encuentra vacio, completalo',

                                 imageUrl           : 'assets/img/advertencia.png',

                                 showCloseButton    : true,

                                 confirmButtonColor : '#044BFD'

                             });

                }else if(inventario.Submarca.length == 0){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Submarca se encuentra vacio, completalo',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });

                }else if(inventario.Modelo.length == 0){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Modelo se encuentra vacio, completalo',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });
                                   
                }else if(inventario.Version.length == 0){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Versión se encuentra vacio, completalo',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });
                                   
                }else if(inventario.Color1.length == 0){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Color1 se encuentra vacio, completalo',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });
                                   
                }else if(!expPlaca.test(inventario.Placas)){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Número de Placa no cumple con las especificaciones, ejemplo: XXX-000 ó XXX-0000',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });
                                   
                }else if(inventario.NoSerie.length < 17){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Número de serie esta incompleto debe tener 17 dígitos, completalo',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });
                                   
                }else if(inventario.NoMotor.length == 0){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Número de motor se encuentra vacio, completalo',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });
                                   
                }else if(inventario.NumeroPuertas.length == 0){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Número de puertas se encuentra vacio, completalo',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });
                                   
                }else if(inventario.Estatus.length == 0){

                                   swal.fire({

                                       title                : 'Oops!',

                                       text                 : 'Estatus se encuentra vacio, completalo',

                                       imageUrl             : 'assets/img/advertencia.png',

                                       showCloseButton      : true,

                                       confirmButtonColor   : '#044BFD'

                                   });
                                   
                }else{

                    inventoryServices.update(inventario).then(function(resp){

                                     if(resp.data == 'UPDATED'){

                                            swal.fire({

                                                title               : 'Modificación exitosa!',

                                                imageUrl            : 'assets/img/completo.png',

                                                showCloseButton     : true,

                                                confirmButtonColor  : '#044BFD'

                                            });

                                            $scope.cleanEdit();

                                            $scope.get();
                                     }else{

                                          swal.fire({

                                              title                 : 'Oops!',

                                              text                  : 'No se logro el registro, inténtalo de nuevo!',

                                              imageUrl              : 'assets/img/error.png',

                                              showCloseButton       : true,

                                              confirmButtonColor    : '#044BFD'

                                          });

                                     }

                    },function(err){

                        console.log('error' + JSON.stringify(err));

                    });

                }
          };


          $scope.destroy = function(id){

              swal.fire({

                  title               : 'ADVERTENCIA',

                  text                : '¿Estás seguro de eliminar este registro?',

                  imageUrl            : 'assets/img/advertencia.png',

                  showCancelButton    : true,

                  showCloseButton     : true,                  

                  cancelButtonColor   : 'rgba(4,75,253,0.5)',                  

                  confirmButtonColor  : '#044BFD',

                  cancelButtonText    : 'Cancelar',

                  confirmButtonText   : 'Si',

              }).then((result) => {

                       if(result.value){

                                inventoryServices.destroy(id).then(function(resp){

                                              if(resp.data == "DELETED"){

                                                      swal.fire({

                                                          title              : 'Registro eliminado con exito!',

                                                          imageUrl           : 'assets/img/completo.png',

                                                          showCloseButton    : true,

                                                          confirmButtonColor : '#044BFD'

                                                      });

                                                      $scope.get();

                                              }else{

                                                      swal.fire({

                                                          title             : 'Oops!',

                                                          text              : 'No se pudo eliminar el registro, inténtalo de nuevo',

                                                          imageUrl          : 'assets/img/error.png',

                                                          showCloseButton   : true,

                                                          confirmButtonColor: '#044BFD'

                                                      });

                                              }

                                },function(err){

                                      console.log('error' + JSON.stringify(err));

                                });

                       }else{

                                swal.fire({

                                    title               : 'Se cancelo la eliminación!',

                                    imageUrl            : 'assets/img/error.png',

                                    showCloseButton     : true,

                                    confirmButtonColor  : '#044BFD'

                                });
                 
                       }
             
              });
        
        };

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //                                                                                                         //
        //                           C R U D   D E   C O N T A C T O S    D E    L A    E M P R E S A              //
        //                                                                                                         //      
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $scope.ficha = {

              nIDFichaMantenimiento : '0',
              nIDAutomovil          : '',
              Fecha                 : '',
              Kilometraje           : '',
              Trabajo               : '',
              Nombre                : '',
              Costo                 : '',
              Notas                  : ''

        };

        $scope.fichaEdit = {

              nIDFichaMantenimiento : '0',
              nIDAutomovil          : '',
              Fecha                 : '',
              Kilometraje           : '',
              Trabajo               : '',
              Nombre                : '',
              Costo                 : '',
              Notas                  : ''

        };

        $scope.cleanFicha = function(id){

              $scope.ficha = {

                    nIDFichaMantenimiento : '0',
                    nIDAutomovil          : id,
                    Fecha                 : '',
                    Kilometraje           : '',
                    Trabajo               : '',
                    Nombre                : '',
                    Costo                 : '',
                    Notas                  : ''

              };

              document.getElementById('Visible').style.display = 'none';
        
        };

        $scope.cleanEditFicha = function(id){

              $scope.fichaEdit = {

                    nIDFichaMantenimiento : '0',
                    nIDAutomovil          : id,
                    Fecha                 : '',
                    Kilometraje           : '',
                    Trabajo               : '',
                    Nombre                : '',
                    Costo                 : '',
                    Notas                  : ''

              };

              document.getElementById('VisibleModificar').style.display = 'none'; 
        
        };

        $scope.Ficha = function(id,marca,modelo,placa){

              $scope.ficha.nIDAutomovil = id;
              $scope.Marca = marca;
              $scope.Modelo = modelo;
              $scope.Placas = placa;
              document.getElementById('Visible').style.display = 'none';
              document.getElementById('VisibleModificar').style.display = 'none';
              $scope.getFichas(id);
              $scope.getHistorial(id);
              $scope.getGps(placa);
              $scope.getFechas(placa);
              //$scope.HistorialFechas(placa);
        };

        $scope.getFichas = function(id){

              $scope.mensajeF = '';
              $scope.fichas   = '';

              recordsServices.get(id).then(function(resp){

                              if(resp.data == 'null'){

                                    $scope.mensajeF = 'No tienes fichas de mantenimiento registradas';

                              }else{

                                    $scope.fichas = resp.data;

                              }

              },function(err){

                    console.log('error' + JSON.stringify(err));

              });

        };

        $scope.getGps = function(placa){

              $scope.Nombre           = "";
              $scope.placas           = "";
              $scope.Imei             = "";
              $scope.Lada             = "";
              $scope.Celular          = "";
              $scope.Numero_Confianza = "";
              $scope.FechaCreacion    = "";

              inventoryServices.getGps(placa).then(function(resp){console.log(resp);

                                if(resp.data == 'null'){


                                }else{

                                    $scope.Nombre           = resp.data[0]['Nombre'];
                                    $scope.placas           = resp.data[0]['Placas'];
                                    $scope.Imei             = resp.data[0]['Imei'];
                                    $scope.Lada             = resp.data[0]['Lada'];
                                    $scope.Celular          = resp.data[0]['Celular'];
                                    $scope.Numero_Confianza = resp.data[0]['Numero_Confianza'];
                                    $scope.FechaCreacion    = resp.data[0]['FechaCreacion'];
                                }
              
              },function(err){

                    console.log('error' + JSON.stringify(err));

              });

        };

        $scope.getHistorial = function(id){ 

              $scope.mensajeH = '';
              $scope.historiales = '';

              inventoryServices.Historial(id).then(function(resp){

                               if(resp.data == 'null'){

                                      $scope.mensajeH = 'Historial vacio';

                               }else{

                                     $scope.historiales = resp.data;

                               }
              
              },function(err){

                    console.log('error' + JSON.stringify(err));

              });
        
        };

        $scope.getFechas = function(objeto){

              $scope.vistas = '';

              historialService.getFechas(objeto).then(function(resp){ console.log(resp.data);

                              if(resp.data == 'null'){


                              }else{

                                  $scope.vistas = resp.data;

                              }

              },function(err){

                    console.log('error' + JSON.stringify(err));

              });

        };



        $scope.mapas = function(id,fecha){

              if(arreglo.length != 0){

                        for(var i = 0; i < arreglo.length; i++){

                                arreglo[i].remove();
                                
                        }
              }

              historialService.buscarRuta(id,fecha).then(function(resp){ console.log(resp.data);

                              if(resp.data == 'null'){


                              }else{

                                  for(var i=0; i < resp.data.length; i++){

                                        var geojson = {

                                            type      : 'FeatureCollection',
                                            features  : [{

                                                type      : 'Feature',
                                                geometry  : {

                                                    type        : 'Point',
                                                    coordinates : [resp.data[i]['Longitud'], resp.data[i]['Latitud']]

                                                },

                                                properties  : {

                                                    title   : resp.data[i]['Identificador'],

                                                }

                                            }]

                                        };

                                        //Agregar marcadores
                                        geojson.features.forEach(function(marker){

                                          //Crear un elemento en html que sera el marcador
                                          var el = document.createElement('div');
                                          el.className = 'marker';

                                          marcador = new mapboxgl.Marker(el)
                                                    .setLngLat(marker.geometry.coordinates)
                                                    .setPopup(new mapboxgl.Popup({ offset : 25 })
                                                    .setHTML('<h3>' + marker.properties.title + '</h3>'))
                                                    .addTo(map);
                                                    arreglo.push(marcador);

                                        });
                                  }

                              }

              },function(err){

                    console.log('error' + JSON.stringify(err));

              });

        };

        $scope.mostrar = function(){

              document.getElementById('Visible').style.display = 'block'; 
        };

        $scope.addFicha = function(ficha){

              if(ficha.Fecha.length == 0){

                      swal.fire({

                          title               : 'Oops!',

                          text                : 'Fecha se escuentra vacio completalo',

                          imageUrl            : 'assets/img/advertencia.png',

                          showCloseButton     : true,

                          confirmButtonColor  : '#044BFD'

                      });

              }else if(ficha.Nombre.length == 0){

                      swal.fire({

                          title               : 'Oops!',

                          text                : 'Nombre se encuentra vacio completalo',

                          imageUrl            : 'assets/img/advertencia.png',

                          showCloseButton     : true,

                          confirmButtonColor  : '#044BFD'

                      });
                      
              }else if(ficha.Trabajo.length == 0){

                      swal.fire({

                          title               : 'Oops!',

                          text                : 'Trabajo ó servicio se encuentra vacio completalo',

                          imageUrl            : 'assets/img/advertencia.png',

                          showCloseButton     : true,

                          confirmButtonColor  : '#044BFD'

                      });
                      
              }else if(ficha.Kilometraje.length == 0){

                      swal.fire({

                          title               : 'Oops!',

                          text                : 'Kilometraje se encuentra vacio completalo',

                          imageUrl            : 'assets/img/advertencia.png',

                          showCloseButton     : true,

                          confirmButtonColor  : '#044BFD'

                      });
                      
              }else if(ficha.Costo.length == 0){

                      swal.fire({

                          title               : 'Oops!',

                          text                : 'Costo se encuentra vacio completalo',

                          imageUrl            : 'assets/img/advertencia.png',

                          showCloseButton     : true,

                          confirmButtonColor  : '#044BFD'

                      });
                      
              }else{

                    recordsServices.add(ficha).then(function(resp){

                                    if(resp.data == "INSERTED"){

                                           swal.fire({

                                                title               : 'Registro exitoso!',

                                                imageUrl            : 'assets/img/completo.png',

                                                showCloseButton     : true,

                                                confirmButtonColor  : '#044BFD'

                                           });

                                           $scope.getFichas(ficha.nIDAutomovil);

                                           $scope.cleanFicha(ficha.nIDAutomovil);

                                           
                                    }else{

                                          swal.fire({

                                              title             : 'Oops!',

                                              text              : 'No se logro el registro, inténtalo de nuevo',

                                              imageUrl          : 'assets/img/error.png',

                                              showCloseButton   : true,

                                              confirmButtonColor: '#044BFD'

                                          });

                                    }

                    },function(err){

                        console.log('error' + JSON.stringify(err));

                    });

              }

        };


        $scope.showFicha = function(ficha){
              document.getElementById('VisibleModificar').style.display = 'block';
              $scope.fichaEdit.nIDFichaMantenimiento    = ficha.nIDFichaMantenimiento;
              $scope.fichaEdit.nIDAutomovil             = ficha.nIDAutomovil;
              $scope.fichaEdit.Fecha                    = ficha.Fecha;
              $scope.fichaEdit.Kilometraje              = ficha.Kilometraje;
              $scope.fichaEdit.Trabajo                  = ficha.Trabajo_Servicio;
              $scope.fichaEdit.Nombre                   = ficha.Nombre;
              $scope.fichaEdit.Costo                    = ficha.Costo;
              $scope.fichaEdit.Notas                    = ficha.Nota;
              document.getElementById('FechaEdit').value= ficha.Fecha.substring(0,10);

        };


        $scope.updateFicha = function(ficha){

              if(ficha.Fecha.length == 0){

                      swal.fire({

                          title               : 'Oops!',

                          text                : 'Fecha se escuentra vacio completalo',

                          imageUrl            : 'assets/img/advertencia.png',

                          showCloseButton     : true,

                          confirmButtonColor  : '#044BFD'

                      });

              }else if(ficha.Nombre.length == 0){

                      swal.fire({

                          title               : 'Oops!',

                          text                : 'Nombre se encuentra vacio completalo',

                          imageUrl            : 'assets/img/advertencia.png',

                          showCloseButton     : true,

                          confirmButtonColor  : '#044BFD'

                      });
                      
              }else if(ficha.Trabajo.length == 0){

                      swal.fire({

                          title               : 'Oops!',

                          text                : 'Trabajo ó servicio se encuentra vacio completalo',

                          imageUrl            : 'assets/img/advertencia.png',

                          showCloseButton     : true,

                          confirmButtonColor  : '#044BFD'

                      });
                      
              }else if(ficha.Kilometraje.length == 0){

                      swal.fire({

                          title               : 'Oops!',

                          text                : 'Kilometraje se encuentra vacio completalo',

                          imageUrl            : 'assets/img/advertencia.png',

                          showCloseButton     : true,

                          confirmButtonColor  : '#044BFD'

                      });
                      
              }else if(ficha.Costo.length == 0){

                      swal.fire({

                          title               : 'Oops!',

                          text                : 'Costo se encuentra vacio completalo',

                          imageUrl            : 'assets/img/advertencia.png',

                          showCloseButton     : true,

                          confirmButtonColor  : '#044BFD'

                      });
                      
              }else{

                    recordsServices.update(ficha).then(function(resp){

                                    if(resp.data == "UPDATED"){

                                           swal.fire({

                                                title               : 'Modificación exitosa!',

                                                imageUrl            : 'assets/img/completo.png',

                                                showCloseButton     : true,

                                                confirmButtonColor  : '#044BFD'

                                           });

                                           $scope.getFichas(ficha.nIDAutomovil);

                                           $scope.cleanEditFicha(ficha.nIDAutomovil);

                                           
                                    }else{

                                          swal.fire({

                                              title             : 'Oops!',

                                              text              : 'No se logro el registro, inténtalo de nuevo',

                                              imageUrl          : 'assets/img/error.png',

                                              showCloseButton   : true,

                                              confirmButtonColor: '#044BFD'

                                          });

                                    }

                    },function(err){

                        console.log('error' + JSON.stringify(err));

                    });

                  }
        };

        $scope.destroyFicha = function(id,nIDAutomovil){

              swal.fire({

                  title                 : 'ADVERTENCIA',

                  text                  : '¿Estás seguro de eliminar este registro?',

                  imageUrl              : 'assets/img/advertencia.png',

                  showCancelButton      : true,

                  showCloseButton       : true,

                  confirmButtonColor    : '#044BFD',

                  cancelButtonColor     : 'rgba(4,75,253,0.5)',

                  confirmButtonText     : 'Si',

                  cancelButtonText      : 'Cancelar'

              }).then((result) => {

                      if(result.value){

                                recordsServices.destroy(id).then(function(resp){

                                                if(resp.data == "DELETED"){

                                                        $scope.getFichas(nIDAutomovil);

                                                        swal.fire({

                                                            title             : 'Registro eliminado con exito!',

                                                            imageUrl          : 'assets/img/completo.png',

                                                            showCloseButton   : true,

                                                            confirmButtonColor: '#044BFD'

                                                        });

                                                }else{

                                                        swal.fire({

                                                            title                 : 'Oops!',

                                                            text                  : 'No se pudo eliminar el registro, inténtalo de nuevo',

                                                            imageUrl              : 'assets/img/error.png',

                                                            showCloseButton       : true,

                                                            confirmButtonColor    : '#044BFD'

                                                        });

                                                }

                                }, function(err){

                                    console.log('error' + JSON.stringify(err));

                                });

                      }else{

                            swal.fire({

                                title               : 'Se cancelo la eliminación!',

                                imageUrl            : 'assets/img/error.png',

                                showCloseButton     : true,

                                confirmButtonColor  : '#044BFD'

                            });

                      }

              });


        };


        }]);