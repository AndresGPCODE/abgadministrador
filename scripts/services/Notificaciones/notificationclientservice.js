'use strict';

angular.module('abgpassportApp')

      .service('notificationclientesServices', ['$http','configService', 

        function($http,configService){


        	return {

        			get : function(){

        				return $http.get(configService.apiUrl + 'Notificaciones.php?accion=consultar',

        					{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

        				);

        			},

        			add : function(objeto){

        				return $http.post(configService.apiUrl + 'Notificaciones.php','accion=agregar&objeto=' + JSON.stringify(objeto),

        					{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

        				);

        			},

        			update : function(objeto){

        				return $http.post(configService.apiUrl + 'Notificaciones.php','accion=modificar&objeto=' + JSON.stringify(objeto),

        					{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

        				);

        			},

        			destroy : function(id){

        				return $http.post(configService.apiUrl + 'Notificaciones.php','accion=eliminar&id=' + JSON.stringify(id),

        					{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

        				);

        			}

        	};
        
      

      }]);