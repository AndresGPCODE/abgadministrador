<?php
  
  error_reporting(E_ALL ^ E_NOTICE);
  ini_set('display_errors','1');

  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Request-Methods');

  include_once '../Configuraciones/Conexion.php';
  include_once '../Usuario/clPuesto.php';
  include_once '../Utilerias/clHerramientas_v2011.php';

  $method = $_SERVER['REQUEST_METHOD'];

  $UtileriasDatos    = new clHerramientasv2011();
  $fechaLocal        = $UtileriasDatos->getFechaYHoraActual_General();
  $fechaLocal        = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

  $marketstall = new clPuesto();

  $marketstall->conexion($SERVER,$USER,$PASSWORD,$BD);

  switch ($method) {
    
    case 'GET':

          $action = $_GET['accion'];

          if($action == 'consultar'){

                $marketstall->leerCondicion(" tbl_cat_puesto.bEstado = 1 ");

                $registro = $marketstall->dtBase();

                echo json_encode($registro);
                break;
          }

    case 'POST' : 

          $action = $_POST['accion'];

          if($action == 'agregar'){

                  $object = $_POST['puesto'];

                  $data   = json_decode($object);

                  $marketstall->setInformacion(

                              $data->nIDPuesto,
                              $data->Puesto,
                              $data->Descripcion,
                              '',
                              '',
                              'Nuevo puesto - ' . $fechaLocal,
                              '1',
                              TRUE,
                              FALSE,
                              FALSE

                  );

                  if($marketstall->ejecutar('tbl_cat_puesto')){

                                echo "INSERTED";

                  }else{

                                echo "NOT INSERTED";

                  }
          }else if($action == 'modificar'){

                  $object = $_POST['puesto'];

                  $data   = json_decode($object);

                  $marketstall->setInformacion(

                              $data->nIDPuesto,
                              $data->Puesto,
                              $data->Descripcion,
                              '',
                              '',
                              'Modificacion al puesto - ' . $fechaLocal,
                              '1',
                              FALSE,
                              TRUE,
                              FALSE

                  );

                  if($marketstall->ejecutar('tbl_cat_puesto')){

                                echo "UPDATED";

                  }else{

                                echo "NOT UPDATED";
                                
                  }
          }else if($action == 'eliminar'){

                  $id = $_POST['id'];

                      $observacion = 'Puesto eliminada temporalmente - ' . $fechaLocal;

                      if($marketstall->ocultar($id,$observacion)){

                            echo "DELETED";
                      }else{

                            echo "NOT DELETED";
                      }
          }
      
  }
?>