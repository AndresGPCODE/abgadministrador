<?php
  error_reporting(E_ALL ^ E_NOTICE);  

  ini_set('display_errors', '1');

  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Request-Methods');

  include_once '../Configuraciones/Conexion.php';
  include_once '../Automovil/clFichaMantenimiento.php';
  include_once '../Utilerias/clHerramientas_v2011.php';

  $method         = $_SERVER['REQUEST_METHOD'];

  $UtileriasDatos = new clHerramientasv2011();
  $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
  $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

  $records        = new clFichaMantenimiento();

  $records->conexion($SERVER,$USER,$PASSWORD,$BD);

  switch($method){

       
        case 'POST' : 

             $action = $_POST['accion'];

             if($action == 'consultar'){

                    $id = $_POST['id'];

                    $records->leerCondicion('nIDAutomovil ='. $id . ' and bEstado = 1');

                    $registros = $records->dtBase();

                    echo json_encode($registros);


             }else if($action == 'agregar'){

                  $object = $_POST['ficha'];

                  $data   = json_decode($object);

                  $records->setInformacion(

                        $data->nIDFichaMantenimiento,
                        $data->nIDAutomovil,
                        $data->Fecha,
                        $data->Kilometraje,
                        $data->Trabajo,
                        $data->Nombre,
                        $data->Costo,
                        $data->Notas,
                        '',
                        '',
                        'Nueva ficha de mantenimiento - ' . $fechaLocal,
                        '1',
                        TRUE,
                        FALSE,
                        FALSE

                  );

                  if($records->ejecutar('tbl_ficha_mantenimiento')){

                          echo "INSERTED";

                  }else{

                          echo "NOT INSERTED";

                  }

             }else if($action == 'modificar'){

                      $object = $_POST['ficha'];

                      $data   = json_decode($object);

                      $records->setInformacion(
                             $data->nIDFichaMantenimiento,
                             $data->nIDAutomovil,
                             $data->Fecha,
                             $data->Kilometraje,
                             $data->Trabajo,
                             $data->Nombre,
                             $data->Costo,
                             $data->Notas,
                             '',
                             '',
                             'Modificación a la ficha de mantenimiento - ' . $fechaLocal,
                             '1',
                             FALSE,
                             TRUE,
                             FALSE
                      );

                      if($records->ejecutar('tbl_ficha_mantenimiento')){

                                echo "UPDATED";

                      }else{

                                echo "NOT UPDATED";

                      }

             }else if($action == 'eliminar'){

                      $id = $_POST['id'];

                      $observacion = 'Ficha de mantenimiento eliminada temporalmente - ' . $fechaLocal;

                      if($records->ocultar($id,$observacion)){

                            echo "DELETED";
                      }else{

                            echo "NOT DELETED";
                      }
             }
  }