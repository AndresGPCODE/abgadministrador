<?php
/*
------OBTENCION DE NOTIFICACIONES TANTO PARA CLIENTES COMO CHOFERES, Y ACTUALIZACION DE  LAS MISMAS A LEIDAS
*/
header('Content-Type: application/json');
date_default_timezone_set('America/Mexico_City');
$method = $_SERVER["REQUEST_METHOD"];
// $method = "POST";
include 'conexion.php';

switch ($method) {
    case 'POST':
        $request = json_decode(file_get_contents('php://input'), true);
        // $request['kndUser'] = 0;
        // $request['usrId'] = 3;
        // $request['option'] = 1;

        if (is_null($request['kndUser']) || is_null($request['usrId'])) {
            $request['resp'] = 'Los Datos enviados están Incompletos||' . $request . " || " . file_get_contents('php://input');
            $request['status'] = false;
            return json_encode($request);
        } else {
            if ($request['option'] == 2) {
                // Actualizar notificaciones a leidas
                if ($request['kndUser'] == 0) {
                    $resp = notificacionLeida($request['idNotificacion']);
                    if ($resp[0]['status']) {
                        echo json_encode($resp);
                        // echo count($resp);
                        return json_encode($resp);
                    } else {
                        echo json_encode($resp);
                        // echo count($resp);
                        return json_encode($resp);
                    }
                } else if ($request['kndUser'] == 1) {
                    $resp = notificacionLeidaChofer($request['idNotificacion']);
                    if ($resp[0]['status']) {
                        echo json_encode($resp);
                        // echo count($resp);
                        return json_encode($resp);
                    } else {
                        echo json_encode($resp);
                        // echo count($resp);
                        return json_encode($resp);
                    }
                }
                
            } else {
                // Obtener notificaciones
                if ($request['kndUser'] == 1) {
                    // Chofer
                    $resp = getNotificationsChofer($request['usrId']);
                    // $resp = getNotificationsUser($request['usrId']);
                } else if ($request['kndUser'] == 0) {
                    // Usuario o cliente
                    $resp = getNotificationsUser($request['usrId']);
                } else {
                    // no se recivio el dato
                    $resp['resp'] = 'No se puede procesar la informacion';
                    $resp['status'] = false;
                    echo json_encode($resp);
                    return json_encode($resp);
                }
                if ($resp[0]['status']) {
                    echo json_encode($resp);
                    // echo count($resp);
                    return json_encode($resp);
                } else {
                    $resp[0]['resp'] = 'No hay notificaciones';
                    $resp[0]['status'] = false;
                    echo json_encode($resp);
                    return json_encode($resp);
                }
            }
        }

    return;
    break;
    default:
        $request['resp'] = 'No se puede procesar la informacion';
        $request['status'] = false;
    return json_encode($request);

}

function getNotificationsUser($usrId){
    // Notificaciones para el usuario
    // Conectar a la base de datos de notificaciones de usuarios
    $pdo = Conexion();
    $select = "SELECT notu.*, tipn.* FROM tbl_cat_notificaciones as notu, tbl_cat_tipo_notificaciones as tipn WHERE nIDCliente = '$usrId' AND tipn.nIDTipoNotificacion = notu.nIDTipoNotificacion AND notu.bEstado = 1 ORDER BY notu.Estatus DESC, notu.FechaCreacion DESC";
    // $select = "SELECT notu.*, tipn.* FROM tbl_cat_notificaciones as notu, tbl_cat_tipo_notificaciones as tipn WHERE nIDCliente = '$usrId' AND tipn.nIDTipoNotificacion = notu.nIDTipoNotificacion AND notu.bEstado = 1 AND notu.Estatus = 'NO LEIDO'";
    $ejecutar = $pdo->prepare($select);
    $ejecutar->execute();
    $result = $ejecutar->fetchAll(PDO::FETCH_ASSOC);
    // var_dump($select);
    $arr = [];
    $notidicaciones = [];
    if ($result) {
        // $arr['status'] = true;
        // $arr['resp'] = 'Notificaciones para el usuario';
        foreach($result as $row){
            $row['status'] = true;
            $row['resp'] = 'Notificaciones para el usuario';
            $row['bEstado'] = (int)$row['bEstado'];
            $row['nIDCliente'] = (int)$row['nIDCliente'];
            $row['nIDTipoNotificacion'] = (int)$row['nIDTipoNotificacion'];
            // $row['FechaModificacion'] = date_parse($row['FechaModificacion']);
            // $row['FechaCreacion'] = date_parse($row['FechaCreacion']);
            array_push($notidicaciones, $row); 
        }
        $arr = $notidicaciones;
        return $arr;
    } else {
        $arr[0]['status'] = false;
        return $arr;
    }
}

function getNotificationsChofer($choferId){
    // Notificaciones para el chofer
    // Conectar a la base de datos de notificaciones de Chofer
    $pdo = Conexion();
    // $select = "SELECT notc.*, tipn.* FROM tbl_cat_notificaciones_chofer AS notc, tbl_cat_tipo_notificaciones AS tipn WHERE notc.nIDChofer = '$choferId' AND tipn.nIDTipoNotificacion = notc.nIDTipoNotificacion AND notc.bEstado = 1 AND notc.Estatus = 'NO LEIDO'";
    $select = "SELECT notc.*, tipn.* FROM tbl_cat_notificaciones_chofer AS notc, tbl_cat_tipo_notificaciones AS tipn WHERE notc.nIDChofer = '$choferId' AND tipn.nIDTipoNotificacion = notc.nIDTipoNotificacion AND notc.bEstado = 1 ORDER BY notc.Estatus DESC";
    $ejecutar = $pdo->prepare($select);
    $ejecutar->execute();
    $result = $ejecutar->fetchAll(PDO::FETCH_ASSOC);
    // var_dump($select);
    $arr = [];
    $notidicaciones = [];
    if ($result) {
        // $arr['status'] = true;
        // $arr['resp'] = 'Notificaciones para el usuario';
        foreach ($result as $row) {
            $row['status'] = true;
            $row['resp'] = 'Notificaciones para el chofer';
            $row['bEstado'] = (int) $row['bEstado'];
            $row['nIDChofer'] = (int) $row['nIDChofer'];
            $row['nIDNotificacionesChofer'] = (int) $row['nIDNotificacionesChofer'];
            array_push($notidicaciones, $row);
        }
        $arr = $notidicaciones;
        return $arr;
    } else {
        $arr[0]['status'] = false;
        return $arr;
    }
}

function notificacionLeida($idNotificacion){
    $pdo = Conexion();
    // actualizamos el registro de solicitud a entregado
    $fechaActual = date('Y-m-d H:i:s');

    $update = "UPDATE tbl_cat_notificaciones SET Estatus = 'LEIDO', FechaModificacion = '$fechaActual' WHERE nIDNotificacion = " . $idNotificacion;
    $ejecutar = $pdo->prepare($update);
    // $result = $pdo->lastInsertId();
    // var_dump($ejecutar);
    $arr = [];
    if ($ejecutar->execute()) {
        $arr[0]['status'] = true;
        $arr[0]['resp'] = "Se actualizo la notificacion";
        return $arr;
    } else {
        $arr[0]['status'] = false;
        $arr[0]['resp'] = "No se pudo actualizar la notificacion";
        return $arr;
    }
}

function notificacionLeidaChofer($idNotificacion){
    $pdo = Conexion();
    // actualizamos el registro de solicitud a entregado
    $fechaActual = date('Y-m-d H:i:s');

    $update = "UPDATE tbl_cat_notificaciones_chofer SET Estatus = 'LEIDO', FechaModificacion = '$fechaActual' WHERE nIDNotificacionesChofer = " . $idNotificacion;
    $ejecutar = $pdo->prepare($update);
    // $result = $pdo->lastInsertId();
    // var_dump($ejecutar);
    $arr = [];
    if ($ejecutar->execute()) {
        $arr[0]['status'] = true;
        $arr[0]['resp'] = "Se actualizo la notificacion";
        return $arr;
    } else {
        $arr[0]['status'] = false;
        $arr[0]['resp'] = "No se pudo actualizar la notificacion";
        return $arr;
    }
}

?>