'use strict';

angular.module('abgpassportApp')

       .service('marketstallServices',['$http','configService', function($http,configService){

            return{

                  get : function(){

                      return $http.get(configService.apiUrl + 'Puesto.php?accion=consultar',

                        { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                      );

                  },

                  add : function(puesto){

                      return $http.post(configService.apiUrl + 'Puesto.php','accion=agregar&puesto=' + JSON.stringify(puesto),

                        { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                      );

                  },

                  update : function(puesto){

                      return $http.post(configService.apiUrl + 'Puesto.php','accion=modificar&puesto=' + JSON.stringify(puesto),

                        { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                      );

                  },

                  destroy : function(id){

                      return $http.post(configService.apiUrl + 'Puesto.php','accion=eliminar&id=' + JSON.stringify(id),

                        { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                      );
                      
                  }
            };

       }]);