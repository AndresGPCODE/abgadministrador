'use strict';

angular.module('abgpassportApp')

	   .service('commandServices',['$http','configService',

	   	function($http,configService){

	   		return {

	   			get : function(id){

	   				return $http.post(configService.apiUrl + 'Comandos.php','accion=buscar&id=' + JSON.stringify(id),

	   					{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   				);

	   			},

	   			add : function(objeto){

	   				return $http.post(configService.apiUrl + 'Comandos.php','accion=agregar&objeto=' + JSON.stringify(objeto),

	   					{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   				);

	   			}

	   		};

	   	}]);