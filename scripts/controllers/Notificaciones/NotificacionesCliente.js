'use strict';

angular.module('abgpassportApp')

       .controller('NotificationClientesCtrl', ['$scope','notificationclientesServices','clientServices','typeNotificationServices', 

        function($scope,notificationclientesServices,clientServices,typeNotificationServices){


        		$scope.notificacion = {

        			  nIDNotificacion : '0',
        			  nIDCliente 	  : '0',
        			  nIDNotificacion : '0',
        			  Encabezado 	  : '',
        			  Body 			  : '',
        			  Permiso 		  : ''

        		};

        		$scope.clean = function(){

        			  $scope.notificacion = {

        			  nIDNotificacion : '0',
        			  nIDCliente 	  : '0',
        			  nIDNotificacion : '0',
        			  Encabezado 	  : '',
        			  Body 			  : '',
        			  Permiso 		  : ''

        		};

        		$('#add_modal').modal('hide');
        		$scope.get();
        		}
         
       			$scope.get = function(){

       				   $scope.getUsuarios();

                 $scope.tipos();

       				   notificationclientesServices.get().then(function(resp){ console.log(resp.data);

       				   							 if(resp.data == 'null'){

       				   							 		console.log('Not Foun Data');
       				   							 		$scope.Titulo = 'No tiens Notificaciones registradas';

       				   							 }else{

       				   							 		$scope.notificaciones = resp.data;

       				   							 }
       				   },function(err){

       				   		console.log('error' + JSON.Stringify(err));

       				   });

       			};

            $scope.tipos = function(){

                  typeNotificationServices.get().then(function(resp){

                                          if(resp.data == 'null'){

                                                 console.log('Not Found Data');

                                          }else{

                                                $scope.tipos = resp.data;

                                          }

                  },function(err){

                        console.log('error' + JSON.Stringify(err));

                  });

            };


       			$scope.getUsuarios = function(){

       				  clientServices.get().then(function(resp){

       				  			    if(resp.data == 'null'){

       				  			    			console.log('Not Found Data');

       				  			    }else{

       				  			    			$scope.clientes = resp.data;

       				  			    }

       				  }, function(err){

       				  			console.log('error' + JSON.Stringify(err));

       				  });

       			};


       			$scope.add = function(objeto){

                  if(objeto.Encabezado.length == 0){

                  swal.fire({

                        title             : 'Oops!',

                        text            : 'Encabezado se encuentra vacio completalo',

                        imageUrl          : 'assets/img/advertencia.png',

                        showCloseButton         : true,

                        confirmButtonColor        : '#044BFD'

                      });

             }else if(objeto.Body.length == 0){

                  swal.fire({

                        title             : 'Oops!',

                        text            : 'Cuerpo de la notificación se encuentra vacio completalo',

                        imageUrl          : 'assets/img/advertencia.png',

                        showCloseButton         : true,

                        confirmButtonColor        : '#044BFD'

                      });

             }else{

       				  notificationclientesServices.add(objeto).then(function(resp){

       				  							  if(resp.data == "INSERTED"){

       				  							  		 swal.fire({

       				  							  		 	title 						: 'Registro exitoso!',

       				  							  		 	imageUrl 					: 'assets/img/completo.png',

       				  							  		 	showCloseButton 		    : true,

       				  							  		 	confirmButtonColor 		    : '#044BFD'

       				  							  		 });

       				  							  		 $scope.clean();

                                     $scope.get();

       				  							  }else{

       				  							  		swal.fire({

       				  							  		 	title 						: 'Oops!',

       				  							  		 	text 						: 'No se completo el registro, Inténtalo de nuevo',

       				  							  		 	imageUrl 					: 'assets/img/error.png',

       				  							  		 	showCloseButton 		    : true,

       				  							  		 	confirmButtonColor 		    : '#044BFD'

       				  							  		 });

       				  							  }
       				  }, function(err){

       				  			console.log('error' + JSON.Stringify(err));

       				  });

              }

       			};




       }]);