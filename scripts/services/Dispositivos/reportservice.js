'use strict';

angular.module('abgpassportApp')
		
	   .service('reportServices',['$http','configService',

	   	function($http,configService){

	   		return {

	   				get : function(){

	   					return $http.get(configService.apiUrl + 'Reporte.php?accion=consultar',

	   						{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   					);

	   				}

	   		};
	   		
	   	}]);
