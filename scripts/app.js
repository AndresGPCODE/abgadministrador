'use strict';

/**
 * @ngdoc overview
 * @name abgpassportApp
 * @description
 * # abgpassportApp
 *
 * Main module of the application.
 */
angular.module('abgpassportApp', [
    
    'ngAnimate',
    
    'ngCookies',
    
    'ngResource',
    
    'ngSanitize',
    
    'ngTouch',

    'ui.router'
  
  ])
  .config(['$stateProvider', '$urlRouterProvider', '$locationProvider',

          function($stateProvider,$urlRouterProvider,$locationProvider){

                  $stateProvider

                  .state('main',{

                    url: '/log',

                    templateUrl : 'views/main.html',

                    controller : 'LoginCtrl'

                  })

                  .state('header',{

                    url: '/home',

                    templateUrl: 'views/Header/header.php',

                    controller: 'HeaderCtrl'

                  })

                  .state('header.company',{

                    url: '/company',

                    templateUrl: 'views/Agencia/Agencias.html',

                    controller: 'CompanyCtrl'

                  })

                  .state('header.client',{

                        url: '/client',

                        templateUrl: 'views/Cliente/Cliente.html',

                        controller: 'ClientCtrl'
                        
                  })

                  .state('header.user',{

                        url: '/user',

                        templateUrl : 'views/Usuario/Usuario.html',

                        controller : 'UserCtrl'
                        
                  })

                  .state('header.role',{

                        url: '/role',

                        templateUrl: 'views/Usuario/Rol.html',

                        controller: 'RoleCtrl'
                  })

                  .state('header.dispositivo', {

                       url: '/dispositivo',

                       templateUrl: 'views/Dispositivos/Dispositivos.html',

                       controller: 'DispositivoCtrl'

                  })

                  .state('header.asignacion', {

                       url: '/asignacion',

                       templateUrl: 'views/Dispositivos/AsignarDispositivo.html',

                       controller: 'AsignarCtrl'
                       
                  })

                  .state('header.marketstall',{

                          url: '/marketstall',

                          templateUrl: 'views/Usuario/Puesto.html',

                          controller: 'MarketStallCtrl'

                  })

                  .state('header.departament',{

                          url: '/departament',

                          templateUrl: 'views/Usuario/Departamento.html',

                          controller: 'DepartamentCtrl'
                  })

                  .state('header.document',{

                    url : '/document',

                    templateUrl : 'views/Automovil/Documentos.html',

                    controller : 'DocumentCtrl'

                  })

                  .state('header.chofer',{

                    url : '/chofer',

                    templateUrl : 'views/Chofer/Chofer.html',

                    controller : 'ChoferCtrl'
                    
                  })

                  .state('header.Inventory',{

                    url : '/Inventory',

                    templateUrl : 'views/Automovil/Inventario.html',

                    controller : 'InventoryCtrl'
                  })

                  .state('header.documentosCliente', {

                    url : '/Documentos clientes',

                    templateUrl : 'views/Documentos/Documentos_Clientes.html',

                    controller : 'DocumentosClientesCtrl'

                  })

                  .state('header.documentosAutomoviles', {

                    url : '/Documentos automoviles',

                    templateUrl : 'views/Documentos/Documentos_Automoviles.html',

                    controller : 'DocCtrl'
                    
                  })

                  .state('header.typeDocument', {

                    url : '/Tipos',

                    templateUrl : 'views/Documentos/TipoDocumento.html',

                    controller : 'typeDocumentCtrl'
                    
                  })

                  .state('header.solicitados',{

                    url: '/Solicitados',

                    templateUrl : 'views/Solicitudes/Solicitados.html',

                    controller:'SolicitadosCtrl'

                  })

                  .state('header.pendiente',{

                    url : '/Pendientes',

                    templateUrl : 'views/Solicitudes/Por_atender.html',

                    controller : 'AtencionCtrl'

                  })

                  .state('header.ruta',{

                    url : '/En_ruta',

                    templateUrl : 'views/Solicitudes/Ruta.html',

                    controller : 'RutaCtrl'
                  })

                  .state('header.AccountState', {

                    url : '/EstadoCuenta',

                    templateUrl : 'views/Cliente/EstadoCuenta.html',

                    controller : 'AccountStatusCtrl'
                    
                  })

                  .state('header.EstadoC',{

                    url : '/Cuenta',

                    templateUrl : 'views/Cliente/CuentaCliente.html',

                    controller : 'EstadoCtrl'
                  })

                  .state('header.NotificationCliente',{

                    url : '/Notificaciones_Clientes',

                    templateUrl : 'views/Notificaciones/NotificacionesClientes.html',

                    controller : 'NotificationClientesCtrl'
                    
                  })

                  .state('header.NotificationChofer',{

                    url : '/Notificaciones_Chofer',

                    templateUrl : 'views/Notificaciones/NotificacionesChofer.html',

                    controller : 'NotificationChoferCtrl'
                    
                  })

                  .state('header.TipoNotificaciones',{

                    url : '/Catalogo_de_notificaciones',

                    templateUrl : 'views/Notificaciones/TipoNotificaciones.html',

                    controller : 'TypeNotificationCtrl'
                    
                  })

                  .state('header.Mensajes',{

                    url : '/Mensajes',

                    templateUrl : 'views/Notificaciones/Mensajes.php',

                    controller : 'MensajeCtrl'

                  })

                  .state('header.Maps',{

                    url : '/Mapa',

                    templateUrl : 'views/Mapa/UbicacionMapa.html',

                    controller : 'UbicacionMapaCtrl'
                    
                  })

                  .state('header.Mantenimiento',{

                    url : '/GraficaMantenimiento',

                    templateUrl : 'views/Mapa/Mantenimiento.html',

                    controller  : 'GraficaCtrl' 
                    
                  })              


                  $urlRouterProvider.otherwise('/log');

          }]);
