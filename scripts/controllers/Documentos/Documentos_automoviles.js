'use strict';


angular.module('abgpassportApp')

       .controller('DocCtrl',['$scope','documentcarServices','inventoryServices','documentServices','typedocumentServices',

       function($scope,documentcarServices,inventoryServices,documentServices,typedocumentServices){

      var count = 0;

       $scope.documento = {

             nIDDocumento     : '0',
             Nombre           : '',
             nIDTipoDocumento : '0',
             Documento        : '',
             nIDAutomovil     : '0'

       };


       $scope.clean = function(){

             $scope.documento = {

                   nIDDocumento     : '0',
                   Nombre           : '',
                   nIDTipoDocumento : '0',
                   Documento        : '',
                   nIDAutomovil     : '0'

             };

             $("#subir_modal").modal('hide');

       };

       $scope.documentoEdit = {

             nIDDocumento     : '0',
             Nombre           : '',
             nIDTipoDocumento : '0',
             Documento        : '',
             nIDAutomovil     : '0'

       };


       $scope.cleanEdit = function(){

             $scope.documentoEdit = {

                   nIDDocumento     : '0',
                   Nombre           : '',
                   nIDTipoDocumento : '0',
                   Documento        : '',
                   nIDAutomovil     : '0'

             };

             $('#update_modal').modal('hide');

       };

       $scope.get = function(){

             //   $scope.getTipoAuto(); 

             $scope.autos = ''     ;

              inventoryServices.get().then(function(resp){

                          if(resp.data == 'null'){

                                 console.log('Not found data');

                          }else{

                                $scope.autos = resp.data;

                          }

              },function(err){

                    console.log('error' + JSON.stringify(err));

              });
       };

       $scope.getTipoAuto = function(id){
             $scope.tipos = '';

              typedocumentServices.getTipoAuto(id).then(function(resp){

                                   if(resp.data == 'null'){

                                          console.log('Not found Data');

                                    }else{

                                          $scope.tipos = resp.data;
              
                                    }
                    
              },function(err){

                         console.log('error' + JSON.stringify(err));

              });

       };

       $scope.obtener = function(id){

             $scope.documento.nIDAutomovil = id;
             $scope.getTipoAuto(id);
       }


       $scope.add = function(documentos){console.log(documentos);

             if(documentos.Nombre.length == 0){

                           swal.fire({

                               title                    : 'Oops!',

                               text                     : 'Nombre se encuentra vacio completalo',

                               imageUrl                 : 'assets/img/advertencia.png',

                               showCloseButton          : true,

                               colorConfirmButton       : '044BFD'

                           });

              }else if(documentos.nIDTipoDocumento == ''){

                                  swal.fire({

                                      title                   : 'Oops!',

                                      text                    : 'Tipo de documentos se encuentra vacio completalo',

                                      imageUrl                : 'assets/img/advertencia.png',

                                      showCloseButton         : true,

                                      colorConfirmButton      : '#044BFD'

                                  });
              
              }else if(documentos.Documento.length == 0){

                                 swal.fire({

                                      title                     : 'Oops!',

                                      text                      : 'Documento se encuentra vacio completalo',

                                      imageUrl                  : 'assets/img/advertencia.png',

                                      showCloseButton           : true,

                                      colorConfirmButton        : '#044BFD'

                                 });

              }else{


                   documentServices.add(documentos).then(function(resp){

                                    if(resp.data == "NOT INSERTED"){

                                           swal.fire({

                                               title                    : 'Oops!',

                                               text                     : 'No se completo el registro, Intétalo de nuevo',

                                               imageUrl                 : 'assets/img/error.png',

                                               showCloseButton          : true,

                                               colorButtonConfirm       : '#044BFD'

                                           });

                                    }else{


                                          $scope.documento.nIDDocumento = resp.data;

                                          $scope.addAutomovilDocumento();

                                          $scope.clean();

                                    }

                    },function(err){

                          console.log('error' + JSON.stringify(err));

                    });
                             
              }

       };


       $scope.addAutomovilDocumento = function(){

             documentcarServices.add($scope.documento).then(function(resp){ console.log(resp.data);

                                if(resp.data == "INSERTED"){

                                       swal.fire({

                                           title                      : 'Registro exitoso!',

                                           imageUrl                   : 'assets/img/completo.png',

                                           showCloseButton            : true,

                                           colorButtonConfirm         : '#044BFD'

                                       });

                                }else{


                                }

             },function(err){

                    console.log('error' + JSON.stringify(err));

             });

       };

        function CargarImagen() {

              var archivoInput = document.getElementById('Documento');

              var archivoRuta  = archivoInput.value;

             

                    if(archivoInput.files && archivoInput.files[0]){

                        var visor = new FileReader();

                        visor.addEventListener("load" , function(e){ 
 
                            $scope.documento.Documento = e.target.result;

                        });

                        visor.readAsDataURL(archivoInput.files[0]);
                    }

              

        }(function(){

              document.getElementById('Documento').addEventListener("change", CargarImagen);
        })();


       $scope.getArchivos = function(id){

             $scope.Titulo = '';
             $scope.documentoAutomoviles = '';

             document.getElementById('VistaImagen').style.display = 'none';

             documentcarServices.get(id).then(function(resp){

                                if(resp.data == 'null'){

                                       console.log('Not found data');

                                       $scope.Titulo = "No tienes documentos guardados";

                                }else{

                                    $scope.documentoAutomoviles = resp.data;

                                }

             }, function(err){

               console.log('error' + JSON.stringify(err));

             });

       };

       $scope.Ver = function(Documento,id){


            count = count + 1; console.log(count);

                  if(count == 1){

                        document.getElementById('doc').src                   = Documento;
                        document.getElementById('VistaImagen').style.display = 'block';
                  }else{
                              document.getElementById('VistaImagen').style.display = 'none';
                              document.getElementById('doc').src                   = '';  
                              count = 0;
                              $scope.getArchivos(id);
                       
                  }

       };

       $scope.show = function(objeto){ console.log(objeto);

              $scope.documentoEdit.nIDDocumento      = objeto.nIDDocumento;
              $scope.documentoEdit.Nombre            = objeto.Nombre;
              $scope.documentoEdit.nIDTipoDocumento  = objeto.nIDTipoDocumento;
              $scope.documentoEdit.Documento         = objeto.Documento;
       };

       function CargarImagenEdit() {

              var archivoInput = document.getElementById('DocumentoEdit');

              var archivoRuta  = archivoInput.value;

             

                    if(archivoInput.files && archivoInput.files[0]){

                        var visor = new FileReader();

                        visor.addEventListener("load" , function(e){ 
 
                            $scope.documentoEdit.Documento = e.target.result;

                        });

                        visor.readAsDataURL(archivoInput.files[0]);
                    }

              

        }(function(){

              document.getElementById('DocumentoEdit').addEventListener("change", CargarImagenEdit);
        })();

       $scope.update = function(objeto){ console.log(objeto);

             documentServices.update(objeto).then(function(resp){ console.log(resp.data);

                             if(resp.data == 'UPDATED'){

                                    swal.fire({

                                        title                    : 'Modificación exitosa!',

                                        imageUrl                 : 'assets/img/completo.png',

                                        showCloseButton          : true,

                                        confirmButtonColor       : '#044BFD'

                                    });

                                    $scope.cleanEdit();


                             }else{


                                  swal.fire({

                                      title                     : 'Oops!',

                                      text                      : 'No se logro el registro, Inténtalo de nuevo',

                                      imageUrl                  : 'assets/img/error.png',

                                      showCloseButton           : true,

                                      confirmButtonColor        : '#044BFD'

                                  });

                             }

             }, function(err){

                        console.log('error' + JSON.stringify(err));

             });

       };

       $scope.destroy = function(id,idA,auto){ console.log(id,idA,auto);

            swal.fire({

                  title                     : 'ADVERTENCIA',

                  text                      : '¿Estás seguro que deseas eliminar el registrio?',

                  imageUrl                  : 'assets/img/advertencia.png',

                  showCancelButton          : true,

                  showCloseButton           : true,

                  cancelButtonColor         : 'rgba(4,75,253,0.5)',

                  confirmButtonColor        : '#044BDF',

                  cancelButtonText          : 'Cancelar',

                  confirmButtonText         : 'Si',

             }).then((result) =>{

                    if(result.value){

                              documentServices.destroy(id).then(function(resp){ console.log(resp.data);

                                              if(resp.data == "DELETED"){

                                                $scope.destroyAutomovil(idA,auto);

                                              }else{

                                                      swal.fire({

                                                          title             : 'Oops!',

                                                          text              : 'No se pudo eliminar el registro inténtalo de nuevo',

                                                          imageUrl          : 'assets/img/error.png',

                                                          showCloseButton   : true,

                                                          confirmButtonColor: '#044BFD'

                                                      });

                                              }

                              },function(err){

                                  console.log('error' + JSON.stringify(err));

                              });

                    }else{

                          swal.fire({

                              title                   : 'Se cancelo la eliminación!',

                              imageUrl                : 'assets/img/error.png',

                              showCloseButton         : true,

                              confirmButtonColor      : '#044BFD'

                          });

                    }


             });

       };


       $scope.destroyAutomovil = function(id,idA){

              documentcarServices.destroy(id).then(function(resp){

                                    if(resp.data == "DELETED"){
                                           swal.fire({

                                                title                 : 'Registro eliminado con exito!',

                                                imageUrl              : 'assets/img/completo.png',

                                                showCloseButton       : true,

                                                confirmButtonColor    : '#044BFD'

                                            });

                                            $scope.getArchivos(idA);

                                    }else{

                                            swal.fire({

                                                title             : 'Oops!',

                                                text              : 'No se pudo eliminar el registro inténtalo de nuevo',

                                                imageUrl          : 'assets/img/error.png',

                                                showCloseButton   : true,

                                                confirmButtonColor: '#044BFD'

                                            });

                                    }
              },function(err){

                    console.log('error' + JSON.stringify(err));

              });

       };



       }]);