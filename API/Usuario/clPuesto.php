<?php
/*
    ** Clase categoria Puesto
    ** Esta clase es la que controlara las categorias de los puestos
*/

/*
    ** Herramienta externa
*/
    include_once "../Utilerias/clHerramientas_v2011.php";

/*
    ** tbl_cat_puesto
*/

/**
 * 
 */


class clPuesto
{

  //Atributos para la conexion a la base de datos

    private $servidor;
    private $usuario;
    private $password;
    private $basededatos;
    private $contarDatos    = 0;

  /*
        ** Constantes generales
    */
    private $ascendente     = "Ascendente";
    private $descendente    = "Descendente";


     //Constantes para la base de datos

    private $nombreTabla    = "tbl_cat_puesto";
    private $nombreVista    = "vw_cat_puesto";
    private $id             = "nIDPuesto";
    private $campoOrdenar   = "nIDPuesto";
    private $ordenamiento   = "Descendente";
    private $limiteInferior = -1;
    private $limiteSuperior = -1;
    public  $last_id        = 0;
    private $dt;
    private $dtGrabar;

    //Constantes de estados

    private $existeError    = FALSE;
    private $existenDatos   = FALSE;
    private $datosConexion  = FALSE;

    //Mensajes

    private $error = "";


    //Constructor

    function __construct()
    {
      $this->Inicializacion();
    }

     /**
        *************** Función de conexión ***************
        * La función realiza la conexión a la base de datos
        * mediante las credenciales previamente establecidas.
        *
        * @param [string] $servidor
        * @param [string] $usuario
        * @param [string] $password
        * @param [string] $basededatos
        * @return void
        */

     public function conexion($servidor,$usuario,$password,$basededatos)
      {

          $this->servidor     = $servidor;
          $this->usuario      = $usuario;
          $this->password     = $password;
          $this->basededatos  = $basededatos;

          $this->datosConexion = TRUE; 
      }

      /**
     **************** Función setInformacion *************
     * La función realiza la asignación de los datos almacenados
     * en  la base de datos a las respectivas variables, los datos
     * que se encuentran como parametro son obligatorios para cada
     * tabla.
     *
     * @param [datetime] $FechaModificacion
     * @param [datetime] $FechaCreacion
     * @param [string] $Observaciones
     * @param [int] $bEstado
     * @param [boolean] $crear
     * @param [boolean] $modificar
     * @param [boolean] $eliminar
     * @return void
    */

    public function setInformacion($nIDPuesto, $Puesto, $Descripcion, $FechaModificacion, $FechaCreacion,$Observaciones, $bEstado, $crear, $modificar, $eliminar)
    {
       $utileria = new clHerramientasv2011();

        $this->dtGrabar[$this->contarDatos]["nIDPuesto"]     = $nIDPuesto;
        $this->dtGrabar[$this->contarDatos]["Puesto"]        = $Puesto;
        $this->dtGrabar[$this->contarDatos]["Descripcion"]   = $Descripcion;

        //Estos datos son obligatorios para todas las tablas

        $fecha = "";

        if( strlen($FechaModificacion) > 0)
        {

          $fecha = $utileria->ConvertirFechaYHora_General($FechaModificacion);
          $this->dtGrabar[$this->contarDatos]["FechaModificacion"] = $FechaModificacion;

        } else {

          $this->dtGrabar[$this->contarDatos]["FechaModificacion"] = "01/01/1980";

        }

        $fecha = "";

        if(strlen($FechaCreacion) > 0){

          $fecha = $utileria->ConvertirFechaYHora_General($FechaCreacion);
          $this->dtGrabar[$this->contarDatos]["FechaCreacion"] = $fecha;

        } else {

            $this->dtGrabar[$this->contarDatos]["FechaCreacion"] = "01/01/1980";
        }

        $this->dtGrabar[$this->contarDatos]["Observaciones"] = $Observaciones;

        if($bEstado >= 0 && $bEstado <=4){

          $this->dtGrabar[$this->contarDatos]["bEstado"]  = $bEstado;
          $this->dtGrabar[$this->contarDatos]["sbEstado"] = $this->vEstado($bEstado, "bEstado");

        } else {

          $this->dtGrabar[$this->contarDatos]["bEstado"]  = 3;
          $this->dtGrabar[$this->contarDatos]["sbEstado"] = "No definido";

        }

        if ($crear == TRUE){

            $this->dtGrabar[$this->contarDatos]["Crear"]    = TRUE;
            $this->dtGrabar[$this->contarDatos]["Cambiar"]  = FALSE;
            $this->dtGrabar[$this->contarDatos]["Eliminar"] = FALSE;
        } else {

            if ($modificar == TRUE){

                $this->dtGrabar[$this->contarDatos]["Crear"]     = FALSE;
                $this->dtGrabar[$this->contarDatos]["Cambiar"]   = TRUE;
                $this->dtGrabar[$this->contarDatos]["Eliminar"]  = FALSE;
            } else {

                if ($eliminar == TRUE){
                    $this->dtGrabar[$this->contarDatos]["Crear"]    = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Cambiar"]  = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Eliminar"] = TRUE;
                } else {

                    $this->dtGrabar[$this->contarDatos]["Crear"]    = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Cambiar"]  = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Eliminar"] = FALSE;
                }
            }
        }

        $this->contarDatos = $this->contarDatos + 1;
        $this->existenDatos = TRUE;

        unset($utileria);

        return TRUE;
    }

    /**
    *********************** Función setInformacionObj ***********
    * Esta función permite darle valor al objeto para despues
    * retornar datos.
    *
    * @param [object] $obj
    * @return void
    */

    public function setInformacionObj($obj)
    {

      $registros  = 0;
      $registros  = $obj->registrosCargados();

      if($registros > 0){

          $i = 0;

          for($i = 0; $i < $registros - 1; $i++){

            $dato = $obj->getInformacionObj();

            $this->setInformacion(

              $dato[$i]["nIDPuesto"],
              $dato[$i]["Puesto"],
              $dato[$i]["Descripcion"],
              $dato[$i]["FechaModificacion"],
              $dato[$i]["FechaCreacion"],
              $dato[$i]["Observaciones"],
              $dato[$i]["bEstado"],
              $dato[$i]["Crear"],
              $dato[$i]["Cambiar"],
              $dato[$i]["Eliminar"]
            );

          }
      }

      return TRUE;
    }

    /**
     ***************** Función getInformacionObj **************
     * Esta función graba los datos traídos de la base de datos
     * para despues agregarlos a una variable u objeto.
     *
     * @return void
    */

    public function getInformacionObj()
    {

        return $this->dtGrabar;

    }

    /**
     *************** Función getInformacion *************
     * Esta función permite devoler el valor de las propiedades
     * que se encuentran en las variables.
     *
     * @param [int] $numero
     * @return void
    */

    public function getInformacion($numero)
    {

        if($this->existenDatos == TRUE){

          if($numero >= 0){

              return $this->dtGrabar[$numero];
          } else {

              return NULL;

          }

        } else {

            return NULL;

        }
    }

     /**
     ************** Función getIndice ***************
     *
     * @param [int] $nID
     * @return void
     */

    public function getIndice($nID)
    {

      if($this->contarDatos > 0){

        $indice = -1;

        for($i = 0; $i < $this->contarDatos - 1; $i++){

            if($this->dtGrabar[$i][$this->id] == $nID){

                $indice = $i;
                break;
            }
        }

        return $indice;
      } else{

          return -1;
      }
    }

    /**
     *************** Función OrdenarTabla **************
     * Ordena la tabla con respecto al campo que se le 
     * pasa por el parametro
     *
     * @param [string] $campos
     * @return void
     */
    public function OrdenarTabla($campos)
    {

      $this->campoOrdenar = $campos;

    }

    /**
     *************** Función formaOrdenamiento **************
     * Ordena la tabla de forma ascendente o descendente según
     * el parametro.
     *
     * @param [type] $forma
     * @return void
     */

    public function formaOrdenamiento($forma)
    {

      $this->ordenamiento = $forma;

    }

    /**
     **************** Función dtBase ******************
     * Graba los registros de la base de datos.
     *
     * @return void
     */

    public function dtBase()
    {

      return $this->dtGrabar;

    }

    /**
     ************** Función errorObjeto ****************
     * La función retorn un error en caso de que este exista.
     *
     * @return void
     */

    public function errorObjeto()
    {

        if($this->existeError == TRUE){

              return $this->error;

        } else {

              return "No existe ningún mensaje de error.";
        }
    }

    /**
     **************** Función registrosCargados ***************
     * Función para retornar el número de registros cargados.
     * 
     * @return void
     */

    public function registrosCargados()
    {

           return $this->contarDatos;

    }

    /**
     **************** Función formaDeOrdenamiento *****************
     * Funcón para retornar la forma de ordenar la tabla.
     *
     * @return void
     */

     public function formaDeOrdenamiento()
     {

            return $this->ordenamiento;

     }

     /**
      ***************** Función errorDeObjeto **************
      * Función para retornar el error de un objeto en caso
      * de que este exista.
      *
      * @return void
      */

    public function errorDeObjeto()
    {

          return $this->existeError;

    }

    /** 
     *************** Función datosCargados ***************
     * 
     * @return void
     */

    public function datosCargados()
    {

          return $this->existenDatos;
    }

    /**
     **************** Función setlimiteInferior **************
     * 
     * @param [int] $limite
     * @return void
     */

      public function setlimiteInferior($limite)
      {

        $this->limiteInferior = $limite;

      }

    /**
     ***************** Función setlimiteSuperior ************
     *
     * @param [int] $limite
     * @return void
     */

       public function setlimiteSuperior($limite)
        {
          $this->limiteSuperior = $limite;
        }


    /**
     ***************** Función Inicializacion **************
     * Esta fución permite inicializar atributos y contenido.
     *
     * @return void
     */

        public function Inicializacion()
        {

              $this->InicializaAtributos();
              $this->InicializacionContenido();

        }

    /**
     ************** Función InicializaAtributos *************
     * Inicializa los atributos.
     * 
     * @return void
     */

        private function InicializaAtributos()
        {

            $this->existeError      = FALSE;
            $this->existenDatos     = FALSE;
            $this->datosConexion    = FALSE;

            $this->error            = "No hay error";
            $this->ordenamiento     = $this->Ascendente;
            $this->contarDatos      = 0;

        }

    /**
     *************** Función InicializacionContenido ************
     * Inicializa el contenido.
     *
     * @return void
     */

        public function InicializacionContenido()
        {

            $this->contarDatos = 0;
            $this->existeError = FALSE;

        }

    /**
     ************** Función existeID **************
     * 
     * @param [int] $nID
     * @return void
     */

        public function existeID($nID)
        {

          //Verifica si existe algun error con la conexión
          if($this->existeError){

              return FALSE;

          }

          if(!$this->datosConexion){

              $this->existeError    = TRUE;
              $this->error          = "Error No tiene los datos para conectarse a la base de
                                       datos [Cargar]";
              return FALSE;
          }

          //Abre la conexión
          $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

          if(mysqli_connect_errno()){

              $this->existeError = TRUE;
              $this->error       = "Error no se puede conectar a la base de datos";
              return FALSE;

          }

          //Construye la consulta

          $txtConsulta = "";
          $txtConsulta = "SELECT * ";
          $txtConsulta = $txtConsulta . "FROM ";
          $txtConsulta = $txtConsulta . $this->nombreTabla;
          $txtConsulta = $txtConsulta . "WHERE";
          $txtConsulta = $txtConsulta . $this->id . "=" . $nID;

          //Ejecuta la consulta
          mysqli_query($mysqli, "SET NAMES UTF8");
          $res = mysqli_query($mysqli, $txtConsulta);

          //Cargar la Información
          if($res){

            if($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){

                return TRUE;

            } else {

                return FALSE;
            }
          } else {

                return FALSE;
          }
        }


        /**
     ************* Función vEstado **************
     * Verifica el estado del registro.
     * 0 = Eliminado; 1 = Activo; 2 = Cancelado;
     * 3 = No definido;
     *
     * @param [string] $estado
     * @param [string] $nombreCampo
     * @return void
     */

        private function vEstado($estado, $nombreCampo)
        {

          $cadena = "";

          switch ($estado) {
            case 0: //Eliminado
              $cadena = "Eliminado";
              break;
            
            case 1: //Activo
              $cadena = "Activo";
              break;

            case 2: //Cancelado
              $cadena = "Cancelado";
              break;

            case 3: //  No definido
              $cadena = "No definido";
              break;
          }

              return $cadena;
        }

    /**
     ****************** Función especialesHTML **************
     * 
     * @param [string] $str
     * @return void
     */

        public function especialesHTML($str)
        {

            $str = mb_convert_encoding($str, 'UTF-8');
            return $str;

        }


     /**
     ************* Función consultar ***************
     * Realiza una consulta simple.
     *
     * @return void
     */

        private function Consultar()
        {

          //Abre la conexión
          $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

          if(mysqli_connect_errno()){

              $this->existeError   = TRUE;
              $this->error         = "Error no se puede conectar a la base de datos";
              return FALSE;

          }

          // Genera la consulta
          $txtConsulta = "";
          $txtConsulta = "SELECT *";
          $txtConsulta = $txtConsulta . "FROM ";
          $txtConsulta = $txtConsulta . $this->nombreTabla;
          $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

          if($this->formaDeOrdenamiento() == $this->Ascendente){

              $txtConsulta = $txtConsulta . " ASC";
          } else {

              $txtConsulta = $txtConsulta . "DESC";
          }

          if($this->limiteInferior >= 0){

              $txtConsulta = $txtConsulta . " LIMIT " . $this->limiteInferior . "," . $this->limiteSuperior;
          } 

          //Ejecuta la consulta
          mysqli_query($mysqli, "SET NAMES UTF8");
          $res = mysqli_query($mysqli, $txtConsulta);

          //Cargar la información
          $contador = 0;
          $this->contarDatos = 0;

          if($res){

              while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){

                $nIDPuesto       = 0;
                $Puesto          = "";
                $Descripcion     = "";

                //campos base
                $FechaModificacion     = "";
                $FechaCreacion         = "";
                $Observaciones         = "";
                $bEstado               = 3;

                //Cargar la información del registro leído
                if($registro['nIDPuesto'] != NULL){

                    $nIDPuesto = $registro['nIDPuesto'];
                }
                if($registro['Puesto'] != NULL){

                    $Puesto = $registro['Puesto'];
                }
                if($registro['Descripcion'] != NULL){

                    $Descripcion = $registro['Descripcion'];
                }

                //Campos base
                if($registro['FechaModificacion'] != NULL){

                    $FechaModificacion = $registro['FechaModificacion'];
                }
                if($registro['FechaCreacion'] != NULL){

                    $FechaCreacion = $registro['FechaCreacion'];
                }
                if($registro['Observaciones'] != NULL){

                    $Observaciones = $registro['Observaciones'];
                }
                if($registro['bEstado'] != NULL){

                    $bEstado = $registro['bEstado'];
                }

                //Cargar la información en el objeto
                $this->setInformacion(
                  $nIDPuesto,
                  $Puesto,
                  $Descripcion,
                  $FechaModificacion,
                  $FechaCreacion,
                  $Observaciones,
                  $bEstado,
                  FALSE,
                  TRUE,
                  FALSE
                );
              }
          } else {

              $this->existenDatos = FALSE;
          }
          mysqli_close($mysqli);
          return TRUE;
        }

    /**
     ************* Función consultarCondicion *************
     *Realiza una consulta con condicion.
     *
     * @param [string] $condicion
     * @return void
     */

        private function consultarCondicion($condicion)
        {

            try{
              //Abre la conexión
              $mysqli = mysqli_connect($this->servidor, $this->usuario,$this->password, $this->basededatos);

              if(mysqli_connect_errno()){

                $this->existeError    = TRUE;
                $this->error          = "Error No se puede conectar a la base de datos";
                return FALSE;
              }

              $txtConsulta = "";
              $txtConsulta = "SELECT *";
              $txtConsulta = $txtConsulta . " FROM ";
              $txtConsulta = $txtConsulta . $this->nombreTabla;
              $txtConsulta = $txtConsulta . " WHERE ";
              $txtConsulta = $txtConsulta . $condicion;
              $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

              if($this->limiteInferior >= 0){

                $txtConsulta = $txtConsulta . " LIMIT " . $this->limiteInferior . "," . $this->limiteSuperior;
              }

              mysqli_query($mysqli, "SET NAMES UTF8");
              $res = mysqli_query($mysqli,$txtConsulta);

              //Cargar la información

              $contador            = 0;
              $this->contarDatos   = 0;

              if($res){

                  while ($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
                    $nIDPuesto                = 0;
                    $Puesto                   = "";
                    $Descripcion              = "";

                    //campos base

                    $FechaModificacion       = "";
                    $FechaCreacion           = "";
                    $Observaciones           = "";
                    $bEstado                 = 3;


                    // Carga la info del registro leido
                    if($registro['nIDPuesto'] != NULL){
                        $nIDPuesto = $registro['nIDPuesto'];
                    }
                    if($registro['Puesto'] != NULL){
                        $Puesto = $registro['Puesto'];
                    }
                    if($registro['Descripcion'] != NULL){
                        $Descripcion = $registro['Descripcion'];
                    }
                   
                   // Campos base
                    if($registro['FechaModificacion']!=NULL){
                        $FechaModificacion=$registro['FechaModificacion'];
                    }

                    if($registro['FechaCreacion']!=NULL){
                        $FechaCreacion=$registro['FechaCreacion'];
                    }

                    if($registro['Observaciones']!=NULL){
                        $Observaciones=$registro['Observaciones'];
                    }

                    if($registro['bEstado']!=NULL){
                        $bEstado=$registro['bEstado'];
                    }

                    //Carga la información en el objeto

                    $this->setInformacion(
                          $nIDPuesto,
                          $Puesto,
                          $Descripcion,
                          $FechaModificacion,
                          $FechaCreacion,
                          $Observaciones,
                          $bEstado,
                          FALSE,
                          TRUE,
                          FALSE
                    );
                  }
              } else {

                  $this->existenDatos = FALSE;
              }

              mysqli_close($mysqli);
              return TRUE;
            }catch (Exception $e){
              echo 'Se capturo la siguiente excepcion: ', $e->getMessage(), "\n";
            }
        }

     /**
     ************** Función registrosTabla ***************
     *
     * @return void
     */

        private function registrosTabla()
        {

            //Abre la conexión
            $mysqli = mysql_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

            if(mysqli_connect_errno()){

              $this->existeError    = TRUE;
              $this->error          = "Error no se puede conectar a la base de datos";
              return FALSE;
            }

            //Construye la consulta
            $txtConsulta = "";
            $txtConsulta = "SELECT *";
            $txtConsulta = $txtConsulta . " FROM ";
            $txtConsulta = $txtConsulta . $this->nombreVista;
            $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

            if($this->formaDeOrdenamiento() == $this->Ascendente){

                $txtConsulta = $txtConsulta . " ASC";

            } else {

                $txtConsulta = $txtConsulta . " DESC";

            }

            //Ejecuta la consulta

            mysqli_query($mysqli, "SET NAMES UTF8");
            $res = mysqli_query($mysqli,$txtConsulta);

            //Carga la información
            $contador = 0;

            //Cuenta los registros
            if($res){
              while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $contador = $contador + 1;
              }
            }

            mysqli_close($mysqli);
            return $contador;
        }


        /**
     ************** Función contarRegistros *************
     *
     * @param [string] $condicion
     * @return void
     */

        public function contarRegistros($condicion)
        {

            $contador = 0;

            //Abre la conexión

            $mysqli = mysqli_connect($this->servidor, $this->usuario, $this->password, $this->basededatos);

            if(mysqli_connect_errno()){

              $this->existeError = TRUE;
              $this->error       = "Error no se puede conectar a la base de datos";
              return 0;
            }

            //Construye la consulta
            $txtConsulta = "";
            $txtConsulta = "SELECT count(nIDPuesto) as Total";
            $txtConsulta = $txtConsulta . " FROM ";
            $txtConsulta = $txtConsulta . $this->nombreVista;
            $txtConsulta = $txtConsulta . " WHERE ";
            $txtConsulta = $txtConsulta . $condicion;

            //Ejecuta la consulta
            mysqli_query($mysqli, "SET NAMES UTF8");
            $res = mysqli_query($mysqli,$txtConsulta);

            //Cargar la información
            //$this->contarDatos = 0;

            if($res){
                if($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                    if($registro['Total'] != NULL){
                          $contador = $registro['Total'];
                    }
                }
            }

            mysqli_close($mysqli);
            return $contador;
        }

    /**
     ***************** Función contarRegistrosCondicion ************
     *
     * @param [string] $condicion
     * @return void 
     */

        private function contarRegistrosCondicion($condicion)
        {

              //Abre la conexión

            $mysqli = mysqli_connect($this->servidor, $this->usuario, $this->password, $this->basededatos);

            if(mysqli_connect_errno()){

                $this->existeError  = TRUE;
                $this->error        = "Error no se puede conectar a la base de datos";
                return FALSE;
            }

            //Construye la consulta
            $txtConsulta = "";
            $txtConsulta = "SELECT *";
            $txtConsulta = $txtConsulta . " FROM ";
            $txtConsulta = $txtConsulta . $this->nombreVista;
            $txtConsulta = $txtConsulta . " WHERE ";
            $txtConsulta = $txtConsulta . $conidicion;
            $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

            if($this->formaDeOrdenamiento() == $this->Ascendente){

                  $txtConsulta = $txtConsulta . " ASC";

            } else {

                $txtConsulta = $txtConsulta . " DESC";
            }

            //Ejectuta la consulta
            mysqli_query($mysqli, "SET NAMES UTF8");
            $res = mysqli_query($mysqli, $txtConsulta);

            //Carga la información
            $contador = 0;

            if($res){

              while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                  $contador = $contador + 1;
              }
            }

            mysql_close($mysqli);
            return $contador;
        }


        /**
     ****************** Función ConsultarEstado **********
     *
     * @param [int] $nID
     * @return void
     */

        private function ConsultarEstado($nID)
        {

            //Abre la conexión
            $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password, $this->basededatos);

            if(mysqli_connect_errno()){
              $this->existeError  = TRUE;
              $this->error        = "Error no se puede conectar a la base de datos";
              return 3;
            }

            $bEstado = 3;

            //GENERAR CONSULTA

            $txtConsulta = "";
            $txtConsulta = "SELECT *";
            $txtConsulta = $txtConsulta . " FROM ";
            $txtConsulta = $txtConsulta . $this->nombreVista;
            $txtConsulta = $txtConsulta . " WHERE ";
            $txtConsulta = $txtConsulta . $this->id . "=" . $nID;
            $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

            if($this->formaDeOrdenamiento() == $this->Ascendente){

                $txtConsulta = $txtConsulta . " ASC";
            } else {

                $txtConsulta = $txtConsulta . " DESC";
            }

            //Ejecuta la consulta
            mysqli_query($mysqli,"SET NAMES UTF8");
            $res = mysqli_query($mysqli,$txtConsulta);

            //Cargar la información
            if($res){
              while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                  if($registro['bEstado'] != NULL){
                      $bEstado = $registro['bEstado'];
                      break;
                  }
              }

              mysqli_close($mysqli);
              return $bEstado;
            }else {
              mysqli_close($mysqli);
              return 3;
            }
        }

    /**
     ***************** Función consultarID ************
     *
     * @param [int] $usuario
     * @return void
     */

        private function consultarID($usuario)
        {
              //Abre la conexión
          $mysqli = mysqli_connect($this->servidor, $this->usuario, $this->password, $this->basededatos);

          if(mysqli_connect_errno()){

              $this->existeError   = TRUE;
              $this->error         = "Error no se puede conectar a la base de datos";
              return 3;
          }

          $txtConsulta = "";
          $txtConsulta = "SELECT *";
          $txtConsulta = $txtConsulta . " FROM ";
          $txtConsulta = $txtConsulta . $this->nombreVista;
          $txtConsulta = $txtConsulta . " WHERE ";
          $txtConsulta = $txtConsulta . "nIDPuesto='" . $id . "'";

          //EJECUTA LA CONSULTA
          mysqli_query($mysqli, "SET NAMES UTF8");
          $res = mysqli_query($mysqli, $txtConsulta);
          $nID = 3;

          //CARGAR LA INFORMACIÓN
          if($res){
            while($registro = mysqli_fetch_array($res ,MYSQLI_ASSOC)){
              $nID = 3;

              if($registro['nIDPuesto'] != NULL){
                  $nID = $registro['nIDPuesto'];
                  break;
              }
            }

            mysqli_close($mysqli);
            return $nID;
          } else {
            mysqli_close($mysqli);
            return 3;
          }
        }


    /**
     ************* Función borrar *************
     *
     * @return void
     */

        private function borrar()
        {

          //Abre la conexión
          $mysqli = mysqli_connect($this->servidor, $this->usuario, $this->password, $this->basededatos);

          if(mysqli_connect_errno()){
            $this->existeError   = TRUE;
            $this->error         = "Error no se puede conectar a la base de datos";
            return 3;
          }

          //Declara variables
          $txtInsercion   = "";
          $fechalocal     = "";
          $UtileriasDatos = new clHerramientasv2011();

          $txtInsercion = "DELETE FROM " . $this->nombreTabla;
          $res = mysqli_query($mysqli, $txtInsercion);

          if($res == FALSE){
              mysqli_close($mysqli);
              return FALSE;
          }

          mysqli_close($mysqli);
          return TRUE;
        }

        /**
     ************  Función borrarID ***********
     *
     * @param [int] $nID
     * @return void
     */

        private function borrarID($nID)
        {

           //Abre la conexión
          $mysqli = mysqli_connect($this->servidor, $this->usuario, $this->password, $this->basededatos);

          if(mysqli_connect_errno()){
            $this->existeError   = TRUE;
            $this->error         = "Error  no se puede conectar a la base de datos";
            return 3;
          }

          //Declara variables

          $txtInsercion   = "";
          $fechalocal     = "";
          $UtileriasDatos = new clHerramientasv2011();

          $txtInsercion = "DELETE FROM " . $this->nombreTabla . " WHERE " . $this->id . "=" . $nID;
          $res = mysqli_query($mysqli, $txtInsercion);

          if($res == FALSE){
              mysqli_close($mysqli);
              return FALSE;
          }

            mysqli_close($mysqli);
            return TRUE;

        }


        /**
     *************** Función borrarCondicion ***************
     *
     * @param [tstring] $condicion
     * @return void
     */

    private function borrarCondicion($condicion)
    {

      //Abre la conexión

      $mysqli = mysqli_connect($this->servidor, $this->usuario, $this->password, $this->basededatos);

      if(mysqli_connect_errno()){
        $this->existeError = TRUE;
        $this->error       = "Error no se puede conectar a la base de datos";
        return 3;
      }

      //Declara variables
      $txtInsercion   = "";
      $fechalocal     = "";
      $UtileriasDatos = new clHerramientasv2011();

      $txtInsercion = "DELETE FROM " . $this->nombreTabla . " WHERE " . $condicion;
      $res = mysqli_query($mysqli, $txtInsercion);
      if($res == FALSE){
          mysqli_close($mysqli);
          return FALSE;
      }

      mysqli_close($mysqli);
      return TRUE;
    }


    /**
     *************** Función borrarTemporal ***************
     *
     * @param [int] $nID
     * @param [string] $Observaciones
     * @return void
     */

    private function borrarTemporal($nID, $Observaciones)
    {

      //Abre la conexión
      $mysqli = mysqli_connect($this->servidor, $this->usuario,$this->password, $this->basededatos);

      if(mysqli_connect_errno()){
        $this->existeError  = TRUE;
        $this->error        = "Error no se puede conectar a la base de datos";
        return 3;
      }

      //Declara variables
      $txtInsercion    = "";
      $fechalocal      = "";
      $UtileriasDatos  = new clHerramientasv2011();

      $txtInsercion = "UPDATE " . $this->nombreTabla;
      $txtInsercion = $txtInsercion . " SET " ;
      $txtInsercion = $txtInsercion . "bEstado = 0" . ",";

      $fechalocal = $UtileriasDatos->getFechaYHoraActual_General();
      $fechalocal = $UtileriasDatos -> ConvertirFechaYHora($fechalocal);
      $txtInsercion = $txtInsercion . "FechaModificacion = '" . $fechalocal . "',";

      $txtInsercion = $txtInsercion . " Observaciones = '" . $Observaciones . "'";
      $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

      $res = mysqli_query($mysqli, $txtInsercion);
      if($res == FALSE){
        mysqli_close($mysqli);
        return FALSE;
      }

      mysqli_close($mysqli);
      return TRUE;

    }

    /**
     *************** Función actualizarEstado ***************
     *
     * @param [int] $nID
     * @param [int] $bEstado
     * @param [string] $Observaciones
     * @return void
     */

    private function actualizarEstado($nID, $bEstado, $Observaciones)
    {
        //Abre la conexión
      $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

      if(mysqli_connect_errno()){

        $this->existeError  = TRUE;
        $this->error        = "Error no se puede conectar a la base de datos";
        return 3;
      }

      //Declara variables
      $txtInsercion   = "";
      $fechalocal     = "";
      $UtileriasDatos = new clHerramientasv2011();

      $txtInsercion = "UPDATE " . $this->nombreTabla;
      $txtInsercion = $txtInsercion . " SET ";
      $txtInsercion = $txtInsercion . " bEstado =" . $bEstado . ",";

      $fechalocal = $UtileriasDatos->getFechaYHoraActual_General();
      $fechalocal = $UtileriasDatos->ConvertirFechaYHora($fechalocal);
      $txtInsercion =$txtInsercion . " FechaModificacion ='". $fechalocal . "',";

      $txtInsercion = $txtInsercion . " Observaciones='". $Observaciones . "'";
      $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

      $res = mysqli_query($mysqli, $txtInsercion);
      if($res == FALSE){
          mysqli_close($mysqli);
          return FALSE;
      }

      mysqli_close($mysqli);
      return TRUE;
    }


     /**
     *************** Función actualizarPassword ***************
     *
     * @param [int] $nID
     * @param [string] $password
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarPassword($nID, $password, $observaciones)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " Password='" . $this->especialesHTML($password) . "',";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res=mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }
        mysqli_close($mysqli);
        return TRUE;
    }
    
    /**
     *************** Función ActualizarNombre ***************
     * 
     * @param [int] $nID
     * @param [string] $nombre
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarNombre($nID, $nombre, $observaciones)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " Nombre=" . $this->especialesHTML($nombre) . ",";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarVIP ***************
     *
     * @param [int] $nIDPuesto
     * @param [string] $Puesto
     * @param [string] $Descripcion
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarVIP($nIDPuesto, $Puesto, $Descripcion, $observaciones)
    {

        // Abre la conexón
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion = "";
        $fechalocal = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " nIDPuesto='" . $this->especialesHTML($nIDPuesto) . "',";
        $txtInsercion = $txtInsercion . " Puesto='" . $this->especialesHTML($Puesto) . "',";
        $txtInsercion = $txtInsercion . " Descripcion='" . $this->especialesHTML($Descripcion) . "',";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nIDPuesto;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarNivel ***************
     *
     * @param [int] $nID
     * @param [int] $nIDNivel
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarNivel($nID, $nIDNivel, $observaciones)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion = "";
        $fechalocal = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " nIDNivel=" . $nIDNivel . ",";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarCodigoUnico ***************
     *
     * @param [int] $nID
     * @param [int] $codigoUnico
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarCodigoUnico($nID, $codigoUnico, $observaciones)
    {
        
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion = "";
        $fechalocal = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " CodigoUnico='" . $codigoUnico . "',";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarTerminos ***************
     *
     * @param [int] $nID
     * @param [string] $aceptoTerminos
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarTerminos($nID, $aceptoTerminos, $observaciones)
    {
        
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion = "";
        $fechalocal = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " AceptoTerminos='" . $aceptoTerminos . "',";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     ************ Funión grabar ************
     *
     * @param [array] $tabla
     * @param [int] $nIDPuesto
     * @param [string] $Puesto
     * @param [string] $Descripcion
     * @return void
     */

   private function grabar($tabla)
   {

        //Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){

          $this->existeError = TRUE;
          $this->error       = "Error no se puede conectar a la base de datos";
          return 3;
        }

        //Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        for ($i = 0; $i<$this->contarDatos; $i++){
            if($tabla[$i]["Crear"] == TRUE){

                  $txtInsercion = "INSERT INTO " . $this->nombreTabla;
                  $txtInsercion = $txtInsercion . "(";

                  $txtInsercion = $txtInsercion . "nIDPuesto,";
                  $txtInsercion = $txtInsercion . "Puesto,";
                  $txtInsercion = $txtInsercion . "Descripcion,";
                  $txtInsercion = $txtInsercion . "FechaModificacion,";
                  $txtInsercion = $txtInsercion . "FechaCreacion,";
                  $txtInsercion = $txtInsercion . "Observaciones,";
                  $txtInsercion = $txtInsercion . "bEstado";

                  $txtInsercion = $txtInsercion . ")";
                  $txtInsercion = $txtInsercion . "VALUES";
                  $txtInsercion = $txtInsercion . "(";

                  $txtInsercion = $txtInsercion . $tabla[$i]["nIDPuesto"] . ",";
                  $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Puesto"] . "',";
                  $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Descripcion"] . "',";

                  //Campos bases
                  $fechalocal = $UtileriasDatos->getFechaYHoraActual_General();
                  $fechalocal = $UtileriasDatos->ConvertirFechaYHora($fechalocal);
                  $txtInsercion = $txtInsercion . "'" . $fechalocal . "',";
                  $txtInsercion = $txtInsercion . "'" . $fechalocal . "',";

                  $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Observaciones"] . "',";

                  $txtInsercion = $txtInsercion . $tabla[$i]["bEstado"];

                  $txtInsercion = $txtInsercion . ")";

            } else {

                if($tabla[$i]["Cambiar"] == TRUE){

                    $txtInsercion = "UPDATE " . $this->nombreTabla;
                    $txtInsercion = $txtInsercion . " SET ";

                    $txtInsercion = $txtInsercion . "Puesto = ' " . $tabla[$i]["Puesto"] . "',";
                    $txtInsercion = $txtInsercion . "Descripcion = '" . $tabla[$i]["Descripcion"] . "',";

                    //Bases
                    $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
                    $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
                    $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

                    $txtInsercion = $txtInsercion . " Observaciones='". $tabla[$i]["Observaciones"] . "'";
                    $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$tabla[$i]["nIDPuesto"];

                    //echo $txtInsercion;
                } else {

                  if($tabla[$i]["Eliminar"]==TRUE){
                        $txtInsercion = "DELETE FROM " . $this->nombreTabla . " WHERE " . $this->id . "=" .$tabla[$i]["nIDPuesto"];

                    }
                }
            }

            mysqli_query($mysqli, "SET NAMER UTF8");
            $res = mysqli_query($mysqli,$txtInsercion);
            $this->last_id = mysqli_insert_id($mysqli);
            if($res == FALSE){

                mysqli_close($mysqli);
                return FALSE;
            }
        }

        mysqli_close($mysqli);
        return TRUE;
   }
   


    /**
     *************** Función limpiarTabla ***************
     *
     * @return void
     */

   public function limpiarTabla()
   {

      if($this->existeError){
        return FALSE;
      }

      if(!$this->datosConexion){
          $this->existeError   = TRUE;
          $this->error         = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
          return FALSE;
      }

      if($this->borrar() == TRUE){

        return TRUE;
      } else {
        return FALSE;
      }
   }

   /**
     *************** Función leer ***************
     *
     * @return void
     */
    public function leer()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if($this->Consultar() == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

   /**
     *************** Función leerCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */

   public function leerCondicion($condicion)
   {
      if($this->existeError){
        return FALSE;
      }

      if(!$this->datosConexion){
          $this->existeError  = TRUE;
          $this->error        = "Error no tiene los datos para conectarse a la base de datos[Cargar]";
          return FALSE;
      }

      if(strlen($condicion) <= 0){
         $this->existeError  = TRUE;
         $this->error        = "Error no tiene Condición";
         return FALSE;
      }

      if($this->consultarCondicion($condicion) == TRUE){
            return TRUE;

      } else {

            return FALSE;
      }
   }

    /**
     *************** Función ocultar ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @return void
     */
    public function ocultar($nID, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->borrarTemporal($nID,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función totalRegistros ***************
     *
     * @return void
     */
       public function totalRegistros()
       {
          if($this->existeError){
              return FALSE;
          }

          if(!$this->datosConexion){
              $this->existeError = TRUE;
              $this->error       = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
              return FALSE;
          }

          return $this->registrosTabla();
       }

    /**
     *************** Función numeroTotalRegistrosCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */

         public function numeroTotalRegistrosCondicion($condicion)
         {
            if($this->existeError){
                return FALSE;
            }

            if(!$this->datosConexion){
                $this->existeError  = TRUE;
                $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
                return FALSE;
            }

            return $this->contarRegistrosCondicion($condicion);
         }

      /**
     *************** Función cambiarPassword ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @param [string] $password
     * @return void
     */
    public function cambiarPassword($nID, $observaciones, $password)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarPassword($nID,$password,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarNombre ***************
     *
     * @param [int] $nID
     * @param [string] $nombre
     * @param [string] $observaciones
     * @return void
     */
    public function cambiarNombre($nID, $nombre, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarNombre($nID,$nombre,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarVIP ***************
     *
     * @param [int] $nIDPuesto
     * @param [string] $Puesto
     * @param [string] $Descripcion
     * @param [string] $observaciones
     * @return void
     */
    public function cambiarVIP($nIDPuesto, $Puesto, $Descripcion, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarVIP($nIDPuesto,$Puesto, $Descripcion, $observaciones)==TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarNivel ***************
     *
     * @param [type] $nID
     * @param [type] $nIDNivel
     * @param [type] $observaciones
     * @return void
     */
    public function cambiarNivel($nID, $nIDNivel, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarNivel($nID,$nIDNivel, $observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarCodigoUnico ***************
     *
     * @param [int] $nID
     * @param [int] $codigoUnico
     * @param [string] $observaciones
     * @return void
     */
    public function cambiarCodigoUnico($nID, $codigoUnico, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarCodigoUnico($nID,$codigoUnico, $observaciones)==TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    
    /**
     *************** Función getEstado ***************
     *
     * @param [int] $nID
     * @return void
     */
    public function getEstado($nID)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->ConsultarEstado($nID);
    }

    /**
     *************** Función getID ***************
     *
     * @param [int] $usuario
     * @return void
     */
    public function getID($usuario)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->consultarID($usuario);
    }


      /**
     *************** Función ejecutar ***************
     *
     * @return void
     */

         public function ejecutar(){

            if($this->existeError){
                return FALSE;
            }

            if(!$this->datosConexion){
                $this->existeError  = TRUE;
                $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
                return FALSE;
            }

            if($this->contarDatos<=0){
                  return FALSE;
              }

            if($this->grabar($this->dtGrabar) == TRUE){
                  return TRUE;
              } else {
                  return FALSE;
              }
         }
}
?>