<?php
  
  error_reporting(E_ALL ^ E_NOTICE);
  ini_set('display_errors','1');

  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Request-Methods');

  include_once '../Configuraciones/Conexion.php';
  include_once '../Notificaciones/clNotificacionesChofer.php';
  include_once '../Utilerias/clHerramientas_v2011.php';

  $method = $_SERVER['REQUEST_METHOD'];

  $UtileriasDatos    = new clHerramientasv2011();
  $fechaLocal        = $UtileriasDatos->getFechaYHoraActual_General();
  $fechaLocal        = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

  $notification = new clNotificacionesChofer();

  $notification->conexion($SERVER,$USER,$PASSWORD,$BD);

  switch ($method) {
    
    case 'GET':

          $action = $_GET['accion'];

          if($action == 'consultar'){

                
                if($notification->consultarCondicion(" tbl_cat_notificaciones_chofer.bEstado = 1 ")){

                echo json_encode($notification->consultarCondicion(" tbl_cat_notificaciones_chofer.bEstado = 1 "));

                }else{

                  echo "null";
                }
                break;
          }

    case 'POST' : 

          $action = $_POST['accion'];

          if($action == 'agregar'){

                  $object = $_POST['objeto'];

                  $data   = json_decode($object);

                  $notification->setInformacion(

                              $data->nIDNotificacionesChofer,
                              $data->nIDChofer,
                              '',
                              $data->nIDTipoNotificacion,
                              '',
                              $data->Encabezado,
                              $data->Body,
                              $data->Permiso,
                              '',
                              '',
                              'Nueva Notificación - ' . $fechaLocal,
                              '1',
                              TRUE,
                              FALSE,
                              FALSE

                  );

                  if($notification->ejecutar('tbl_cat_notificaciones_chofer')){
                                $notification->sendPushNotification($notification->getToken($data->nIDChofer), $data->Encabezado, $data->Body, ['data' => 'valor']);
                                echo "INSERTED";

                  }else{

                                echo "NOT INSERTED";

                  }
          }
      
  }
?>