<?php
/*
------ LISTA DE VEHICULOS DISPONIBLES PARA SOLICITUD
*/
header('Content-Type: application/json');
date_default_timezone_set('America/Mexico_City');
$method = $_SERVER["REQUEST_METHOD"];
$method = 'POST';
include 'conexion.php';
switch ($method) {
    case 'POST':
        $request = json_decode(file_get_contents('php://input'), true);
        // $request['kndUser'] = 0;
        // $request['usrId'] = 3;
        if (is_null($request['usrId'])) {
            $request['resp'] = 'Los Datos enviados están Incompletos||' . $request . " || " . file_get_contents('php://input');
            $request['status'] = false;
            return json_encode($request);
        } else {
            $resp = getListVehicles();
            // var_dump($resp);
            if ($resp[0]['status']) {
                
                echo json_encode($resp);
                return json_encode($resp);
            } else {
                $request['resp'] = 'No hay notificaciones';
                $request['status'] = false;
                return json_encode($request);
            }
        }
    break;

    default:
        $request['resp'] = 'No se puede procesar la informacion';
        $request['status'] = false;
        return json_encode($request);
}


function getListVehicles(){
    $pdo = Conexion();
    $select = "SELECT * FROM tbl_automoviles WHERE bEstado = 1 AND Estatus != 'OCUPADO'";
    $ejecutar = $pdo->prepare($select);
    $ejecutar->execute();
    $result = $ejecutar->fetchAll(PDO::FETCH_ASSOC);
    // var_dump($result);
    $arr = [];
    if ($result) {
        foreach ($result as $row){
            $row['status'] = true;
            array_push($arr, $row);
        }
        return $arr;
    } else {
        $arr[0]['status'] = false;
        return $arr;
    }
}




?>