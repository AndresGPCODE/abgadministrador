'use strict';

angular.module('abgpassportApp')
	   
	   .service('notificationChoferServices', ['$http','configService', function($http,configService){

	   		return {

        			get : function(){

        				return $http.get(configService.apiUrl + 'Notificaciones_Chofer.php?accion=consultar',

        					{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

        				);

        			},

        			add : function(objeto){

        				return $http.post(configService.apiUrl + 'Notificaciones_Chofer.php','accion=agregar&objeto=' + JSON.stringify(objeto),

        					{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

        				);

        			},

        			update : function(objeto){

        				return $http.post(configService.apiUrl + 'Notificaciones_Chofer.php','accion=modificar&objeto=' + JSON.stringify(objeto),

        					{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

        				);

        			},

        			destroy : function(id){

        				return $http.post(configService.apiUrl + 'Notificaciones_Chofer.php','accion=eliminar&id=' + JSON.stringify(id),

        					{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

        				);

        			}

        	};

	   }]);	