<?php

	error_reporting(E_ALL ^ E_NOTICE);
    ini_set('display_errors','1');

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Request-Methods');

      include_once '../Configuraciones/Conexion.php';
      include_once '../Dispositivos/clDispositivo.php';
      include_once '../Utilerias/clHerramientas_v2011.php';

      
      $method = $_SERVER['REQUEST_METHOD'];

      $UtileriasDatos   = new clHerramientasv2011();
      $fechaLocal       = $UtileriasDatos->getFechaYHoraActual_General();
      $fechaLocal       = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
      
      $dispositivo = new clDispositivo();

      
      $dispositivo->conexion($SERVER,$USER,$PASSWORD,$BD);

      switch($method){

      		case 'GET' :

      			  $action = $_GET['accion'];

      			  if($action == 'buscar'){

      			  		$dispositivo->leerCondicion(" tbl_dispositivos.bEstado = 1 ");

      			  		$registro = $dispositivo->dtBase();

      			  		echo json_encode($registro);

      			  		break;

      			  }else if($action == 'buscarDispositivo'){

                                    $dispositivo->leerCondicion(" tbl_dispositivos.bEstado = 1 and tbl_dispositivos.nIDDispositivo != (SELECT nIDDispositivo from tbl_autos_dispositivos where bEstado = 1)");

                                    $registro = $dispositivo->dtBase();

                                    echo json_encode($registro);

                                    break;

                          }

      	    case 'POST' : 

      	    	 $action = $_POST['accion'];

      	    	 if($action == 'agregar'){

      	    	 		$object = $_POST['objeto'];

      	    	 		$data = json_decode($object);

      	    	 		$dispositivo->setInformacion(
      	    	 			$data->nIDDispositivo,
      	    	 			$data->Nombre,
      	    	 			$data->Tipo,
      	    	 			$data->Directo,
      	    	 			'',
      	    	 			'',
      	    	 			'Se ha agregado un nuevo dispositivo - ' . $fechaLocal,
      	    	 			'1',
      	    	 			TRUE,
      	    	 			FALSE,
      	    	 			FALSE
      	    	 		
      	    	 		);

      	    	 		if($dispositivo->ejecutar('tbl_dispositivos')){

      	    	 				echo "INSERTED";

      	    	 		}else{

      	    	 			    echo "NOT INSERTED";

      	    	 		}
      	    	 
      	    	 }else if($action == 'modificar'){

      	    	 		$object = $_POST['objeto'];

      	    	 		$data = json_decode($object);

      	    	 		$dispositivo->setInformacion(
      	    	 			$data->nIDDispositivo,
      	    	 			$data->Nombre,
      	    	 			$data->Tipo,
      	    	 			$data->Directo,
      	    	 			'',
      	    	 			'',
      	    	 			'Se ha modificado el dispositivo - ' . $fechaLocal,
      	    	 			'1',
      	    	 			FALSE,
      	    	 			TRUE,
      	    	 			FALSE
      	    	 		);

      	    	 		if($dispositivo->ejecutar('tbl_dispositivos')){

      	    	 				echo "UPDATED";

      	    	 		}else{

      	    	 				echo "NOT UPDATED";
      	    	 		}
      	    	 
      	    	 }else if($action == 'eliminar'){

      	    	 		$id = $_POST['id'];

      	    	 		$observacion = 'El dispositivo ha sido eliminado temporalmente - ' . $fechaLocal;

      	    	 		if($dispositivo->ocultar($id,$observacion)){

      	    	 				echo "DELETED";

      	    	 		}else{

      	    	 				echo "NOT DELETED";

      	    	 		}
      	    	 
      	    	 }
      }

?>