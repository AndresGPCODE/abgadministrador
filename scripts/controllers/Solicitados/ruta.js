'use strict';

angular.module('abgpassportApp')
	   
	   .controller('RutaCtrl',['$scope','solicitudesServices',

	   	function($scope,solicitudesServices){

	   		$scope.get = function(){

	   			  $scope.Titulo = '';
	   			  $scope.solicitudes = '';
	   			  
	   			  solicitudesServices.getSolicitudes().then(function(resp){

	   			  					 if(resp.data == 'null'){

	   			  					 		console.log("Not Found Data");
	   			  					 		$scope.Titulo = "No tienes solicitudes";

	   			  					 }else{

	   			  					 		$scope.solicitudes = resp.data;

	   			  					 }

	   			  },function(err){

	   			  		console.log('error' + JSON.stringify(err));

	   			  });

	   		};

	   		$scope.imagenes = function(id){ 
	   			
          $scope.Imgs = ''; 

	   			solicitudesServices.getImagenes(id).then(function(resp){
	   			console.log(resp.data[0]['Imagenes']);
          	if(resp.data[0]['Imagenes'] == 'null'){
	   			
          		console.log('Not Found data');
  
	   				}else{
	   			
          		$scope.Imgs = resp.data[0]['Imagenes'];
	   			
          	
	   			
          	}
	   			
          });
	   		
        };

	   	}]);