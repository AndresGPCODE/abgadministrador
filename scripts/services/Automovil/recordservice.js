'use strict';

angular.module('abgpassportApp')

       .service('recordsServices',['$http','configService',function($http,configService){

       return{

              get : function(id){

                  return $http.post(configService.apiUrl + 'FichaMantenimiento.php','accion=consultar&id=' + JSON.stringify(id),

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                  );

              },

              add : function(ficha){

                  return $http.post(configService.apiUrl + 'FichaMantenimiento.php','accion=agregar&ficha=' + JSON.stringify(ficha),

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                  );

              },

              update : function(ficha){

                  return $http.post(configService.apiUrl + 'FichaMantenimiento.php','accion=modificar&ficha=' + JSON.stringify(ficha),

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                  );

              },

              destroy : function(id){

                return $http.post(configService.apiUrl + 'FichaMantenimiento.php','accion=eliminar&id=' + JSON.stringify(id),

                  { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                );
              
              }
       }

       }]);