<?php
    
  error_reporting(E_ALL ^ E_NOTICE);
  ini_set('display_errors','1');

  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Request-Methods');

  include_once '../Configuraciones/Conexion.php';
  include_once '../Documento/clDocumentos.php';
  include_once '../Utilerias/clHerramientas_v2011.php';

  $method = $_SERVER['REQUEST_METHOD'];

  $UtileriasDatos    = new clHerramientasv2011();
  $fechaLocal        = $UtileriasDatos->getFechaYHoraActual_General();
  $fechaLocal        = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

  $departament = new clDocumentos();

  $departament->conexion($SERVER,$USER,$PASSWORD,$BD);

  switch($method){

      case 'GET' : 

            $action = $_GET['accion'];

            if($action == 'consulta'){

                      $departament->leer();

                      $registro = $departament->dtBase();

                      echo json_encode($registro);

                      break;

            }

      case 'POST' :

            $action = $_POST['accion'];

            if($action == 'agregar'){

                      $object = $_POST['documento'];

                      $data   = json_decode($object);

                      $Archivo= str_replace(' ', '+', $data->Documento);

                      $departament->setInformacion(

                                  $data->nIDDocumento,
                                  $data->Nombre, 
                                  $Archivo,
                                  $data->nIDTipoDocumento,
                                  '',
                                  '',
                                  '',
                                  'Nuevo documento - ' . $fechaLocal,
                                  '1',
                                  TRUE,
                                  FALSE,
                                  FALSE

                      );

                      if($departament->ejecutar('tbl_documentos')){

                                      echo $departament->mostrarID();

                      }else{

                                      echo "NOT INSERTED";
                      
                      }
            
            }else if($action == 'modificar'){

                      $object = $_POST['documento'];

                      $data   = json_decode($object);

                      $Archivo= str_replace(' ', '+', $data->Documento);

                      $departament->setInformacion(

                                  $data->nIDDocumento,
                                  $data->Nombre, 
                                  $Archivo,
                                  $data->nIDTipoDocumento,
                                  '',
                                  '',
                                  '',
                                  'Modificacion al documento - ' . $fechaLocal,
                                  '1',
                                  FALSE,
                                  TRUE,
                                  FALSE

                      );

                      if($departament->ejecutar('tbl_documentos')){

                                      echo "UPDATED";

                      }else{

                                      echo "NOT UPDATED";
                      
                      }
            
            }else if($action == 'eliminar'){

                              $id = $_POST['id'];

                              $observacion = 'Documento eliminado temporalmente - ' . $fechaLocal;

                              if($departament->ocultar($id,$observacion)){

                                            echo "DELETED";

                              }else{

                                            echo "NOT DELETED";

                              }
                              
            }
  }
?>