'use strict';

angular.module('abgpassportApp')

       .controller('DocumentCtrl',['$scope','documentServices','typeDocumentServices',

        function($scope,documentServices,typeDocumentServices){

       $scope.documento = {
             nIDDocumento     : '0',
             Nombre           : '',
             Documento        : '',
             nIDTipoDocumento : ''

       };


       $scope.clean = function(){

             $scope.documento = {
             nIDDocumento     : '0',
             Nombre           : '',
             Documento        : '',
             nIDTipoDocumento : ''

       };
             $('#add_modal').modal('hide');

       };


  

       $scope.get = function(){

            $scope.getTipos();
            documentServices.get().then(function(resp){
                          console.log(resp.data);
                          if(resp.data == 'null'){

                              $scope.icono = 'assets/img/hecho.png';
                              $scope.mensaje = 'No tienes documentos registrados';

                          }else{

                              $scope.documentos = resp.data;

                          }

              },function(err){

                  console.log('error' + JSON.stringify(err));

              });

       };

       $scope.getTipos = function(){

             typeDocumentServices.get().then(function(resp){

                                 if(resp.data == 'null'){


                                 }else{

                                      $scope.tipos = resp.data;

                                 }

             },function(err){

                        console.log('error' + JSON.stringify(err));

             });

       };
       $scope.add = function(documento){

             

                      documentServices.add(documento).then(function(resp){

                                          if(resp.data == 'INSERTED'){

                                                  swal.fire({

                                                      title            : 'Registro exitoso!',

                                                      imageUrl         : 'assets/img/completo.png',

                                                      showCloseButton  : true,

                                                      confirmButtonColor : '#044BFD'

                                                  });

                                                  $scope.get();

                                                  $scope.clean();

                                          }else{

                                                  swal.fire({

                                                      title                 : 'Oops!',
 
                                                      text                  : 'No se logro el registro,inténtalo de nuevo',

                                                      imageUrl              : 'assets/img/error.png',

                                                      showCloseButton       : true,

                                                      confirmButtonColor    : '#044BFD'

                                                  });

                                          }

                      },function(err){

                                console.log('error' + JSON.stringify(err));

                      });

             

       };


      


       $scope.destroy = function(id){

              swal.fire({

                  title               : 'ADVERTENCIA',

                  text                : '¿Estás seguro de eliminar este registro?',

                  imageUrl            : 'assets/img/advertencia.png',

                  showCancelButton    : true,

                  showCloseButton     : true,                  

                  cancelButtonColor   : 'rgba(4,75,253,0.5)',                  

                  confirmButtonColor  : '#044BFD',

                  cancelButtonText    : 'Cancelar',

                  confirmButtonText   : 'Si',

              }).then((result) => {

                       if(result.value){

                                marketstallServices.destroy(id).then(function(resp){

                                              if(resp.data == "DELETED"){

                                                      swal.fire({

                                                          title              : 'Registro eliminado con exito!',

                                                          imageUrl           : 'assets/img/completo.png',

                                                          showCloseButton    : true,

                                                          confirmButtonColor : '#044BFD'

                                                      });

                                                      $scope.get();

                                              }else{

                                                      swal.fire({

                                                          title             : 'Oops!',

                                                          text              : 'No se pudo eliminar el registro inténtalo de nuevo',

                                                          imageUrl          : 'assets/img/error.png',

                                                          showCloseButton   : true,

                                                          confirmButtonColor: '#044BFD'

                                                      });

                                              }

                                },function(err){

                                      console.log('error' + JSON.stringify(err));

                                });

                       }else{

                                swal.fire({

                                    title               : 'Se cancelo la eliminicación!',

                                    imageUrl            : 'assets/img/error.png',

                                    showCloseButton     : true,

                                    confirmButtonColor  : '#044BFD'

                                });
                 
                       }
             
              });
        
        };

       }]);