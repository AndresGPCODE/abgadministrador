<?php

  error_reporting(E_ALL ^ E_NOTICE);  

  ini_set('display_errors', '1');

  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Request-Methods');

  include_once '../Configuraciones/Conexion.php';
  include_once '../Cliente/clSolicitud.php';
  include_once '../Utilerias/clHerramientas_v2011.php';

  $method = $_SERVER['REQUEST_METHOD'];

  $UtileriasDatos = new clHerramientasv2011();
  $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
  $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

  $solicitud = new clSolicitud();

  $solicitud->conexion($SERVER,$USER,$PASSWORD,$BD);

  switch($method){

        case 'GET' : 

            $action = $_GET['accion'];

                  $valores = " tbl_solicitud.nIDSolicitud,tbl_solicitud.nIDCliente,tbl_solicitud.nIDAutomovil,tbl_solicitud.nIDChofer,";
                  $valores = $valores . "tbl_solicitud.Calle,tbl_solicitud.NoExterior,tbl_solicitud.Longitud,tbl_solicitud.Latitud,tbl_solicitud.Altitud,";
                  $valores = $valores . "tbl_solicitud.FechaHora,tbl_solicitud.Notas,tbl_solicitud.Estatus,tbl_solicitud.FechaModificacion,";
                  $valores = $valores . "tbl_solicitud.FechaCreacion,tbl_solicitud.Observaciones,tbl_solicitud.bEstado,tbl_clientes.Nombre,tbl_clientes.Celular,";
                  $valores = $valores . "tbl_automoviles.Marca,tbl_automoviles.Modelo,tbl_automoviles.Placas ";

                  $inner = " INNER JOIN tbl_clientes ON tbl_clientes.nIDCliente = tbl_solicitud.nIDCliente";
                  $inner = $inner . " INNER JOIN tbl_automoviles ON tbl_automoviles.nIDAutomovil = tbl_solicitud.nIDAutomovil";

            if($action == 'buscar'){


                  if($solicitud->consultarCondicion("tbl_solicitud.bEstado = 1 and tbl_solicitud.Estatus = 'SOLICITADO' ", $valores,$inner)){

                      echo json_encode($solicitud->consultarCondicion("tbl_solicitud.bEstado = 1 and tbl_solicitud.Estatus = 'SOLICITADO' ", $valores,$inner));
                  }else{

                      echo "null";
                  }
 
                  
            }else if($action == 'buscarSolicitudes'){

                      $valores = $valores . ",tbl_choferes.Nombre as NombreChofer,tbl_choferes.Celular as CelularChofer ";

                      $inner = $inner . " INNER JOIN tbl_choferes ON tbl_choferes.nIDChofer = tbl_solicitud.nIDChofer ";

                      if($solicitud->consultarCondicion("tbl_solicitud.bEstado = 1  ", $valores,$inner)){

                        echo json_encode($solicitud->consultarCondicion("tbl_solicitud.bEstado = 1 ", $valores,$inner));
                      
                      }else{

                        echo "null";
                      
                      }

            }else if($action == 'buscarRutas'){

                    $valores = " nIDSolicitud ";

                    $inner   = " ";

                    $condicion = " tbl_solicitud.bEstado = 1 and tbl_solicitud.Estatus = 'ENTREGADO' OR tbl_solicitud.Estatus = 'EN RUTA' ";

                    if($solicitud->consultarCondicion($condicion,$valores,$inner)){

                          echo json_encode($solicitud->consultarCondicion($condicion,$valores,$inner));

                    }else{

                      echo "null";
                    }

            }else if($action == 'buscarCuenta'){

              $valores = " tbl_solicitud.nIDSolicitud,tbl_solicitud.nIDCliente, ";

              $valores = $valores . " tbl_solicitud.nIDAutomovil,tbl_solicitud.FechaFin,tbl_solicitud.Estatus,";

              $valores = $valores . " tbl_clientes.Nombre,tbl_clientes.Celular,tbl_clientes.Email,tbl_automoviles.Marca,";

              $valores = $valores . " tbl_automoviles.Modelo,tbl_automoviles.Placas ";


              $inner   =  " INNER JOIN tbl_automoviles ON tbl_automoviles.nIDAutomovil = tbl_solicitud.nIDAutomovil ";

              $inner   = $inner . " INNER JOIN tbl_clientes ON tbl_clientes.nIDCliente = tbl_solicitud.nIDCliente ";

               if($solicitud->consultarCondicion('tbl_solicitud.bEstado = 1', $valores,$inner)){

                      echo json_encode($solicitud->consultarCondicion('tbl_solicitud.bEstado = 1', $valores,$inner));
                  }else{

                      echo "null";
                  }


            }else if($action == 'buscarFechasRestantes'){

                     $valores = " tbl_solicitud.nIDSolicitud,tbl_solicitud.nIDAutomovil,tbl_solicitud.FechaHora, ";

                     $valores = $valores . " tbl_solicitud.FechaHoraRecepcion,tbl_automoviles.Placas,";

                     $valores = $valores . " -datediff(NOW(),tbl_solicitud.FechaHoraRecepcion) AS DIAS ";

                     $inner   = " INNER JOIN tbl_automoviles ON tbl_automoviles.nIDAutomovil = tbl_solicitud.nIDAutomovil ";

                     $condicion = " tbl_solicitud.Estatus = 'EN RUTA' OR tbl_solicitud.Estatus = 'ENTREGADO' ";

                     if($solicitud->consultarCondicion($condicion,$valores,$inner)){

                                  echo json_encode($solicitud->consultarCondicion($condicion,$valores,$inner));

                     }else{

                          echo "null";
                     }

            }else if($action == 'fechasEnCalendario'){

                    $valores = " tbl_automoviles.Marca,tbl_automoviles.Modelo,tbl_automoviles.Placas,tbl_clientes.Nombre,";

                    $valores = $valores . "tbl_solicitud.Calle,tbl_solicitud.NoExterior,Date_format(tbl_solicitud.FechaHora, '%d-%m-%Y %H:%i') AS FechaHora,tbl_solicitud.FechaFin, ";

                    $valores = $valores . "Date_format(tbl_solicitud.FechaHora, '%d') as dia,Date_format(tbl_solicitud.FechaHora, '%c') as mes,";

                    $valores = $valores . "Date_format(tbl_solicitud.FechaHora, '%Y') as año ";

                    $inner   = " INNER JOIN tbl_automoviles ON tbl_automoviles.nIDAutomovil = tbl_solicitud.nIDAutomovil ";

                    $inner   = $inner . " INNER JOIN tbl_clientes ON tbl_clientes.nIDCliente = tbl_solicitud.nIDCliente ";

                    $condicion = " tbl_solicitud.Estatus = 'SOLICITADO' OR tbl_solicitud.Estatus = 'ASIGNADO' ";

                    if($solicitud->consultarCondicion($condicion,$valores,$inner)){

                        echo json_encode($solicitud->consultarCondicion($condicion,$valores,$inner));

                    }else{

                        echo 'null';

                    }
                     
            }else if($action == 'Recepcion'){

              $valores = " tbl_solicitud.nIDSolicitud,tbl_automoviles.Marca,tbl_automoviles.Modelo,tbl_automoviles.Placas,";

              $valores = $valores . " tbl_clientes.Nombre,Date_format(tbl_solicitud.FechaFin, '%d-%m-%Y') AS Fechas_Fin,";

              $valores = $valores . "Date_format(tbl_solicitud.FechaFin, '%d') AS dia,Date_format(tbl_solicitud.FechaFin, '%c') AS mes,";

              $valores = $valores . "Date_format(tbl_solicitud.FechaFin, '%Y') AS año ";

              $inner   = " INNER JOIN tbl_automoviles ON tbl_automoviles.nIDAutomovil = tbl_solicitud.nIDAutomovil ";

              $inner   = $inner . " INNER JOIN tbl_clientes ON tbl_clientes.nIDCliente = tbl_solicitud.nIDCliente ";

              $condicion = " tbl_solicitud.Estatus = 'ENTREGADO' and tbl_solicitud.FechaFin > (SELECT now())";

              if($solicitud->consultarCondicion($condicion,$valores,$inner)){

                  echo json_encode($solicitud->consultarCondicion($condicion,$valores,$inner));

              }else{

                 echo 'null';
              }


            }else if($action == 'buscarAsignados'){

              $valores = $valores . ",tbl_choferes.Nombre as NombreChofer,tbl_choferes.Celular as CelularChofer ";

              $inner = $inner . " INNER JOIN tbl_choferes ON tbl_choferes.nIDChofer = tbl_solicitud.nIDChofer ";

              if($solicitud->consultarCondicion("tbl_solicitud.bEstado = 1 and tbl_solicitud.Estatus = 'ASIGNADO' ", $valores,$inner)){

                      echo json_encode($solicitud->consultarCondicion("tbl_solicitud.bEstado = 1 and tbl_solicitud.Estatus = 'ASIGNADO' ", $valores,$inner));
                  }else{

                      echo "null";
                  }

              break;
            }

        case 'POST':
                    
                    $action = $_POST['accion'];

                    if($action == 'modificar'){

                              $objeto = $_POST['objeto'];

                              $data   = json_decode($objeto);

                              $solicitud->setInformacion(

                                      $data->nIDSolicitud,
                                      '',
                                      '',
                                      $data->nIDChofer,
                                      '',
                                      '',
                                      '',
                                      '',
                                      '',
                                      '',
                                      '',
                                      '',
                                      '',
                                      '',
                                      '',
                                      '',
                                      '',
                                      '',
                                      '',
                                      '',
                                      'ASIGNADO',
                                      '',
                                      '',
                                      'Asignación de un chofer - ' . $fechaLocal,
                                      '1',
                                      FALSE,
                                      TRUE,
                                      FALSE
                              );

                              if($solicitud->ejecutar('tbl_cat_contacto')){

                                        echo "UPDATED";
                              
                              }else{

                                        echo "NOT UPDATED";

                              }
                    }else if($action == 'Imagenes'){
                      $id = $_POST['id'];
              $valores = ' tbl_solicitud.nIDSolicitud, tbl_solicitud.Imagenes ';

              $inner = '';

              if($solicitud->consultarCondicion("tbl_solicitud.bEstado = 1 and tbl_solicitud.nIDSolicitud = " . $id, $valores, $inner)){
                echo json_encode($solicitud->consultarCondicion("tbl_solicitud.bEstado = 1 and tbl_solicitud.nIDSolicitud = ". $id, $valores, $inner));
              }else{
                echo 'null';
              }
            }else if($action == 'modificarEstado'){

                            $id = $_POST['id'];

                            $Estado = 'EN RUTA';

                            $Observaciones = 'Vehiculo en camino - ' . $fechaLocal;


                            if($solicitud->actualizarEstatus($id,$Estado,$Observaciones)){

                                      echo "UPDATED";

                            }else{

                                      echo "NOT UPDATED";

                            }

                    }

       
  }
?>