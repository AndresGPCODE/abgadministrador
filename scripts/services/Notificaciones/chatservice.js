'use strict';

angular.module('abgpassportApp')

	   .service('chatServices',['$http','configService',

	   	function($http,configService){

	   			return {

	   				chatsUser : function(usuario){

	   						return $http.post(configService.apiUrl + 'Chat.php','accion=conversaciones&usuario=' + JSON.stringify(usuario),

	   							{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   						);

	   				},

	   				getChats : function(){ 
	   						return $http.get(configService.apiUrl + 'Chat.php?accion=chats',
	   							{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   						);

	   				},

	   				contadores : function(id){

	   						return $http.post(configService.apiUrl + 'Chat.php','accion=contador&id=' + JSON.stringify(id),

	   							{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   						);

	   				},

	   				contadoresTodos : function(id){

	   						return $http.get(configService.apiUrl + 'Chat.php?accion=contadorTodos',

	   							{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   						);

	   				},

	   				Leer : function(id){

	   						return $http.post(configService.apiUrl + 'Chat.php','accion=leer&id=' + JSON.stringify(id),

	   							{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   						);
	   						
	   				},

	   				LeerTodos : function(id){

	   						return $http.get(configService.apiUrl + 'Chat.php?accion=leerTodos',

	   							{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   						);
	   						
	   				},

	   				conversacion : function(id){

	   					return $http.post(configService.apiUrl + 'Chat.php','accion=mensajes&id=' + JSON.stringify(id),

	   						{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   					);
	   					
	   				},

	   				send : function(idusuario,idcliente,texto){ console.log(idusuario,idcliente,texto);

	   					 return $http.post(configService.apiUrl + 'Chat.php','accion=enviar&idusuario=' + JSON.stringify(idusuario) +
	   					 	'&idcliente=' + JSON.stringify(idcliente) + '&texto=' + JSON.stringify(texto),
	   					 	{ headers: {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   					 );
	   				}

	   			};

	   	}]);