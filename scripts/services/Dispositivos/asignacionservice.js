'use strict';

angular.module('abgpassportApp')

	   .service('asignacionServices',['$http','configService',

	   	function($http,configService){

	   		return{

	   			get : function(){

	   				return $http.get(configService.apiUrl + 'Asignacion.php?accion=buscar',

	   					{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   				);

	   			},

	   			add : function(objeto){

	   				return $http.post(configService.apiUrl + 'Asignacion.php','accion=agregar&objeto=' + JSON.stringify(objeto),

	   					{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   				);

	   			},

	   			update : function(objeto){

	   				return $http.post(configService.apiUrl + 'Asignacion.php','accion=modificar&objeto=' + JSON.stringify(objeto),

	   					{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   				);

	   			},

	   			destroy : function(id){

	   				return $http.post(configService.apiUrl + 'Asignacion.php','accion=eliminar&id=' + JSON.stringify(id),

	   					{ headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

	   				);
	   				
	   			}
	   		};

	   	}]);