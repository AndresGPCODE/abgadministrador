<?php
    error_reporting(E_ALL ^ E_NOTICE);
    ini_set('display_errors','1');

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Request-Methods');

      include_once '../Configuraciones/Conexion.php';
      include_once '../Usuario/clRoles.php';
      include_once '../Utilerias/clHerramientas_v2011.php';

      
      $method = $_SERVER['REQUEST_METHOD'];

      $UtileriasDatos   = new clHerramientasv2011();
      $fechaLocal       = $UtileriasDatos->getFechaYHoraActual_General();
      $fechaLocal       = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
      
      $role = new clRoles();

      
      $role->conexion($SERVER,$USER,$PASSWORD,$BD);
      
      switch($method){

        
        case 'GET' : 
            
            $action = $_GET['accion'];

            
            if($action == 'consulta'){

              
              $role->leerCondicion(" tbl_roles.bEstado = 1 ");

              
              $registros = $role->dtBase();

              
              echo json_encode($registros);
              break;
            }
            


        case 'POST' :

              $action = $_POST['accion'];

              if($action == 'agregar'){

                        $object = $_POST['rol'];

                        $data   = json_decode($object);

                        $role->setInformacion(

                                $data->nIDRol,
                                $data->TipoDeRol,
                                $data->Descripcion,
                                '',
                                '',
                                '',
                                $data->Clasificacion,
                                '',
                                '',
                                '',
                                'Registro de un nuevo rol - ' . $fechaLocal,
                                '1',
                                TRUE,
                                FALSE,
                                FALSE

                        );

                        if($role->ejecutar('tbl_roles')){

                                echo "INSERTED";

                        }else{

                                echo "NOT INSERTED";
                        }
              
              }else if($action == 'modificar'){

                               $object = $_POST['rol'];

                               $data   = json_decode($object);

                               $role->setInformacion(

                                      $data->nIDRol,
                                      $data->TipoDeRol,
                                      $data->Descripcion,
                                      '',
                                      '',
                                      '',
                                      $data->Clasificacion,
                                      '',
                                      '',
                                      '',
                                      'Modificación al rol - ' . $fechaLocal,
                                      '1',
                                      FALSE,
                                      TRUE,
                                      FALSE
                               );

                               if($role->ejecutar('tbl_roles')){

                                        echo "UPDATED";
                               }else{

                                        echo "NOT UPDATED";

                               }

              }else if($action == 'eliminar'){

                                $id = $_POST['id'];

                      $observacion = 'rol eliminado temporalmente - ' . $fechaLocal;

                      if($role->ocultar($id,$observacion)){

                            echo "DELETED";
                      }else{

                            echo "NOT DELETED";
                      }
              }
            
      }


?>