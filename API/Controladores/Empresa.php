<?php
    error_reporting(E_ALL ^ E_NOTICE);
    ini_set('display_errors','1');

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Request-Methods');

      include_once '../Configuraciones/Conexion.php';
      include_once '../Agencia/clEmpresas.php';
      include_once '../Utilerias/clHerramientas_v2011.php';

      
      $method = $_SERVER['REQUEST_METHOD'];

      $UtileriasDatos   = new clHerramientasv2011();
      $fechaLocal       = $UtileriasDatos->getFechaYHoraActual_General();
      $fechaLocal       = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
      
      $agency = new clEmpresas();

      
      $agency->conexion($SERVER,$USER,$PASSWORD,$BD);
      
      switch($method){

        
        case 'GET' : 
            
            $action = $_GET['accion'];

            
            if($action == 'consulta'){

              echo json_encode($agency->consultarCondicion(" tbl_empresa.bEstado = 1 "));
              break;
            }
            


        case 'POST' :

              $action = $_POST['accion'];

              if($action == 'agregar'){

                        $object = $_POST['agencia'];

                        $data   = json_decode($object);

                        $agency->setInformacion(

                                $data->nIDEmpresa,
                                $data->RazonSocial,
                                $data->Calle,
                                $data->NoExterior,
                                $data->NoInterior,
                                $data->Colonia,
                                $data->Ciudad,
                                $data->Estado,
                                '',
                                $data->Cp,
                                $data->RFC,
                                '',
                                '',
                                '',
                                $data->Telefono,
                                $data->Email,
                                '',
                                '',
                                'Registro de una nueva empresa - ' . $fechaLocal,
                                $data->bEstado,
                                TRUE,
                                FALSE,
                                FALSE

                        );

                        if($agency->ejecutar('tbl_empresa')){

                                echo "INSERTED";

                        }else{

                                echo "NOT INSERTED";
                        }
              
              }else if($action == 'modificar'){

                               $object = $_POST['agencia'];

                               $data   = json_decode($object);

                               $agency->setInformacion(

                                      $data->nIDEmpresaEdit,
                                      $data->RazonSocialEdit,
                                      $data->CalleEdit,
                                      $data->NoExteriorEdit,
                                      $data->NoInteriorEdit,
                                      $data->ColoniaEdit,
                                      $data->CiudadEdit,
                                      $data->EstadoEdit,
                                      '',
                                      $data->CpEdit,
                                      $data->RFCEdit,
                                      '',
                                      '',
                                      '',
                                      $data->TelefonoEdit,
                                      $data->EmailEdit,
                                      '',
                                      '',
                                      'Modificacion a la agencia - ' . $fechaLocal,
                                      '1',
                                      FALSE,
                                      TRUE,
                                      FALSE
                               );

                               if($agency->ejecutar('tbl_empresa')){

                                        echo "UPDATED";
                               }else{

                                        echo "NOT UPDATED";

                               }

              }else if($action == 'eliminar'){

                               $id = $_POST['id'];

                      $observacion = 'Agencia eliminada temporalmente - ' . $fechaLocal;

                      if($agency->ocultar($id,$observacion)){

                            echo "DELETED";
                      }else{

                            echo "NOT DELETED";
                      }
              }
            
      }


?>