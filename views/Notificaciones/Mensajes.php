
<?php
SESSION_START();
?>
<div class="container-fluid" ng-init="correo('<?php echo $_SESSION["nIDUsuario"] ?>')" ng-if="'Usuario' == '<?php echo $_SESSION["Rol"] ?>'">
<div id="chat-container">
	
	 <div id="search-container">
	 	  
	 	  <input type="text" placeholder="Buscar..."/>

	 </div>

	 <div id="conversation-list"><!--VISTA DE TODA LA BANDEJA DE ENTRADA-->
	 	
	 	  <div class="conversation" ng-repeat="entrada in entradas" ng-click="conversacion(entrada.nIDCliente,entrada.Nombre)">
	 	  	
	 	  	   <img src="{{entrada.Foto}}">

	 	  	   <div class="title-text"> {{ entrada.Nombre}}</div>

	 	  	   <div class="created-date">{{entrada.FechaCreacion}}</div>

	 	  	   <div class="conversation-message">{{entrada.Texto}}</div>

	 	  </div>
		 
	 </div>

	 <div id="new-message-container"><!--NUEVO MENSAJE-->
	 	


	 </div>



	 <div id="chat-title"><!--NOMBRE DE MI CONTACTO-->
	 	
	 	<span>{{NombreCliente}}</span>

	 	

	 </div>

	 <div id="chat-message-list" >

	 	<div class="message-row" ng-repeat="mensaje in mensajes">
	 	  
	 	  <div class="you-message" ng-if="mensaje.Remitente == 'USUARIO'">

	 	  	   <div class="message-content">
	 	  	   	
	 	  	   	    <div class="message-text">{{mensaje.Texto}}</div>

	 	  	   		<div class="message-time">{{mensaje.FechaCreacion}}</div>

	 	  	   </div>	 	  		 	  	   

	 	  </div>

	 	  <div class="other-message" ng-if="mensaje.Remitente == 'CLIENTE'">

	 	  		<div class="message-content">
	 	  		 
	 	  		 	 <img src="{{mensaje.Foto}}" alt="ABG">
	 	  	
	 	  	   		 <div class="message-text">{{mensaje.Texto}}</div>

	 	  	   		 <div class="message-time">{{mensaje.FechaCreacion}}</div>

	 	  		</div>
	 	  	
	 	  </div>

	 	  </div>	  

	 </div>

	 <div id="chat-form"><!--FORMULARIO MENSAJE-->
	 	<img src="assets/img/send.png" alt="Enviar" ng-click="send()">
	 		 	  
	 	  <input id="Mensaje" type="text" placeholder="Escribe un mensaje..." ng-model="Texto"/> 

	 	  

	 </div>

</div>
</div>


<div class="container-fluid" ng-init="correoTodos()" ng-if="'Administrador' == '<?php echo $_SESSION["Rol"] ?>'">
<div id="chat-container">
	
	 <div id="search-container">
	 	  
	 	  <input type="text" placeholder="Buscar..."/>

	 </div>

	 <div id="conversation-list"><!--VISTA DE TODA LA BANDEJA DE ENTRADA-->
	 	
	 	  <div class="conversation" ng-repeat="entrada in entradas" ng-click="conversacion(entrada.nIDCliente,entrada.Nombre)">
	 	  	
	 	  	   <img src="{{entrada.Foto}}">

	 	  	   <div class="title-text"> {{ entrada.Nombre}}</div>

	 	  	   <div class="created-date">{{entrada.FechaCreacion}}</div>

	 	  	   <div class="conversation-message">{{entrada.Texto}}</div>

	 	  </div>
		 
	 </div>

	 <div id="new-message-container"><!--NUEVO MENSAJE-->
	 	


	 </div>



	 <div id="chat-title"><!--NOMBRE DE MI CONTACTO-->
	 	
	 	<span>{{NombreCliente}}</span>

	 	

	 </div>

	 <div id="chat-message-list" >

	 	<div class="message-row" ng-repeat="mensaje in mensajes">
	 	  
	 	  <div class="you-message" ng-if="mensaje.Remitente == 'USUARIO'">

	 	  	   <div class="message-content">
	 	  	   	
	 	  	   	    <div class="message-text">{{mensaje.Texto}}</div>

	 	  	   		<div class="message-time">{{mensaje.FechaCreacion}}</div>
	 	  	   		<div class="message-time">De {{mensaje.NombreAdmin}}</div>

	 	  	   </div>	 	  		 	  	   

	 	  </div>

	 	  <div class="other-message" ng-if="mensaje.Remitente == 'CLIENTE'">

	 	  		<div class="message-content">
	 	  		 
	 	  		 	 <img src="{{mensaje.Foto}}" alt="ABG">
	 	  	
	 	  	   		 <div class="message-text">{{mensaje.Texto}}</div>

	 	  	   		 <div class="message-time">{{mensaje.FechaCreacion}} </div>

	 	  		</div>
	 	  	
	 	  </div>

	 	  </div>	  

	 </div>

	 <div id="chat-form"><!--FORMULARIO MENSAJE-->
	 	<img src="assets/img/send.png" alt="Enviar" ng-click="send()">
	 		 	  
	 	  <input id="Mensaje" type="text" placeholder="Escribe un mensaje..." ng-model="Texto"> 

	 	  

	 </div>

</div>
</div>
<!--<div class="container-fluid" ng-init="correo('<?php// echo $_SESSION["nIDUsuario"] ?>')">
	
	 <div class="row">
	 	
	 	  <div class="col-md-3" style="background: white;">
	 	  	
			   <h2 style="padding-top: 3%">Todos los mensajes</h2>

			   <hr>

			   <div class="overflow-auto mt-2" style="height: 60vh;">
			   	
			   	    <div class="table-responsive">
			   	    	
			   	    	 <table class="table">
			   	    	 	
			   	    	 	    <tr id="cuerpo" ng-repeat="chat in chats">

									<td class="d-flex no-block align-items-center p-10" ng-click="conversacion(chat.nIDChat)">
		
										<img src="{{chat.Foto}}" class="rounded-circle ml-2" height="50px" width="50px">
										<div class="m-l-10">	
											 <h4 class="m-b-0 ml-2">{{chat.Nombre}}</h4>
											 <span class="mail-desc ml-2">{{chat.Texto}}</span>
										</div>						
									</td>		


			   	    	 	    </tr>

			   	    	 </table>

			   	    </div>

			   </div>
	 	  </div>

	 	  <div class="col-md-9" style="background:white;height: 100vh">
	 	  	   <div class="card" >
	 	  	   	
	 	  	   	    <div class="card-body">
	 	  	   	    	
	 	  	   	    	  <h4 class="card-title">Nombre del cliente</h4>

	 	  	   	    	  <div class="chat-box scrollable">-->
	 	  	   	    	  	
	 	  	   	    	  	<!-- CHAT ROW-->
	 	  	   	    	  	<!--<ul class="chat-list">
	 	  	   	    	  		
	 	  	   	    	  		<li class="chat-item">
	 	  	   	    	  			
	 	  	   	    	  			<div class="chat-im"><img src="assets/img/ABG flat negativo.png" width="50px" height="50px"></div>

	 	  	   	    	  			<div class="chat-content">
	 	  	   	    	  				
	 	  	   	    	  				 <h6 class="font-medium">CLIENTE</h6>

	 	  	   	    	  				 <div class="box bg-light-info">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum vero quasi saepe accusantium excepturi ducimus consectetur eligendi facilis suscipit enim pariatur hic veniam illum, id architecto? Minus eos maxime id.</div>

	 	  	   	    	  			</div>

	 	  	   	    	  			<div class="chat-time">10:50</div>

	 	  	   	    	  		</li>

	 	  	   	    	  	</ul>

	 	  	   	    	  </div>

	 	  	   	    </div>

	 	  	   </div>

	 	  </div>

	 </div>

</div>-->