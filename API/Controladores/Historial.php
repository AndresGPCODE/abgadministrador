<?php

	error_reporting(E_ALL ^ E_NOTICE);
    ini_set('display_errors','1');

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Request-Methods');

      include_once '../Configuraciones/Conexion.php';
      include_once '../Dispositivos/clHistorial.php';
      include_once '../Utilerias/clHerramientas_v2011.php';

      
      $method = $_SERVER['REQUEST_METHOD'];

      $UtileriasDatos   = new clHerramientasv2011();
      $fechaLocal       = $UtileriasDatos->getFechaYHoraActual_General();
      $fechaLocal       = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
      
      $historial = new clHistorial();

      
      $historial->conexion($SERVER,$USER,$PASSWORD,$BD);

      switch($method){


            case 'GET' :

              $action = $_GET['accion'];

              if($action == 'buscarTodos'){ 

              if($historial->buscarTodosDispositivos()){
                            
                            echo json_encode($historial->buscarTodosDispositivos());
                            
                        }else{

                              echo 'null';

                        } break;

                   }

      		

      	    case 'POST' : 

      	    	 $action = $_POST['accion']; 

                   if($action == 'buscar'){ 
                        $id = $_POST['id']; 

                        $valor = " tbl_historial.nIDHistorial AS ID,tbl_historial.Latitud,tbl_historial.Longitud, ";
                        $valor = $valor . "tbl_autos_dispositivos.Identificador,tbl_historial.FechaCreacion ";

                        $condicion = " tbl_historial.nIDDispositivoAuto = " . $id;
                        $condicion = $condicion . " and tbl_historial.nIDHistorial = (SELECT max(nIDHistorial) FROM tbl_historial WHERE nIDDispositivoAuto = " .$id . ")";

                       if($historial->consultarCondicion($valor,$condicion)){

                  echo json_encode($historial->consultarCondicion($valor,$condicion));
                        }else{

                              echo 'null';

                        } 

                   }else if($action == 'buscarRutas'){

                      $placas = $_POST['placas']; 
                      $PLACAS = str_replace('"', '', $placas);

                      $valores = " tbl_historial.nIDDispositivoAuto,tbl_autos_dispositivos.Identificador, ";
                      $valores = $valores . " DATE_FORMAT(tbl_historial.Fecha, '%d-%m-%Y') AS Fecha ";

                      $inner   = " INNER JOIN tbl_autos_dispositivos ON tbl_autos_dispositivos.nIDDispositivoAuto = tbl_historial.nIDDispositivoAuto "; 

                      $condicion = " WHERE tbl_autos_dispositivos.Identificador = '".$PLACAS."' GROUP BY DATE_FORMAT(tbl_historial.Fecha, '%d-%m-%Y')";
                      $condicion = $condicion . " ORDER BY DATE_FORMAT(tbl_historial.Fecha, '%d-%m-%Y') DESC";

                      if($historial->consultarFechas($valores,$inner,$condicion)){

                          echo json_encode($historial->consultarFechas($valores,$inner,$condicion));

                      }else{

                          echo 'null';
                      }

                      

                   }else if($action == 'puntos'){

                            $id    = $_POST['id'];
                            $fecha = $_POST['fecha'];
                            $PLACAS  = str_replace('"', '', $id);
                            $FECHA   = str_replace('"', '', $fecha);

                            $valores = " tbl_historial.nIDHistorial,tbl_historial.nIDDispositivoAuto,tbl_autos_dispositivos.Identificador, ";
                            $valores = $valores . " tbl_historial.nIDSolicitud,tbl_historial.Latitud,tbl_historial.Longitud, ";
                            $valores = $valores . " DATE_FORMAT(tbl_historial.Fecha, '%d-%m-%Y') AS Fecha ";

                            $inner   = " INNER JOIN tbl_autos_dispositivos ON tbl_autos_dispositivos.nIDDispositivoAuto = tbl_historial.nIDDispositivoAuto ";

                            $condicion = " WHERE tbl_autos_dispositivos.Identificador = '".$PLACAS."' ";
                            $condicion = $condicion . " and DATE_FORMAT(tbl_historial.Fecha, '%d-%m-%Y') = '".$FECHA."'";

                            if($historial->consultarFechas($valores,$inner,$condicion)){

                                echo json_encode($historial->consultarFechas($valores,$inner,$condicion));

                            }else{

                                echo 'null';
                            }

                   }else{

      	                 
                             $request = json_decode(file_get_contents('php://input'), true);  
                            



                                  $valor =  $historial->SepararCadena($request['Respuesta']);

                                    $historial->setInformacion(
                    $request['nIDHistorial'],
                    $request['nIDDispositivoAuto'],
                    $historial->consultarSolicitud($request['nIDDispositivoAuto']),
                    '',
                    $request['Respuesta'],
                    $valor['Latitud'],
                    $valor['Longitud'],
                    '',
                    $valor['Hora'],
                    $valor['Fecha'],
                    '',
                    '',
                    'Se ha ingresado un nuevo registro - ' . $fechaLocal,
                    '1',
                    TRUE,
                    FALSE,
                    FALSE
                  
                  );


      	    	 		

      	    	 		if($historial->ejecutar('tbl_historial')){

      	    	 				echo json_encode("INSERTED",JSON_UNESCAPED_UNICODE);

      	    	 		}else{

      	    	 			    echo json_encode("NOT INSERTED",JSON_UNESCAPED_UNICODE);

      	    	 		}
                        }


      	    	 
      	    	 
      }

?>