'use strict';

angular.module('abgpassportApp')

	   .controller('AtencionCtrl',['$scope','solicitudesServices','notificationclientesServices',

	   	function($scope,solicitudesServices,notificationclientesServices){

	   		$scope.notificacion = {

	   			  nIDNotificacion : '0',

	   			  nIDCliente 	  : '0',

	   			  nIDTipoNotificacion : '5',

	   			  Encabezado : 'Tu vehículo va en camino',

	   			  Body : 'Espera el vehículo en el punto de entrega',

	   			  Permiso : 'Si'

	   		};

	   		$scope.clean = function(){

	   			  $scope.notificacion = {

	   			  	    nIDNotificacion 		: '0',

	   			  	    nIDCliente 				: '0' ,

	   			  	    nIDTipoNotificacion     : '5',

	   			  	    Encabezado 				: 'Tu vehículo va en camino',

	   			  	    Body 					: 'Espera el vehículo en el punto de entrega',

	   			  	    Permiso 				: 'Si'
	   			  
	   			  };

	   		};

	   		$scope.get = function(){

	   			  $scope.Titulo = '';

	   			  $scope.solicitudes = '';

	   			  solicitudesServices.getPorAtender().then(function(resp){

	   			  					 if(resp.data == 'null'){

	   			  					 		console.log('Not Found data');

	   			  					 		$scope.Titulo = "No tienes solicitudes pendientes";

	   			  					 }else{

	   			  					 		$scope.solicitudes = resp.data;

	   			  					 }

	   			  },function(err){

	   			  				console.log('error' + JSON.stringify(err));

	   			  });

	   		};

	   		$scope.Estado = function(id,cliente){$scope.notificacion.nIDCliente = cliente; 

	   			  swal.fire({

	   			  	  title  				: 'CONFIRMACIÓN!',

	   			  	  text 					: '¿Estás seguro de eliminar este registro?',

	   			  	  imageUrl 				: 'assets/img/advertencia.png',

	   			  	  showCancelButton		: true,

	   			  	  showCloseButton 		: true,

	   			  	  cancelButtonColor 	: 'rgba(4,75,253,0.5)',

	   			  	  confirmButtonColor 	: '#044BFD',

	   			  	  cancelButtonText 		: 'Cancelar',

	   			  	  confirmButtonText 	: 'Si',

	   			  }).then((result) => {

	   			  		   if(result.value){

	   			  		   			solicitudesServices.updateEstado(id).then(function(resp){

	   			  		   							   if(resp.data == "UPDATED"){

	   			  		   							   		  swal.fire({

	   			  		   							   		  	  title 				: 'Vehiculo en camino',

	   			  		   							   		  	  imageUrl 				: 'assets/img/completo.png',

	   			  		   							   		  	  showCloseButton 		: true,

	   			  		   							   		  	  confirmButtonColor    : '#044BFD'

	   			  		   							   		  });

	   			  		   							   		  $scope.get();

	   			  		   							   		  $scope.addNotificacion($scope.notificacion);

	   			  		   							   }else{

	   			  		   							   		  swal.fire({

	   			  		   							   		  	  title 					: 'Oops!',

	   			  		   							   		  	  text 						: 'No se pudo autorizar el vehiculo, Inténtalo de nuevo',

	   			  		   							   		  	  imageUrl 					: 'assets/img/error.png',

	   			  		   							   		  	  showCloseButton 			: true,

	   			  		   							   		  	  confirmButtonColor 		: '#044BFD'

	   			  		   							   		  });

	   			  		   							   }

	   			  		   			},function(err){

	   			  		   					console.log('error' + JSON.stringify(err));

	   			  		   			});
	   			  		   
	   			  		   }else{

	   			  		   			swal.fire({

	   			  		   				title 				: 'Se cancelo la eliminación!',

	   			  		   				imageUrl 			: 'assets/img/error.png',

	   			  		   				showCloseButton 	: true,

	   			  		   				confirmButtonColor  : '#044BFD'

	   			  		   			});
	   			  		   
	   			  		   }

	   			  });

	   		};

	   		$scope.addNotificacion = function(objeto){

	   			  notificationclientesServices.add(objeto).then(function(resp){


	   			  },function(err){

	   			  		console.log('error' + JSON.stringify(err));
	   			  		
	   			  });

	   		};


	   	}]);