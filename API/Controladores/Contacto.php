<?php

    error_reporting(E_ALL ^ E_NOTICE);
    ini_set('display_errors','1');

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Request-Methods');

      include_once '../Configuraciones/Conexion.php';
      include_once '../Agencia/clContacto.php';
      include_once '../Utilerias/clHerramientas_v2011.php';

      
      $method = $_SERVER['REQUEST_METHOD'];

      $UtileriasDatos   = new clHerramientasv2011();
      $fechaLocal       = $UtileriasDatos->getFechaYHoraActual_General();
      $fechaLocal       = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
      
      $contact = new clContacto();

      
      $contact->conexion($SERVER,$USER,$PASSWORD,$BD);

      switch ($method) {
              
              case 'POST':
                    
                    $action = $_POST['accion'];

                    if($action == 'contactos'){

                              $id = $_POST['id'];

                              $condicion = ' bEstado = 1  and nIDEmpresa = ' . $id;

                              if($contact->consultarCondicion($condicion) == false){

                                      echo 'null';
                              }else{

                                      echo json_encode($contact->consultarCondicion($condicion));
                              }
                    }else if($action == 'agregar'){

                              $objeto = $_POST['contacto'];

                              $data = json_decode($objeto);

                              $contact->setInformacion(
                                      $data->nIDContacto,
                                      $data->Nombre,
                                      $data->Telefono,
                                      $data->Email,
                                      $data->nIDEmpresa,
                                      '',
                                      '',
                                      'Se registo un nuevo contacto - ' . $fechaLocal,
                                      '1',
                                      TRUE,
                                      FALSE,
                                      FALSE
                              );

                              if($contact->ejecutar('tbl_cat_contacto')){

                                        echo "INSERTED";

                              }else{

                                       echo "NOT INSERTED";
                              }

                    }else if($action == 'modificar'){

                              $objeto = $_POST['contacto'];

                              $data   = json_decode($objeto);

                              $contact->setInformacion(

                                      $data->nIDContacto,
                                      $data->Nombre,
                                      $data->Telefono,
                                      $data->Email,
                                      $data->nIDEmpresa,
                                      '',
                                      '',
                                      'Modificicación al contacto - ' . $fechaLocal,
                                      '1',
                                      FALSE,
                                      TRUE,
                                      FALSE
                              );

                              if($contact->ejecutar('tbl_cat_contacto')){

                                        echo "UPDATED";
                              
                              }else{

                                        echo "NOT UPDATED";

                              }
                    }else if($action == 'eliminar'){

                             $id = $_POST['id'];

                             $observacion = 'contact eliminado temporalmente - ' . $fechaLocal;

                            if($contact->ocultar($id,$observacion)){

                                      echo "DELETED";
                            
                            }else{

                                      echo "NOT DELETED";
                            
                            }
                              
                    }
      }
?>