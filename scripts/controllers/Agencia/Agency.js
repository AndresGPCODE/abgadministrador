'use strict';

angular.module('abgpassportApp')

        .controller('CompanyCtrl', ['$scope','agencyServices','contactServices','$location', function($scope,agencyServices,contactServices,$location){
              
        var expRFC      = /^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\d]{3})?$/;

        var expEmail    = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;

        var expTelefono = /^[0-9]{10}$/;


        $scope.agencia = {

              nIDEmpresa          : '0',
              RazonSocial         : '',
              RFC                 : '',
              Telefono            : '',
              Email               : '',
              Calle               : '',
              NoExterior          : '',
              NoInterior          : '',
              Colonia             : '',
              Cp                  : '',
              Ciudad              : '',
              Estado              : '',
              Observaciones       : '',
              bEstado             : '1'

        };

        $scope.agenciaEdit = {

              nIDEmpresaEdit          : '0',
              RazonSocialEdit         : '',
              RFCEdit                 : '',
              TelefonoEdit            : '',
              EmailEdit               : '',
              CalleEdit               : '',
              NoExteriorEdit          : '',
              NoInteriorEdit          : '',
              ColoniaEdit             : '',
              CpEdit                  : '',
              CiudadEdit              : '',
              EstadoEdit              : ''              

        };

        $scope.contacto = {

              nIDContacto      : '0',
              Nombre           : '',
              Telefono         : '',
              Email            : '',
              nIDEmpresa       : ''

        };

        $scope.contactoEdit = {

              nIDContacto      : '0',
              Nombre           : '',
              Telefono         : '',
              Email            : '',
              nIDEmpresa       : ''

        };

        $scope.cleanContacto = function(id){

              $scope.contacto={

                  nIDContacto      : '0',
                  Nombre           : '',
                  Telefono         : '',
                  Email            : '',
                  nIDEmpresa       : id
              
              };

              document.getElementById('Visible').style.display = 'none';
        };

        $scope.cleanContactoEdit = function(id){

              $scope.contactoEdit = {

                  nIDContacto      : '0',
                  Nombre           : '',
                  Telefono         : '',
                  Email            : '',
                  nIDEmpresa       : id
              
              };

              document.getElementById('VisibleModificar').style.display = 'none';
        };

        $scope.clean = function(){

              $scope.agencia = {

                    nIDEmpresa          : '0',
                    RazonSocial         : '',
                    RFC                 : '',
                    Telefono            : '',
                    Email               : '',
                    Calle               : '',
                    NoExterior          : '',
                    NoInterior          : '',
                    Colonia             : '',
                    Cp                  : '',
                    Ciudad              : '',
                    Estado              : '',
                    Observaciones       : '',
                    bEstado             : '1'

              };

              $('#add_modal').modal('hide');

        };

        $scope.cleanEdit = function(){

              $scope.agenciaEdit = {

                    nIDEmpresaEdit          : '0',
                    RazonSocialEdit         : '',
                    RFCEdit                 : '',
                    TelefonoEdit            : '',
                    EmailEdit               : '',
                    CalleEdit               : '',
                    NoExteriorEdit          : '',
                    NoInteriorEdit          : '',
                    ColoniaEdit             : '',
                    CpEdit                  : '',
                    CiudadEdit              : '',
                    EstadoEdit              : ''              

              };

              $('#update_modal').modal('hide');
        
        };


        //LEER DATOS
        $scope.get = function(){

              agencyServices.get().then(function(resp){

                            if(resp.data == 'null'){

                                    $scope.icono   = "assets/img/hecho.png";

                                    $scope.mensaje = "No tienes agencias registradas";

                            }else{

                                  $scope.agencias = resp.data;

                            }

              },function(err){

                  console.log('error' + JSON.stringify(err));

              });

        };


        $scope.add = function(agencias){

              

              if(agencias.RazonSocial.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Nombre de la agencia vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });
                    

              }else if(!expRFC.test(agencias.RFC)){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'RFC no cumple con las especificaciones, inténtalo de nuevo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(!expTelefono.test(agencias.Telefono)){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Número de teléfono no cumple con las especificaciones, inténtalo de nuevo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(!expEmail.test(agencias.Email)){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Email no cumple con las especificaciones, inténtalo de nuevo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(agencias.Calle.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Domicilio vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(agencias.NoExterior.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Número exterior vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(agencias.NoInterior.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Número interior vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(agencias.Colonia.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Nombre de la colonia vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(agencias.Cp.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Código postal vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(agencias.Ciudad.length == 0){

                        swal.fire({

                              title               : 'Oops!',

                              text                : 'Nombre de la ciudad vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(agencias.Estado.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Nombre del estado vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else{

                    agencyServices.add(agencias).then(function(resp){

                              console.log(resp.data);

                                  if(resp.data == 'INSERTED'){

                                         swal.fire({

                                              title               : 'Registro exitoso!',

                                              imageUrl            : 'assets/img/completo.png',

                                              showCloseButton     : true,

                                              confirmButtonColor  : '#044BFD'

                                         });

                                         $scope.clean();

                                         $scope.get();

                                  }else{

                                         swal.fire({

                                             title                : 'Oops!',

                                             text                 : 'No se logro el registro, inténtalo de nuevo',

                                             imageUrl             : 'assets/img/error.png',

                                             confirmButtonColor   : '#044BFD'

                                         });

                                  }

                    },function(err){

                          console.log('error' + JSO.stringify(err));

                    });

              }


        };


        $scope.show = function(agencia){

               $scope.agenciaEdit.nIDEmpresaEdit  = agencia.nIDEmpresa;

               $scope.agenciaEdit.RazonSocialEdit = agencia.RazonSocial;

               $scope.agenciaEdit.RFCEdit         = agencia.RFC;

               $scope.agenciaEdit.TelefonoEdit    = agencia.Telefono;

               $scope.agenciaEdit.EmailEdit       = agencia.Email;

               $scope.agenciaEdit.CalleEdit       = agencia.Calle;

               $scope.agenciaEdit.NoExteriorEdit  = agencia.NoExterior;

               $scope.agenciaEdit.NoInteriorEdit  = agencia.NoInterior;

               $scope.agenciaEdit.ColoniaEdit     = agencia.Colonia;

               $scope.agenciaEdit.CpEdit          = agencia.Cp;

               $scope.agenciaEdit.CiudadEdit      = agencia.Ciudad;

               $scope.agenciaEdit.EstadoEdit      = agencia.Estado;

        };


        $scope.update = function(agenciaEdit){

            if(!expRFC.test(agenciaEdit.RFCEdit)){

                      swal.fire({

                          title               : 'Oops!',

                          text                : 'RFC no cumple con las especificaciones, inténtalo de nuevo',

                          imageUrl            : 'assets/img/advertencia.png',

                          showCloseButton     : true,

                          confirmButtonColor  : '#044BFD'

                      });
            
            }else if(!expEmail.test(agenciaEdit.EmailEdit)){

                              swal.fire({

                                  title               : 'Oops!',

                                  text                : 'Email no cumple con las especificaciones, inténtalo de nuevo',

                                  imageUrl            : 'assets/img/advertencia.png',

                                  showCloseButton     : true,

                                  confirmButtonColor  : '#044BFD'

                              });

            }else if(!expTelefono.test(agenciaEdit.TelefonoEdit)){

                                 swal.fire({

                                      title               : 'Oops!',

                                      text                : 'Número de teléfono no cumple con las especificaciones, inténtalo de nuevo',

                                      imageUrl            : 'assets/img/advertencia.png',

                                      showCloseButton     : true,

                                      confirmButtonColor  : '#044BFD'

                                 });

            }else{

                  agencyServices.update(agenciaEdit).then(function(resp){

                                if(resp.data == "UPDATED"){

                                        swal.fire({

                                            title              : 'Modificación exitosa!',

                                            imageUrl           : 'assets/img/completo.png',

                                            showCloseButton    : true,

                                            confirmButtonColor : '#044BFD'

                                        }); 

                                        $scope.cleanEdit();

                                        $scope.get();
                                
                                }else{

                                        swal.fire({

                                            title             : 'Oops!',

                                            text              : 'No se completo la modificación, inténtalo de nuevo',

                                            imageUrl          : 'assets/img/error.png',

                                            showCloseButton   : true,

                                            confirmButtonColor: '#044BFD'

                                        });

                                }
                  
                  },function(err){

                          console.log('error' + JSON.stringify(err));

                  });

            }

        };


        $scope.destroy = function(id){

              swal.fire({

                  title               : 'ADVERTENCIA',

                  text                : '¿Estás seguro de eliminar este registro?',

                  imageUrl            : 'assets/img/advertencia.png',

                  showCancelButton    : true,

                  showCloseButton     : true,                  

                  cancelButtonColor   : 'rgba(4,75,253,0.5)',                  

                  confirmButtonColor  : '#044BFD',

                  cancelButtonText    : 'Cancelar',

                  confirmButtonText   : 'Si',

              }).then((result) => {

                       if(result.value){

                                agencyServices.destroy(id).then(function(resp){

                                              if(resp.data == "DELETED"){

                                                      swal.fire({

                                                          title              : 'Registro eliminado con exito!',

                                                          imageUrl           : 'assets/img/completo.png',

                                                          showCloseButton    : true,

                                                          confirmButtonColor : '#044BFD'

                                                      });

                                                      $scope.get();

                                              }else{

                                                      swal.fire({

                                                          title             : 'Oops!',

                                                          text              : 'No se pudo eliminar el registro inténtalo de nuevo',

                                                          imageUrl          : 'assets/img/error.png',

                                                          showCloseButton   : true,

                                                          confirmButtonColor: '#044BFD'

                                                      });

                                              }

                                },function(err){

                                      console.log('error' + JSON.stringify(err));

                                });

                       }else{

                                swal.fire({

                                    title               : 'Se cancelo la eliminación!',

                                    imageUrl            : 'assets/img/error.png',

                                    showCloseButton     : true,

                                    confirmButtonColor  : '#044BFD'

                                });
                 
                       }
             
              });
        
        };

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                                                //
//                           C R U D   D E   C O N T A C T O S    D E    L A    E M P R E S A                                                     //
//                                                                                                                                                //      
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $scope.lista = function(id){

              $scope.contacto.nIDEmpresa = id;

              document.getElementById('Visible').style.display = 'none';
              document.getElementById('VisibleModificar').style.display = 'none';

              $scope.getContacto(id);

        };

        $scope.getContacto = function(id){

            $scope.contactos = '';
            $scope.mensajeC  = '';
              contactServices.get(id).then(function(resp){

                    

                    if(resp.data == 'null'){

                           $scope.mensajeC = 'No tienes contactos';

                    }else{

                          $scope.contactos = resp.data;
                    }

              },function(err){

                    console.log('error' + JSON.stringify(err));

              });

        };

        $scope.mostrar = function(){

              document.getElementById('Visible').style.display = 'block';
        };

        $scope.addContacto = function(contacto){

               if(contacto.Nombre.length == 0){

                          swal.fire({

                               title                : 'Oops!',

                               text                 : 'Nombre del contacto vacio completalo',

                               imageUrl             : 'assets/img/advertencia.png',

                               showCloseButton      : true,

                               confirmButtonColor   : "#044BFD"

                          });

               }else if(!expTelefono.test(contacto.Telefono)){

                                    swal.fire({

                                        title       : 'Oops!',

                                        text        : 'Número de teléfono no cumple con las especificaciones, inténtalo de nuevo',

                                        imageUrl    : 'assets/img/advertencia.png',

                                        showCloseButton : true,

                                        confirmButtonColor : '#044BFD'

                                    });

               }else if(!expEmail.test(contacto.Email)){

                                    swal.fire({

                                        title       : 'Oops!',

                                        text        : 'Email no cumple con las especificaciones, inténtalo de nuevo',

                                        imageUrl    : 'assets/img/advertencia.png',

                                        showCloseButton : true,

                                        confirmButtonColor : '#044BFD'

                                    });

               }else{

                    contactServices.add(contacto).then(function(resp){

                                    if(resp.data == "INSERTED"){

                                           swal.fire({

                                                title         : 'Registro exitoso!',

                                                imageUrl      : 'assets/img/completo.png',

                                                showCloseButton : true,

                                                confirmButtonColor : '#044BFD'

                                           });

                                           $scope.getContacto(contacto.nIDEmpresa);

                                           $scope.cleanContacto(contacto.nIDEmpresa);

                                    }else{

                                          swal.fire({

                                                title             : 'Oops!',

                                                text              : 'No se logro el registro, inténtalo de nuevo',

                                                imageUrl          : 'assets/img/error.png',

                                                showCloseButton   : true,

                                                confirmButtonColor: '#044BFD'
                                          
                                          });
                                    
                                    }
                    },function(err){

                          console.log('error' + JSON.stringify(err));

                    });

               }

        };

        $scope.showContacto = function(contacto){

              document.getElementById('VisibleModificar').style.display = 'block';

              $scope.contactoEdit.nIDContacto = contacto.nIDContacto;
              $scope.contactoEdit.Nombre      = contacto.Nombre;
              $scope.contactoEdit.Telefono    = contacto.Telefono;
              $scope.contactoEdit.Email       = contacto.Email;
              $scope.contactoEdit.nIDEmpresa  = contacto.nIDEmpresa;


        };


        $scope.updateContacto = function(contacto){

              if(!expTelefono.test(contacto.Telefono)){

                             swal.fire({

                                  title               : 'Oops!',

                                  text                : 'Número de teléfono no cumple con las especificaciones, inténtalo de nuevo',

                                  imageUrl            : 'assets/img/advertencia.png',

                                  showCloseButton     : true,

                                  confirmButtonColor  : '#044BFD'

                             });

              }else if(!expEmail.test(contacto.Email)){

                                swal.fire({

                                     title                : 'Oops!',

                                     text                 : 'Email no cumple con las especificaciones, inténtalo de nuevo',

                                     imageUrl             : 'assets/img/advertencia.png',

                                     showCloseButton      : true,

                                     confirmButtonColor   : '#044BFD'

                                });

              }else if(contacto.Nombre.length == 0){

                               swal.fire({

                                   title              : 'Oops!',

                                   text               : 'Nombre vacio completalo',

                                   imageUrl           : 'assets/img/advertencia.png',

                                   showCloseButton    : true,

                                   confirmButtonColor : '#044BFD'

                               });

              }else{

                    contactServices.update(contacto).then(function(resp){

                                    if(resp.data == "UPDATED"){

                                           swal.fire({

                                                title              : 'Modificación exitosa!',

                                                imageUrl           : 'assets/img/completo.png',

                                                showCloseButton    : true,

                                                confirmButtonColor : '#044BFD'

                                           });

                                           $scope.getContacto(contacto.nIDEmpresa);

                                           $scope.cleanContactoEdit(contacto.nIDEmpresa);

                                    }else{

                                          swal.fire({

                                               title              : 'Oops!',

                                               text               : 'No se logro el registro, inténtalo de nuevo',

                                               imageUrl           : 'assets/img/error.png',

                                               showCloseButton    : true,

                                               confirmButtonColor : '#044BFD'

                                          });

                                    }

                    },function(err){

                            console.log('error' + JSON.stringify(err));

                    });

              }

        };


        $scope.destroyContacto = function(id,nIDEmpresa){

              swal.fire({

                  title                 : 'ADVERTENCIA',

                  text                  : '¿Estás seguro de eliminar este registro?',

                  imageUrl              : 'assets/img/advertencia.png',

                  showCancelButton      : true,

                  showCloseButton       : true,

                  confirmButtonColor    : '#044BFD',

                  cancelButtonColor     : 'rgba(4,75,253,0.5)',

                  confirmButtonText     : 'Si',

                  cancelButtonText      : 'Cancelar'

              }).then((result) => {

                      if(result.value){

                                contactServices.destroy(id).then(function(resp){

                                                if(resp.data == "DELETED"){

                                                        $scope.getContacto(nIDEmpresa);

                                                        swal.fire({

                                                            title             : 'Registro eliminado con exito!',

                                                            imageUrl          : 'assets/img/completo.png',

                                                            showCloseButton   : true,

                                                            confirmButtonColor: '#044BFD'

                                                        });

                                                }else{

                                                        swal.fire({

                                                            title                 : 'Oops!',

                                                            text                  : 'No se pudo eliminar el registro, inténtalo de nuevo',

                                                            imageUrl              : 'assets/img/error.png',

                                                            showCloseButton       : true,

                                                            confirmButtonColor    : '#044BFD'

                                                        });

                                                }

                                }, function(err){

                                    console.log('error' + JSON.stringify(err));

                                });

                      }else{

                            swal.fire({

                                title               : 'Se cancelo la eliminación!',

                                imageUrl            : 'assets/img/error.png',

                                showCloseButton     : true,

                                confirmButtonColor  : '#044BFD'

                            });

                      }

              });


        };





        }]);  



