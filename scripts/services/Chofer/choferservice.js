'use strict';

angular.module('abgpassportApp')

       .service('choferServices',['$http','configService', function($http,configService){

       return{

            get : function(){

                return $http.get(configService.apiUrl + 'Chofer.php?accion=consultar',

                  { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                );
            },

            getChofer : function(id){

                      return $http.post(configService.apiUrl + 'Chofer.php','accion=consultarID&id=' + JSON.stringify(id),

                        { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } } 

                      );

            },

            add : function(chofer){

                return $http.post(configService.apiUrl + 'Chofer.php','accion=agregar&chofer=' + JSON.stringify(chofer),

                  { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                );

            },

            update : function(chofer){

                return $http.post(configService.apiUrl + 'Chofer.php','accion=modificar&chofer=' + JSON.stringify(chofer),

                  { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                );

            },

            destroy : function(id){

                return $http.post(configService.apiUrl +'Chofer.php','accion=eliminar&id=' + JSON.stringify(id),

                  { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                );

            }
       };

       }]);