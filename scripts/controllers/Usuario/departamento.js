'use strict';

angular.module('abgpassportApp')
  
       .controller('DepartamentCtrl',['$scope','departamentServices',function($scope,departamentServices){

       $scope.departamento = {
             nIDDepartamento     : '0',
             Departamento        : '',
             Descripcion         : ''

       };

       $scope.departamentoEdit = {

             nIDDepartamento     : '0',
             Departamento        : '',
             Descripcion         : ''

       };


       $scope.clean = function(){

             $scope.departamento = {

                   nIDDepartamento        : '0',
                   Departamento           : '',
                   Descripcion            : ''

             };

             $('#add_modal').modal('hide');

       };


       $scope.cleanEdit = function(){

              $scope.departamentoEdit = {

                    nIDDepartamento        : '0',
                    Departamento           : '',
                    Descripcion            : ''

              };

              $('#update_modal').modal('hide');

       };

       $scope.get = function(){

             departamentServices.get().then(function(resp){

                         if(resp.data == 'null'){

                                $scope.icono   = 'assets/img/hecho.png';
                                $scope.mensaje = 'No tienes departamentos registrados';

                         }else{

                              $scope.departamentos =  resp.data;

                         }

             },function(err){

                console.log('error' + JSON.stringify(err));

             });

       };

       $scope.add = function(departamento){

             if(departamento.Departamento.length == 0){

                            swal.fire({

                                title              : 'Oops!',

                                text               : 'Departamento vacio completalo',

                                imageUrl           : 'assets/img/advertencia.png',

                                showCloseButton    : true,

                                confirmButtonColor : '#044BFD'

                            });

             }else if(departamento.Descripcion.length == 0){

                                  swal.fire({

                                      title                : 'Oops!',

                                      text                 : 'Descripcion vacio completalo',

                                      imageUrl             : 'assets/img/advertencia.png',

                                      showCloseButton      : true,

                                      confirmButtonColor   : '#044BFD'

                                  });  

             }else{


                      departamentServices.add(departamento).then(function(resp){

                                          if(resp.data == 'INSERTED'){

                                                  swal.fire({

                                                      title            : 'Registro exitoso!',

                                                      imageUrl         : 'assets/img/completo.png',

                                                      showCloseButton  : true,

                                                      confirmButtonColor : '#044BFD'

                                                  });

                                                  $scope.get();

                                                  $scope.clean();

                                          }else{

                                                  swal.fire({

                                                      title                 : 'Oops!',
 
                                                      text                  : 'No se logro el registro,inténtalo de nuevo',

                                                      imageUrl              : 'assets/img/error.png',

                                                      showCloseButton       : true,

                                                      confirmButtonColor    : '#044BFD'

                                                  });

                                          }

                      },function(err){

                                console.log('error' + JSON.stringify(err));

                      });

             }

       };


       $scope.show = function(departamento){
        
             $scope.departamentoEdit.nIDDepartamento     = departamento.nIDDepartamento;
             $scope.departamentoEdit.Departamento        = departamento.Departamento;
             $scope.departamentoEdit.Descripcion         = departamento.Descripcion;
       };

       $scope.update = function(departamento){

              if(departamento.Departamento.length == 0){

                              swal.fire({

                                  title              : 'Oops!',

                                  text               : 'Departamento vacio completalo',

                                  imageUrl           : 'assets/img/advertencia.png',

                                  showCloseButton    : true,

                                  confirmButtonColor : '#044BFD'

                              });

             }else if(departamento.Descripcion.length == 0){

                                  swal.fire({

                                      title                : 'Oops!',

                                      text                 : 'Descripcion vacio completalo',

                                      imageUrl             : 'assets/img/advertencia.png',

                                      showCloseButton      : true,

                                      confirmButtonColor   : '#044BFD'

                                  });

             }else{

                        departamentServices.update(departamento).then(function(resp){

                                    if(resp.data == 'UPDATED'){

                                           swal.fire({

                                                title              : 'Modificación exitosa!',

                                                imageUrl           : 'assets/img/hecho.png',

                                                showCloseButton    : true,

                                                confirmButtonColor : '#044BFD'

                                           });

                                           $scope.cleanEdit();

                                           $scope.get();

                                    }else{

                                           swal.fire({

                                               title                : 'Oops!',

                                               text                 : 'No se completo la modificación, inténtalo de nuevo',

                                               imageUrl             : 'assets/img/error.png',

                                               showCloseButton      : true,

                                               confirmButtonColor   : '#044BFD'

                                           });

                                    }

                        },function(err){

                                  console.log('error' + JSON.stringify(err));

                        });

             }
       
       };


       $scope.destroy = function(id){

              swal.fire({

                  title               : 'ADVERTENCIA',

                  text                : '¿Estás seguro de eliminar este registro?',

                  imageUrl            : 'assets/img/advertencia.png',

                  showCancelButton    : true,

                  showCloseButton     : true,                  

                  cancelButtonColor   : 'rgba(4,75,253,0.5)',                  

                  confirmButtonColor  : '#044BFD',

                  cancelButtonText    : 'Cancelar',

                  confirmButtonText   : 'Si',

              }).then((result) => {

                       if(result.value){

                                departamentServices.destroy(id).then(function(resp){

                                              if(resp.data == "DELETED"){

                                                      swal.fire({

                                                          title              : 'Registro eliminado con exito!',

                                                          imageUrl           : 'assets/img/completo.png',

                                                          showCloseButton    : true,

                                                          confirmButtonColor : '#044BFD'

                                                      });

                                                      $scope.get();

                                              }else{

                                                      swal.fire({

                                                          title             : 'Oops!',

                                                          text              : 'No se pudo eliminar el registro inténtalo de nuevo',

                                                          imageUrl          : 'assets/img/error.png',

                                                          showCloseButton   : true,

                                                          confirmButtonColor: '#044BFD'

                                                      });

                                              }

                                },function(err){

                                      console.log('error' + JSON.stringify(err));

                                });

                       }else{

                                swal.fire({

                                    title               : 'Se cancelo la eliminicación!',

                                    imageUrl            : 'assets/img/error.png',

                                    showCloseButton     : true,

                                    confirmButtonColor  : '#044BFD'

                                });
                 
                       }
             
              });
        
        };


       }]);