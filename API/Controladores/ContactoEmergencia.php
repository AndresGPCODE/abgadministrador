<?php

    error_reporting(E_ALL ^ E_NOTICE);
    ini_set('display_errors','1');

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Request-Methods');

      include_once '../Configuraciones/Conexion.php';
      include_once '../Cliente/clContactoEmergencia.php';
      include_once '../Utilerias/clHerramientas_v2011.php';

      
      $method = $_SERVER['REQUEST_METHOD'];

      $UtileriasDatos   = new clHerramientasv2011();
      $fechaLocal       = $UtileriasDatos->getFechaYHoraActual_General();
      $fechaLocal       = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
      
      $contactEmergency = new clContactoEmergencia();

      
      $contactEmergency->conexion($SERVER,$USER,$PASSWORD,$BD);

      switch ($method) {
              
              case 'POST':
                    
                    $action = $_POST['accion'];

                    if($action == 'contactos'){

                              $id = $_POST['id'];

                              $condicion = ' bEstado = 1 and  nIDCliente = ' . $id;

                              if($contactEmergency->consultarCondicion($condicion) == false){

                                      echo 'null';
                              }else{

                                      echo json_encode($contactEmergency->consultarCondicion($condicion));
                              }
                    }else if($action == 'agregar'){

                              $objeto = $_POST['contacto'];

                              $data = json_decode($objeto);

                              $contactEmergency->setInformacion(
                                      $data->nIDContactoEmergencia,
                                      $data->Nombre,
                                      $data->Telefono,
                                      $data->Calle,
                                      $data->NoExterior,
                                      $data->NoInterior,
                                      $data->Colonia,
                                      $data->Pais,
                                      $data->nIDCliente,
                                      '',
                                      '',
                                      'Nuevo contacto de emergencia - ' . $fechaLocal,
                                      '1',
                                      TRUE,
                                      FALSE,
                                      FALSE
                              );

                              if($contactEmergency->ejecutar('tbl_cat_contacto_emergencia')){

                                        echo "INSERTED";

                              }else{

                                       echo "NOT INSERTED";
                              }

                    }else if($action == 'modificar'){

                              $objeto = $_POST['contacto'];

                              $data   = json_decode($objeto);

                              $contactEmergency->setInformacion(

                                      $data->nIDContactoEmergencia,
                                      $data->Nombre,
                                      $data->Telefono,
                                      $data->Calle,
                                      $data->NoExterior,
                                      $data->NoInterior,
                                      $data->Colonia,
                                      $data->Pais,
                                      $data->nIDCliente,
                                      '',
                                      '',
                                      'Modificacion contacto de emergencia - ' . $fechaLocal,
                                      '1',
                                      FALSE,
                                      TRUE,
                                      FALSE
                              );

                              if($contactEmergency->ejecutar('tbl_cat_contacto_emergencia')){

                                        echo "UPDATED";
                              
                              }else{

                                        echo "NOT UPDATED";

                              }
                    }else if($action == 'eliminar'){

                              $id = $_POST['id'];

                      $observacion = 'Contacto de emergencia eliminado temporalmente - ' . $fechaLocal;

                      if($contactEmergency->ocultar($id,$observacion)){

                            echo "DELETED";
                      }else{

                            echo "NOT DELETED";
                      }
                              
                    }
      }
?>