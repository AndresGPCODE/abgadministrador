'use strict'; 

angular.module('abgpassportApp')

       .controller('RoleCtrl',['$scope','roleServices','agencyServices', function($scope,roleServices,agencyServices){

       var expEmail    = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;

       $scope.rol = {
             nIDRol        : '0',
             TipoDeRol     : '',
             Descripcion   : '',
             Clasificacion : ''

       };

       $scope.rolEdit = {

             nIDRol        : '0',
             TipoDeRol     : '',
             Descripcion   : '',
             Clasificacion : ''

       };


       $scope.clean = function(){

             $scope.rol = {

                   nIDRol           : '0',
                   TipoDeRol        : '',
                   Descripcion      : '',
                   Clasificacion    : ''

             };

             $('#add_modal').modal('hide');

       };


       $scope.cleanEdit = function(){

              $scope.rolEdit = {

                    nIDRol           : '0',
                    TipoDeRol        : '',
                    Descripcion      : '',
                    Clasificacion    : '',

              };

              $('#update_modal').modal('hide');

       };

       $scope.get = function(){

            $scope.getEmpresa();

             roleServices.get().then(function(resp){

                         if(resp.data == 'null'){

                                $scope.icono   = 'assets/img/hecho.png';
                                $scope.mensaje = 'No tienes roles registrados';

                         }else{

                              $scope.roles =  resp.data;

                         }

             },function(err){

                console.log('error' + JSON.stringify(err));

             });

       };

       $scope.getEmpresa = function(){

             agencyServices.get().then(function(resp){

                           if(resp.data == null ){


                           }else{

                                $scope.empresas = resp.data;

                           }
             
             }, function(err){

                    console.log('error' + JSON.stringify(err));

             });
             

       };

       $scope.add = function(rol){

             if(rol.TipoDeRol.length == 0){

                   swal.fire({

                       title              : 'Oops!',

                       text               : 'Tipo de rol vacio completalo',

                       imageUrl           : 'assets/img/advertencia.png',

                       showCloseButton    : true,

                       confirmButtonColor : '#044BFD'

                   });

             }else if(rol.Descripcion.length == 0){

                         swal.fire({

                             title                : 'Oops!',

                             text                 : 'Descripcion vacio completalo',

                             imageUrl             : 'assets/img/advertencia.png',

                             showCloseButton      : true,

                             confirmButtonColor   : '#044BFD'

                         });

             }else if(rol.Clasificacion.length == 0){

                         swal.fire({

                             title                  : 'Oops!',

                             text                   : 'Clasificación vacio completalo',

                             imageUrl               : 'assets/img/advertencia.png',

                             showCloseButton        : true,

                             confirmButtonColor     : '#044BFD'

                         });

             }else{


                      roleServices.add(rol).then(function(resp){

                                  if(resp.data == 'INSERTED'){

                                         swal.fire({

                                             title            : 'Registro exitoso!',

                                             imageUrl         : 'assets/img/completo.png',

                                             showCloseButton  : true,

                                             confirmButtonColor : '#044BFD'

                                         });

                                         $scope.get();

                                         $scope.clean();

                                  }else{

                                        swal.fire({

                                            title                 : 'Oops!',
 
                                            text                  : 'No se logro el registro,inténtalo de nuevo',

                                            imageUrl              : 'assets/img/error.png',

                                            showCloseButton       : true,

                                            confirmButtonColor    : '#044BFD'

                                        });

                                  }

                      },function(err){

                                console.log('error' + JSON.stringify(err));

                      });

             }

       };


       $scope.show = function(rol){
        console.log(rol);
             $scope.rolEdit.nIDRol        = rol.nIDRol;
             $scope.rolEdit.TipoDeRol     = rol.TipoDeRol;
             $scope.rolEdit.Descripcion   = rol.Descripcion;             
             $scope.rolEdit.Clasificacion = rol.Clasificacion;
         
       };

       $scope.update = function(rol){

              if(rol.TipoDeRol.length == 0){

                   swal.fire({

                       title              : 'Oops!',

                       text               : 'Tipo de rol vacio completalo',

                       imageUrl           : 'assets/img/advertencia.png',

                       showCloseButton    : true,

                       confirmButtonColor : '#044BFD'

                   });

             }else if(rol.Descripcion.length == 0){

                         swal.fire({

                             title                : 'Oops!',

                             text                 : 'Descripcion vacio completalo',

                             imageUrl             : 'assets/img/advertencia.png',

                             showCloseButton      : true,

                             confirmButtonColor   : '#044BFD'

                         });

             }else if(rol.Clasificacion.length == 0){

                         swal.fire({

                             title                  : 'Oops!',

                             text                   : 'Clasificación vacio completalo',

                             imageUrl               : 'assets/img/advertencia.png',

                             showCloseButton        : true,

                             confirmButtonColor     : '#044BFD'

                         });

             }else{

                        roleServices.update(rol).then(function(resp){

                                    if(resp.data == 'UPDATED'){

                                           swal.fire({

                                                title              : 'Modificación exitosa!',

                                                imageUrl           : 'assets/img/hecho.png',

                                                showCloseButton    : true,

                                                confirmButtonColor : '#044BFD'

                                           });

                                           $scope.cleanEdit();

                                           $scope.get();

                                    }else{

                                           swal.fire({

                                               title                : 'Oops!',

                                               text                 : 'No se completo la modificación, inténtalo de nuevo',

                                               imageUrl             : 'assets/img/error.png',

                                               showCloseButton      : true,

                                               confirmButtonColor   : '#044BFD'

                                           });

                                    }

                        },function(err){

                                  console.log('error' + JSON.stringify(err));

                        });

             }
       
       };


       $scope.destroy = function(id){

              swal.fire({

                  title               : 'ADVERTENCIA',

                  text                : '¿Estás seguro de eliminar este registro?',

                  imageUrl            : 'assets/img/advertencia.png',

                  showCancelButton    : true,

                  showCloseButton     : true,                  

                  cancelButtonColor   : 'rgba(4,75,253,0.5)',                  

                  confirmButtonColor  : '#044BFD',

                  cancelButtonText    : 'Cancelar',

                  confirmButtonText   : 'Si',

              }).then((result) => {

                       if(result.value){

                                roleServices.destroy(id).then(function(resp){

                                              if(resp.data == "DELETED"){

                                                      swal.fire({

                                                          title              : 'Registro eliminado con exito!',

                                                          imageUrl           : 'assets/img/completo.png',

                                                          showCloseButton    : true,

                                                          confirmButtonColor : '#044BFD'

                                                      });

                                                      $scope.get();

                                              }else{

                                                      swal.fire({

                                                          title             : 'Oops!',

                                                          text              : 'No se pudo eliminar el registro inténtalo de nuevo',

                                                          imageUrl          : 'assets/img/error.png',

                                                          showCloseButton   : true,

                                                          confirmButtonColor: '#044BFD'

                                                      });

                                              }

                                },function(err){

                                      console.log('error' + JSON.stringify(err));

                                });

                       }else{

                                swal.fire({

                                    title               : 'Se cancelo la eliminicación!',

                                    imageUrl            : 'assets/img/error.png',

                                    showCloseButton     : true,

                                    confirmButtonColor  : '#044BFD'

                                });
                 
                       }
             
              });
        
        };


       }]);
       