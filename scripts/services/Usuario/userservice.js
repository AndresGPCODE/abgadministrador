'use strict';

angular.module('abgpassportApp')
       
       .service('userServices',['$http','configService', function($http,configService){

        return{

              get : function(){

                  return $http.get(configService.apiUrl + 'Usuario.php?accion=consultar',

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                  );

              },

              add : function(usuario){

                  return $http.post(configService.apiUrl + 'Usuario.php','accion=agregar&usuario=' + JSON.stringify(usuario),

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                  );

              },

              obtenerUsuario : function(id){

                  return $http.post(configService.apiUrl + 'Usuario.php','accion=buscarUsuario&id=' + JSON.stringify(id),

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                  );

              },

              update : function(usuario){

                  return $http.post(configService.apiUrl + 'Usuario.php','accion=modificar&usuario=' + JSON.stringify(usuario),

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                  );

              },

              changeP: function(password){ console.log(password);
                return $http.post(configService.apiUrl + 'Usuario.php','accion=change&password=' + JSON.stringify(password),
                  { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                  );
              },


              destroy : function(id){

                  return $http.post(configService.apiUrl + 'Usuario.php','accion=eliminar&id=' + JSON.stringify(id),

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                  );

              }
        
        };

       }]);