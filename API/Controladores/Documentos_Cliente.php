<?php
    
  error_reporting(E_ALL ^ E_NOTICE);
  ini_set('display_errors','1');

  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Request-Methods');

  include_once '../Configuraciones/Conexion.php';
  include_once '../Documento/clDocumentosCliente.php';
  include_once '../Utilerias/clHerramientas_v2011.php';

  $method = $_SERVER['REQUEST_METHOD'];

  $UtileriasDatos    = new clHerramientasv2011();
  $fechaLocal        = $UtileriasDatos->getFechaYHoraActual_General();
  $fechaLocal        = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

  $documentC = new clDocumentosClientes();

  $documentC->conexion($SERVER,$USER,$PASSWORD,$BD);

  switch($method){


      case 'POST' :

            $action = $_POST['accion'];

            if($action == 'buscar'){

                      $id = $_POST['id'];

                      $nIDCliente = str_replace('"', '', $id);

                      $documentC->leerCondicion("tbl_cat_documentos_cliente.bEstado = 1 and tbl_cat_documentos_cliente.nIDCliente = " . $nIDCliente);

                      $registros = $documentC->dtBase();

                      echo json_encode($registros);


            }else if($action == 'agregar'){

                      $object = $_POST['objeto'];

                      $data   = json_decode($object);

                      $documentC->setInformacion(

                                  '0',
                                  $data->nIDDocumento, 
                                  '',
                                  '',
                                  '',
                                  $data->nIDCliente,
                                  '',
                                  '',
                                  'Nuevo documento del cliente ' . $data->nIDCliente . ' - ' . $fechaLocal,
                                  '1',
                                  TRUE,
                                  FALSE,
                                  FALSE

                      );

                      if($documentC->ejecutar('tbl_cat_documentos_cliente')){

                                      echo "INSERTED";

                      }else{

                                      echo "NOT INSERTED";
                      
                      }
            
            }else if($action == 'eliminar'){

                              $id = $_POST['id'];

                              $observacion = 'Documento eliminado temporalmente - ' . $fechaLocal;

                              if($documentC->ocultar($id,$observacion)){

                                            echo "DELETED";

                              }else{

                                            echo "NOT DELETED";

                              }
                              
            }
  }
?>