'use strict';

angular.module('abgpassportApp')
       
       .service('documentclientServices', ['$http','configService', function($http,configService){
                

                return {

                      get : function(id){

                          return $http.post(configService.apiUrl + 'Documentos_Cliente.php','accion=buscar&id=' + JSON.stringify(id),

                            { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                          );

                      },

                      add : function(objeto){

                          return $http.post(configService.apiUrl + 'Documentos_Cliente.php','accion=agregar&objeto=' + JSON.stringify(objeto),

                            { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                          );

                      },

                      update : function(objeto){

                             return $http.post(configService.apiUrl + 'Documentos_Cliente.php','accion=modificar&objeto=' + JSON.stringify(objeto),

                              { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                            );

                      },

                      destroy : function(id){

                             return $http.post(configService.apiUrl + 'Documentos_Cliente.php','accion=eliminar&id=' + JSON.stringify(id),

                              { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                            );
                             
                      }
                };

       }]);