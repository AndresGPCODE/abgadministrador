<?php

  error_reporting(E_ALL ^ E_NOTICE);  

  ini_set('display_errors', '1');

  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Request-Methods');

  include_once '../Configuraciones/Conexion.php';
  include_once '../Cliente/clCliente.php';
  include_once '../Utilerias/clHerramientas_v2011.php';

  $method = $_SERVER['REQUEST_METHOD'];

  $UtileriasDatos = new clHerramientasv2011();
  $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
  $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

  $client = new clCliente();

  $client->conexion($SERVER,$USER,$PASSWORD,$BD);

  switch($method){

        case 'GET' : 

            $action = $_GET['accion'];

            if($action == 'consultar'){

                  $client->leerCondicion("tbl_clientes.bEstado = 1 ");

                  $registros = $client->dtBase();

                  echo json_encode($registros);

                  break;
            }

        case 'POST' : 

              $action = $_POST['accion'];

              if($action == 'agregar'){

                    $object = $_POST['cliente'];

                    $data = json_decode($object);

                    $Foto = str_replace(' ', '+', $data->Foto);

                    $path = $data->Foto;

                    // Extensión de la imagen
                    $type = pathinfo($path, PATHINFO_EXTENSION);

                    // Cargando la imagen
                    $imagen = file_get_contents($path);

                    // Decodificando la imagen en base64
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($imagen);

                    // Mostrando la imagen
                    //echo '<img src="'.$base64.'"/>';

                    // Mostrando el código base64
                    //echo $base64;

                    $email  = str_replace('"','',$data->Email); 

                      if($client->consultaGeneralEmail(' tbl_clientes ',' tbl_clientes.Email ="' . $email . '" ' )){

                              echo 'EXISTE';

                      }else if($client->consultaGeneralEmail(' tbl_usuarios ',' tbl_usuarios.Email ="' . $email . '" ' )){

                              echo 'EXISTE';

                      }else if($client->consultaGeneralEmail(' tbl_choferes ',' tbl_choferes.Email ="' . $email . '" ')){

                              echo 'EXISTE';
                      
                      }else{

                    $client->setInformacion(

                            $data->nIDCliente,
                            $data->Nombre,
                            $data->Usuario,
                            $data->Password,
                            $data->RFC,
                            $data->Email,
                            $data->Celular,
                            $base64,
                            $data->Calle,
                            $data->NoExterior,
                            $data->NoInterior,
                            $data->Colonia,
                            $data->Cp,
                            '',
                            $data->NSS,
                            $data->NoLicencia,
                            $data->FechaVigencia_Licencia,
                            $data->NoTarjeta,
                            $data->FechaVigencia_Tarjeta,
                            $data->NombreTitular,
                            $data->nIDUsuario,
                            '',
                            '',
                            '',
                            '',
                            'Se agrego un nuevo usuario - ' . $fechaLocal,
                            '1',
                            TRUE,
                            FALSE,
                            FALSE

                    );

                    if($client->ejecutar('tbl_clientes')){

                              echo $client->mostrarID();

                    }else{

                              echo "NOT INSERTED";
                    }

                  }
                  
              }else if($action == 'modificar'){

                    $object = $_POST['cliente'];

                    $data = json_decode($object);

                    $Foto = str_replace(' ', '+', $data->Foto);

                    //$path = $data->Foto;

                    // Extensión de la imagen
                    //$type = pathinfo($path, PATHINFO_EXTENSION);

                    // Cargando la imagen
                    //$imagen = file_get_contents($path);

                    // Decodificando la imagen en base64
                    //$base64 = 'data:image/' . $type . ';base64,' . base64_encode($imagen);

                    // Mostrando la imagen
                    //echo '<img src="'.$base64.'"/>';

                    // Mostrando el código base64
                    //echo $base64;

                    $client->setInformacion(

                            $data->nIDCliente,
                            $data->Nombre,
                            $data->Usuario,
                            $data->Password,
                            $data->RFC,
                            $data->Email,
                            $data->Celular,
                            $Foto,
                            $data->Calle,
                            $data->NoExterior,
                            $data->NoInterior,
                            $data->Colonia,
                            $data->Cp,
                            '',
                            $data->NSS,
                            $data->NoLicencia,
                            $data->FechaVigencia_Licencia,
                            $data->NoTarjeta,
                            $data->FechaVigencia_Tarjeta,
                            $data->NombreTitular,
                            $data->nIDUsuario,
                            '',
                            '',
                            '',
                            '',
                            'Usuario modificado - ' . $fechaLocal,
                            '1',
                            FALSE,
                            TRUE,
                            FALSE

                    );

                    if($client->ejecutar('tbl_clientes')){

                              echo "UPDATED";

                    }else{

                              echo "NOT UPDATED";
                    }
              }else if($action == 'eliminar'){

                   $id = $_POST['id'];

                      $observacion = 'cliente eliminado temporalmente - ' . $fechaLocal;

                      if($client->ocultar($id,$observacion)){

                            echo "DELETED";
                      }else{

                            echo "NOT DELETED";
                      }
              }
  }
?>