<?php
/*
------ ESTADO DE CUENTA DEL CLIENTE, SI TIENE ADEUDOS O SI YA LOS PAGO
*/
header('Content-Type: application/json');
date_default_timezone_set('America/Mexico_City');
$method = $_SERVER["REQUEST_METHOD"];
// $method = 'POST';
include 'conexion.php';


switch($method){
    case 'POST':
        $request = json_decode(file_get_contents('php://input'), true);
        // $request['option'] = 1;
        // $request['nIDCliente'] = 3;
        if(is_null($request['nIDCliente']) || is_null($request['option'])){
            $resp['status'] = false;
            $resp['resp'] = "No se reciieron los datos necesarios para la operacion";
            return json_encode($resp);
        } else {
            $resp = obtenerEstadoCuenta($request['nIDCliente']);
            if ($resp[0]['status']) {
                // return true
                echo json_encode($resp);
                return json_encode($resp);
            } else {
                // return false
                echo json_encode($resp);
                return json_encode($resp);
            }
        }
    break;

    default:
    break;
}


function obtenerEstadoCuenta($idCliente){
    $pdo = Conexion();
    $select = "SELECT * FROM tbl_estado_cuenta WHERE nIDCliente = ".$idCliente. " AND Estatus = 'NO PAGADO'";
    $ejecutar = $pdo->prepare($select);
    $ejecutar->execute();
    $result = $ejecutar->fetchAll(PDO::FETCH_ASSOC);
    $arr = [];
    if (count($result) > 0) {
        foreach ($result as $row) {
            $row['status'] = true;
            array_push($arr, $row);
        }
        return $arr;
    } else {
        $row['status'] = false;
        $row['resp'] = "No hay estados de cuenta pendientes";
        array_push($arr, $row);
        return $arr;
    }
}