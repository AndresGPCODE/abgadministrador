'use strict'

angular.module('abgpassportApp')
        
       .controller('ClientCtrl',['$scope','clientServices','contactEmergencyServices','userServices',

        function($scope,clientServices,contactEmergencyServices,userServices){

       var expRFC      = /^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\d]{3})?$/;

        var expEmail    = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;

        var expTelefono = /^[0-9]{10}$/;

        $scope.cliente = {

              nIDCliente             : '0',
              Nombre                 : '',
              RFC                    : '',
              Usuario                : '',
              Password               : '',
              Celular                : '',
              Foto                   : '',
              Email                  : '',
              Calle                  : '',
              NoExterior             : '',
              NoInterior             : '',
              Colonia                : '',
              Cp                     : '',
              NSS                    : '',
              NoLicencia             : '',
              FechaVigencia_Licencia : '',
              NoTarjeta              : '',
              FechaVigencia_Tarjeta  : '',
              NombreTitular          : '',
              nIDUsuario             : ''

        };

        $scope.clienteEdit = {

              nIDCliente             : '0',
              Nombre                 : '',
              RFC                    : '',
              Usuario                : '',
              Password               : '',
              Celular                : '',
              Foto                   : '',
              Email                  : '',
              Calle                  : '',
              NoExterior             : '',
              NoInterior             : '',
              Colonia                : '',
              Cp                     : '',
              NSS                    : '',
              NoLicencia             : '',
              FechaVigencia_Licencia : '',
              NoTarjeta              : '',
              FechaVigencia_Tarjeta  : '',
              NombreTitular          : '',
              nIDUsuario             : ''

        };


        $scope.contacto = {

              nIDContactoEmergencia : '0',
              Nombre                : '',
              Telefono              : '',
              Calle                 : '',
              NoExterior            : '',
              NoInterior            : '',
              Colonia               : '',
              Pais                  : '',
              nIDCliente            : ''

        };

        $scope.contactoEdit = {

              nIDContactoEmergencia : '0',
              Nombre                : '',
              Telefono              : '',
              Calle                 : '',
              NoExterior            : '',
              NoInterior            : '',
              Colonia               : '',
              Pais                  : '',
              nIDCliente            : ''

        };

        $scope.clean = function(){

              $scope.cliente = {

                    nIDCliente             : '0',
                    Nombre                 : '',
                    RFC                    : '',
                    Usuario                : '',
                    Password               : '',
                    Celular                : '',
                    Foto                   : '',
                    Email                  : '',
                    Calle                  : '',
                    NoExterior             : '',
                    NoInterior             : '',
                    Colonia                : '',
                    Cp                     : '',
                    NSS                    : '',
                    NoLicencia             : '',
                    FechaVigencia_Licencia : '',
                    NoTarjeta              : '',
                    FechaVigencia_Tarjeta  : '',
                    NombreTitular          : '',
                    nIDUsuario             : ''

              };

                $('#add_modal').modal('hide');
        
        };


        $scope.cleanEdit = function(){

              $scope.clienteEdit = {

                    nIDCliente             : '0',
                    Nombre                 : '',
                    RFC                    : '',
                    Usuario                : '',
                    Password               : '',
                    Celular                : '',
                    Email                  : '',
                    Foto                   : '',
                    Calle                  : '',
                    NoExterior             : '',
                    NoInterior             : '',
                    Colonia                : '',
                    Cp                     : '',
                    NSS                    : '',
                    NoLicencia             : '',
                    FechaVigencia_Licencia : '',
                    NoTarjeta              : '',
                    FechaVigencia_Tarjeta  : '',
                    NombreTitular          : '',
                    nIDUsuario             : ''

              };

                  $('#update_modal').modal('hide');
        };


        $scope.cleanContacto = function(id){

              $scope.contacto = {

                    nIDContactoEmergencia : '0',
                    Nombre                : '',
                    Telefono              : '',
                    Calle                 : '',
                    NoExterior            : '',
                    NoInterior            : '',
                    Colonia               : '',
                    Pais                  : '',
                    nIDCliente            : id

              };

              document.getElementById('Visible').style.display = 'none';

        };

        $scope.cleanContactoEdit = function(id){

              $scope.contactoEdit = {

                    nIDContactoEmergencia : '0',
                    Nombre                : '',
                    Telefono              : '',
                    Calle                 : '',
                    NoExterior            : '',
                    NoInterior            : '',
                    Colonia               : '',
                    Pais                  : '',
                    nIDCliente            : id

              };

              document.getElementById('VisibleModificar').style.display = 'none';

        };

        $scope.get = function(){

          $scope.icono    = '';
          $scope.mensaje  = '';
          $scope.getUsuario();

              clientServices.get().then(function(resp){
                
                          if(resp.data == 'null'){

                              $scope.icono = '';
                              $scope.mensaje = 'No tienes clientes registrados';

                          }else{

                              $scope.clientes = resp.data;

                          }

              },function(err){

                  console.log('error' + JSON.stringify(err));

              });

        };

        $scope.getUsuario = function(){


              userServices.get().then(function(resp){

                          if(resp.data == 'null'){


                          }else{

                              $scope.usuarios = resp.data;

                          }

              },function(err){

                  console.log('error' + JSON.stringify(err));

              });

        };


        function ValidarExt() {

              var archivoInput = document.getElementById('Foto');

              var archivoRuta  = archivoInput.value;

              var expExtensiones = /(.PNG)|(.JPG)|(.JPEG)|(.png)|(.jpg)|(.jpeg)$/i;

              if(!expExtensiones.test(archivoRuta)){

                    swal.fire({

                      title           : 'Oops!',

                      text            : 'Solo puedes subir imagenes con extension .png, .jpg, .jpeg',

                      imageUrl        : 'assets/img/advertencia.png',

                      showCloseButton : true,

                      confirmButtonColor: '#044BFD'

                    });

                    archivoRuta.value = '';

                    return false;

              }else{ 

                    if(archivoInput.files && archivoInput.files[0]){ console.log(archivoInput.files,archivoInput.files[0]);

                        var visor = new FileReader(); 

                        visor.addEventListener("load" , function(e){console.log(e.target.result);

                            document.getElementById("doc").src = e.target.result;

                            $scope.cliente.Foto = e.target.result;

                        });

                        visor.readAsDataURL(archivoInput.files[0]);
                    }

              }

        }(function(){

              document.getElementById('Foto').addEventListener("change", ValidarExt);
        })();

        function ValidarExtEdit() {

              var archivoInput = document.getElementById('FotoEdit');

              var archivoRuta  = archivoInput.value;

              var expExtensiones = /(.PNG)|(.JPG)|(.JPEG)$/i;

              if(!expExtensiones.test(archivoRuta)){

                    swal.fire({

                      title           : 'Oops!',

                      text            : 'Solo puedes subir imagenes con extension .png, .jpg, .jpeg',

                      imageUrl        : 'assets/img/advertencia.png',

                      showCloseButton : true,

                      confirmButtonColor: '#044BFD'

                    });

                    archivoRuta.value = '';

                    return false;

              }else{ 

                    if(archivoInput.files && archivoInput.files[0]){

                        var visor = new FileReader();

                        visor.addEventListener("load" , function(e){ console.log(e.target.result);

                            document.getElementById("docEdit").src = e.target.result;

                            $scope.clienteEdit.Foto = e.target.result;

                        });

                        visor.readAsDataURL(archivoInput.files[0]);
                    }

              }

        }(function(){

              document.getElementById('FotoEdit').addEventListener("change", ValidarExtEdit);
        })();
        
        $scope.add = function(cliente){ 
              if(cliente.Nombre.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Nombre vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });
                    

              }else if(!expRFC.test(cliente.RFC)){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'RFC no cumple con las especificaciones, inténtalo de nuevo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(cliente.Usuario.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Nombre de usuario vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(cliente.Password.length == 0){

                              swal.fire({

                                  title             : 'Oops!',

                                  text              : 'Password vacio completalo',

                                  imageUrl          : 'assets/img/advertencia.png',

                                  showCloseButton   : true,

                                  confirmButtonColor: '#044BFD'

                              });

              }else if(!expTelefono.test(cliente.Celular)){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Número de celular no cumple con las especificaciones, inténtalo de nuevo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(!expEmail.test(cliente.Email)){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Email no cumple con las especificaciones, inténtalo de nuevo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(cliente.Calle.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Calle vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(cliente.NoExterior.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Número exterior vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(cliente.Cp.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Código postal vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(cliente.NSS.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Número de seguro social vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(cliente.NoLicencia.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Número de licencia vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else{

                    clientServices.add(cliente).then(function(resp){

                              

                                  if(resp.data == 'NOT INSERTED'){

                                         swal.fire({

                                             title                : 'Oops!',

                                             text                 : 'No se logro el registro, inténtalo de nuevo',

                                             imageUrl             : 'assets/img/error.png',

                                             showCloseButton      : true,

                                             confirmButtonColor   : '#044BFD'

                                         });

                                  }else if(resp.data == 'EXISTE'){

                                                swal.fire({

                                                    title                  : 'Correo electrónico existente!',

                                                    text                   : 'Ingresa un correo electrónico diferente',

                                                    imageUrl               : 'assets/img/advertencia.png',

                                                    showCloseButton        : true,

                                                    confirmButtonColor     : '#044BFD'

                                                });

                                  }else{

                                        

                                         swal.fire({

                                              title               : 'Registro exitoso!',

                                              imageUrl            : 'assets/img/completo.png',

                                              showCloseButton     : true,

                                              confirmButtonColor  : '#044BFD'

                                         });

                                         $scope.clean();

                                         $scope.get();

                                         $scope.contacto.nIDCliente = resp.data;

                                         $scope.obtenerUsuario(cliente.nIDUsuario);

                                  }

                    },function(err){

                          console.log('error' + JSON.stringify(err));

                    });

              }

        };


        $scope.show = function(cliente){

          console.log(cliente);

              $scope.clienteEdit.nIDCliente            = cliente.nIDCliente;
              $scope.clienteEdit.Nombre                = cliente.Nombre;
              $scope.clienteEdit.RFC                   = cliente.RFC;
              $scope.clienteEdit.Usuario               = cliente.Usuario;
              $scope.clienteEdit.Password              = cliente.Password;
              $scope.clienteEdit.Celular               = cliente.Celular;
              $scope.clienteEdit.Email                 = cliente.Email;
              $scope.clienteEdit.Calle                 = cliente.Calle;
              $scope.clienteEdit.NoExterior            = cliente.NoExterior;
              $scope.clienteEdit.NoInterior            = cliente.NoInterior;
              $scope.clienteEdit.Colonia               = cliente.Colonia;
              $scope.clienteEdit.Cp                    = cliente.Cp;
              $scope.clienteEdit.NSS                   = cliente.NSS;
              $scope.clienteEdit.NoLicencia            = cliente.NoLicencia;
              $scope.clienteEdit.FechaVigencia_Licencia= cliente.FechaVigencia_Licencia;
              $scope.clienteEdit.NoTarjeta             = cliente.NoTarjeta;
              $scope.clienteEdit.FechaVigencia_Tarjeta = cliente.FechaVigencia_Tarjeta;
              $scope.clienteEdit.NombreTitular         = cliente.NombreTitular;
              $scope.clienteEdit.nIDUsuario            = cliente.nIDUsuario;
              document.getElementById("FechaVigencia_LicenciaEdit").value = cliente.FechaVigencia_Licencia.substring(0,10);
              document.getElementById("FechaVigencia_TarjetaEdit").value = cliente.FechaVigencia_Tarjeta.substring(0,10);
              document.getElementById("docEdit").src   = cliente.Foto;
        
        };



        $scope.obtenerUsuario = function(id){

              userServices.obtenerUsuario(id).then(function(resp){

                          if(resp.data == 'null'){

                                 console.log('Not Found data');

                          }else{


                                $scope.contacto.Nombre     = resp.data[0]['Nombre'];
                                $scope.contacto.Telefono   = resp.data[0]['Celular'];
                                $scope.contacto.Calle      = resp.data[0]['Direccion'];

                                contactEmergencyServices.add($scope.contacto).then(function(resp){

                                      console.log(resp.data);

                                },function(err){

                                     console.log('error' + JSON.stringify(err));

                                });                                

                          }

              },function(err){

                    console.log('error' + JSON.stringify(err));

              });
        }


        $scope.update = function(cliente){  
        console.log(cliente);

              if(cliente.Nombre.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Nombre vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });
                    

              }else if(!expRFC.test(cliente.RFC)){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'RFC no cumple con las especificaciones, inténtalo de nuevo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(cliente.Usuario.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Nombre de usuario vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(cliente.Password.length == 0){

                              swal.fire({

                                  title             : 'Oops!',

                                  text              : 'Password vacio completalo',

                                  imageUrl          : 'assets/img/advertencia.png',

                                  showCloseButton   : true,

                                  confirmButtonColor: '#044BFD'

                              });

              }else if(!expTelefono.test(cliente.Celular)){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Número de celular no cumple con las especificaciones, inténtalo de nuevo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(!expEmail.test(cliente.Email)){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Email no cumple con las especificaciones, inténtalo de nuevo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(cliente.Calle.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Calle vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(cliente.NoExterior.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Número exterior vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(cliente.Cp.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Código postal vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(cliente.NSS.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Número de seguro social vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else if(cliente.NoLicencia.length == 0){

                         swal.fire({

                              title               : 'Oops!',

                              text                : 'Número de licencia vacio completalo',

                              imageUrl            : 'assets/img/advertencia.png',

                              showCloseButton     : true,

                              confirmButtonColor  : '#044BFD'

                         });

              }else{

                    clientServices.update(cliente).then(function(resp){                              

                                  if(resp.data == 'UPDATED'){

                                         swal.fire({

                                              title               : 'Registro exitoso!',

                                              imageUrl            : 'assets/img/completo.png',

                                              showCloseButton     : true,

                                              confirmButtonColor  : '#044BFD'

                                         });

                                         $scope.cleanEdit();

                                         $scope.get();

                                  }else{

                                         swal.fire({

                                             title                : 'Oops!',

                                             text                 : 'No se logro el registro, inténtalo de nuevo',

                                             imageUrl             : 'assets/img/error.png',

                                             confirmButtonColor   : '#044BFD'

                                         });

                                  }

                    },function(err){

                          console.log('error' + JSO.stringify(err));

                    });

              }

        };


        $scope.destroy = function(id){

              swal.fire({

                  title               : 'ADVERTENCIA',

                  text                : '¿Estás seguro de eliminar este registro?',

                  imageUrl            : 'assets/img/advertencia.png',

                  showCancelButton    : true,

                  showCloseButton     : true,                  

                  cancelButtonColor   : 'rgba(4,75,253,0.5)',                  

                  confirmButtonColor  : '#044BFD',

                  cancelButtonText    : 'Cancelar',

                  confirmButtonText   : 'Si',

              }).then((result) => {

                       if(result.value){

                                clientServices.destroy(id).then(function(resp){

                                              if(resp.data == "DELETED"){

                                                      swal.fire({

                                                          title              : 'Registro eliminado con exito!',

                                                          imageUrl           : 'assets/img/completo.png',

                                                          showCloseButton    : true,

                                                          confirmButtonColor : '#044BFD'

                                                      });

                                                      $scope.get();

                                              }else{

                                                      swal.fire({

                                                          title             : 'Oops!',

                                                          text              : 'No se pudo eliminar el registro inténtalo de nuevo',

                                                          imageUrl          : 'assets/img/error.png',

                                                          showCloseButton   : true,

                                                          confirmButtonColor: '#044BFD'

                                                      });

                                              }

                                },function(err){

                                      console.log('error' + JSON.stringify(err));

                                });

                       }else{

                                swal.fire({

                                    title               : 'Se cancelo la eliminicación!',

                                    imageUrl            : 'assets/img/error.png',

                                    showCloseButton     : true,

                                    confirmButtonColor  : '#044BFD'

                                });
                 
                       }
             
              });
        
        };


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                                                //
//                           C R U D   D E   C O N T A C T O S    D E    L A    E M P R E S A                                                     //
//                                                                                                                                                //      
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

       $scope.lista = function(id){

              $scope.contacto.nIDCliente = id;
              document.getElementById('Visible').style.display          = 'none';
              document.getElementById('VisibleModificar').style.display = 'none';
              $scope.getContactoEmergencia(id);

       };


       $scope.getContactoEmergencia = function(id){

              
              $scope.mensajeC = '';
              $scope.contactos = '';

              contactEmergencyServices.get(id).then(function(resp){

                                      if(resp.data == 'null'){

                                              $scope.mensajeC = 'No tienes contactos';

                                      }else{

                                              $scope.contactos = resp.data;

                                      }

              },function(err){

                    console.log('error' + JSON.stringify(err));

              });

       };

       $scope.mostrar = function(){

              document.getElementById('Visible').style.display = 'block';

       };

       $scope.addContacto = function(contacto){

               if(contacto.Nombre.length == 0){

                          swal.fire({

                               title                : 'Oops!',

                               text                 : 'Nombre del contacto vacio completalo',

                               imageUrl             : 'assets/img/advertencia.png',

                               showCloseButton      : true,

                               confirmButtonColor   : "#044BFD"

                          });

               }else if(!expTelefono.test(contacto.Telefono)){

                                    swal.fire({

                                        title       : 'Oops!',

                                        text        : 'Número de teléfono no cumple con las especificaciones, inténtalo de nuevo',

                                        imageUrl    : 'assets/img/advertencia.png',

                                        showCloseButton : true,

                                        confirmButtonColor : '#044BFD'

                                    });

               }else if(contacto.Calle.length == 0){

                                    swal.fire({

                                        title       : 'Oops!',

                                        text        : 'Domicilio vacio completalo',

                                        imageUrl    : 'assets/img/advertencia.png',

                                        showCloseButton : true,

                                        confirmButtonColor : '#044BFD'

                                    });

               }else if(contacto.NoExterior.length == 0){

                                    swal.fire({

                                        title       : 'Oops!',

                                        text        : 'NoExterior vacio completalo',

                                        imageUrl    : 'assets/img/advertencia.png',

                                        showCloseButton : true,

                                        confirmButtonColor : '#044BFD'

                                    });

               }else{

                    contactEmergencyServices.add(contacto).then(function(resp){

                                    if(resp.data == "INSERTED"){

                                           swal.fire({

                                                title         : 'Registro exitoso!',

                                                imageUrl      : 'assets/img/completo.png',

                                                showCloseButton : true,

                                                confirmButtonColor : '#044BFD'

                                           });

                                           $scope.getContactoEmergencia(contacto.nIDCliente);

                                           $scope.cleanContacto(contacto.nIDCliente);

                                    }else{

                                          swal.fire({

                                                title             : 'Oops!',

                                                text              : 'No se logro el registro, inténtalo de nuevo',

                                                imageUrl          : 'assets/img/error.png',

                                                showCloseButton   : true,

                                                confirmButtonColor: '#044BFD'
                                          
                                          });
                                    
                                    }
                    },function(err){

                          console.log('error' + JSON.stringify(err));

                    });

               }

        };


        $scope.showContacto = function(contacto){

              document.getElementById('VisibleModificar').style.display = 'block';

              $scope.contactoEdit.nIDContactoEmergencia = contacto.nIDContactoEmergencia;
              $scope.contactoEdit.Nombre                = contacto.Nombre;
              $scope.contactoEdit.Telefono              = contacto.Telefono;
              $scope.contactoEdit.Calle                 = contacto.Calle;
              $scope.contactoEdit.NoExterior            = contacto.NoExterior;
              $scope.contactoEdit.NoInterior            = contacto.NoInterior;
              $scope.contactoEdit.Colonia               = contacto.Colonia;
              $scope.contactoEdit.nIDCliente            = contacto.nIDCliente;

        };

        $scope.cerrarMod = function(){
          document.getElementById('VisibleModificar').style.display = 'none';
        }

        $scope.cerrarAdd = function(){
         document.getElementById('Visible').style.display = 'none'; 
        }


        $scope.updateContacto = function(contacto){

               if(contacto.Nombre.length == 0){

                          swal.fire({

                               title                : 'Oops!',

                               text                 : 'Nombre del contacto vacio completalo',

                               imageUrl             : 'assets/img/advertencia.png',

                               showCloseButton      : true,

                               confirmButtonColor   : "#044BFD"

                          });

               }else if(!expTelefono.test(contacto.Telefono)){

                                    swal.fire({

                                        title       : 'Oops!',

                                        text        : 'Número de teléfono no cumple con las especificaciones, inténtalo de nuevo',

                                        imageUrl    : 'assets/img/advertencia.png',

                                        showCloseButton : true,

                                        confirmButtonColor : '#044BFD'

                                    });

               }else if(contacto.Calle.length == 0){

                                    swal.fire({

                                        title       : 'Oops!',

                                        text        : 'Domicilio vacio completalo',

                                        imageUrl    : 'assets/img/advertencia.png',

                                        showCloseButton : true,

                                        confirmButtonColor : '#044BFD'

                                    });

               }else if(contacto.NoExterior.length == 0){

                                    swal.fire({

                                        title       : 'Oops!',

                                        text        : 'NoExterior vacio completalo',

                                        imageUrl    : 'assets/img/advertencia.png',

                                        showCloseButton : true,

                                        confirmButtonColor : '#044BFD'

                                    });

               }else{

                    contactEmergencyServices.update(contacto).then(function(resp){

                                    if(resp.data == "UPDATED"){

                                           swal.fire({

                                                title         : 'Registro exitoso!',

                                                imageUrl      : 'assets/img/completo.png',

                                                showCloseButton : true,

                                                confirmButtonColor : '#044BFD'

                                           });

                                           $scope.getContactoEmergencia(contacto.nIDCliente);

                                           $scope.cleanContactoEdit(contacto.nIDCliente);

                                    }else{

                                          swal.fire({

                                                title             : 'Oops!',

                                                text              : 'No se logro el registro, inténtalo de nuevo',

                                                imageUrl          : 'assets/img/error.png',

                                                showCloseButton   : true,

                                                confirmButtonColor: '#044BFD'
                                          
                                          });
                                    
                                    }
                    },function(err){

                          console.log('error' + JSON.stringify(err));

                    });

               }

        };

        $scope.destroyContacto = function(id,nIDCliente){

              swal.fire({

                  title                 : 'ADVERTENCIA',

                  text                  : '¿Estás seguro de eliminar este registro?',

                  imageUrl              : 'assets/img/advertencia.png',

                  showCancelButton      : true,

                  showCloseButton       : true,

                  confirmButtonColor    : '#044BFD',

                  cancelButtonColor     : 'rgba(4,75,253,0.5)',

                  confirmButtonText     : 'Si',

                  cancelButtonText      : 'Cancelar'

              }).then((result) => {

                      if(result.value){

                                contactEmergencyServices.destroy(id).then(function(resp){

                                                if(resp.data == "DELETED"){

                                                        $scope.getContactoEmergencia(nIDCliente);

                                                        swal.fire({

                                                            title             : 'Registro eliminado con exito!',

                                                            imageUrl          : 'assets/img/completo.png',

                                                            showCloseButton   : true,

                                                            confirmButtonColor: '#044BFD'

                                                        });

                                                }else{

                                                        swal.fire({

                                                            title                 : 'Oops!',

                                                            text                  : 'No se pudo eliminar el registro, inténtalo de nuevo',

                                                            imageUrl              : 'assets/img/error.png',

                                                            showCloseButton       : true,

                                                            confirmButtonColor    : '#044BFD'

                                                        });

                                                }

                                }, function(err){

                                    console.log('error' + JSON.stringify(err));

                                });

                      }else{

                            swal.fire({

                                title               : 'Se cancelo la eliminicación!',

                                imageUrl            : 'assets/img/error.png',

                                showCloseButton     : true,

                                confirmButtonColor  : '#044BFD'

                            });

                      }

              });


        };


       }]);