'use strict';

angular.module('abgpassportApp')
  .service('typeDocumentServices', ['$http','configService',function($http,configService){
    return {

      get : function(){

          return $http.get(configService.apiUrl + 'TipoDocumento.php?accion=consultar',

            { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }

        );

      }

      
    };

  }]);