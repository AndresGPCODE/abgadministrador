<?php

  error_reporting(E_ALL ^ E_NOTICE);  

  ini_set('display_errors', '1');

  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Request-Methods');

  include_once '../Configuraciones/Conexion.php';
  include_once '../Usuario/clUsuarios.php';
  include_once '../Utilerias/clHerramientas_v2011.php';

  $method = $_SERVER['REQUEST_METHOD'];

  $UtileriasDatos = new clHerramientasv2011();
  $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
  $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

  $user = new clUsuarios();

  $user->conexion($SERVER,$USER,$PASSWORD,$BD);

  switch($method){

        case 'GET' :

            $action = $_GET['accion'];

            if($action == 'consultar'){

                      $user->leerCondicion(" tbl_usuarios.bEstado = 1 ");

                      $registros = $user->dtBase();

                      echo json_encode($registros);
                      break;
            }

        case 'POST' : 

            $action = $_POST['accion'];

            if($action == 'agregar'){

                      $object = $_POST['usuario'];

                      $data   = json_decode($object);

                      $Archivo= str_replace(' ', '+', $data->Foto);

                      $email  = str_replace('"','',$data->Email); 

                      if($user->consultaGeneralEmail(' tbl_usuarios ',' tbl_usuarios.Email ="' . $email . '" ' )){

                              echo 'EXISTE';

                      }else if($user->consultaGeneralEmail(' tbl_clientes ',' tbl_clientes.Email ="' . $email . '" ' )){

                              echo 'EXISTE';

                      }else if($user->consultaGeneralEmail(' tbl_choferes ',' tbl_choferes.Email ="' . $email . '" ')){

                              echo 'EXISTE';
                      
                      }else{

                      $user->setInformacion(

                            $data->nIDUsuario,
                            $data->Usuario,
                            $data->Password,
                            '',
                            $data->Nombre,
                            $data->Direccion,
                            $data->RFC,
                            $data->Telefono,
                            $data->Celular,
                            $data->Email,
                            $Archivo,
                            'Si',
                            '',
                            $data->nIDRol,
                            0,
                            0,
                            '',
                            '',
                            '',
                            'Activo',
                            '',
                            $data->nIDEmpresa,
                            '',
                            '',
                            '',
                            'Nuevo usuario - ' . $fechaLocal,
                            '1',
                            TRUE,
                            FALSE,
                            FALSE

                      );

                      if($user->ejecutar('tbl_usuarios')){

                              echo "INSERTED";

                      }else{

                              echo "NOT INSERTED";

                      }

                  }
                  
            }else if($action == 'modificar'){

                      $object = $_POST['usuario'];

                      $data   = json_decode($object);

                      $Archivo= str_replace(' ', '+', $data->Foto);

                      $user->setInformacion(

                            $data->nIDUsuario,
                            $data->Usuario,
                            '',
                            '',
                            $data->Nombre,
                            $data->Direccion,
                            $data->RFC,
                            $data->Telefono,
                            $data->Celular,
                            $data->Email,
                            $Archivo,
                            'Si',
                            '',
                            $data->nIDRol,
                            0,
                            0,
                            '',
                            '',
                            '',
                            'Activo',
                            '',
                            $data->nIDEmpresa,
                            '',
                            '',
                            '',
                            'Usuario modificado - ' . $fechaLocal,
                            '1',
                            FALSE,
                            TRUE,
                            FALSE

                      );

                      if($user->ejecutar('tbl_usuarios')){

                              echo "UPDATED";

                      }else{

                              echo "NOT UPDATED";

                      }

            }else if($action == 'eliminar'){

                    $id = $_POST['id'];

                      $observacion = 'usuario eliminado temporalmente - ' . $fechaLocal;

                      if($user->ocultar($id,$observacion)){

                            echo "DELETED";
                      }else{

                            echo "NOT DELETED";
                      }

            }else if($action == 'buscar'){

                    $object = $_POST['log'];

                    $data = json_decode($object);

                    $condicion = " Email ='" . $data->email . "' and Password = '" . $data->password ."'";

                    $user->leerCondicion($condicion);

                    $registros = $user->dtBase();

                    if($registros){                        

                        session_start();
                        $_SESSION['nIDUsuario'] = $registros[0]['nIDUsuario'];
                        $_SESSION['Nombre'] = $registros[0]['Nombre'];
                        $_SESSION['Email'] = $registros[0]['Email'];
                        $_SESSION['Imagen'] = $registros[0]['Imagen'];
                        $_SESSION['Celular'] = $registros[0]['Celular'];
                        $_SESSION['Rol'] = $registros[0]['TipoDeRol'];

                        echo json_encode($registros);
                      

                    }else{

                        echo "null";
                    }

            }else if($action == 'buscarUsuario'){

                      $id = $_POST['id'];

                      $user->leerCondicion(" tbl_usuarios.bEstado = 1 and tbl_usuarios.nIDUsuario = " . $id);

                      $registros = $user->dtBase();

                      echo json_encode($registros);
                      
            }else if($action == 'change'){
                $object = $_POST['password'];

                $data = json_decode($object);

                if($user->change($data->nIDUsuario, $data->Password)){
                    echo json_encode($user->change($data->nIDUsuario, $data->Password));
                }else{
                  echo json_encode("FALSE");
                }
            }else if($action == 'buscarUsuarioCorreo'){

                    $correo = $_POST['correo'];

                    if($user->buscarId($correo)){

                          echo json_encode($user->buscarId($correo));

                    }else{

                          echo "null";
                          
                    }
            }
        
  }


?>