'use strict';

angular.module('abgpassportApp')

       .service('typedocumentServices',['$http','configService',

        function($http,configService){

        return {


              get : function(){

                  return $http.get(configService.apiUrl + 'TipoDocumento.php?accion=consultar',

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                  );

              },

              getA : function(){

                   return $http.get(configService.apiUrl + 'TipoDocumento.php?accion=consultarA',

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                  );

              },

              getTipo : function(id){

                  return $http.post(configService.apiUrl + 'TipoDocumento.php','accion=consultarTipos&id=' + JSON.stringify(id),

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                  );

              },

              getTipoAuto : function(id){

                  return $http.post(configService.apiUrl + 'TipoDocumento.php','accion=consultarTiposAutos&id=' + JSON.stringify(id),

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                  );

              },

              add : function(tipo){

                  return $http.post(configService.apiUrl + 'TipoDocumento.php','accion=agregar&tipo=' + JSON.stringify(tipo),

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                  );

              },

              update : function(tipo){

                  return $http.post(configService.apiUrl + 'TipoDocumento.php','accion=modificar&tipo=' + JSON.stringify(tipo),

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                  );

              },

              destroy : function(id){

                  return $http.post(configService.apiUrl + 'TipoDocumento.php','accion=eliminar&id=' + JSON.stringify(id),

                    { headers : {'Content-Type' : 'application/x-www-form-urlencoded' } }

                  );
              }


        };

        }]);