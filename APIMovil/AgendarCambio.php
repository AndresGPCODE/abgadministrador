<?php
/*
------
*/
header('Content-Type: application/json');
date_default_timezone_set('America/Mexico_City');
$method = $_SERVER["REQUEST_METHOD"];
// $method = 'POST';
include 'conexion.php';
//$pdo = Conexion();

switch ($method) {
    case 'POST':
        $request = json_decode(file_get_contents('php://input'), true);
        
        // $request['nIDCliente'] = 3;
            // $request['nIDAutomovil'] = 4;
        //     $request['Calle'] = 'Av. royal Country';
        //     $request['NoExterior'] = '2456';
        //     $request['Longitud'] = 20.1234567890;
        //     $request['Latitud'] = 109.987654321;
        //     $request['FechaHora'] = date('Y-m-d H:i:s');
        //     $request['Notas'] = 'Peticionesdel cliente';
        //     $request['Estatus'] = 1;
        // $request['option'] = 2;
        // $request['nIDSolicitud'] = 22; // Id de la solictud a <modificar></modificar>
        // $request['option'] = 4;
        // $request['lastIdSolicitud'] = 26;

        $request['nIDCliente'] = isset($request['nIDCliente']) ? $request['nIDCliente'] : 0;
        $request['nIDAutomovil'] = isset($request['nIDAutomovil']) ? $request['nIDAutomovil'] : 0;
        $request['Calle'] = isset($request['Calle']) ? $request['Calle'] : 0;
        $request['NoExterior'] = isset($request['NoExterior']) ? $request['NoExterior'] : 0;
        $request['Longitud'] = isset($request['Longitud']) ? $request['Longitud'] : 0;
        $request['Latitud'] = isset($request['Latitud']) ? $request['Latitud'] : 0;
        $request['FechaHora'] = isset($request['FechaHora']) ? $request['FechaHora'] : '';
        $request['Notas'] = isset($request['Notas']) ? $request['Notas'] : '';
        $request['Estatus'] = isset($request['Estatus']) ? $request['Estatus'] : 0;
        if (is_null($request['nIDCliente']) || is_null($request['nIDAutomovil']) || is_null($request['Calle']) || is_null($request['NoExterior']) || is_null($request['Longitud']) || is_null($request['Latitud']) || is_null($request['FechaHora']) || is_null($request['Notas']) || is_null($request['Estatus'])) {
            $request['resp'] = 'Los Datos enviados están Incompletos||' . $request . " || " . file_get_contents('php://input');
            $request['status'] = false;
            return json_encode($request);
        } else {
            // echo 'asdasd';
            //$pdo = Conexion();
            switch ($request['option']){
                case 1:
                    // Insertar nuevo cambio de vehiculo
                    $resp = agendarCambio($request);
                break;
                case 2:
                    $resp = actualizarCambio($request);
                break;
                case 3:
                    $resp = cancelarCambio($request);
                    break;
                case 4:
                    // echo json_encode($request);
                    $resp = revisarVigenciaVehiculo($request);
                    // echo $resp;
                    break;
                default:
                    $resp[0]['status'] = false;
                break;
            }
            if ($resp[0]['status']) {
                echo json_encode($resp);
                return json_encode($resp);
            } else {
                $resp[0]['resp'] = 'No se pudo realizar el proceso';
                $resp[0]['status'] = false;
                echo json_encode($resp);
                return json_encode($resp);
            }
        }
    break;
    default:
        $request[0]['resp'] = 'No se puede procesar la informacion';
        $request[0]['status'] = false;
        return json_encode($request);
}

function agendarCambio($cambio){
    // Insertar proximo cambio del cliente
    // $plazo = date("Y-m-d H:i:s", strtotime("2020-07-3 12:34:17" . "+ 30 days"));
    $plazo = date("Y-m-d H:i:s", strtotime($cambio['FechaHora'] . "+ 25 days"));
    $pdo = Conexion();
    /**
     * Abrir una transaccion
     * Insertar el registro de la solicitud
     * seleccionar un registro de solicitud que tenga el mismo id del vehiculo y que este en estatus devuelto
     */

    $fechaActual = date('Y-m-d H:i:s');
    $insert = "INSERT INTO tbl_solicitud (nIDCliente, nIDAutomovil, nIDChofer, Calle, NoExterior, Longitud, Latitud, Altitud, FechaHora, Notas, FechaFin, Estatus, FechaCreacion, FechaModificacion, Observaciones, bEstado) VALUES ( '" . $cambio['nIDCliente'] . "','" . $cambio['nIDAutomovil'] . "', 0,'" . $cambio['Calle'] . "','" . $cambio['NoExterior'] . "','" . $cambio['Longitud'] . "', '" . $cambio['Latitud'] . "', '0', '" . $cambio['FechaHora'] . "','" . $cambio['Notas'] . "', '$plazo','SOLICITADO', '$fechaActual', '$fechaActual', 'Creacion de Solicitud', 1)";
    $ejecutar = $pdo->prepare($insert);
    // var_dump($insert);
    $ejecutar->execute();
    $result = $pdo->lastInsertId();
    $arr = [];
    if ($result > 0) {
        $res = actualizarEstatusVehiculo($cambio['nIDAutomovil']);
        if ($res[0]['status']){
            $row['lastID'] = $result;
            $row['status'] = true;
            array_push($arr, $row);
            return $arr;
        } else {
            $row['lastID'] = $result;
            $row['status'] = false;
            array_push($arr, $row);
            return $arr;
        }
    } else {
        $row['status'] = false;
        array_push($arr, $row);
        return $arr;
    }
}
function actualizarEstatusVehiculo($id){
    // Modificar proximo cambio del cliente
    // Insertar proximo cambio del cliente
    $pdo = Conexion();
    $update = "UPDATE tbl_automoviles SET Estatus = 'OCUPADO' WHERE nIDAutomovil = $id";
    $ejecutar = $pdo->prepare($update);
    // var_dump($update);
    $ejecutar->execute();
    $result = $pdo->lastInsertId();

    $arr = [];
    $row['lastID'] = 0;
    $row['status'] = true;
    array_push($arr, $row);
    return $arr;

}
function actualizarCambio($cambio){
    // Modificar proximo cambio del cliente
    // Insertar proximo cambio del cliente
    $pdo = Conexion();
    $fechaActual = date('Y-m-d H:i:s');
    $update = "UPDATE tbl_solicitud SET Calle = '".$cambio['Calle']."', NoExterior =  '" . $cambio['NoExterior']. "', Longitud=  '" . $cambio['Longitud'] . "', Latitud=  '" . $cambio['Latitud'] . "', FechaHora=  '" . $cambio['FechaHora'] . "', Notas=  '" . $cambio['Notas'] . "', FechaModificacion = '$fechaActual', Observaciones = 'Modificacion Cambio' WHERE nIDSolicitud = ". $cambio['nIDSolicitud'];
    $ejecutar = $pdo->prepare($update);
    // var_dump($update);
    $ejecutar->execute();
    $result = $pdo->lastInsertId();
    // $result = $ejecutar->fetchAll();
    // var_dump();
    $arr = [];
    if ($ejecutar->rowCount() > 0) {
        $row['lastID'] = 0;
        $row['status'] = true;
        array_push($arr, $row);
        return $arr;
    } else {
        $row['status'] = false;
        array_push($arr, $row);
        return $arr;
    }
}
function cancelarCambio($cambio){
    // Modificar proximo cambio del cliente
    // Insertar proximo cambio del cliente
    $pdo = Conexion();
    $fechaActual = date('Y-m-d H:i:s');
    $update = "UPDATE tbl_solicitud SET bEstado = 0 WHERE nIDSolicitud = " . $cambio['lastIdSolicitud'];
    $ejecutar = $pdo->prepare($update);
    //var_dump($update);
    $ejecutar->execute();
    $result = $pdo->lastInsertId();
    //var_dump($result);
    $arr = [];
    $respuesta = actualizarEstatusVehiculoDisponible($cambio['lastIdSolicitud']);
    if ($ejecutar->rowCount() > 0) {
        //$respuesta = actualizarEstatusVehiculoDisponible($cambio['lastIdSolicitud']);
        //var_dump($respuesta);
        if ($respuesta[0]['status']) {
            $row['lastID'] = 0;
            $row['status'] = true;
            array_push($arr, $row);
            return $arr;
        } else {
            $row['lastID'] = 0;
            $row['status'] = true;
            array_push($arr, $row);
            return $arr;
        }
    } else {
            $row['lastID'] = 0;
        $row['status'] = true;
        array_push($arr, $row);
        return $arr;
    }
}
function actualizarEstatusVehiculoDisponible($id, $estado = 'DISPONIBLE'){
    // Modificar proximo cambio del cliente
    // Insertar proximo cambio del cliente
    $pdo = Conexion();
    $update = "UPDATE tbl_automoviles, tbl_solicitud set tbl_automoviles.Estatus = '$estado' where tbl_automoviles.nIDAutomovil = tbl_solicitud.nIDAutomovil AND tbl_solicitud.nIDSolicitud = $id";
    $ejecutar = $pdo->prepare($update);
    //var_dump($update);
    $ejecutar->execute();
    //var_dump($ejecutar);
    $result = $pdo->lastInsertId();
    $arr = [];
    if ($ejecutar->rowCount() > 0) {
        $row['lastID'] = 0;
        $row['status'] = true;
        array_push($arr, $row);
        return $arr;
    } else {
        $row['status'] = false;
        array_push($arr, $row);
        return $arr;
    }
}
function revisarVigenciaVehiculo($datos){
    $pdo = Conexion();
    $id = $datos['nIDAutomovil'];
    $select = "SELECT * FROM tbl_automoviles WHERE Estatus = 'DISPONIBLE' AND nIDAutomovil = $id;";
    $ejecutar = $pdo->prepare($select);
    $ejecutar->execute();
    $result = $ejecutar->fetchAll(PDO::FETCH_ASSOC);
    $arr = [];
    if (count($result) > 0) {
        $arr[0]['status'] = true;
        return $arr;
    } else {
        $arr[0]['status'] = false;
        return $arr;
    }
}

?>