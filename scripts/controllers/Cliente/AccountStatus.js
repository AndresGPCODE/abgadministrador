'use strict';

angular.module('abgpassportApp')
       
       .controller('AccountStatusCtrl',['$scope','accountstatuServices','solicitudesServices',

        function($scope,accountstatuServices,solicitudesServices){

        	var suma = 0;
            var expCosto = /^[0-9]{1,6}$/;

        	$scope.Estado = {

        		  nIDEstadoCuenta : '0',
        		  Detalle 		  : '',
        		  Cantidad 		  : '',
        		  Costo 		  : '',
                  Descripcion     : '',
        		  nIDCliente 	  : '',
        		  nIDSolicitud    : ''

        	};

        	$scope.clean = function(solicitud,cliente){

        		  $scope.Estado = {

        		  		nIDEstadoCuenta : '0',
        		  		Detalle 		: '',
        		  		Cantidad 		: '',
        		  		Costo 			: '',
                        Descripcion     : '',
        		  		nIDCliente 		: cliente,
        		  		nIDSolicitud    : solicitud
        		  }
        	}

        	$scope.get = function(){

        		  solicitudesServices.getBuscar().then(function(resp){ console.log(resp.data);

        		  					  if(resp.data == 'null'){

        		  					  		 console.log('Not Found Data');
        		  					  		 $scope.mensaje = 'No hay entregas';

        		  					  }else{

        		  					  		$scope.estados = resp.data;

        		  					  }

        		  },function(err){

        		  		console.log('error' + JSON.stringify(err));

        		  });

        	};


        	$scope.detalles = function(id,cliente){ console.log(id,cliente);

        		  $scope.Estado.nIDSolicitud      = id;
        		  $scope.Estado.nIDCliente        = cliente;

        		  $scope.getDetalles(id)

        	};

        	$scope.getDetalles = function(idsolicitud){

        		  $scope.mensajeE = '';
        		  $scope.cuentas  = '';
        		  suma = 0;
        		  $scope.Total    = '';

        		  accountstatuServices.getDetalles(idsolicitud).then(function(resp){

        		  					  if(resp.data == 'null'){

        		  					  		 console.log('Not Found Data');
        		  					  		 $scope.mensajeE = 'Noy detalles registrados';

        		  					  }else{

        		  					  		 $scope.cuentas = resp.data;

        		  					  		 for(var i = 0; i < resp.data.length; i++){

        		  					  		 		 suma = parseInt(suma) + parseInt(resp.data[i]['Costo']); console.log(suma);

        		  					  		 }

        		  					  		 $scope.Total = suma;
        		  					  		

        		  					  }

        		  },function(err){

        		  		console.log('error' + JSON.stringify(err));

        		  });

        	};


        	$scope.add = function(objeto){

        		   if(objeto.Detalle.length == 0){

        		   		     swal.fire({

        		   		     	 title 						: 'Oops!',

        		   		     	 text						: 'Detalle se encuentra vacio, completalo',

        		   		     	 imageUrl 					: 'assets/img/advertencia.png',

        		   		     	 showCloseButton			: true,

        		   		     	 confirmButtonColor 		: '#044BFD'

        		   		     });

        		   }else if(objeto.Cantidad.length == 0){

        		   				  swal.fire({

        		   				  	  title 						: 'Oops!',

        		   				  	  text	 						: 'Cantidad se encuentra vacio, completalo',

        		   				  	  imageUrl 						: 'assets/img/advertencia.png',

        		   				  	  showCloseButton 				: true,

        		   				  	  confirmButtonColor	 		: '#044BFD'

        		   				  });

        		   }else if(!expCosto.test(objeto.Costo)){

        		   				  swal.fire({

        		   				  	  title 						: 'Oops!',

        		   				  	  text 							: 'Costo no permite caracteres especiales, únicamente números',

        		   				  	  imageUrl 						: 'assets/img/advertencia.png',

        		   				  	  showCloseButton 				: true,

        		   				  	  confirmButtonColor 			: '#044BFD'

        		   				  });

        		   }else{

        		   		accountstatuServices.add(objeto).then(function(resp){

        		   							if(resp.data == 'INSERTED'){

        		   									swal.fire({

        		   										title 						: 'Registro exitoso!',

        		   										imageUrl 					: 'assets/img/completo.png',

        		   										showCloseButton 			: true,

        		   										confirmButtonColor 			: '#044BFD'

        		   									});

        		   									$scope.detalles(objeto.nIDSolicitud,objeto.nIDCliente);

        		   									$scope.clean(objeto.nIDSolicitud,objeto.nIDCliente);

        		   							}else{

        		   									swal.fire({

        		   										title 						: 'Oops!',

        		   										text						: 'No se completo el registro, Inténtalo de nuevo',

        		   										imageUrl 					: 'assets/img/advertencia.png',

        		   										showCloseButton 			: true,

        		   										confirmButtonColor 			: '#044BFD'

        		   									});

        		   							}

        		   		},function(err){

        		   				console.log('error' + JSON.stringify(err));

        		   		});

        		   }

        	};


   


        }]);