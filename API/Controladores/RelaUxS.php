<?php

  error_reporting(E_ALL ^ E_NOTICE);  

  ini_set('display_errors', '1');

  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Request-Methods');

  include_once '../Configuraciones/Conexion.php';
  include_once '../Usuario/clRelauxs.php';
  include_once '../Utilerias/clHerramientas_v2011.php';

  $method = $_SERVER['REQUEST_METHOD'];

  $UtileriasDatos = new clHerramientasv2011();
  $fechaLocal     = $UtileriasDatos->getFechaYHoraActual_General();
  $fechaLocal     = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);

  $relacion = new clRelauxs();

  $relacion->conexion($SERVER,$USER,$PASSWORD,$BD);

  switch($method){

        case 'POST' : 

            $action = $_POST['accion'];

            if($action == 'agregar'){

                      $object = $_POST['objeto'];

                      $data   = json_decode($object);

                      $relacion->setInformacion(

                            $data->nIDRelaUxS,
                            $data->nIDUsuario,
                            '',
                            '',
                            $data->GUID,
                            $data->nIDEmpresa,
                            '',
                            '',
                            'Nuevo inicio de sesión - ' . $fechaLocal,
                            '1',
                            TRUE,
                            FALSE,
                            FALSE

                      );

                      if($relacion->ejecutar('tbl_relauxs')){

                              echo "INSERTED";

                      }else{

                              echo "NOT INSERTED";

                      }
            }else if($action == 'modificar'){

                      $GUID = $_POST['GUID'];

                      $relacion->setInformacion(

                            '',
                            '',
                            '',
                            '',
                            $GUID,
                            '',
                            '',
                            '',
                            'Cierre de sesión - ' . $fechaLocal,
                            '1',
                            FALSE,
                            TRUE,
                            FALSE

                      );

                      if($relacion->ejecutar('tbl_relauxs')){
                              session_start();
                              session_destroy();
                              echo "UPDATED";



                      }else{

                              echo "NOT UPDATED";

                      }
            }
        
  }


?>