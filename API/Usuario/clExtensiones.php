<?php

   //Clase Extension
    //Esta clase se encarga de los procesos del registro de las extensiones

    //Herramienta Externa
    include_once "clHerramientas_v2011.php";

    //tbl_extensiones


class clExtensiones
{

  //Atributos de conexion a la base de datos
  private $servidor;
  private $usuario;
  private $password;
  private $basededatos;
  private $contarDatos   = 0;

  //Constantes generales
  private $ascendente     = "Ascendente";
  private $descendente    = "Descendente";

  //Constantes para la base de datos
  private $nombreTabla    = "tbl_extensiones";
  private $nombreVista    = "vw_extensiones";
  private $id             = "nIDExtension";
  private $campoOrdenar   = "nIDExtension";
  private $ordenamiento   = "Descendente";
  private $limiteInferior = -1;
  private $limiteSuperior = -1;
  public  $last_id = 0;
  private $dt;
  private $dtGrabar;

  //Constantes de estado
  private $existeError    = FALSE;
  private $existenDatos   = FALSE;
  private $datosConexion  = FALSE;

  //Mensajes
  private $error          = "";

  //Constructor
  function __construct()
  {
    $this->Inicializacion();
  }

  /**
     *************** Función de conexión ***************
     * La función realiza la conexión a la base de datos 
     * mediante las credenciales previamente establecidas. 
     * 
     * @param [string] $servidor
     * @param [string] $usuario
     * @param [string] $password
     * @param [string] $basededatos
     * @return void
     */
    public function conexion($servidor, $usuario, $password, $basededatos)
    {
        $this->servidor     = $servidor;
        $this->usuario      = $usuario;
        $this->password     = $password;
        $this->basededatos  = $basededatos;

        $this->datosConexion    = TRUE;
    }

    /**
     ************** Función setInfomacion **************
     * La función realiza la asignación de los datos almacenados
     * en la base de datos a las respectivas variables, los datos
     * que se encuentran como parametro son obligatorios para cada
     * tabla. 
     * 
     * @param [datetime] $FechaModificacion
     * @param [datetime] $FechaCreacion
     * @param [string] $Observaciones
     * @param [int] $bEstado
     * @param [boolean] $crear
     * @param [boolean] $modificar
     * @param [boolean] $eliminar
     * @return void
     */
    public function setInformacion($nIDExtension,$Extension,$Identificador,$SIP_Usuario,$SIP_Registro,
    $SIP_Password,$SIP_IPEquipo,$SIP_IPConmutador,$SIP_PortMin,$SIP_PortMax,$Tipo,$nIDLinea,
    $FechaModificacion, $FechaCreacion,$Observaciones, $bEstado, $crear, $modificar, $eliminar) 
    {
        $utileria = new clHerramientasv2011();
        $
        
        $this->dtGrabar[$this->contarDatos]["nIDExtension"]                 = $nIDExtension;
        $this->dtGrabar[$this->contarDatos]["Extension"]                    = $Extension;
        $this->dtGrabar[$this->contarDatos]["Identificador"]                = $Identificador;
        $this->dtGrabar[$this->contarDatos]["SIP_Usuario"]                  = $SIP_Usuario;
        $this->dtGrabar[$this->contarDatos]["SIP_Registro"]                 = $SIP_Registro;
        $this->dtGrabar[$this->contarDatos]["SIP_Password"]                 = $SIP_Password;
        $this->dtGrabar[$this->contarDatos]["SIP_IPEquipo"]                 = $SIP_IPEquipo;
        $this->dtGrabar[$this->contarDatos]["SIP_IPConmutador"]             = $SIP_IPConmutador;
        $this->dtGrabar[$this->contarDatos]["SIP_PortMin"]                  = $SIP_PortMin;
        $this->dtGrabar[$this->contarDatos]["SIP_PortMax"]                  = $SIP_PortMax;
        $this->dtGrabar[$this->contarDatos]["Tipo"]                         = $Tipo;
        $this->dtGrabar[$this->contarDatos]["nIDLinea"]                     = $nIDLinea;
     

        /*
         ** Estos datos son obligatorios para todas las tablas
        */
        $fecha = "";

        if( strlen($FechaModificacion) > 0 )
        {
            $fecha = $utileria->ConvertirFechaYHora_General($FechaModificacion);
            $this->dtGrabar[$this->contarDatos]["FechaModificacion"] = $fecha;
        } else {
            $this->dtGrabar[$this->contarDatos]["FechaModificacion"] = "01/01/1980";
        }

        $fecha = "";

        if(strlen($FechaCreacion)>0){
            $fecha=$utileria->ConvertirFechaYHora_General($FechaCreacion);
            $this->dtGrabar[$this->contarDatos]["FechaCreacion"] = $fecha;
        } else {
            $this->dtGrabar[$this->contarDatos]["FechaCreacion"] = "01/01/1980";
        }

        $this->dtGrabar[$this->contarDatos]["Observaciones"] = $Observaciones;

        if($bEstado >= 0 && $bEstado <= 46){
            $this->dtGrabar[$this->contarDatos]["bEstado"]  = $bEstado;
            $this->dtGrabar[$this->contarDatos]["sbEstado"] = $this->vEstado($bEstado,"bEstado");
        } else {
            $this->dtGrabar[$this->contarDatos]["bEstado"]  = 3;
            $this->dtGrabar[$this->contarDatos]["sbEstado"] = "No definido";
        }

        if ($crear==TRUE){
            $this->dtGrabar[$this->contarDatos]["Crear"]    = TRUE;
            $this->dtGrabar[$this->contarDatos]["Cambiar"]  = FALSE;
            $this->dtGrabar[$this->contarDatos]["Eliminar"] = FALSE;
        } else {
            if ($modificar==TRUE){
                $this->dtGrabar[$this->contarDatos]["Crear"]    = FALSE;
                $this->dtGrabar[$this->contarDatos]["Cambiar"]  = TRUE;
                $this->dtGrabar[$this->contarDatos]["Eliminar"] = FALSE;
            } else {
                if ($eliminar==TRUE){
                    $this->dtGrabar[$this->contarDatos]["Crear"]    = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Cambiar"]  = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Eliminar"] = TRUE;
                } else {
                    $this->dtGrabar[$this->contarDatos]["Crear"]    = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Cambiar"]  = FALSE;
                    $this->dtGrabar[$this->contarDatos]["Eliminar"] = FALSE;
                }
            }
        }


        $this->contarDatos=$this->contarDatos+1;
        $this->existenDatos=TRUE;

        unset($utileria);

        return TRUE;

    }

    /**
     ************** Función setInformacionObj **************
     * Esta función permite darle valor al objeto para despues
     * retornar datos.
     * 
     * @param [object] $obj
     * @return void
     */
    public function setInformacionObj($obj)
    {
        $registros  = 0;
        $registros  = $obj->registrosCargados();

        if($registros > 0){
            
            $i=0;

            for($i=0; $i < $registros - 1; $i = $i + 1){

                $dato = $obj->getInformacionObj();

                $this->setInformacion(
                    $dato[$i]["nIDExtension"],
                    $dato[$i]["Extension"],
                    $dato[$i]["Identificador"],
                    $dato[$i]["SIP_Usuario"],
                    $dato[$i]["SIP_Registro"],
                    $dato[$i]["SIP_Password"],
                    $dato[$i]["SIP_IPEquipo"],
                    $dato[$i]["SIP_IPConmutador"],
                    $dato[$i]["SIP_PortMin"],
                    $dato[$i]["SIP_PortMax"],
                    $dato[$i]["Tipo"],
                    $dato[$i]["nIDLinea"],
                    $dato[$i]["FechaModificacion"],
                    $dato[$i]["FechaCreacion"],
                    $dato[$i]["Observaciones"],
                    $dato[$i]["bEstado"],
                    $dato[$i]["Crear"],
                    $dato[$i]["Cambiar"],
                    $dato[$i]["Eliminar"]


                );
            }

        }
        return TRUE;
    }

    /**
     ************** Función getInformacionObj **************
     * Esta función graba los datos traídos de la base de datos
     * para despues agregarlos a una variable u objeto.
     * 
     * @return void
     */
    public function getInformacionObj()
    {
        return $this->dtGrabar;
    }

    /**
     ************** Función getInformacion **************
     * Esta función permite devolver el valor de las propiedades
     * que se encuentan en las variables.
     * 
     * @param [int] $numero
     * @return void
     */
    public function getInformacion($numero)
    {
        if($this->existenDatos == TRUE){
            if($numero >= 0){
                return $this->dtGrabar[$numero];
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    /**
     ************** Función getIndice **************
     *
     * @param [int] $nID
     * @return void
     */
    public function getIndice($nID)
    {
        if($this->contarDatos>0){

            $indice = -1;

            for($i=0; $i < $this->contarDatos - 1; $i = $i + 1){
                if($this->dtgrabar[$i][$this->id] == $nID){
                    $indice = $i;
                    break;
                }
            }
            return $indice;
        } else {
            return -1;
        }
    }

    /**
     ************** Función OrdenarTabla **************
     * Ordena la tabla con respecto al campo que se le
     * pasa por parametro.
     * 
     * @param [string] $campos
     * @return void
     */
    public function OrdenarTabla($campos)
    {
        $this->campoOrdenar = $campos;
    }

    /**
     ************** Función formaOrdenamiento **************
     * Ordena la tabla de forma ascendente o descendente según
     * el parametro.
     * 
     * @param [type] $forma
     * @return void
     */
    public function formaOrdenamiento($forma)
    {
        $this->ordenamiento = $forma;
    }

    /**
     ************** Función dtBase **************
     * Graba los registros de la base de datos.
     * 
     * @return void
     */
    public function dtBase()
    {
        return $this->dtGrabar;
    }

    /**
     ************** Función errorObjeto **************
     * La funcion retorna un error en caso de que este
     * exista.
     * 
     * @return void
     */
    public function errorObjeto()
    {
        if($this->existeError==TRUE){
            return $this->error;
        } else {
            return "No existe ningún mensaje de error.";
        }
    }

    /**
     ************** Función registrosCargados **************
     * Función para retornar el número de registros cargados.
     * @return void
     */
    public function registrosCargados()
    {
        return $this->contarDatos;
    }

    /**
     ************** Función formaDeOrdenamiento **************
     * Función para retornar la forma de ordenar la tabla.
     * 
     * @return void
     */
    public function formaDeOrdenamiento()
    {
        return $this->ordenamiento;
    }

    /**
     ************** Función errorDeObjeto **************
     * Función para retornar el error de un objeto en caso
     * de que este exista.
     * 
     * @return void
     */
    public function errorDeObjeto()
    {
        return $this->existeError;
    }

    /**
     ************** Función datosCargados **************
     *
     * @return void
     */
    public function datosCargados()
    {
        return $this->existenDatos;
    }

    /**
     ************** Función setlimiteInferior **************
     *
     * @param [int] $limite
     * @return void
     */
    public function setlimiteInferior($limite)
    {
        $this->limiteInferior = $limite;
    }

    /**
     ************** Función setlimiteSuperior **************
     *
     * @param [int] $limite
     * @return void
     */
    public function setlimiteSuperior($limite)
    {
        $this->limiteSuperior = $limite;
    }
    
    /**
     *************** Función Inicializacion ***************
     * Esta función permite inicializar atributos y contenido.
     * 
     * @return void
     */
    public function Inicializacion()
    {
        $this->InicializaAtributos();
        $this->InicializacionContenido();
    }

    /**
     *************** Función InicializaAtributos ***************
     * Inicializa los atributos.
     * 
     * @return void
     */
    private function InicializaAtributos()
    {
        $this->existeError      = FALSE;
        $this->existenDatos     = FALSE;
        $this->datosConexion    = FALSE;

        $this->error        = "No hay error";
        $this->ordenamiento = $this->Ascendente;
        $this->contarDatos  = 0;
    }

    /**
     *************** Función InicializacionContenido ***************
     * Inicializa el contenido.
     * @return void
     */
    public function InicializacionContenido()
    {
        $this->contarDatos  = 0;
        $this->existeError  = FALSE;
    }

    /**
     *************** Función existeID ***************
     *
     * @param [int] $nID
     * @return void
     */
    public function existeID($nID)
    {

        // Verifica si existe algún error con la conexión
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        ="Error No tiene los datos para conectarse a la base de 
                                 datos [Cargar]";
            return FALSE;
        }

        // Abre la conexion
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT * ";
        $txtConsulta = $txtConsulta . "FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . $this->id ."=" . $nID ;

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Cargar la informacion
        if($res){
            if($registro=mysqli_fetch_array($res, MYSQLI_ASSOC)){
                return TRUE;
            } else {

                return FALSE;
            }

        } else {
            return FALSE;
        }
    }

    /**
     *************** Función vEstado ***************
     * Verifica el estado del registro.
     * 0 = Activo; 1 = Eliminado; 2 = Cancelado; 
     * 4 = Cerrado/Finalizado; -1 = No definido;
     * 
     * @param [string] $estado
     * @param [string] $nombreCampo
     * @return void
     */
    private function vEstado($estado, $nombreCampo)
    {
        $cadena = "";
        switch($estado){
            case 0: // Activo
                $cadena = "Activo";
                break;
            case 1: // Eliminado
                $cadena = "Eliminado";
                break;
            case 2: // Cancelado
                $cadena = "Cancelado";
                break;
            case 3: // Cerrado/Finalizo
                $cadena = "Cerrado/Finalizado";
                break;
            default: // Otro
                $cadena = "No definido";
                break;
        }
        return $cadena;
    }

    /**
     *************** Funcion especialesHTML ***************
     *
     * @param [string] $str
     * @return void
     */
    public function especialesHTML($str)
    {
        $str = mb_convert_encoding($str,  'UTF-8');
        return $str;
    }

    /**
     ***************  Función consultar *************** 
     * Realiza una consulta simple.
     * 
     * @return void
     */
    private function consultar()
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Genera la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT *";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

        if($this->formaDeOrdenamiento()==$this->Ascendente){
            $txtConsulta = $txtConsulta . " ASC";
        } else {
            $txtConsulta = $txtConsulta . " DESC";
        }

        if($this->limiteInferior>=0){
            $txtConsulta = $txtConsulta . " LIMIT " . $this->limiteInferior . "," . $this->limiteSuperior;
        }

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli, $txtConsulta);

        // Carga la informacion
        $contador = 0;
        $this->contarDatos = 0;

        if($res){

            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $nIDExtension            = 0;
                $Extension               = "";
                $Identificador           = "";
                $SIP_Usuario             = "";
                $SIP_Registro            = "";
                $SIP_Password            = "";
                $SIP_IPEquipo            = "";
                $SIP_IPConmutador        = "";
                $SIP_PortMin             = 0;
                $SIP_PortMax             = 0;
                $Tipo                    = "";
                $nIDLinea                = 0;


                // Campos base
                $FechaModificacion  = "";
                $FechaCreacion      = "";
                $Observaciones      = "";
                $bEstado            = 3;

                // Carga la información del registro leído
                if($registro['nIDExtension'] != NULL){
                    $nIDExtension = $registro['nIDExtension'];
                }
                if($registro['Extension'] != NULL){
                    $Extension = $registro['Extension'];
                }
                if($registro['Identificador'] != NULL){
                    $Identificador = $registro['Identificador'];
                }
                if($registro['SIP_Usuario'] != NULL){
                    $SIP_Usuario = $registro['SIP_Usuario'];
                }
                if($registro['SIP_Registro'] != NULL){
                   $SIP_Registro = $registro['SIP_Registro'];
                }
                if($registro['SIP_Password'] != NULL){
                   $SIP_Password = $registro['SIP_Password'];
                }
                if($registro['SIP_IPEquipo'] != NULL){
                   $SIP_IPEquipo = $registro['SIP_IPEquipo'];
                }
                if($registro['SIP_IPConmutador'] != NULL){
                   $SIP_IPConmutador = $registro['SIP_IPConmutador'];
                }
                if($registro['SIP_PortMin'] != NULL){
                   $SIP_PortMin = $registro['SIP_PortMin'];
                }
                if($registro['SIP_PortMax'] != NULL){
                   $SIP_PortMax = $registro['SIP_PortMax'];
                }
                if($registro['Tipo'] != NULL){
                   $Tipo = $registro['Tipo'];
                }
                if($registro['nIDLinea'] != NULL) {
                   $nIDLinea = $registro['nIDLinea'];
                }


                // Campos base
                if($registro['FechaModificacion']!= NULL){
                    $FechaModificacion = $registro['FechaModificacion'];
                }

                if($registro['FechaCreacion']!= NULL){
                    $FechaCreacion = $registro['FechaCreacion'];
                }

                if($registro['Observaciones']!= NULL){
                    $Observaciones = $registro['Observaciones'];
                }

                if($registro['bEstado']!= NULL){
                    $bEstado = $registro['bEstado'];
                }

                // Carga la información en el objeto
                $this->setInformacion(
                        $nIDExtension,
                        $Extension,
                        $Identificador,
                        $SIP_Usuario,
                        $SIP_Registro,
                        $SIP_Password,
                        $SIP_IPEquipo,
                        $SIP_IPConmutador,
                        $SIP_PortMin,
                        $SIP_PortMax,
                        $Tipo,
                        $nIDLinea,
                        $FechaModificacion,
                        $FechaCreacion,
                        $Observaciones,
                        $bEstado,
                        FALSE,
                        TRUE,
                        FALSE
                );
            }

        } else {
            $this->existenDatos = FALSE;
        }
        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     ***************  Función consultarCondicion *************** 
     * Realiza una consulta con condición.
     * 
     * @param [string] $condicion
     * @return void
     */
    private function consultarCondicion($condicion)
    {

        try{
            // Abre la conexión
            $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

            if(mysqli_connect_errno()){
                $this->existeError  = TRUE;
                $this->error        = "Error No se puede conectar a la base de datos";
                return FALSE;
            }

            $txtConsulta = "";
            $txtConsulta = "SELECT *";
            $txtConsulta = $txtConsulta . " FROM ";
            $txtConsulta = $txtConsulta . $this->nombreTabla;
            $txtConsulta = $txtConsulta . " WHERE ";
            $txtConsulta = $txtConsulta . $condicion;
            $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

            if($this->limiteInferior >= 0){
                $txtConsulta = $txtConsulta . " LIMIT " . $this->limiteInferior . "," . $this->limiteSuperior;
            }

            mysqli_query($mysqli, "SET NAMES UTF8");
            $res=mysqli_query($mysqli,$txtConsulta);

            // Cargar la informacion
            $contador           = 0;
            $this->contarDatos  = 0;

            if($res){

                while($registro=mysqli_fetch_array($res, MYSQLI_ASSOC)){
                    $nIDExtension        = 0;
                    $Extension           = "";
                    $Identificador       = "";
                    $SIP_Usuario         = "";
                    $SIP_Registro        = "";
                    $SIP_Password        = "";
                    $SIP_IPEquipo        = "";
                    $SIP_IPConmutador    = "";
                    $SIP_PortMin         = 0;
                    $SIP_PortMax         = 0;
                    $Tipo                = "";
                    $nIDLinea            = 0;

                    // Campos base
                    $FechaModificacion  = "";
                    $FechaCreacion      = "";
                    $Observaciones      = "";
                    $bEstado            = 3;

                    // Carga la info del registro leido
                    if($registro['nIDExtension'] != NULL){
                        $nIDExtension = $registro['nIDExtension'];
                    }
                    if($registro['Extension'] != NULL){
                        $Extension = $registro['Extension'];
                    }
                    if($registro['Identificador'] != NULL){
                        $Identificador = $registro['Identificador'];
                    }
                    if($registro['SIP_Usuario'] != NULL){
                        $SIP_Usuario = $registro['SIP_Usuario'];
                    }
                    if($registro['SIP_Registro'] != NULL){
                        $SIP_Registro = $registro['SIP_Registro'];
                    }
                    if($registro['SIP_Password'] != NULL){
                        $SIP_Password = $registro['SIP_Password'];
                    }
                    if($registro['SIP_IPEquipo'] != NULL){
                        $SIP_IPEquipo = $registro['SIP_IPEquipo'];
                    }
                    if($registro['SIP_IPConmutador'] != NULL){
                        $SIP_IPConmutador = $registro['SIP_IPConmutador'];
                    }
                    if($registro['SIP_PortMin'] != NULL){
                        $SIP_PortMin = $registro['SIP_PortMin'];
                    }
                    if($registro['SIP_PortMax'] != NULL){
                        $SIP_PortMax = $registro['SIP_PortMax'];
                    }
                    if($registro['Tipo'] != NULL){
                        $Tipo = $registro['Tipo'];
                    }
                    if($registro['nIDLinea'] != NULL){
                        $nIDLinea = $registro['nIDLinea'];
                    }

                    // Campos base
                    if($registro['FechaModificacion']!=NULL){
                        $FechaModificacion=$registro['FechaModificacion'];
                    }

                    if($registro['FechaCreacion']!=NULL){
                        $FechaCreacion=$registro['FechaCreacion'];
                    }

                    if($registro['Observaciones']!=NULL){
                        $Observaciones=$registro['Observaciones'];
                    }

                    if($registro['bEstado']!=NULL){
                        $bEstado=$registro['bEstado'];
                    }

                    // Carga la informacion en el objeto
                    $this->setInformacion(
                            $nIDExtension,
                            $Extension,
                            $Identificador,
                            $SIP_Usuario,
                            $SIP_Registro,
                            $SIP_Password,
                            $SIP_IPEquipo,
                            $SIP_IPConmutador,
                            $SIP_PortMin,
                            $SIP_PortMax,
                            $Tipo,
                            $nIDLinea,
                            $FechaModificacion,
                            $FechaCreacion,
                            $Observaciones,
                            $bEstado,
                            FALSE,
                            TRUE,
                            FALSE
                    );
                }
            } else {
                $this->existenDatos = FALSE;
            }
            mysqli_close($mysqli);
            return TRUE;
        } catch (Exception $e) {
            echo 'Se capturo la siguiente excepción: ',  $e->getMessage(), "\n";
        }
    }

    /**
     *************** Función registrosTabla ***************
     *
     * @return void
     */
    private function registrosTabla()
    {

        // Abre la conexión
        $mysqli=mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT *";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

        if($this->formaDeOrdenamiento()==$this->Ascendente){
            $txtConsulta=$txtConsulta . " ASC";
        } else {
            $txtConsulta=$txtConsulta . " DESC";
        }

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Carga la informacion
        $contador=0;

        // Cuenta los registros
        if($res){
            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $contador = $contador + 1;
            }
        }

        mysqli_close($mysqli);
        return $contador;
    }

    /**
     *************** Función contarRegistros ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function contarRegistros($condicion)
    {
        
        $contador = 0;

        // Abrea la conexion
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error No se puede conectar a la base de datos";
            return 0;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT count(nIDExtension) as Total";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . $condicion;

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Cargar la informacion


        //$this->contarDatos=0;
        if($res){
            if($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                if($registro['Total']!=NULL){
                    $contador=$registro['Total'];
                }
            }
        }

        mysqli_close($mysqli);
        return $contador;
    }

    /**
     *************** Función contarRegistrosCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */
    private function contarRegistrosCondicion($condicion)
    {
        
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return FALSE;
        }

        // Construye la consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT *";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . $condicion;
        $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

        if($this->formaDeOrdenamiento()==$this->Ascendente){
            $txtConsulta=$txtConsulta . " ASC";
        } else {
            $txtConsulta=$txtConsulta . " DESC";
        }

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Carga la informacion
        $contador = 0;

        if($res){
            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $contador = $contador + 1;
            }
        }

        mysqli_close($mysqli);
        return $contador;
    }

    /**
     *************** Función ConsultarEstado ***************
     *
     * @param [int] $nID
     * @return void
     */
    private function ConsultarEstado($nID)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        $bestado = -1;

        // Generar consulta
        $txtConsulta = "";
        $txtConsulta = "SELECT *";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . $this->id . "=" . $nID;
        $txtConsulta = $txtConsulta . " ORDER BY " . $this->campoOrdenar;

        if($this->formaDeOrdenamiento() == $this->Ascendente){
            $txtConsulta=$txtConsulta . " ASC";
        } else {
            $txtConsulta=$txtConsulta . " DESC";
        }

        // Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);

        // Cargar la información
        if($res){
            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                if($registro['bEstado']!= NULL){
                    $bestado=$registro['bEstado'];
                    break;
                }
            }
            mysqli_close($mysqli);
            return $bestado;
        } else {
            mysqli_close($mysqli);
            return -1;
        }
    }

    /**
     *************** Función consultarID ***************
     *
     * @param [int] $extension
     * @return void
     */
    private function consultarID($extension)
    {

        // Abrea la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        $txtConsulta = "";
        $txtConsulta = "SELECT *";
        $txtConsulta = $txtConsulta . " FROM ";
        $txtConsulta = $txtConsulta . $this->nombreTabla;
        $txtConsulta = $txtConsulta . " WHERE ";
        $txtConsulta = $txtConsulta . "nIDExtension='" . $extension . "'";

        //' Ejecuta la consulta
        mysqli_query($mysqli, "SET NAMES UTF8");
        $res = mysqli_query($mysqli,$txtConsulta);
        $nid = -1;

        // Cargar la informacion
        if($res){
            while($registro = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                $nid = -1;

                if($registro['nIDExtension']!= NULL){
                    $nid = $registro['nIDExtension'];
                    break;
                }
            }
            mysqli_close($mysqli);
            return $nid;
        } else {
            mysqli_close($mysqli);
            return -1;
        }
    }

     if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " bEstado=" . $bEstado . ",";

        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $Observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res=mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función borrar ***************
     *
     * @return void
     */
    private function borrar()
    {
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "DELETE FROM " . $this->nombreTabla;
        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función borrarID ***************
     *
     * @param [int] $nID
     * @return void
     */
    private function borrarID($nID)
    {
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "DELETE FROM " . $this->nombreTabla . " WHERE " . $this->id . "=" . $nID;
        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función borrarCondicion ***************
     *
     * @param [tstring] $condicion
     * @return void
     */
    private function borrarCondicion($condicion)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "DELETE FROM " . $this->nombreTabla . " WHERE " . $condicion;
        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función borrarTemporal ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @return void
     */
    private function borrarTemporal($nID, $observaciones)
    {
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " bEstado=1" . ",";

        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res=mysqli_query($mysqli,$txtInsercion);
        if($res==FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarEstado ***************
     *
     * @param [int] $nID
     * @param [int] $bEstado
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarEstado($nID, $bEstado, $observaciones)
    {
        
        // Abre la conexión
        $mysqli=mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " bEstado=" . $bEstado . ",";

        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res=mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarPassword ***************
     *
     * @param [int] $nID
     * @param [string] $password
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarPassword($nID, $password, $observaciones)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " Password='" . $this->especialesHTML($password) . "',";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res=mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }
        mysqli_close($mysqli);
        return TRUE;
    }
    
    /**
     *************** Función ActualizarNombre ***************
     * 
     * @param [int] $nID
     * @param [string] $nombre
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarNombre($nID, $nombre, $observaciones)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " Nombre=" . $this->especialesHTML($nombre) . ",";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarVIP ***************
     *
     * @param [int] $nIDExtension
     * @param [string] $Extension
     * @param [string] $Identificador
     * @param [string] $SIP_Usuario
     * @param [string] $SIP_Registro
     * @param [string] $SIP_Password
     * @param [string] $SIP_IPEquipo
     * @param [string] $SIP_IPConmutador
     * @param [int] $SIP_PortMin
     * @param [int] $SIP_PortMax
     * @param [string] $Tipo
     * @param [int] $nIDLinea
     * @param [type] $observaciones
     * @return void
     */
    private function actualizarVIP($nIDExtension, $Extension, $Identificador, $SIP_Usuario,$SIP_Registro,$SIP_Password,$SIP_IPEquipo,$SIP_IPConmutador,
    $SIP_PortMin,$SIP_PortMax,$Tipo,$nIDLinea, $observaciones)
    {

        // Abre la conexón
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion = "";
        $fechalocal = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";

        $txtInsercion = $txtInsercion . " Extension='" . $this->especialesHTML($Extension) . "',";
        $txtInsercion = $txtInsercion . " Identificador='" . $this->especialesHTML($Identificador) . "',";
        $txtInsercion = $txtInsercion . " SIP_Usuario='" . $this->especialesHTML($SIP_Usuario) . "',";
        $txtInsercion = $txtInsercion . " SIP_Registro='" . $this->especialesHTML($SIP_Registro) . "',";
        $txtInsercion = $txtInsercion . " SIP_Password='" . $this->especialesHTML($SIP_Password) . "',";
        $txtInsercion = $txtInsercion . " SIP_IPEquipo='" . $this->especialesHTML($SIP_IPEquipo) . "',";
        $txtInsercion = $txtInsercion . " SIP_IPConmutador='" . $this->especialesHTML($SIP_IPConmutador) . "',";
        $txtInsercion = $txtInsercion . " SIP_PortMin='" . $this->especialesHTML($SIP_PortMin) . "',";
        $txtInsercion = $txtInsercion . " SIP_PortMax='"  . $this->especialesHTML($SIP_PortMax) . "',";
        $txtInsercion = $txtInsercion . " Tipo='" . $this->especialesHTML($Tipo) . "',";
        $txtInsercion = $txtInsercion . " nIDLinea='" . $this->especialesHTML($nIDLinea) . "',";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nIDExtension;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarNivel ***************
     *
     * @param [int] $nID
     * @param [int] $nIDNivel
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarNivel($nID, $nIDNivel, $observaciones)
    {

        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion = "";
        $fechalocal = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " nIDNivel=" . $nIDNivel . ",";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarCodigoUnico ***************
     *
     * @param [int] $nID
     * @param [int] $codigoUnico
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarCodigoUnico($nID, $codigoUnico, $observaciones)
    {
        
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion = "";
        $fechalocal = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " CodigoUnico='" . $codigoUnico . "',";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función actualizarTerminos ***************
     *
     * @param [int] $nID
     * @param [string] $aceptoTerminos
     * @param [string] $observaciones
     * @return void
     */
    private function actualizarTerminos($nID, $aceptoTerminos, $observaciones)
    {
        
        // Abre la conexión
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion = "";
        $fechalocal = "";
        $UtileriasDatos = new clHerramientasv2011();

        $txtInsercion = "UPDATE " .  $this->nombreTabla;
        $txtInsercion = $txtInsercion . " SET ";
        $txtInsercion = $txtInsercion . " AceptoTerminos='" . $aceptoTerminos . "',";


        $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
        $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
        $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

        $txtInsercion = $txtInsercion . " Observaciones='". $observaciones . "'";
        $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$nID;

        $res = mysqli_query($mysqli,$txtInsercion);
        if($res == FALSE){
            mysqli_close($mysqli);
            return FALSE;
        }

        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función grabar ***************
     *
     * @param [array] $tabla
     * @param [int] $nIDExtension
     * @param [string] $Extension
     * @param [string] $Identificador
     * @param [string] $SIP_Usuario
     * @param [string] $SIP_Registro
     * @param [string] $SIP_Password
     * @param [string] $SIP_IPEquipo
     * @param [int] $SIP_PortMin
     * @param [int] $SIP_PortMax
     * @param [string] $Tipo
     * @param [int] $nIDLinea
     * @return void
     */
    private function grabar($tabla,$nIDExtension,$Extension,$Identificador,$SIP_Usuario,$SIP_Registro,
    $SIP_Password,$SIP_IPEquipo,$SIP_PortMin,$SIP_PortMax,$Tipo,$nIDLinea)
    {

        // Abre la conexion
        $mysqli = mysqli_connect($this->servidor,$this->usuario,$this->password,$this->basededatos);

        if(mysqli_connect_errno()){
            $this->existeError  = TRUE;
            $this->error        = "Error no se puede conectar a la base de datos";
            return -1;
        }

        // Declara variables
        $txtInsercion   = "";
        $fechalocal     = "";
        $UtileriasDatos = new clHerramientasv2011();

        for ($i = 0;$i<$this->contarDatos; $i++){
            if($tabla[$i]["Crear"]==TRUE){
                $txtInsercion = "INSERT INTO " . $this->nombreTabla;
                $txtInsercion = $txtInsercion . "(";

                $txtInsercion = $txtInsercion . "nIDExtension,";
                $txtInsercion = $txtInsercion . "Extension,";
                $txtInsercion = $txtInsercion . "Identificador,";
                $txtInsercion = $txtInsercion . "SIP_Usuario,";
                $txtInsercion = $txtInsercion . "SIP_Registro,";
                $txtInsercion = $txtInsercion . "SIP_Password,";
                $txtInsercion = $txtInsercion . "SIP_IPEquipo,";
                $txtInsercion = $txtInsercion . "SIP_IPConmutador,";
                $txtInsercion = $txtInsercion . "SIP_PortMin,";
                $txtInsercion = $txtInsercion . "SIP_PortMax,";
                $txtInsercion = $txtInsercion . "Tipo,";
                $txtInsercion = $txtInsercion . "nIDLinea,";
                $txtInsercion = $txtInsercion . "FechaModificacion,";
                $txtInsercion = $txtInsercion . "FechaCreacion,";
                $txtInsercion = $txtInsercion . "Observaciones,";
                $txtInsercion = $txtInsercion . "bEstado";

                $txtInsercion = $txtInsercion . ")";
                $txtInsercion = $txtInsercion . " VALUES ";
                $txtInsercion = $txtInsercion . "(";

                $txtInsercion = $txtInsercion . $tabla[$i]["nIDExtension"] . ",";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Extension"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Identificador"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["SIP_Usuario"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["SIP_Registro"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["SIP_Password"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["SIP_IPEquipo"] . "',";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["SIP_IPConmutador"] . "',";
                $txtInsercion = $txtInsercion . $tabla[$i]["SIP_PortMin"] . ",";
                $txtInsercion = $txtInsercion . $tabla[$i]["SIP_PortMax"]. ",";
                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Tipo"] . "',";
                $txtInsercion = $txtInsercion . $tabla[$i]["nIDLinea"] ., ",";


                // Campos bases
                $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
                $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
                $txtInsercion = $txtInsercion . "'" . $fechaLocal . "',";
                $txtInsercion = $txtInsercion . "'" . $fechaLocal . "',";

                $txtInsercion = $txtInsercion . "'" . $tabla[$i]["Observaciones"] . "',";

                $txtInsercion = $txtInsercion . $tabla[$i]["bEstado"];

                $txtInsercion = $txtInsercion . ")";

            } else {

                if($tabla[$i]["Cambiar"]==TRUE){

                    $txtInsercion = "UPDATE " .  $this->nombreTabla;
                    $txtInsercion = $txtInsercion . " SET ";

                    $txtInsercion = $txtInsercion . " Extension ='" . $tabla[$i]["Extension"] .  "',";
                    $txtInsercion = $txtInsercion . " Identificador ='" . $tabla[$i]["Identificador"] . "',";
                    $txtInsercion = $txtInsercion . " SIP_Usuario ='" . $tabla[$i]["SIP_Usuario"] . "',";
                    $txtInsercion = $txtInsercion . " SIP_Registro ='" . $tabla[$i]["SIP_Registro"] . "',";
                    $txtInsercion = $txtInsercion . " SIP_Password ='" . $tabla[$i]["SIP_Password"] . "',";
                    $txtInsercion = $txtInsercion . " SIP_IPEquipo ='" . $tabla[$i]["SIP_IPEquipo"] . "',";
                    $txtInsercion = $txtInsercion . " SIP_IPConmutador ='" . $tabla[$i]["SIP_IPConmutador"] . "',";
                    $txtInsercion = $txtInsercion . " SIP_PortMin ='" . $tabla[$i]["SIP_PortMin"] . "',";
                    $txtInsercion = $txtInsercion . " SIP_PortMax ='" . $tabla[$i]["SIP_PortMax"] . "',";
                    $txtInsercion = $txtInsercion . " Tipo ='" . $tabla[$i]["Tipo"] . "',";
                    $txtInsercion = $txtInsercion . " nIDLinea ='" . $tabla[$i]["nIDLinea"] . "',";

                    // Basicas
                    $fechaLocal = $UtileriasDatos->getFechaYHoraActual_General();
                    $fechaLocal = $UtileriasDatos->ConvertirFechaYHora($fechaLocal);
                    $txtInsercion = $txtInsercion . " FechaModificacion='". $fechaLocal . "',";

                    $txtInsercion = $txtInsercion . " Observaciones='". $tabla[$i]["Observaciones"] . "'";
                    $txtInsercion = $txtInsercion . " WHERE " . $this->id . "=" .$tabla[$i]["nIDExtension"];

                    //echo $txtInsercion;

                } else {

                    if($tabla[$i]["Eliminar"]==TRUE){
                        $txtInsercion = "DELETE FROM " . $this->nombreTabla . " WHERE " . $this->id . "=" .$tabla[$i]["nIDExtencion"];

                    }
                }

            }
            mysqli_query($mysqli, "SET NAMES UTF8");
            $res=mysqli_query($mysqli,$txtInsercion);
            $this->last_id = mysqli_insert_id($mysqli);
            if($res==FALSE){
                mysqli_close($mysqli);
                return FALSE;
            }

        }
        mysqli_close($mysqli);
        return TRUE;
    }

    /**
     *************** Función eliminarPorID ***************
     *
     * @param [int] $nID
     * @return void
     */
    public function eliminarPorID($nID)
    {

        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->borrarID($nID) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función eliminarConCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function eliminarConCondicion($condicion)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if(strlen($condicion)<=0){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene condicion";
            return FALSE;
        }

        if ($this->borrarCondicion($condicion) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función ocultar ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @return void
     */
    public function ocultar($nID, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->borrarTemporal($nID,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarEstado ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @param [int] $bEstado
     * @return void
     */
    public function cambiarEstado($nID, $observaciones, $bEstado)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarEstado($nID,$bEstado,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarPassword ***************
     *
     * @param [int] $nID
     * @param [string] $observaciones
     * @param [string] $password
     * @return void
     */
    public function cambiarPassword($nID, $observaciones, $password)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarPassword($nID,$password,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarNombre ***************
     *
     * @param [int] $nID
     * @param [string] $nombre
     * @param [string] $observaciones
     * @return void
     */
    public function cambiarNombre($nID, $nombre, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarNombre($nID,$nombre,$observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarVIP ***************
     *
     * @param [int] $nIDExtension
     * @param [string] $Extension
     * @param [string] $Identificador
     * @param [string] $SIP_Usuario
     * @param [string] $SIP_Registro
     * @param [string] $SIP_Password
     * @param [string] $SIP_IPEquipo
     * @param [string] $SIP_IPConmutador
     * @param [int] $SIP_PortMin
     * @param [int] $SIP_PortMax
     * @param [string] $Tipo
     * @param [int] $nIDLinea
     * @param [string] $observaciones
     * @return void
     */
    public function cambiarVIP($nIDExtension, $Extension, $Identificador, $SIP_Usuario,$SIP_Registro,
    $SIP_Password,$SIP_IPEquipo,$SIP_IPConmutador,$SIP_PortMin,$SIP_PortMax,$Tipo,$nIDLinea, 
    $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarVIP($nIDExtension,$Extension, $Identificador, $SIP_Usuario,$SIP_Registro,
        $SIP_Password,$SIP_IPEquipo,$SIP_IPConmutador,$SIP_PortMin,$SIP_PortMax,$Tipo,$nIDLinea,
        $observaciones)==TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarNivel ***************
     *
     * @param [type] $nID
     * @param [type] $nIDNivel
     * @param [type] $observaciones
     * @return void
     */
    public function cambiarNivel($nID, $nIDNivel, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarNivel($nID,$nIDNivel, $observaciones) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función cambiarCodigoUnico ***************
     *
     * @param [int] $nID
     * @param [int] $codigoUnico
     * @param [string] $observaciones
     * @return void
     */
    public function cambiarCodigoUnico($nID, $codigoUnico, $observaciones)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if ($this->actualizarCodigoUnico($nID,$codigoUnico, $observaciones)==TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función leer ***************
     *
     * @return void
     */
    public function leer()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if($this->Consultar() == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función leerCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function leerCondicion($condicion)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        if(strlen($condicion) <= 0){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene Condicion";
            return FALSE;
        }

        if($this->consultarCondicion($condicion) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *************** Función totalRegistros ***************
     *
     * @return void
     */
    public function totalRegistros()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError = TRUE;
            $this->error       = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->registrosTabla();
    }

    /**
     *************** Función numeroTotalRegistrosCondicion ***************
     *
     * @param [string] $condicion
     * @return void
     */
    public function numeroTotalRegistrosCondicion($condicion)
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }

        return $this->contarRegistrosCondicion($condicion);
    }

    /**
     *************** Función ejecutar ***************
     *
     * @return void
     */
    public function ejecutar()
    {
        if($this->existeError){
            return FALSE;
        }

        if(!$this->datosConexion){
            $this->existeError  = TRUE;
            $this->error        = "Error no tiene los datos para conectarse a la base de datos [Cargar]";
            return FALSE;
        }


        if($this->contarDatos<=0){
            return FALSE;
        }

        if($this->grabar($this->dtGrabar) == TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

?>