<?php
/*
------PETICIONS REALIZADAS POR EL CHOFER
*/
header('Content-Type: application/json');
date_default_timezone_set('America/Mexico_City');
$method = $_SERVER["REQUEST_METHOD"];
//$method = 'POST';
include 'conexion.php';
$image = '';

switch ($method) {
    case 'POST':
        $file = file_get_contents('php://input');
        $file = str_replace(PHP_EOL, '', $file);
        $request = json_decode($file, true);
        //$request = file_get_contents('php://input');
        //$request['idChofer'] = 10;
        // $request['nIDSolicitud'] = 76;
        // $request['idSolicitud'] = 76;
        //$request['option'] = 2; 
        // // 1 para obtener datos de proxima entrega, 2 para guardar evidencias de la entrega
        // $request['nombreCliente'] = 'administrador';
        // $request['calle'] = 'simple calle';
        // $request['Kilometraje'] = '120';

        if (is_null($request['idChofer']) || is_null($request['option'])) {
            $request['resp'] = 'Los Datos enviados están Incompletos||' . $request . " || " . file_get_contents('php://input');
            $request['status'] = false;
            // $r['cont'] = strlen(file_get_contents('php://input'));
            // $r['status'] = false;
            // $r['idChofer'] = $request['idChofer'];
            echo json_encode($request);
            return json_encode($request);
        } else {
            switch ($request['option']){
                case 1:
                    $resp = obtenerProximaEntrega($request['idChofer']);
                    // var_dump($resp);
                    if ($resp['status']) {
                        echo json_encode($resp);
                        // base64ToImage($resp['Foto'], "foto.png");
                        return json_encode($resp);
                    } else {
                        $resp['resp'] = 'No hay entregas asignadas';
                        $resp['status'] = false;
                        echo json_encode($resp);
                        return json_encode($resp);
                    }
                break;
                case 2:
                    // entrega de vehiculo y carga de imagenees de evidencia
                    $resp = guardarEvidenciasEntrega($request);
                    // var_dump($resp);
                    if ($resp['status']) {
                        echo json_encode($resp);
                        // base64ToImage($resp['Foto'], "foto.png");
                        $request['resp'] = 'Se actualizo la entregua';
                        return json_encode($resp);
                    } else {
                        $request['resp'] = 'No se acualizo la entrega';
                        $request['status'] = false;
                        echo json_encode($request);
                        return json_encode($request);
                    }
                break;
                case 3:
                    // solicitar recepcion de vehiculo
                    $resp = solicitarRecepcion($request);
                    // var_dump($resp);
                    if ($resp['status']) {
                        echo json_encode($resp);
                        // base64ToImage($resp['Foto'], "foto.png");
                        $request['resp'] = 'Se realizo la solicitud';
                        return json_encode($resp);
                    } else {
                        $request['resp'] = 'No se acualizo la entrega';
                        $request['status'] = false;
                        echo json_encode($request);
                        return json_encode($request);
                    }
                    break;
                case 4:
                    // checar recepcion de vehiculo
                    $resp = checarValidacionRecepcion($request);
                    // var_dump($resp);
                    if ($resp['status']) {
                        echo json_encode($resp);
                        // base64ToImage($resp['Foto'], "foto.png");
                        $request['resp'] = 'Se realizo la solicitud';
                        return json_encode($resp);
                    } else {
                        $request['resp'] = 'No se acualizo la entrega';
                        $request['status'] = false;
                        echo json_encode($request);
                        return json_encode($request);
                    }
                    break;
                case 5:
                    // checar recepcion de vehiculo
                    $resp = validarRecepcion($request);
                    if ($resp['status']) {
                        echo json_encode($resp);
                        // base64ToImage($resp['Foto'], "foto.png");
                        $request['resp'] = 'Se realizo la solicitud';
                        return json_encode($resp);
                    } else {
                        $request['resp'] = 'No se acualizo la entrega';
                        $request['status'] = false;
                        echo json_encode($request);
                        return json_encode($request);
                    }
                    break;
                default:
                break;
            } // fin del switch de option
        }
        break;
    default:
        $request['resp'] = 'No se puede procesar la informacion';
        $request['status'] = false;
        return json_encode($request);
}

function obtenerProximaEntrega($idChofer){
    $pdo = Conexion();
    $select = "SELECT sol.*, cli.*, sol.Calle as CalleSol, sol.NoExterior as NoExteriorSol FROM tbl_solicitud AS sol, tbl_clientes AS cli WHERE sol.nIDChofer = $idChofer AND (sol.Estatus = 'EN RUTA' OR sol.Estatus = 'ASIGNADO' OR sol.Estatus = 'ACEPTADO' OR sol.Estatus = 'POR ACEPTAR') AND cli.nIDCliente = sol.nIDCliente AND sol.bEstado = 1 ORDER BY sol.FechaHora DESC   LIMIT 1";
    $ejecutar = $pdo->prepare($select);
    $ejecutar->execute();
    $result = $ejecutar->fetchAll(PDO::FETCH_ASSOC);
    $arr = [];
    if (count($result) > 0) {
        foreach ($result as $row) {
            $row['status'] = true;
            $row['bEstado'] = boolval($row['bEstado']);
            $row['resp'] = 'Proxima entrega para: '. $row['Nombre'];
            $fecha = date_parse($row['FechaHora']);
            // $row['FechaHora'] = $fecha;
            $fecha['day'] = ($fecha['day'] < 10)? "0". $fecha['day']: $fecha['day'];
            $fecha['month'] = ($fecha['month'] < 10) ? "0" . $fecha['month'] : $fecha['month'];
            $fecha['hour'] = ($fecha['hour'] < 10) ? "0" . $fecha['hour'] : $fecha['hour'];
            $fecha['minute'] = ($fecha['minute'] < 10) ? "0" . $fecha['minute'] : $fecha['minute'];
            $fecha['second'] = ($fecha['second'] < 10) ? "0" . $fecha['second'] : $fecha['second'];
            $row['FechaHora'] = $fecha['year']."-". $fecha['month']."-". $fecha['day']."T". $fecha['hour'].":" . $fecha['minute'].":" . $fecha['second'];
            // "2016-04-14T10:44:00+0000"
        }
        $arr = $row;
        return $arr;
    } else {
        $arr['status'] = false;
        return $arr;
    }
}
function guardarEvidenciasEntrega($datos){
    $pdo = Conexion();
    // actualizamos el registro de solicitud a entregado
    $fechaActual = date('Y-m-d H:i:s');
    $carpeta = '/Entregas';
    $carpetaEstatus = crearCarpetaSiNoExiste($carpeta);
    $carpetaCliente = $carpeta . '/' . $datos['nombreCliente'] . '_' . $datos['idChofer'] . '_' . $datos['idSolicitud'] . '_' . $fechaActual;
    $carpetaClienteEstatus = crearCarpetaSiNoExiste($carpetaCliente);
    $update = "UPDATE tbl_solicitud SET Imagenes = '$carpetaCliente', Estatus = 'ENTREGADO', KilometrajeInicio = ". $datos['Kilometraje']." WHERE nIDSolicitud = ". $datos['idSolicitud'];
    $ejecutar = $pdo->prepare($update);
    // $result = $pdo->lastInsertId();
    //var_dump($ejecutar);
    $arr = [];
    if ($ejecutar->execute()) {
        // creamos la carpteta para las imagenes de evidencia y creamos las imagenes dentro
        // Almacenar las fotografias de evidencia de la entrega del chofer al cliente
        $date = date("Y-m-d H:i:s");
        if (array_key_exists('fotoFrente', $datos) && $datos['fotoFrente'] != "") {
            $imgFrenteVidencia = $carpetaCliente . '/imgFrente.jpeg';
            $imgEstatus = base64ToImage($datos['fotoFrente'], $imgFrenteVidencia);
            // unlink($imgvidencia);
        } else {
            $imgFrenteVidencia = "";
        }
        if (array_key_exists('fotoAtras', $datos) && $datos['fotoAtras'] != "") {
            $imgAtrasVidencia = $carpetaCliente . '/imgAtras.jpeg';
            $imgEstatus = base64ToImage($datos['fotoAtras'], $imgAtrasVidencia);
            // unlink($imgAtrasVidencia);
        } else {
            $imgAtrasVidencia = "";
        }
        if (array_key_exists('fotoDerecha', $datos) && $datos['fotoDerecha'] != "") {
            $imgDerechaVidencia = $carpetaCliente . '/imgDerecha.jpeg';
            $imgEstatus = base64ToImage($datos['fotoDerecha'], $imgDerechaVidencia);
            // unlink($imgvidencia);
        } else{
            $imgDerechaVidencia = "";
        }
        if (array_key_exists('fotoIzquierda', $datos) && $datos['fotoIzquierda'] != "") {
            $imgIzquierdaVidencia = $carpetaCliente . '/imgIzquierda.jpeg';
            $imgEstatus = base64ToImage($datos['fotoIzquierda'], $imgIzquierdaVidencia);
            // unlink($imgvidencia);
        } else {
            $imgIzquierdaVidencia = "";
        }
        if (array_key_exists('fotoInteriorAtras', $datos) && $datos['fotoInteriorAtras'] != "") {
            $imgInteriorAtrasVidencia = $carpetaCliente . '/imgInteriorAtras.jpeg';
            $imgEstatus = base64ToImage($datos['fotoInteriorAtras'], $imgInteriorAtrasVidencia);
            // unlink($imgvidencia);
        } else {
            $imgInteriorAtrasVidencia = "";
        }
        if (array_key_exists('fotoInteriorAdelante', $datos) && $datos['fotoInteriorAdelante'] != "") {
            $imgInteriorAdelanteVidencia = $carpetaCliente . '/imgInteriorAdelante.jpeg';
            $imgEstatus = base64ToImage($datos['fotoInteriorAdelante'], $imgInteriorAdelanteVidencia);
            // unlink($imgvidencia);
        } else {
            $imgInteriorAdelanteVidencia = "";
        }
        if (array_key_exists('fotoTablero', $datos) && $datos['fotoTablero'] != "") {
            $imgTableroVidencia = $carpetaCliente . '/imgTablero.jpeg';
            $imgEstatus = base64ToImage($datos['fotoTablero'], $imgTableroVidencia);
            // unlink($imgvidencia);
        } else {
            $imgTableroVidencia = "";
        }
        if (array_key_exists('fotoSuperior', $datos) && $datos['fotoSuperior'] != "") {
            $imgSuperiorVidencia = $carpetaCliente . '/imgSuperior.jpeg';
            $imgEstatus = base64ToImage($datos['fotoSuperior'], $imgSuperiorVidencia);
            // unlink($imgvidencia);
        } else {
            $imgSuperiorVidencia = "";
        }
        //***************************** */
        if (array_key_exists('fotoCajuela', $datos) && $datos['fotoCajuela'] != "") {
            $imgCajuela = $carpetaCliente . '/imgCajuela.jpeg';
            $imgEstatus = base64ToImage($datos['fotoCajuela'], $imgCajuela);
            // unlink($imgvidencia);
        } else {
            $imgCajuela = "";
        }
        if (array_key_exists('fotoHerramientas', $datos) && $datos['fotoHerramientas'] != "") {
            $imgHerramientas = $carpetaCliente . '/imgHerramientas.jpeg';
            $imgEstatus = base64ToImage($datos['fotoHerramientas'], $imgHerramientas);
            // unlink($imgvidencia);
        } else {
            $imgHerramientas = "";
        }

        // insertar la entrega del vehiculo detalle entrega
        $slectSolicitud = "SELECT * FROM tbl_solicitud WHERE nIDSolicitud = ". $datos['idSolicitud'];
        $ejecutarSelect = $pdo->prepare($slectSolicitud);
        $ejecutarSelect->execute();
        $result = $ejecutarSelect->fetchAll(PDO::FETCH_ASSOC);
        
        if (count($result) > 0){
            foreach ($result as $row) {
                // Select de la solicitud anterior
                $slectSolicitud2 = "SELECT * FROM tbl_solicitud WHERE nIDCliente = " . $row['nIDCliente'] . " AND Estatus = 'Entregado' AND nIDSolicitud != ". $datos['idSolicitud']." ORDER BY nIDSolicitud DESC LIMIT 1;";
                $ejecutarSelect2 = $pdo->prepare($slectSolicitud2);
                $ejecutarSelect2->execute();
                $row2 = $ejecutarSelect2->fetchAll(PDO::FETCH_ASSOC);
                // var_dump($row2[0]['Calle']);
                $row2[0]['nIDSolicitud'] = isset($row2[0]['nIDSolicitud']) ? $row2[0]['nIDSolicitud'] : 0;
                $dateEntrega = date('Y-m-d H:i:s');
                // actualizar la anterior
                $updateLastSolicitud = "UPDATE tbl_solicitud SET Estatus = 'DEVUELTO' , CalleRecepcion = '". $row['Calle']."', NoExteriorRecepcion = '". $row['NoExterior']."', LongitudRecepcion = ". $row['Longitud'].", LatitudRecepcion = ". $row['Latitud'].", FechaModificacion = '$date', FechaHoraRecepcion = '$dateEntrega', kilometrajeFin = ". $datos['KilometrajeRecepcion']." WHERE nIDSolicitud = " . $row2[0]['nIDSolicitud'];
                $ejecutarLastSolicitud = $pdo->prepare($updateLastSolicitud);
                //Actualizar vehiculo estado del vehiculo anterior
                actualizarEstatusVehiculoDisponible($row2[0]['nIDSolicitud'], 'DISPONIBLE');
                // var_dump($updateLastSolicitud);
                $insertDetalle = "INSERT INTO tbl_detalle_entrega (nIDCliente, nIDAutomovil, nIDSolicitud, Calle, NoExterior, Longitud, Latitud, Altitud, Imagenes, FechaHora, FechaFin, Kilometrajeinicio, KilometrajeFin, Notas, Estatus, FechaModificacion, FechaCreacion, Observaciones, bEstado) 
                VALUES (".$row['nIDCliente']. "," . $row['nIDAutomovil'] . "," . $row['nIDSolicitud'] . ",'".$row['Calle']. "','" . $row['NoExterior'] . "','" . $row['Longitud'] . "','" . $row['Latitud'] . "','0', '$carpetaCliente','$dateEntrega','$dateEntrega','" . $datos['Kilometraje'] . "', '" . $datos['KilometrajeRecepcion'] . "','" . $row['Notas'] . "','ENTREGADO','$dateEntrega','$dateEntrega','Entrega Realizada',1)";
                $ejecutar = $pdo->prepare($insertDetalle);
                $ejecutar->execute();
                $arr = [];
                if ($ejecutar->rowCount() > 0 && $ejecutarLastSolicitud->execute()) {
                    $dateFecha = date('Y-m-d');
                    $dateHora = date('H:i:s');
                    $fechaActual = $dateFecha." ".$dateHora;
                    $emailCLiente = emailCliente($row['nIDCliente']);
                    $nombreCliente = nombreCliente($row['nIDCliente']);
                    // insertamos el registro en la tabla TBL_email_enviar? para enviar el correo al cliente con los dato de su entrega
                    $updateSolicitud = "INSERT INTO tbl_email_enviar (Folio, nIDUsuario, Fecha, Hora, Para, Cc, Cco, Asunto, Cuerpo, Archivo1, Archivo2, Archivo3, Archivo4, Archivo5, Archivo6, Archivo7, Archivo8, Archivo9, Archivo10, Estatus, FechaEnviado, nIDCorreo, Firma, FechaModificacion, FechaCreacion, Observaciones, bEstado) VALUE (0,0,'$dateFecha','$dateHora','$emailCLiente','','','Fotografias de evidencia de entrega del Vehculo','Hola $nombreCliente, se te envian las fotografias de evidencia del estado del vehiculo','$imgFrenteVidencia','$imgAtrasVidencia','$imgDerechaVidencia','$imgIzquierdaVidencia','$imgInteriorAtrasVidencia','$imgInteriorAdelanteVidencia','$imgTableroVidencia','$imgSuperiorVidencia', '$imgCajuela','$imgHerramientas','NO ENVIADO','',0,0,'$fechaActual','$fechaActual','Envio de evidencias al cliente',1)";
                    // $updateSolicitud = "";
                    $ejecutar = $pdo->prepare($updateSolicitud);
                    $ejecutar->execute();
                    // $result = $pdo->lastInsertId();
                    $arr = [];
                    if ($ejecutar->rowCount() > 0) {
                        // insertamos el registro en la tabla TBL_email_enviar? para enviar el correo al cliente con los dato de su entrega
                        $row['status'] = true;
                        $row['resp'] = "Se registrar la entrega";
                        // array_push($arr, $row);
                        $arr = $row;
                        return $arr;
                    } else {
                        $row['status'] = false;
                        $row['resp'] = 'no inserto';
                        // array_push($arr, $row);
                        $arr = $row;
                        return $arr;
                    }
                } else {
                    $row['status'] = false;
                    $row['resp'] = 'no inserto';
                    // array_push($arr, $row);
                    $arr = $row;
                    return $arr;
                }
            }
        } else {
            $row['status'] = false;
            $row['resp'] = 'no select solicitud';
            // array_push($arr, $row);
            $arr = $row;
            return $arr;
        }
    } else {
        $row['resp'] = 'No se pudo actualizar, intentalo de nuevo.';
        $row['status'] = false;
        // array_push($arr, $row);
        $arr = $row;
        return $arr;
    }
}
function base64ToImage($base64_string, $output_file){
    $base64_string = "data:image/jpeg;base64,".$base64_string;
    $file = fopen('.'.$output_file, "wb");
    $data = explode(',', $base64_string);
    fwrite($file, base64_decode($data[1]));
    fclose($file);
    return $output_file;
}
function crearCarpetaSiNoExiste($carpeta){
    try {
        $carpeta = "." . $carpeta;
        // $carpeta =  $carpeta;
        if (!file_exists($carpeta)) {
            mkdir($carpeta, 0777, true);
            return true;
        } else {
            return false;
        }
    } catch(Exception $e) {
        return "".$e;
    }
}
function emailCliente($id){
    $pdo = Conexion();
    $slectSolicitud = "SELECT Email FROM tbl_clientes WHERE nIDCliente = ". $id;
    $ejecutarSelect = $pdo->prepare($slectSolicitud);
    $ejecutarSelect->execute();
    $result = $ejecutarSelect->fetchAll(PDO::FETCH_ASSOC);
    if (count($result) > 0) {
        return $result[0]['Email'];
    } else {
        return false;
    }
}
function nombreCliente($id){
    $pdo = Conexion();
    $slectSolicitud = "SELECT Nombre FROM tbl_clientes WHERE nIDCliente = " . $id;
    $ejecutarSelect = $pdo->prepare($slectSolicitud);
    $ejecutarSelect->execute();
    $result = $ejecutarSelect->fetchAll(PDO::FETCH_ASSOC);
    if (count($result) > 0) {
        return $result[0]['Nombre'];
    } else {
        return false;
    }
}
function vigenciaAutomovil($id){
    $pdo = Conexion();
    $updateAuto = "UPDATE tbl_solicitud SET Imagenes = '' WHERE nIDAutomovil = $id";
    $ejecutar = $pdo->prepare($updateAuto);

}
function solicitarRecepcion($datos){
    $pdo = Conexion();
    $fecha = date('Y-m-d H:i:s');
    $select = "INSERT INTO tbl_cat_notificaciones (nIDCliente, nIDTipoNotificacion, Encabezado, Body, Permiso, Estatus, FechaModificacion, FechaCreacion, Observaciones, bEstado) VALUES (".$datos['nIDCliente']. ", 9, 'Aceptar Entrega', 'Aceptas la entrega del vehiculo?,," . $datos['nIDSolicitud'] . "', 'SI', 'NO LEIDO', '$fecha', '$fecha', 'notificacion para aceptar recepcion', 1)";
    $ejecutar = $pdo->prepare($select);
    $ejecutar->execute();
    //$result = $ejecutar->fetchAll(PDO::FETCH_ASSOC);
    $arr = [];
    if ($ejecutar->rowCount() > 0) {
        $fechaActual = date('Y-m-d H:i:s');
        $update = "UPDATE tbl_solicitud SET Estatus = 'POR ACEPTAR' WHERE nIDSolicitud = " . $datos['nIDSolicitud'];
        $ejecutar = $pdo->prepare($update);
        $arr = [];
        if ($ejecutar->execute()) {
            $arr['status'] = true;
            return $arr;
        }else{
            $arr['status'] = false;
            return $arr;
        }
    } else {
        $arr['status'] = false;
        return $arr;
    }
}
function validarRecepcion($datos){
    $pdo = Conexion();
    $fechaActual = date('Y-m-d H:i:s');
    $update = "UPDATE tbl_solicitud SET Estatus = 'ACEPTADO' WHERE nIDSolicitud = " . $datos['nIDSolicitud'];
    //var_dump($update);
    $ejecutar = $pdo->prepare($update);
    $arr = [];
    if ($ejecutar->execute()) {
        $arr['status'] = true;
        return $arr;
    } else {
        $arr['status'] = false;
        return $arr;
    }
}
function checarValidacionRecepcion($datos){
    $pdo = Conexion();
    $slectSolicitud = "SELECT * FROM tbl_solicitud WHERE nIDSolicitud = " . $datos['nIDSolicitud'] . " AND Estatus = 'ACEPTADO'";
    $ejecutarSelect = $pdo->prepare($slectSolicitud);
    $ejecutarSelect->execute();
    $result = $ejecutarSelect->fetchAll(PDO::FETCH_ASSOC);
    if (count($result) > 0) {
        $arr['status'] = true;
        return $arr;
    } else {
        $arr['status'] = false;
        return $arr;
    }
}

function actualizarEstatusVehiculoDisponible($id, $estado = 'DISPONIBLE'){
    // Modificar proximo cambio del cliente
    // Insertar proximo cambio del cliente
    $pdo = Conexion();
    $update = "UPDATE tbl_automoviles, tbl_solicitud set tbl_automoviles.Estatus = '$estado' where tbl_automoviles.nIDAutomovil = tbl_solicitud.nIDAutomovil AND tbl_solicitud.nIDSolicitud = $id";
    $ejecutar = $pdo->prepare($update);
    //var_dump($update);
    //var_dump($ejecutar);
    $result = $pdo->lastInsertId();
    $arr = [];
    if($ejecutar->execute()) {
        $row['lastID'] = 0;
        $row['status'] = true;
        array_push($arr, $row);
        return $arr;
    } else {
        $row['status'] = false;
        array_push($arr, $row);
        return $arr;
    }
}





?>